<cfcomponent output="false">
	<cfset this.delegate = CreateObject("component", "com.mce.db.EnergyAccountDelegate")>


	<cffunction name="getEnergyAccountUid" access="public" returntype="string">
		<cfargument name="energyAccountFriendlyName" type="string" required="true">

		<cfquery name="qEnergyAccount" datasource="#this.delegate.datasource#">
			select energy_account_uid from EnergyAccounts ea
			where ea.friendly_name = '#energyAccountFriendlyName#'
		</cfquery>

		<cfif qEnergyAccount.recordCount neq 1>
			<cfthrow message="Couldn't get UID for energy account '#energyAccountFriendlyName#'">
		</cfif>

		<cfreturn qEnergyAccount.energy_account_uid>
	</cffunction>



	<cffunction name="getUsageRecords" access="public" returntype="query">
		<cfargument name="energyAccountUid" type="string" required="true">
		<cfargument name="periodStart" type="date" required="true">
		<cfargument name="usageTypeCode" type="string" required="false" default="actual">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.delegate.datasource#">
			select * from EnergyUsage eu
			where is_active = 1
			and period_start >= <cfqueryparam cfsqltype="cf_sql_date" value="#periodStart#">
			and energy_account_uid = '#energyAccountUid#'
			and usage_type_code = '#usageTypeCode#'
			order by period_start
		</cfquery>

		<cfreturn result>
	</cffunction>


</cfcomponent>