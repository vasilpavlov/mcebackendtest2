<cfcomponent output="false"
			 displayname="Structure Utilities"
			 hint="This component is used to store any structure utility methods that are leveraged by the rate model engine.">

	<!--- Combines the contents of two structure objects. For any keys that both structures both have, struct1's values win --->
	<cffunction name="StructMerge" access="public" returntype="struct"
				hint="This method is used to combine the contents of two structure objects.  For any keys that both structures may have, the values in the struct1 argument will win / overwrite the values of struct2.">
		<cfargument name="struct1" type="struct" required="true" hint="Defines the primary structure whose keys /values will be combined.  This structure's keys will overwrite the keys for struct2.">
		<cfargument name="struct2" type="struct" required="true" hint="Defines the secondary structure whose keys / values will be combined.  This structure's keys will be overwritten by the keys in struct1.">

		<cfset var result = Duplicate(struct1)>
		<cfset var key = "">

		<cfloop collection="#struct2#" item="key">
			<cfif not StructKeyExists(result, key)>
				<cfset result[key] = struct2[key]>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>
	
</cfcomponent>