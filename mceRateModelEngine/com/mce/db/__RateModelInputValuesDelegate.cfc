<cfcomponent extends="BaseDatabaseDelegate" output="false">


	<!--- Note: Important to keep parity between the SQL criteria in getInputValueDates() and getInputValues() --->
	<cffunction name="getInputValues" hint="Gets the input actual values associated with a rate model." access="public" output="true" returntype="query">
		<cfargument name="model_lookup_code" type="string" hint="The model lookup code for the rate model in question">
		<cfargument name="asOfDate" type="date" hint="The basis date to use to get rate values; the value in effect on this date will be returned">
		<cfargument name="input_set_uid" type="string" required="false" default="default" hint="Optional input set uid to get rate values from; if ommitted, the default input set from the rate model is used">
		<cfargument name="requiredInputCodes" type="string" required="false" hint="Optional list of input codes that must be returned; an error is thrown if any are missing">
		<cfargument name="checkForNulls" type="boolean" required="false" default="true" hint="Flag whether to check if the values for all codes are non-null; default is true.">
		<cfargument name="autoAddRequiredInputCodes" type="boolean" required="false" default="false">

		<cfset var result = "">
		<cfset var foundValues = "">
		<cfset var requiredCode = "">
		<cfset var missingValues = "">

		<cfset asOfDate = Dateformat(asOfDate)>

		<!--- This could be converted to a stored procedure... plusses and minuses either way --->
		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				inputs.input_lookup_code,
				sets.input_set_uid,
				inputs.friendly_name,
				1.0 AS input_value,
				(SELECT factor_lookup_code
				 FROM RateModelInputSet_RateModelInputs rel
				 WHERE rel.input_set_uid = sets.input_set_uid
				 AND rel.model_input_uid = inputs.model_input_uid
				 AND (rel.relationship_start <= <cfqueryparam cfsqltype="timestamp" value="#asOfDate#"> AND ISNULL(rel.relationship_end, '1/1/3000') > <cfqueryparam cfsqltype="timestamp" value="#asOfDate#">)) as factor_lookup_code
			FROM
				RateModels models,
				RateModelInputs inputs,
				RateModelInputSets sets
			WHERE
				models.model_lookup_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#model_lookup_code#"> AND
				inputs.rate_model_uid = models.rate_model_uid AND

				<!--- Allow a specific input set to be specified; if we don't get one, use the default one from the rate model record --->
				<cfif input_set_uid eq "default">
					sets.input_set_uid = models.defaults_input_set_uid
				<cfelse>
					sets.input_set_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#input_set_uid#">
				</cfif>

			ORDER BY
				inputs.input_lookup_code
		</cfquery>

		<!--- Check that we got something (even a null) from the database for the inputs that we are told are required --->
		<cfif isDefined("requiredInputCodes")>
			<cfset foundValues = ValueList(result.input_lookup_code)>
			<cfset missingValues = "">
			<cfloop list="#requiredInputCodes#" index="requiredCode">
				<cfif not ListFindNoCase(foundValues, requiredCode)>
					<cfset missingValues = ListAppend(missingValues, requiredCode)>
				</cfif>
			</cfloop>
			<cfif missingValues neq "">
				<cfif autoAddRequiredInputCodes>
					<cfloop list="#missingValues#" index="requiredCode">
						<cfset addRateModelInput(model_lookup_code, requiredCode, 0)>
					</cfloop>
					<cfreturn getInputValues(argumentCollection=arguments)>
				<cfelse>
					<cfinvoke
						component="#Application.utils.Logger#"
						method="error"
						message="No rate model input value retrieved for #ListLen(missingValues)# lookup code(s)"
						detail="The required code(s) '#missingValues#' did not map to actual values. This may mean that the lookup code(s) do not correspond to RateInput records in the database. A total of #ListLen(foundValues)# values were actually retrieved, for these codes: '#foundValues#'">
				</cfif>
			</cfif>
		</cfif>


		<!--- Check for inconsistent values unless we are told to ignore them --->
		<cfif checkForNulls>
			<cfloop query="result">
				<cfif input_value eq "">
					<cfinvoke
						component="#Application.utils.Logger#"
						method="error"
						message="Rate model input value returned for '#input_lookup_code#' is null."
						detail="Check that there is a value in the database for the dates in question (date is '#asOfDate#', input set appears to be '#result.input_set_uid#').">
				</cfif>
			</cfloop>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- Note: Important to keep parity between the SQL criteria in getInputValueDates() and getInputValues() --->
	<cffunction name="getInputValueDates" returntype="query" access="public">
		<cfargument name="model_lookup_code" type="string" hint="The model lookup code for the rate model in question">
		<cfargument name="input_set_uid" type="string" required="false" default="default" hint="Optional input set uid to get rate values from; if ommitted, the default input set from the rate model is used">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			SELECT DISTINCT
				rel.relationship_start
			FROM
				RateModels models
				JOIN RateModelInputs inputs
					ON inputs.rate_model_uid = models.rate_model_uid
				JOIN RateModelInputSets sets
					ON sets.input_set_uid = models.defaults_input_set_uid
				JOIN RateModelInputSet_RateModelInputs rel
					ON (rel.model_input_uid = inputs.model_input_uid AND rel.input_set_uid = sets.input_set_uid)
			WHERE
				models.model_lookup_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#model_lookup_code#">
				AND rel.is_active = 1
				AND inputs.is_active = 1
				AND (rel.relationship_start > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#period_start#"> and
					 rel.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#period_end#"> )

			<!--- The following UNION'ed part is almost the same as above, but gets factor change dates --->
			UNION <!--- Important to note that this is NOT a UNION ALL --->
			SELECT DISTINCT
				rfv.relationship_start
			FROM
				RateModels models
				JOIN RateModelInputs inputs
					ON inputs.rate_model_uid = models.rate_model_uid
				JOIN RateModelInputSets sets
					ON sets.input_set_uid = models.defaults_input_set_uid
				JOIN RateModelInputSet_RateModelInputs rel
					ON (rel.model_input_uid = inputs.model_input_uid AND rel.input_set_uid = sets.input_set_uid)
				JOIN RateFactors rf
					ON rf.factor_lookup_code = rel.factor_lookup_code
				JOIN RateFactorValues rfv
					ON rfv.rate_factor_uid = rf.rate_factor_uid
			WHERE
				models.model_lookup_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#model_lookup_code#">
				AND rel.is_active = 1
				AND inputs.is_active = 1
				AND rf.is_active = 1
				AND rfv.is_active = 1
				AND (rfv.relationship_start > <cfqueryparam cfsqltype="cf_sql_timestamp" value="#period_start#"> and
					 rfv.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#period_end#"> )
			ORDER BY
				rel.relationship_start
		</cfquery>

		<cfreturn result>
	</cffunction>



	<cffunction name="getDefaultInputSetUidForModel" access="public" output="true" returntype="string">
		<cfargument name="model_lookup_code" type="string" required="true">
		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				models.defaults_input_set_uid
			FROM
				RateModels models
			WHERE
				models.model_lookup_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#model_lookup_code#">
		</cfquery>

		<cfreturn result.defaults_input_set_uid>
	</cffunction>

	<cffunction name="addRateModelInput" access="public">
		<cfargument name="model_lookup_code" type="string" required="true">
		<cfargument name="input_lookup_code" type="string" required="true">
		<cfargument name="input_value" type="numeric" required="false">
		<cfargument name="factor_lookup_code" type="string" required="false">

		<cfset var defaults_input_set_uid = getDefaultInputSetUidForModel(model_lookup_code)>

		<!--- TODO: Migrate to stored procedure --->
		<cfquery datasource="#this.datasource#">
			INSERT INTO RateModelInputs (
				model_input_uid,
				rate_model_uid,
				input_lookup_code,
				friendly_name
			)
			SELECT
				newid(),
				rate_model_uid,
				'#input_lookup_code#',
				'#input_lookup_code#'
			FROM
				RateModels
			WHERE
				model_lookup_code = '#model_lookup_code#'

			INSERT INTO RateModelInputSet_RateModelInputs (
				input_set_uid,
				model_input_uid,
				relationship_start,
				relationship_end,
				input_value,
				factor_lookup_code
			)
			SELECT
				'#defaults_input_set_uid#',
				model_input_uid,
				'1/1/1970',
				'1/1/2100',
				<cfif isDefined("input_value")>#input_value#<cfelse>null</cfif>,
				<cfif isDefined("factor_lookup_code")>'#factor_lookup_code#'<cfelse>null</cfif>
			FROM
				RateModelInputs
			WHERE
				input_lookup_code = '#input_lookup_code#'
		</cfquery>
	</cffunction>

</cfcomponent>