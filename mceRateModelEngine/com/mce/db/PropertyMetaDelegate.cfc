<cfcomponent extends="BaseDatabaseDelegate">

	<cffunction name="getPropertyMetaValuesX" access="public" returntype="query">
		<cfargument name="energyAccountUid" type="string">
		<cfargument name="periodStart" type="date">
		<cfargument name="periodEnd" type="date">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
				SELECT
					types.type_lookup_code,
					types.friendly_name,
					meta.meta_value,
					meta.relationship_start,
					meta.relationship_end
				FROM
					PropertyMetaTypes types,
					PropertyMeta meta,
					Properties props,
					EnergyAccounts accounts
				WHERE
					accounts.property_uid = props.property_uid AND
					meta.property_uid = props.property_uid AND
					types.meta_type_uid = meta.meta_type_uid AND
					accounts.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#energyAccountUid#"> AND
					meta.relationship_start >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#periodStart#"> AND
					meta.relationship_end <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#periodEnd#">
				ORDER BY
					types.type_lookup_code
			</cfquery>

			<cfreturn result>
		</cffunction>

	<cffunction name="getPropertyMetaValues" access="public" returntype="query">
		<cfargument name="energyAccountUid" type="string">
		<cfargument name="asOfDate" type="date">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			select
				types.type_lookup_code,
				dbo.getPropertyMetaValue(accounts.property_uid, types.type_lookup_code, <cfqueryparam cfsqltype="cf_sql_timestamp" value="#asOfDate#">) as meta_value
			from
				PropertyMetaTypes types,
				EnergyAccounts accounts
			where
				accounts.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#energyAccountUid#">
			ORDER BY type_lookup_code
		</cfquery>

		<cfreturn result>
	</cffunction>
	
	<cffunction name="getTripCodeForProperty" access="public" returntype="numeric">
		<cfargument name="propertyUid" type="string">
		<cfargument name="asOfDate" type="date">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			select
				ISNULL(dbo.getPropertyTripCode(<cfqueryparam cfsqltype="cf_sql_varchar" value="#propertyUid#">, <cfqueryparam cfsqltype="cf_sql_timestamp" value="#asOfDate#">), -1) as trip_code			
		</cfquery>

		<cfreturn result.trip_code>
	</cffunction>
	
</cfcomponent>