<cfcomponent extends="BaseDatabaseDelegate">

	<cffunction name="getEnergyUsage" access="public" returntype="query">
		<cfargument name="energyAccountUid" type="string" required="true">
		<cfargument name="periodStart" type="date" required="true">
		<cfargument name="periodEnd" type="date" required="true">
		<cfargument name="usage_type_code" type="string" required="true" default="actual">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				usage.energy_usage_uid,
				usage.period_start,
				usage.period_end,
				usage.energy_unit_uid,
				usage.recorded_value,
				usage.total_cost,
				euv.supplyChargesfromsubaccount,
				units.unit_lookup_code
				,dbo.rsEnergyUsageUnderContract(usage.energy_usage_uid) as hasSupplyContract
			FROM
				EnergyUsage usage,
				EnergyUnits units,
				EnergyUsageView euv
			WHERE
				units.energy_unit_uid = usage.energy_unit_uid AND
				usage.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#energyAccountUid#"> AND
				report_date >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#periodStart#"> AND
				report_date <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#periodEnd#"> AND
				usage.usage_type_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#usage_type_code#"> AND
				usage.is_active = 1 AND
				units.is_active = 1 AND
				euv.energy_usage_uid = usage.energy_usage_uid AND
				EUV.USAGE_TYPE_CODE = <cfqueryparam cfsqltype="cf_sql_varchar" value="#usage_type_code#"> 
				
			ORDER BY
				period_start
		</cfquery>

		<cfreturn result>
	</cffunction>


	<cffunction name="getEnergyUsageMeta" access="public" returntype="query">
		<cfargument name="energyAccountUid" type="string" required="true">
		<cfargument name="periodStart" type="date" required="true">
		<cfargument name="periodEnd" type="date" required="true">
		<cfargument name="usage_type_code" type="string" required="true">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				meta.energy_usage_uid,
				types.type_lookup_code,
				types.friendly_name,
				meta.meta_value
			FROM
				EnergyUsage usage,
				EnergyUsageMeta meta,
				EnergyUsageMetaTypes types
			WHERE
				usage.energy_usage_uid = meta.energy_usage_uid AND
				meta.meta_type_uid = types.meta_type_uid AND
				usage.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#energyAccountUid#"> AND
				usage.report_date >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#periodStart#"> AND
				usage.report_date <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#periodEnd#"> AND
				usage.usage_type_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#usage_type_code#"> AND
				usage.is_active = 1 AND
				meta.is_active = 1
		</cfquery>

		<cfreturn result>
	</cffunction>

	<cffunction name="getEnergyUsageForPropertyElectricityAccounts" access="public" returntype="query">
		<cfargument name="property" type="string" required="true">
		<cfargument name="periodStart" type="date" required="true">
		<cfargument name="periodEnd" type="date" required="true">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			SELECT 
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'total_cost'),0)) as 'TotalElectricityCharges',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'DeliverySalesTax'),0)) as 'Salestax_Delivery',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'SupplyTax'),0)) as 'Salestax_Supply',
				sum(eu.recorded_value) as 'Consumption',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'Demand'),0)) as 'Demand_Property',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'total_delivery_charge_jcpl'),0)) as 'TotalDeliveryCharges',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'SupplyCharges'),0)) as 'TotalSupplycharges_aftertax',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'DemandSuplyCharge'),0)) as 'DemandSupplycharge',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'GRTandOtherSurchargesSupply'),0)) as 'GRTandOthertaxsurcharges_Supply',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'SupplyChargesGRT'),0)) as 'Supplycharges_WithGRT',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'DemandDeliveryCharge'),0)) as 'DemandDeliveryCharge',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'BillableReactivePower'),0)) as 'BillableReactivePowerDemand',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'GRTandOtherSurcharges'),0)) as 'GRTandOthertaxsurcharges_Delivery',
				sum(isnull(dbo.getEnergyUsageMetaValueById(eu.energy_usage_uid, 'DeliveryChargesWithTax'),0)) as 'Deliverycharges_withGRT'
			FROM 
				Properties p JOIN 
				EnergyAccounts ea on p.property_uid = ea.property_uid JOIN
				EnergyUsage eu on eu.[energy_account_uid] = ea.[energy_account_uid] 
			WHERE eu.usage_type_code = 'Actual' and ea.energy_type_uid='603876EF-0EB9-44F9-8F95-4A91B13309A9' and 
				ea.is_active = 1 and p.is_active = 1 and ea.master_energy_account_uid IS NULL and 
				p.friendly_name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#property#"> AND
				period_start >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#periodStart#"> AND 
				period_end <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#periodEnd#">
		</cfquery>

		<cfreturn result>
	</cffunction>
    
</cfcomponent>