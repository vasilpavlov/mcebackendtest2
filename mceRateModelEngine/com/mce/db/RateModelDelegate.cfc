<cfcomponent extends="BaseDatabaseDelegate">

	<cffunction name="getModelComponentRecord" access="public" returntype="query">
		<cfargument name="lookupCode" type="string">
		<cfargument name="allow_disabled" type="boolean" required="false" default="false">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				rate_model_uid,
				implementation_file
			FROM
				RateModels
			WHERE
				(model_lookup_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#lookupCode#">)
				<cfif not allow_disabled>
					AND (is_active = 1)
				</cfif>
		</cfquery>

		<cfif result.recordCount neq 1>
			<cfinvoke
				component="#Application.utils.Logger#"
				method="error"
				message="Can't find component name for locator string '#lookupCode#'."
				detail="Expected 1 record, found #result.recordCount#.">
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="getModels">
		<cfargument name="modelLookupCodePattern" type="string" required="false">

		<cfset var result = "">

		<cfquery name="result" datasource="#this.datasource#">
			SELECT
				model_lookup_code,
				friendly_name
			FROM
				RateModels
			WHERE
				is_active = 1
				<cfif isDefined("modelLookupCodePattern")>
					AND modelLookupCodePattern LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="#modelLookupCodePattern#">
				</cfif>
			ORDER BY
				model_lookup_code
		</cfquery>

		<cfreturn result>
	</cffunction>

</cfcomponent>