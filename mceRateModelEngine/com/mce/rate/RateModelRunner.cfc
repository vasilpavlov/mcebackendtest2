<cfcomponent output="false"
			 displayName="Rate Model Runner"
			 hint="This component is used to broker requests / queries against a given energy rate model.">

	<cffunction name="runModel" access="public" returntype="mceRateModelEngine.com.mce.rate.engine.RateModelResponse"
				hint="This method is used to execute a request / query against the specified rate model.">

		<cfargument name="requestArgs" required="true" type="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments">

		<cfset var calculator = Application.singletons.ModelFactory.getModelInstance(requestArgs.modelLookupCode, requestArgs)>
		<cfset var modelRequest = Application.singletons.RequestFactory.getRequestInstance(calculator, requestArgs)>
		<cfset var modelResponse = Application.singletons.RateModelRequestBroker.processRequest(modelRequest)>

		<cfreturn modelResponse>
	</cffunction>

</cfcomponent>