<cfcomponent output="false"
	extends="com.mce.rate.dataaccess.impl.BaseDataAccessor"
	implements="com.mce.rate.dataaccess.IEnergyAccountInfoDataAccessor">

	<cfset this.databaseDelegate = CreateObject("component", "com.mce.db.PropertyMetaDelegate")>

	<cffunction name="getData" access="public" returntype="query">
		<cfargument name="account_identifier" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">

		<cfreturn this.databaseDelegate.getPropertyMetaValues(account_identifier, period_start)>
	</cffunction>
</cfcomponent>