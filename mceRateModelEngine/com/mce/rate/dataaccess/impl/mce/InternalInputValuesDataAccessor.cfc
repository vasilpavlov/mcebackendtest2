<cfcomponent output="false"
	extends="com.mce.rate.dataaccess.impl.BaseDataAccessor"
	implements="com.mce.rate.dataaccess.IInputValuesDataAccessor">

	<cfset this.databaseDelegate = CreateObject("component", "com.mce.db.RateModelInputValuesDelegate")>

	<cffunction name="getData" access="public" returntype="query">
		<cfargument name="modelLookupCode" type="string" required="true">
		<cfargument name="inputCodes" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">

		<cfreturn this.databaseDelegate.getInputValues(modelLookupCode, period_start, "default", inputCodes, true, true)>
	</cffunction>
</cfcomponent>