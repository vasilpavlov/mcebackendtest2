<cfcomponent output="false">
	<cfset this.basePackageName = "com.mce.rate.dataaccess.impl">
	<cfset this.types = StructNew()>

	<!--- Constructor --->
	<cfset registerKnownAccessors()>

	<cffunction name="registerKnownAccessors" access="public" output="false">
		<cfset this.register("InputValues", "mce_internal", "mce.InternalInputValuesDataAccessor")>
		<cfset this.register("EnergyUsage", "mce_internal", "mce.InternalEnergyUsageDataAccessor")>
		<cfset this.register("EnergyInterval", "mce_internal", "mce.InternalEnergyIntervalDataAccessor")>
		<cfset this.register("EnergyAccountInfo", "mce_internal", "mce.InternalEnergyAccountInfoDataAccessor")>
	</cffunction>

	<cffunction name="register" access="public" output="false" returntype="void">
		<cfargument name="type" type="string" required="true">
		<cfargument name="accessor_code" type="string" required="true">
		<cfargument name="component_name" type="string" required="true">
		<cfset this.types[type][accessor_code] = component_name>
	</cffunction>

	<cffunction name="getInputValuesDataAccessor" access="public" output="false" returntype="IInputValuesDataAccessor">
		<cfargument name="accessor_code" type="string" required="true">
		<cfreturn getAccessorInstanceFor("InputValues", accessor_code)>
	</cffunction>

	<cffunction name="getEnergyUsageDataAccessor" access="public" output="false" returntype="IEnergyUsageDataAccessor">
		<cfargument name="accessor_code" type="string" required="true">
		<cfreturn getAccessorInstanceFor("EnergyUsage", accessor_code)>
	</cffunction>

	<cffunction name="getEnergyIntervalDataAccessor" access="public" output="false" returntype="IEnergyIntervalDataAccessor">
		<cfargument name="accessor_code" type="string" required="true">
		<cfreturn getAccessorInstanceFor("EnergyInterval", accessor_code)>
	</cffunction>

	<cffunction name="getEnergyAccountInfoDataAccessor" access="public" output="false" returntype="IEnergyAccountInfoDataAccessor">
		<cfargument name="accessor_code" type="string" required="true">
		<cfreturn getAccessorInstanceFor("EnergyAccountInfo", accessor_code)>
	</cffunction>


	<cffunction name="getAccessorInstanceFor" access="private" output="false" returntype="any">
		<cfargument name="type" type="string" required="true">
		<cfargument name="accessor_code" type="string" required="true">

		<cfif StructKeyExists(this.types, type) and StructKeyExists(this.types[type], accessor_code)>
			<cfreturn createObject("component", "#this.basePackageName#.#this.types[type][accessor_code]#")>
		<cfelse>
			<cfinvoke
				component="#Application.utils.Logger#"
				method="error"
				message="No matching accessor is registered"
				detail="No accessor of type '#type#' and accessor code '#accessor_code#'">
		</cfif>
	</cffunction>

</cfcomponent>