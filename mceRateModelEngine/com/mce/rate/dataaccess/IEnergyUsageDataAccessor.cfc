<cfinterface>
	<cffunction name="getEnergyUsageData" returntype="query">
		<cfargument name="account_identifier" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">
		<cfargument name="usageCriteria" type="struct" required="true">
	</cffunction>

	<cffunction name="getEnergyUsageMetaData" access="public" returntype="query">
		<cfargument name="account_identifier" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">
		<cfargument name="usageCriteria" type="struct" required="true">
	</cffunction>
	
	 <cffunction name="getEnergyUsageForPropertyElectricityAccounts" access="public" returntype="query">
		<cfargument name="property" type="string" required="true">
		<cfargument name="period_start" type="date" required="true">
		<cfargument name="period_end" type="date" required="true">
	</cffunction>
</cfinterface>