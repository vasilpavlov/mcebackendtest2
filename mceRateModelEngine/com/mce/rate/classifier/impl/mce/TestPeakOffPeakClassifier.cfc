<cfcomponent output="false"
	extends="com.mce.rate.classifier.impl.BaseIntervalClassifier"
	implements="com.mce.rate.classifier.IIntervalClassifier">

	<!---
		A classifier class must implement this method (requirement enforced by IIntervalClassifier interface)
		Its responsibility is to stamp each record in the interval data query with a classification code
		The classification code presumably has meaning to rate models that employ this classifier.
	--->
	<cffunction name="classifyData" access="public" output="false" returntype="void">
		<cfargument name="intervalData" type="query" required="true">

		<cfloop query="arguments.intervalData">
			<cfif isPeak(interval_start)>
				<cfset arguments.intervalData.classification_code = "PK">
			<cfelse>
				<cfset arguments.intervalData.classification_code = "OP">
			</cfif>
		</cfloop>
	</cffunction>

	<!---
		Classifiers can implement whatever helper methods prove handy to do the work of classifying each record.
		If there are several related classifiers that share some helper methods, put the common ones in a superclass.
	--->
	<cffunction name="isPeak" access="private" returntype="boolean">
		<cfargument name="interval_start" type="date" required="true">
		<!--- As an example let's assume that peak is anytime between 8am and 4pm, inclusive, on weekdays --->
		<!--- We could look at other things such as holiday status, summer/winter, etc --->
		<cfreturn isWeekday(interval_start) and isPeakTime(interval_start)>
	</cffunction>

	<cffunction name="isWeekday" access="private" returntype="boolean">
		<cfargument name="interval_start" type="date" required="true">
		<cfreturn dayOfWeek(interval_start) gt 1 and dayOfWeek(interval_start) lt 7>
	</cffunction>

	<cffunction name="isPeakTime" access="private" returntype="boolean">
		<cfargument name="interval_start" type="date" required="true">
		<cfreturn (hour(interval_start) gte 8) and (hour(interval_start) lte 16)>
	</cffunction>
</cfcomponent>