<!--- The basis for current impplementations of the ITimesliceAdapter interface --->
<!--- Note that this base class doesn't declare itself as an implementor of the interface (think of it as an abstract class) --->
<!--- Its subclasses should include the implements="ITimesliceAdapter" declaration --->
<cfcomponent output="false">


	<!--- Required by interface --->
	<cffunction name="getTimeslices" access="public" returntype="array">
		<cfargument name="requestArgs" type="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments" required="true">
		<cfargument name="timeslices" type="array" required="true">

		<!--- Here we are simply returning one slice that represents the whole period --->
		<!--- Subclasses can add more slices as they wish; they can also choose to override so as not to return this "main" slice at all --->
		<cfif ArrayLen(timeslices) eq 0>
			<cfset ArrayAppend(timeslices, makeTimeslice(requestArgs.periodStart, requestArgs.periodEnd))>
		</cfif>

		<!--- Return the set of slices --->
		<cfreturn timeslices>
	</cffunction>


	<!--- Convenience method available to this implementation and its subclasses --->
	<cffunction name="makeTimeslice" access="package" returntype="mceRateModelEngine.com.mce.rate.timeslice.Timeslice">
		<cfargument name="periodStart" type="date" required="true">
		<cfargument name="periodEnd" type="date" required="true">
		<cfargument name="numberOfDaysOffset" type="numeric" required="false" default="0">

		<!--- The number of days... but note that it doesn't make sense for the number of days to be zero --->
		<cfset var numberOfDays = Max(1, DateDiff("d", arguments.periodStart, arguments.periodEnd) + arguments.numberOfDaysOffset)>

		<!--- Create an instance of the appropriate Timeslice class --->
		<cfset var slice = CreateObject("component", getTimesliceClass()).init(arguments.periodStart, arguments.periodEnd, numberOfDays)>

		<!--- Return the Timeslice --->
		<cfreturn slice>
	</cffunction>


	<!--- If a subclass of this class wants to return a subclass of the Timeslice class  --->
	<!--- for some reason, it can specify the name of Timeslice subclass here --->
	<cffunction name="getTimesliceClass" returntype="string" access="private">
		<cfreturn "mceRateModelEngine.com.mce.rate.timeslice.Timeslice">
	</cffunction>

</cfcomponent>