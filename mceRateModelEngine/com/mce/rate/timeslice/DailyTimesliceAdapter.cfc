<!--- Declare ourselves an implementation of ITimeslice adapter --->
<cfcomponent output="false" implements="ITimesliceAdapter" extends="BaseTimesliceAdapter">

	<!--- We use some general-pupose utility functions for dates in this component --->
	<cfset dateUtils = CreateObject("component", "mceRateModelEngine.com.mce.util.DateUtils")>


	<!--- The idea here is to return one slice for each portion of the calendar month(s) represented by the period --->
	<!--- So the period "Oct17 to Dec18" is sliced into three slices ("Oct17 to Oct31", "Nov1 to Nov30", and "Dec1 to Dec18") --->
	<cffunction name="getTimeslices" access="public" returntype="array">
		<cfargument name="periodStart" type="date" required="true">
		<cfargument name="periodEnd" type="date" required="true">

		<!--- Local variables --->
		<cfset var sliceStart = "">
		<cfset var sliceEnd = "">
		<cfset var endOfMonth = "">
		<cfset var numSlices = "">


		<!--- Here, we are overloading the method in the base class; we will add to the one slice that the base returns --->
		<cfset var result = super.getTimeslices(argumentCollection=arguments)>

		<!--- Assuming that the period start/end do not sit in the same calendar month --->
		<!--- <cfif not Month(periodStart) eq Month(periodEnd) and Year(periodStart) eq Year(periodEnd)> --->

	
			<!--- Start with the start of the overall period, but include the first day --->
			<cfset sliceStart = DateAdd("d", periodStart,1)>
			
			<!--- Keep slicing until we've reached the end of the period --->

			<!--- <cfloop from="1" to="#billingDays#" index="i"> --->
			<cfloop condition=" sliceStart lte periodEnd ">
				<!--- We already know the start of this slice (it's sliceStart) --->
				<!--- The end of the slice is the end of that same month, or the end of the overall period, whichever comes first --->
				<cfset sliceEnd = DateAdd('d',1,sliceStart)>

				<!--- Now we can add that slice to our answer --->
				<cfset ArrayAppend(result, this.makeTimeslice(sliceStart, sliceEnd,1))>
		
				<cfset numSlices = ArrayLen(result)>
				<!--- Set up for next iteration through loop; the start of the next slice is one day later --->
				<cfset sliceStart = DateAdd('d',sliceEnd,0)>
				
			</cfloop>
		<!--- </cfif> --->


<cfdump var="#result#">

		<!--- Return the slices --->

		<cfreturn result>
	</cffunction>

	<cffunction name="makeTimeslice" access="package" returntype="mceRateModelEngine.com.mce.rate.timeslice.Timeslice">
		<cfargument name="periodStart" type="date" required="true">
		<cfargument name="periodEnd" type="date" required="true">
		<cfargument name="numberOfDaysOffset" type="numeric" required="false" default="0">

		<!--- The number of days... but note that it doesn't make sense for the number of days to be zero --->
		<cfset var numberOfDays = DateDiff('d',periodStart,periodEnd)>

		<!--- Create an instance of the appropriate Timeslice class --->
		<cfset var slice = CreateObject("component", getTimesliceClass()).init(arguments.periodStart, arguments.periodEnd, numberOfDays)>

		<!--- Return the Timeslice --->
		<cfreturn slice>
	</cffunction>

</cfcomponent>