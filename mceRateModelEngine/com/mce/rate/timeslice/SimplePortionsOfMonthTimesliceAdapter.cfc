<!--- Declare ourselves an implementation of ITimeslice adapter --->
<cfcomponent output="false" implements="ITimesliceAdapter" extends="BaseTimesliceAdapter">

	<!--- We use some general-pupose utility functions for dates in this component --->
	<cfset dateUtils = CreateObject("component", "mceRateModelEngine.com.mce.util.DateUtils")>


	<!--- The idea here is to return one slice for each portion of the calendar month(s) represented by the period --->
	<!--- So the period "Oct17 to Dec18" is sliced into three slices ("Oct17 to Oct31", "Nov1 to Nov30", and "Dec1 to Dec18") --->
	<cffunction name="getTimeslices" access="public" returntype="array">
		<cfargument name="requestArgs" type="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments" required="true">
		<cfargument name="timeslices" type="array" required="true">

		<!--- Local variables --->
		<cfset var sliceStart = "">
		<cfset var sliceEnd = "">
		<cfset var endOfMonth = "">

		<!--- Here, we are overloading the method in the base class; we will add to the one slice that the base returns --->
		<cfset var result = super.getTimeslices(argumentCollection=arguments)>

		<!--- Assuming that the period start/end do not sit in the same calendar month --->
		<cfif not (Month(requestArgs.periodStart) eq Month(requestArgs.periodEnd) and Year(requestArgs.periodStart) eq Year(requestArgs.periodEnd))>

			<!--- Start with the start of the overall period, but don't include the first day --->
			<cfset sliceStart = DateAdd("d", requestArgs.periodStart, 1)>

			<!--- Keep slicing until we've reached the end of the period --->
			<cfloop condition=" sliceStart lte requestArgs.periodEnd ">

				<!--- We already know the start of this slice (it's sliceStart) --->
				<!--- The end of the slice is the end of that same month, or the end of the overall period, whichever comes first --->
				<cfset sliceEnd = dateUtils.earlierOf(dateUtils.lastDayOfMonth(sliceStart), requestArgs.periodEnd)>

				<!--- Now we can add that slice to our answer --->
				<cfset ArrayAppend(result, makeTimeslice(sliceStart, sliceEnd, 1))>

				<!--- Set up for next iteration through loop; the start of the next slice is one day later --->
				<cfset sliceStart = DateAdd("d", 1, sliceEnd)>
			</cfloop>
		</cfif>

		<!--- Return the slices --->
		<cfreturn result>
	</cffunction>

</cfcomponent>