<cfcomponent output="false">


	<cffunction name="init">
		<cfargument name="calculator" type="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator" required="true">
		<cfargument name="requestArgs" type="RateModelRequestArguments" required="true">
		<cfargument name="timeout" type="numeric" required="false" default="60">

		<!--- Keep the argument values --->
		<cfset this.calculator = arguments.calculator>
		<cfset this.requestArgs = arguments.requestArgs>
		<cfset this.timeout = arguments.timeout>

		<!--- We use these to report on time elapsed, etc --->
		<cfset this.timeRequested = now()>
		<cfset this.finished = false>
		<cfset this.status = "constructed">
		<cfset this.exception = "">

		<!--- Convention is to return "this" --->
		<cfreturn this>
	</cffunction>


	<cffunction name="calculate">
		<!--- Start timer and set internal status --->
		<cfset this.timeCalculationStarted = now()>
		<cfset this.status = "running">

		<!--- Configure the model calculator --->
		<cfset this.calculator.configure(this.requestArgs)>
		<!--- Tell the calculator to load its data (this is where most of the time is spent) --->
		<cfset this.calculator.loadData()>

		<!--- Tell the model to calculate (generally where an exception will occur) --->
		<cfset this.status = "calculating">
		<cfset this.response.result = this.calculator.calculate()>

		<!--- And we're done! --->
		<cfset this.status = "ok">
		<cfset this.exception = "">
		<cfset this.finished = true>
		<cfset this.timeCalculationFinished = now()>
	</cffunction>

</cfcomponent>