<!---
	Simple queuieng mechanism for rate calculation requests
	Intended to be used as a singleton
	Current implementation forces the rate calculation engine to be single threaded (process one request at a time)
	Will be expanded to allow a tunable number of concurrent calculations, perhaps add priority levels as well.
--->
<cfcomponent output="false">
	<cfset this.defeatQueueByDefault = true>
	<cfset this.defaultRequestTimeout = 60 * 1000>
	<cfset this.pollingInterval = 100>
	<cfset this.queuedRequests = ArrayNew(1)>
	<cfset this.running = false>
	<cfset this.start()>


	<!--- Main public method --->
	<cffunction name="processRequest" returntype="RateModelResponse">
		<cfargument name="modelRequest" type="RateModelRequest" required="true" hint="The rate model request instance to process">
		<cfargument name="defeatQueue" type="boolean" default="#this.defeatQueueByDefault#" required="false" hint="The rate model request instance to process">
		<cfargument name="timeout" type="numeric" default="#this.defaultRequestTimeout#" hint="Timeout in milliseconds">

		<cfif defeatQueue>
			<cfreturn runRequest(modelRequest)>
		<cfelse>
			<cfreturn processRequestWithQueue(modelRequest, timeout)>
		</cfif>
	</cffunction>

	<cffunction name="start" access="public" returntype="void">
		<cfset startPollingLoop()>
		<cfset this.running = true>
	</cffunction>

	<cffunction name="stop" access="public" returntype="void">
		<cfset this.running = false>
	</cffunction>



	<!--- Remainder of methods intended to be used privately --->
	<cffunction name="addRequest" returntype="void">
		<cfargument name="request" type="RateModelRequest" required="true">
		<cfset ArrayAppend(this.queuedRequests, request)>
	</cffunction>

	<cffunction name="popRequest" returntype="RateModelRequest">
		<cfset var request = this.queuedRequests[1]>
		<cfset ArrayDeleteAt(this.queuedRequests, 1)>
		<cfreturn request>
	</cffunction>

	<cffunction name="numRequests" returntype="numeric">
		<cfreturn ArrayLen(this.queuedRequests)>
	</cffunction>

	<cffunction name="hasRequests" returntype="boolean">
		<cfreturn not ArrayIsEmpty(this.queuedRequests)>
	</cffunction>

	<cffunction name="runNextRequest" returntype="RateModelResponse">
		<cfset var modelRequest = popRequest()>
		<cfset var response = "">

		<cftry>
			<cfset response = runRequest(modelRequest)>
			<cfcatch>
				<cfset modelRequest.error = cfcatch>
				<cfset modelRequest.finished = true>
			</cfcatch>
		</cftry>
		<cfreturn response>
	</cffunction>

	<cffunction name="runRequest" returntype="RateModelResponse">
		<cfargument name="modelRequest" type="RateModelRequest" required="true">

		<cfset var response = CreateObject("component", "RateModelResponse").init(modelRequest)>
		<cftry>
			<cfset response.request.calculate()>

			<cfcatch type="any">
				<cfset response.request.status = "error">
				<cfset response.request.exception = cfcatch>
				<cfrethrow>
			</cfcatch>
		</cftry>

		<cfreturn response>
	</cffunction>

	<cffunction name="processRequestWithQueue" returntype="RateModelResponse" access="private">
		<cfargument name="modelRequest" type="RateModelRequest" required="true" hint="The rate model request instance to process">
		<cfargument name="timeout" type="numeric" default="60000" hint="Timeout in milliseconds">

		<cfset var timeoutExpires = DateAdd("l", timeout, now())>

		<!--- Add the request to the queue --->
		<cfset addRequest(modelRequest)>

		<!--- Wait until the queue processes this request --->
		<cfloop condition="now() lte timeoutExpires">
			<cfthread action="sleep" duration="#this.pollingInterval#"/>
			<cfif modelRequest.finished>
				<cfset rethrowIfRequestGeneratedError(modelRequest)>
				<cfreturn modelRequest.response>
			</cfif>
		</cfloop>

		<cfinvoke
			component="#Application.utils.Logger#"
			method="error"
			message="Rate model request timed out."
			detail="The timeout of #timeout# milliseconds has expired.">
	</cffunction>

	<cffunction name="rethrowIfRequestGeneratedError">
		<cfargument name="modelRequest" type="RateModelRequest" required="true">
		<cfthrow
			errorcode="#modelRequest.error.errorCode#"
			message="#modelRequest.error.message#"
			detail="#modelRequest.error.detail#">
	</cffunction>

	<!--- Starts the process that monitors the queue for requests. Called once internally upon instantiation. --->
	<cffunction name="startPollingLoop" returntype="void">
		<cfif not this.running>
			<cfthread action="run" name="RateModelRequestBrokerThread">
				<cfloop condition="#this.running#">
					<cfif this.hasRequests()>
						<cfset this.runNextRequest()>
					</cfif>
					<cfthread action="sleep" duration="#this.pollingInterval#"/>
				</cfloop>
			</cfthread>
		</cfif>
	</cffunction>


</cfcomponent>