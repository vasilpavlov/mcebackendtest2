<cfcomponent extends="com.mce.rate.calculator.impl.BaseSimpleThirtyDayProratingModelCalculator"
			 implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<cffunction name="getRequiredInputCodes" returntype="string" access="package">
				<cfreturn "adjDeliveryrevenuesurcharge,adjMAC,adjMACreconciliation,adjMACtransitionadjustment,adjMACuncollectiblebillexpense,adjMSC,adjMSCi,adjMSCii,adjPSL18a,adjRevenuedecouplingmechanismadjustment,demandDeliveryCharge100,demandDeliveryCharge1100,demandDeliveryCharge2000,demandDeliveryCharge800,demandMarketsupply,demandMonthlyadjustments,energyBillingandpaymentprocessingsingleservice,energyDeliverycharge,energyDeliveryrevenuesurcharge,energyMarketsupply,energyMerchantfunctioncharge,energyMetercharges,energyMeterdatacharge,energyMeterservicecharge,energyMonthlyadjustments,energyRenewableportfoliostandardprogram,energyRevenuedecouplingmechanismadjustment,energySurchargePslSection18aAssessments,energySystembenefits,taxGRTCommodity,taxSales,taxTD">
	</cffunction>
	
	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedIntervalData" access="package" returntype="string">
		<cfreturn "none">
	</cffunction>


	<!--- Perform actual calculations... all retrieved data is available via "this.data" structure --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfscript>
			// Call supermethod to get blank "CalculatorResult" object
			var result = super.calculate();
			var numslices = ArrayLen(this.timeslices); //includes full period slice
			var mainSlice = this.timeslices[1]; // 1 is the full billing period slice

		// Input & Calculations
		
			//Usage Info
			var period_start = mainSlice.periodStart;
			var period_end = mainSlice.periodEnd;
			var recorded_value = mainSlice.energyUsage.recorded_value;
			var demand = mainSlice.getEnergyUsageMetaValue("demand");
			var billingdays = mainSlice.numberOfDays;
			var hasSupplyContract = mainSlice.energyUsage.hasSupplyContract; //IIF(mainSlice.getEnergyUsageMetaValue("tdcharges") eq mainSlice.energyUsage.total_cost,True,False);
			
			//Energy Charges
			var energyMarketsupply = IIF(hasSupplyContract eq True,0, recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("energyMarketsupply")/100);
			var energyMonthlyadjustments = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("energyMonthlyadjustments")/100;
			var energyDeliverycharge = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("energyDeliverycharge")/100;
			var energySystembenefits = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("energySystembenefits")/100;
			var energyRenewableportfoliostandardprogram = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("energyRenewableportfoliostandardprogram")/100;
			var energyMerchantfunctioncharge = IIF(hasSupplyContract eq True,0,recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("energyMerchantfunctioncharge")/100);
			var energyRevenuedecouplingmechanismadjustment = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("energyRevenuedecouplingmechanismadjustment")/100;
			var energyDeliveryrevenuesurcharge = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("energyDeliveryrevenuesurcharge")/100;
			var energySurchargePslSection18aAssessments = recorded_value * prorateInputValueUsingPortionOfTotalDaysPolicy("energySurchargePslSection18aAssessments")/100;

			//Adjustment Factors
			var adjMSCi = mainSlice.getInputValue("adjMSCi");
			var adjMSCii = mainSlice.getInputValue("adjMSCii");			
			var adjMSC = adjMSCi + adjMSCii;//mainSlice.getInputValue("adjMSC");

			var adjMACreconciliation = mainSlice.getInputValue("adjMACreconciliation");
			var adjMACuncollectiblebillexpense = mainSlice.getInputValue("adjMACuncollectiblebillexpense");
			var adjMACtransitionadjustment = mainSlice.getInputValue("adjMACtransitionadjustment");
			var adjRevenuedecouplingmechanismadjustment = prorateInputValueUsingPortionOfTotalDaysPolicy("energyRevenuedecouplingmechanismadjustment")/100;
			var adjDeliveryrevenuesurcharge = prorateInputValueUsingPortionOfTotalDaysPolicy("energyDeliveryrevenuesurcharge")/100;
			var adjPSL18a = mainSlice.getInputValue("energySurchargePslSection18aAssessments");
			//adjMAC for 4-I & 9-I don't include adjPSL18a
			var adjMAC = adjMACreconciliation + adjMACuncollectiblebillexpense +  adjMACtransitionadjustment
			+ adjRevenuedecouplingmechanismadjustment + adjDeliveryrevenuesurcharge;
			
			//Energy Subtotal
			var subTotalEnergyChargeBeforeAdjustments = energyMarketsupply + energyMonthlyadjustments
			+ energyDeliverycharge + energySystembenefits + energyRenewableportfoliostandardprogram
			+ energyMerchantfunctioncharge + energyRevenuedecouplingmechanismadjustment + energyDeliveryrevenuesurcharge
			+ energySurchargePslSection18aAssessments;

			var adjustmentFactorMSC = IIF(hasSupplyContract eq True,0,recorded_value * adjMSC);
			var adjustmentFactorMAC = recorded_value * adjMAC;
			var adjustmentFactorTotal = adjustmentFactorMSC + adjustmentFactorMAC;

			var totalCommodityCharge = adjustmentFactorMSC + energyMarketsupply 
			+ energyMerchantfunctioncharge;
			
			var grtCommodityTaxCharge = (totalCommodityCharge * mainSlice.getInputValue("taxGRTCommodity"))/100;
			
			var energyCharges = energyMonthlyadjustments + (adjMAC * recorded_value) + (recorded_value * (prorateInputValueUsingPortionOfTotalDaysPolicy("energyDeliverycharge")/100));

			var totalEnergyDeliveryCharges  = 	energyMonthlyadjustments + energyDeliverycharge 
			+ adjustmentFactorMAC;
			var totalSBCRPSCharge = energySystembenefits + energyRenewableportfoliostandardprogram;
			
			//Meter Charges
			var energyMetercharges = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("energyMetercharges"));
			var energyMeterservicecharge = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("energyMeterservicecharge"));
			var energyMeterdatacharge = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("energyMeterdatacharge"));
			var energyBillingandpaymentprocessingsingleservice = mainSlice.getInputValue("energyBillingandpaymentprocessingsingleservice");
			
			//Energy Totals Final
			var totalTDCharge = energyMonthlyadjustments
			+ energyDeliverycharge + energySystembenefits + energyRenewableportfoliostandardprogram
			+ energySurchargePslSection18aAssessments + adjustmentFactorMAC
			+ energyMetercharges + energyMeterservicecharge + energyMeterdatacharge + energyBillingandpaymentprocessingsingleservice;
			var grtTDTaxCharge = totalTDCharge * mainSlice.getInputValue("taxTD")/100;
			
			var totalEnergyFuelUsageBeforeTax = totalCommodityCharge + totalTDCharge;
			var subTotalEnergyCharge = totalEnergyFuelUsageBeforeTax + grtCommodityTaxCharge + grtTDTaxCharge;

			//Demand value in Tier
			var demand100 = IIF(demand < 100,demand,100);
			var demand800 = IIF(demand100 eq 100 and demand lt 900,demand - 100,800);
			var demand1100 = IIF("demand800" eq 800,IIF(demand lte 900 and demand lt 2000,demand - 900,1100),0);
			var demand2000 = IIF("demand1100" eq 1100 and demand gt 2000,demand - 2000,0);
			
			//Demand Market Supply Charges (Tier Pricing)
			var demandMarketSupply100 = IIF(hasSupplyContract eq True,0,demand100 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMarketSupply"));
			var demandMarketSupply800 = IIF(hasSupplyContract eq True,0,demand800 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMarketSupply"));
			var demandMarketSupply1100 = IIF(hasSupplyContract eq True,0,demand1100 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMarketSupply"));
			var demandMarketSupply2000 = IIF(hasSupplyContract eq True,0,demand2000 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMarketSupply"));

			var demandMarketSupplyTotal = demandMarketSupply100 + demandMarketSupply800
			+ demandMarketSupply1100 + demandMarketSupply2000;

			//Demand Monthly Adjustments (Tier Pricing)
			var demandMonthlyAdj100 = IIF(hasSupplyContract eq True,0,demand100 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMonthlyAdjustments"));
			var demandMonthlyAdj800 = IIF(hasSupplyContract eq True,0,demand800 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMonthlyAdjustments"));
			var demandMonthlyAdj1100 = IIF(hasSupplyContract eq True,0,demand1100 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMonthlyAdjustments"));
			var demandMonthlyAdj2000 = IIF(hasSupplyContract eq True,0,demand2000 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandMonthlyAdjustments"));
			var demandMonthlyAdjustmentsTotal = demandMonthlyAdj100 + demandMonthlyAdj800
			+ demandMonthlyAdj1100 + demandMonthlyAdj2000;
	
			
			//Demand Delivery Charges (Tier Pricing)			
			var demandDeliveryCharge100 = demand100 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandDeliveryCharge100");
			var demandDeliveryCharge800 = demand800 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandDeliveryCharge800");
			var demandDeliveryCharge1100 = demand1100 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandDeliveryCharge1100");
			var demandDeliveryCharge2000 = demand2000 * prorateInputValueUsingPortionOfTotalDaysPolicy("demandDeliveryCharge2000");

			var demandDeliveryChargeTotal = demandDeliveryCharge100 + demandDeliveryCharge800
			+ demandDeliveryCharge1100 + demandDeliveryCharge2000;
			
			//Demand Totals
			var subTotalLowTensionDemandChargeSupply = demandMarketSupplyTotal;
			var subTotalLowTensionDemandChargeDelivery = demandMonthlyAdjustmentsTotal + demandDeliveryChargeTotal;
			
			var subTotalLowTensionDemandCharge	= subTotalLowTensionDemandChargeSupply + subTotalLowTensionDemandChargeDelivery;		
			

			//Pro-Rated Demand Totals
			var proratedTotalDemandSupplyCharge = prorateUsingThirtyDayPolicy(subTotalLowTensionDemandChargeSupply);
			var proratedTotalDemandDeliveryCharge = prorateUsingThirtyDayPolicy(subTotalLowTensionDemandChargeDelivery);
			var proratedTotalDemandCharge = proratedTotalDemandDeliveryCharge + proratedTotalDemandSupplyCharge;
			
			var demandGRTCommodityTaxCharge = proratedTotalDemandSupplyCharge * mainSlice.getInputValue("taxGRTCommodity")/100 ;
			var demandGRTTDTaxCharge = proratedTotalDemandCharge * mainSlice.getInputValue("taxTD")/100;
			var totalDemandSupplyChargeWithGRTTax = proratedTotalDemandSupplyCharge + demandGRTCommodityTaxCharge;
			var totalDemandDeliveryChargeWithGRTTax = proratedTotalDemandDeliveryCharge + demandGRTTDTaxCharge;
			
			var subTotalDemandCharge = totalDemandSupplyChargeWithGRTTax + totalDemandDeliveryChargeWithGRTTax;
			
			var totalDeliveryCharges = totalTDCharge + proratedTotalDemandCharge;
			var grossReceiptsTaxDelivery = grtTDTaxCharge + demandGRTTDTaxCharge;
			var finalSubTotalDelivery = totalDeliveryCharges + grossReceiptsTaxDelivery;
			var salesTaxDelivery = mainSlice.getinputvalue("taxSales") * finalSubTotalDelivery/100;
			var grandTotalDelivery = finalSubTotalDelivery + salesTaxDelivery;

			var totalSupplyCharges = totalCommodityCharge + proratedTotalDemandSupplyCharge;
			var grossReceiptsTaxSupply = grtCommodityTaxCharge + demandGRTCommodityTaxCharge;
			var finalSubTotalSupply = totalSupplyCharges + grossReceiptsTaxSupply;
			var salesTaxSupply = mainSlice.getinputvalue("taxSales") * finalSubTotalSupply/100;
			var grandTotalSupply = finalSubTotalSupply + salesTaxSupply;
			
			var totalBillingCharges = grandTotalSupply + grandTotalDelivery;
		// Results Output
		
			//Simple Supply Check - not very scientific
			result.addOutput("Supply Check","Has Supply Contract ?",hasSupplyContract,"boolean");
			result.addMoneyOutput("Supply Check","Total Cost $",mainSlice.energyUsage.total_cost);
			result.addMoneyOutput("Supply Check","T&D Charges $",mainSlice.getEnergyUsageMetaValue("tdcharges"));
			result.addMoneyOutput("Supply Check","Supply Charges $",mainSlice.getEnergyUsageMetaValue("supplycharges"));
			
			//Usage Info
			result.addOutput("Usage Info","Billing Period",period_start & ' to ' & period_end,"date");
			result.addOutput("Usage Info","Days in Billing Period",billingdays,"numeric");
			result.addOutput("Usage Info","Number of Timeslices in Billing Period",numslices-1,"numeric");
			result.addEnergyOutput("Usage Info","Total Consumption",recorded_value);
			result.addEnergyOutput("Usage Info","Demand",demand);
			result.addEnergyOutput("Usage Info","Demand (Tier 100kW)",demand100);
			result.addEnergyOutput("Usage Info","Demand (Tier 800kW)",demand800);
			result.addEnergyOutput("Usage Info","Demand (Tier 1100kW)",demand1100);
			result.addEnergyOutput("Usage Info","Demand (Tier 2000kW)",demand2000);
			
			//Rates
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMSC"),adjMSC);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMAC"),adjMAC);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMSCi"),adjMSCi);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMSCii"),adjMSCii);

			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMACreconciliation"),adjMACreconciliation);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMACuncollectiblebillexpense"),adjMACuncollectiblebillexpense);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjMACtransitionadjustment"),adjMACtransitionadjustment);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjRevenuedecouplingmechanismadjustment"),adjRevenuedecouplingmechanismadjustment);
			result.addMoneyOutput("Rate Info",mainSlice.getInputName("adjDeliveryrevenuesurcharge"),adjDeliveryrevenuesurcharge);
						
			//Charges
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("energyMeterservicecharge"),energyMeterservicecharge);					
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("energyMetercharges"),energyMetercharges);
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("energyMeterdatacharge"),energyMeterdatacharge);
			result.addMoneyOutput("Meter Charges",mainSlice.getInputName("energyBillingandpaymentprocessingsingleservice"),energyBillingandpaymentprocessingsingleservice);

			result.addMoneyOutput("Demand Charges",mainSlice.getInputName("demandDeliveryCharge100"), demandDeliveryCharge100);
			result.addMoneyOutput("Demand Charges",mainSlice.getInputName("demandDeliveryCharge800"), demandDeliveryCharge800);
			result.addMoneyOutput("Demand Charges",mainSlice.getInputName("demandDeliveryCharge1100"), demandDeliveryCharge1100);
			result.addMoneyOutput("Demand Charges",mainSlice.getInputName("demandDeliveryCharge2000"), demandDeliveryCharge2000);


			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyMarketsupply"),energyMarketsupply);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyMonthlyadjustments"),energyMonthlyadjustments);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyDeliverycharge"),energyDeliverycharge);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energySystembenefits"),energySystembenefits);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyRenewableportfoliostandardprogram"),energyRenewableportfoliostandardprogram);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyMerchantfunctioncharge"),energyMerchantfunctioncharge);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyRevenuedecouplingmechanismadjustment"),energyRevenuedecouplingmechanismadjustment);
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energyDeliveryrevenuesurcharge"),energyDeliveryrevenuesurcharge);							
			result.addMoneyOutput("Energy Charges",mainSlice.getInputName("energySurchargePslSection18aAssessments"),energySurchargePslSection18aAssessments);

			result.addMoneyOutput("Energy Totals","Sub-Total Energy Charge (Before Adjustments)",subTotalEnergyChargeBeforeAdjustments);
			result.addMoneyOutput("Energy Totals","Adjustment Factor (MSC)",adjustmentFactorMSC);
			result.addMoneyOutput("Energy Totals","Adjustment Factor (MAC)",adjustmentFactorMAC);
			result.addMoneyOutput("Energy Totals","Adjustment Factors (MSC + MAC)",adjustmentFactorTotal);
			result.addMoneyOutput("Energy Totals","Total Commodity Charge (Market Supply + MSC Adjustment + Merchant Function Charge)",totalCommodityCharge);
			result.addMoneyOutput("Energy Totals","GRT Commodity Tax Charge",grtCommodityTaxCharge);
			result.addMoneyOutput("Energy Totals","Energy Charges (Monthly Adjustments + Adjustment Factor (MAC) + Delivery Charge)",energyCharges);
			result.addMoneyOutput("Energy Totals","Total Delivery (Energy Only + MAC Adjustment)",totalEnergyDeliveryCharges);
			result.addMoneyOutput("Energy Totals","Total SBC/RPS Charge",totalSBCRPSCharge);
			result.addMoneyOutput("Energy Totals","Total T&D Charge (Delivery Charges + MAC Adjustment)",totalTDCharge);
			result.addMoneyOutput("Energy Totals","GRT T&D Tax Charge",grtTDTaxCharge);
			result.addMoneyOutput("Energy Totals","Sub-Total Energy Charge (Supply + Delivery w/Adjustments and Tax)",subTotalEnergyCharge);

			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Demand Charge (Supply)",subTotalLowTensionDemandChargeSupply);
			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Demand Charge (Delivery)",subTotalLowTensionDemandChargeDelivery);
			result.addMoneyOutput("Demand Totals","Sub-Total Low Tension Charge",subTotalLowTensionDemandCharge);

			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Delivery Charge",proratedTotalDemandDeliveryCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Supply Charge",proratedTotalDemandSupplyCharge);
			result.addMoneyOutput("Pro-Rated Totals","Pro-Rated Total Demand Charge",proratedTotalDemandCharge);
			result.addMoneyOutput("Pro-Rated Totals","GRT Commodity Tax Charge (Demand)",demandGRTCommodityTaxCharge);
			result.addMoneyOutput("Pro-Rated Totals","GRT T&D Tax Charge (Demand)",demandGRTTDTaxCharge);
			result.addMoneyOutput("Pro-Rated Totals","Total Demand Supply Charge with GRT Tax",totalDemandSupplyChargeWithGRTTax);
			result.addMoneyOutput("Pro-Rated Totals","Total Demand Delivery Charge with GRT Tax",totalDemandDeliveryChargeWithGRTTax);
			result.addMoneyOutput("Pro-Rated Totals","Sub-Total Demand Charge (Supply + Delivery w/Tax)",subTotalDemandCharge);
	
			result.addMoneyOutput("Delivery Totals","Total Delivery Charges",totalDeliveryCharges);
			result.addMoneyOutput("Delivery Totals","Gross Receipts Tax (Delivery)",grossReceiptsTaxDelivery);
			result.addMoneyOutput("Delivery Totals","Sub-Total (Delivery)",finalSubTotalDelivery);
			result.addMoneyOutput("Delivery Totals","Sales Tax (Delivery)",salesTaxDelivery);
			result.addMoneyOutput("Delivery Totals","Grand Total (Delivery)",grandTotalDelivery);
			
			result.addMoneyOutput("Delivery Totals","Total Supply Charges",totalSupplyCharges);
			result.addMoneyOutput("Delivery Totals","Gross Receipts Tax (Supply)",grossReceiptsTaxSupply);
			result.addMoneyOutput("Delivery Totals","Sub-Total (Supply)",finalSubTotalSupply);
			result.addMoneyOutput("Delivery Totals","Sales Tax (Supply)",salesTaxSupply);
			result.addMoneyOutput("Delivery Totals","Grand Total (Supply)",grandTotalSupply);
			
			result.addMoneyOutput("Grand Totals","Grand Total",totalBillingCharges);
			
			//Set Calculator Grand Total
			result.setTotal(totalBillingCharges);
		</cfscript>

		<cfreturn result>
	</cffunction>

</cfcomponent>