<cfcomponent
	extends="com.mce.rate.calculator.impl.BaseModelCalculator">


	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<cffunction name="getRequiredInputCodes" returntype="string" access="package">
		<cfreturn "lossFactor,demandCostRatePerKw,grossReceiptTaxRate,salesTaxRate,tdAccessCreditFactor,ntacChargesRate,ancillaryChargesRate,mfcChargesRate,milChargesRate">
	</cffunction>

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedIntervalData" access="package" returntype="string">
		<cfreturn "first">
	</cffunction>

	<!--- Perform actual calculations... all retrieved data is available via "this.data" structure --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfscript>
			// Call supermethod to get blank "CalculatorResult" object with a total of 0
			var result = super.calculate();
			var data = this.timeslices[1];
			var usageFromIntervalsInKwh = data.getIntervalDataAggregate("totalIntervalValue");

			var intervalTotalHours = data.getIntervalDataAggregate("totalIntervalLength") / 60;
			var intervalTotalCost = data.getIntervalDataAggregate("totalIntervalCost") / 1000;	// Line A
			var avgRateFromIntervals = intervalTotalCost / (usageFromIntervalsInKwh); // / 1000; // Line B
			var usageFromBillInKwh = data.energyUsage.recorded_value; // Line C1
			var peakDemandFromIntervals = data.getIntervalDataAggregate("maxIntervalValue"); // Line C2

			var peakDemandFromBill = data.getEnergyUsageMetaValue("demand");
			var lossFactor = data.getInputValue("lossFactor"); // Line D
			var adjustedUsageInKwh = usageFromBillInKwh * lossFactor; // Line E
			var adjustedEnergyCost = adjustedUsageInKwh * avgRateFromIntervals; // Line F
			var supplyRate = adjustedEnergyCost / (usageFromBillInKwh); // Line G
			var finalEnergyCost = supplyRate * usageFromBillInKwh; // Line H, though this seems to just be same as line F?
			var miscAncillaryCharges = data.getInputValue("ancillaryChargesRate"); // Line I Item 1
			var miscNtacCharges = data.getInputValue("ntacChargesRate"); // Line I Item 2
			var miscDemandCost = data.getInputValue("demandCostRatePerKw") * peakDemandFromBill; // Line I Item 3
			var mfcRate = data.getInputValue("mfcChargesRate"); // MFC
			var milRate = data.getInputValue("milChargesRate"); // Plus 1 mil Access Credit
			var milCharges = milRate * usageFromBillInKwh;
 			var miscChargesTotal = ((miscAncillaryCharges + miscNtacCharges + mfcRate) * usageFromBillInKwh) + miscDemandCost; // Line I
			var supplyCostTotal = finalEnergyCost + miscChargesTotal; // Line J
			var grossReceiptTaxRate = data.getInputValue("grossReceiptTaxRate") / 100; // Line K
			var salesTaxRate = data.getInputValue("salesTaxRate") / 100; // Line L
			var grossReceiptTax = grossReceiptTaxRate * supplyCostTotal; // Line M
			var salesTax = (grossReceiptTax + supplyCostTotal) * salesTaxRate;
			var supplyCostWithTax = supplyCostTotal + salesTax + grossReceiptTax;
			var grandTotal = supplyCostWithTax;

			result.addEnergyOutput("Intervals", "Consumption from intervals", usageFromIntervalsInKwh);
			result.addNumberOutput("Intervals", "Hours represented by intervals", intervalTotalHours, "0.00");
			result.addNumberOutput("Intervals", "Peak demand from intervals", peakDemandFromIntervals, "0.00");

			result.addMoneyOutput("Intervals", "Cost based on intervals (Line A)", intervalTotalCost);
			result.addFactorOutput("Intervals", "Rate based on interval cost (Line B)", avgRateFromIntervals);
			result.addEnergyOutput("From Bill", "Consumption from paper bill (Line C1)", usageFromBillInKwh);
			result.addEnergyOutput("From Bill", "Peak demand (Line C2)", peakDemandFromBill);
			result.addFactorOutput("Rates", "Loss factor (Line D)", lossFactor);
			result.addEnergyOutput("Components", "Adjusted kWh (Line E)", adjustedUsageInKwh);
			result.addMoneyOutput("Components", "Adjusted Energy Cost (Line F)", adjustedEnergyCost);
			result.addFactorOutput("Rates", "Computed Supply Rate (Line G)", supplyRate);
			result.addMoneyOutput("Components", "Final Energy Cost (Line H)", finalEnergyCost);
			result.addFactorOutput("Rates", "Ancillary Charges Rate (Line I-1)", miscAncillaryCharges);
			result.addFactorOutput("Rates", "NTAC Charges Rate (Line I-2)", miscNtacCharges);
			result.addMoneyOutput("Components", "Demand Cost (Line I-3)", miscDemandCost);
			result.addMoneyOutput("Components", "Non-Energy Costs total (Line I)", miscChargesTotal);
			result.addMoneyOutput("Totals", "Supply Cost total (Line J)", supplyCostTotal);

			// Add taxes
			result.addFactorOutput("Tax", "Gross Receipt Tax Rate (Line K)", grossReceiptTaxRate);
			result.addFactorOutput("Tax", "Sales Tax Rate (Line L)", salesTaxRate);
			result.addMoneyOutput("Tax", "Gross Receipt Tax", grossReceiptTax);
			result.addMoneyOutput("Tax", "Sales Tax", salesTax);
			result.addMoneyOutput("Totals", "Grand Total", grandTotal);

			// Final total
			result.setTotal(grandTotal);
		</cfscript>
		<cfreturn result>
	</cffunction>

</cfcomponent>