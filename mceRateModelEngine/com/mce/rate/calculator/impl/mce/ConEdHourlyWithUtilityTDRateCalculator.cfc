<cfcomponent
	extends="com.mce.rate.calculator.impl.mce.ConEdHourlyRateCalculator"
	implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">

	<!--- Perform actual calculations... all retrieved data is available via "this.data" structure --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfscript>
			var result = super.calculate();
			var data = this.timeslices[1];

			var salesTaxRate = data.inputValues.salesTaxRate / 100;
			var milChargesTotal = data.inputValues.milChargesRate * data.energyUsage.recorded_value;
			var tdDeliveryAmount = data.getEnergyUsageMetaValue("TDCharges"); // T&D Line 1
			var tdAccessCredit = tdDeliveryAmount * data.getInputValue("tdAccessCreditFactor"); // T&D Line 2
			var tdTotalTaxableAmount = tdDeliveryAmount + tdAccessCredit + milChargesTotal;
			var tdSalesTax = tdTotalTaxableAmount * salesTaxRate; // T&D Line 3
			var tdTotalCost = tdDeliveryAmount + tdAccessCredit + tdSalesTax + milChargesTotal; // T&D Line 4

			result.addEnergyOutput("Misc", "T&D Delivery amount (Line 1)", tdDeliveryAmount);
			result.addEnergyOutput("Misc", "T&D Access Credit (Line 2)", tdAccessCredit);
			result.addMoneyOutput("Misc", "T&D Sales Tax (Line 3)", tdSalesTax);
			result.addMoneyOutput("Misc", "Plus 1 mil Access Credit", milChargesTotal);
			result.addMoneyOutput("Misc", "T&D TotalCost", tdTotalCost);

			result.setTotal(result.getTotal() + tdTotalCost);
		</cfscript>
		<cfreturn result>
	</cffunction>

</cfcomponent>