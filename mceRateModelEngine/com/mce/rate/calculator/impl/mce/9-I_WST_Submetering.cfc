<cfcomponent extends="com.mce.rate.calculator.impl.BaseSimpleThirtyDayProratingModelCalculator"
			 implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<cffunction name="getRequiredInputCodes" returntype="string" access="package">
				<cfreturn "ConEd.WST.EL9-I.Monthly.Adjustments.Demand">
	</cffunction>
	
	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedIntervalData" access="package" returntype="string">
		<cfreturn "none">
	</cffunction>
	
	<!--- function for summer and non-summer calc --->
	<cffunction name="isSummerTime" access="private" returntype="boolean">
  <cfargument name="interval_start" type="date" required="true">
  <cfreturn ((month(interval_start) gte 6) and (day(interval_start) gte 1) and (month(interval_start) lte 9) and (day(interval_start) lte 30))>
 </cffunction>


	<!--- Perform actual calculations... all retrieved data is available via "this.data" structure --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfscript>
			// Call supermethod to get blank "CalculatorResult" object
			var result = super.calculate();
			var numslices = ArrayLen(this.timeslices); //includes full period slice
			var mainSlice = this.timeslices[1]; // 1 is the full billing period slice

		// Input & Calculations
		
			//Usage Info
			var period_start = mainSlice.periodStart;
			var period_end = mainSlice.periodEnd;
			
			var isSummer = IIF(isSummerTime(period_start) eq true, 1, 0);
			var notSummer = 1 - isSummer;
			
			var usage = this.params.usage; 
			//IIF(this.params.usage > 0, this.params.usage, this.params.inputValueOverrides.mainSliceData.usage);
			var demand = this.params.demand;
			//IIF(this.params.demand > 0, this.params.demand, this.params.inputValueOverrides.mainSliceData.demand);
			var billingdays = mainSlice.numberOfDays;
			
			//var rdmadjustment = mainSlice.getInputValue("ny.conedison.WST.9-I.maccharges.revdecmecadj.kwh");
			
			//TODO: should be sent from MS2 or we can get it here from the property
			var tripCode = this.params.tripCode;
			//IIF(this.params.tripcode > 0, this.params.tripcode, this.params.inputValueOverrides.mainSliceData.tripCode);												
			var energyMarketSupplyInputValue = "ConEd.WST.EL9-I.Market.Supply.Energy" & tripCode;
			
			var billableKVar =  mainSlice.energyPropertyUsage["BillableReactivePowerDemand"]; //this.params.inputValueOverrides.mainSliceData.bkvar;			
			
			//Energy Charges
			var energyMarketsupply = usage * prorateInputValueUsingPortionOfTotalDaysPolicy(energyMarketSupplyInputValue)/100;//prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Market.Supply.Energy")/100;
			var energyMonthlyadjustments = usage * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Monthly.Adjustments")/100;
			var energyDeliverycharge = usage * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delivery.Charge.Energy")/100;
			var energySystembenefits = usage * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.System.Benefits")/100;
			var energyRenewableportfoliostandardprogram = usage * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Renewable.Portfolio.Standard.Prog")/100;
			var energyMerchantfunctioncharge = usage * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Merchant.Function.Charge")/100;
			var energyRevenuedecouplingmechanismadjustment = usage * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Revenue.Decoupling.Adj")/100;
			var energyDeliveryrevenuesurcharge = usage * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delivery.Revenue.Surcharge")/100;
			var energySurchargePslSection18aAssessments = usage * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Collect.PSL.Sec18a")/100;
			
			var billableReactivePower = 0;//billableKVar * billingdays/30;
			var energyBillableReactivePowerChargesDemand = billableReactivePower * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Billable.Reactive.Power.Demand")/100;
			
			//Adjustment Factors
			var adjMSCi = mainSlice.getInputValue("ConEd.WST.EL9-I.MSC-I.Adjustment")/100;
			var adjMSCii = mainSlice.getInputValue("ConEd.WST.EL9-I.MSC-II.Adjustment")/100;			
			var adjMSC = adjMSCi + adjMSCii;

			var adjMACreconciliation = mainSlice.getInputValue("ConEd.WST.EL9-I.MAC.Reconciliation.Adjustment")/100;
			var adjMACuncollectiblebillexpense = mainSlice.getInputValue("ConEd.WST.EL9-I.MAC.Uncollectable.Bill.Expense")/100;
			var adjMACtransitionadjustment = mainSlice.getInputValue("ConEd.WST.EL9-I.MAC.Transition.Adjustment")/100;
			var adjRevenuedecouplingmechanismadjustment = prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Revenue.Decoupling.Adj")/100;
			var adjDeliveryrevenuesurcharge = prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delivery.Revenue.Surcharge")/100;
			
			//var adjPSL18a =  mainSlice.getInputValue("ConEd.WST.EL9-I.Sec18a");

			//adjMAC for 4-I & 9-I don't include adjPSL18a
			var adjMAC = adjMACreconciliation + adjMACuncollectiblebillexpense +  adjMACtransitionadjustment
			+ adjRevenuedecouplingmechanismadjustment + adjDeliveryrevenuesurcharge;
			
			//Energy Subtotal
			var subTotalEnergyChargeBeforeAdjustments = energyMarketsupply + energyMonthlyadjustments
			+ energyDeliverycharge + energySystembenefits + energyRenewableportfoliostandardprogram
			+ energyMerchantfunctioncharge + energyRevenuedecouplingmechanismadjustment + energyDeliveryrevenuesurcharge
			+ energySurchargePslSection18aAssessments;

			var adjustmentFactorMSC = usage * adjMSC;
			var adjustmentFactorMAC = usage * adjMAC;
			var adjustmentFactorTotal = adjustmentFactorMSC + adjustmentFactorMAC;

			var totalCommodityCharge = adjustmentFactorMSC + energyMarketsupply 
			+ energyMerchantfunctioncharge;
			
			var grtCommodityTaxCharge = (totalCommodityCharge * mainSlice.getInputValue("ConEd.WST.EL9-I.GRT.Commodity"))/100;
			
			//var energyCharges = energyMonthlyadjustments + (adjMAC * usage) + (usage * (mainSlice.getInputValue("energyDeliverycharge")/100));

			var totalEnergyDeliveryCharges  = 	energyMonthlyadjustments + energyDeliverycharge 
			+ adjustmentFactorMAC;
			var totalSBCRPSCharge = energySystembenefits + energyRenewableportfoliostandardprogram;
			
			//Meter Charges are not included for submetering billing
			//var energyMetercharges = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("energyMetercharges"));
			//var energyMeterservicecharge = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("energyMeterservicecharge"));
			//var energyMeterdatacharge = prorateUsingThirtyDayPolicy(mainSlice.getInputValue("energyMeterdatacharge"));
			//var energyBillingandpaymentprocessingsingleservice = mainSlice.getInputValue("energyBillingandpaymentprocessingsingleservice");
			
			//Energy Totals Final
			var totalTDCharge = energyMonthlyadjustments
			+ energyDeliverycharge + energySystembenefits + energyRenewableportfoliostandardprogram
			+ energySurchargePslSection18aAssessments + adjustmentFactorMAC + adjRevenuedecouplingmechanismadjustment;
			var grtTDTaxCharge = totalTDCharge * mainSlice.getInputValue("ConEd.WST.EL9-I.GRT.Delivery")/100;
			
			var totalEnergyFuelUsageBeforeTax = totalCommodityCharge + totalTDCharge;
			var subTotalEnergyCharge = totalEnergyFuelUsageBeforeTax + grtCommodityTaxCharge + grtTDTaxCharge;


			//Demand value in Tier
			var demand5kW = IIF(demand < 5,demand, 5);
			var demandNext95kW = IIF(demand5kW >= 5 ,demand - 5, 95);
			var demandOver100kW = IIF(demandNext95kW >= 95 ,demand - 100, 0);
			
			//Demand Market Supply Charges (Tier Pricing)
			var demandMarketSupply5kW = demand5kW * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Market.Supply.Dmd");
			var demandMarketSupplyNext95kW = demandNext95kW * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Market.Supply.Dmd");
			var demandMarketSupplyOver100kW = demandOver100kW * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Market.Supply.Dmd");

			var demandMarketSupplyTotal = demandMarketSupply5kW + demandMarketSupplyNext95kW + demandMarketSupplyOver100kW;

			//Demand Monthly Adjustments (Tier Pricing)
			var demandMonthlyAdj5kW = demand5kW * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Monthly.Adjustments.Demand");
			var demandMonthlyAdjNext95kW = demandNext95kW * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Monthly.Adjustments.Demand");
			var demandMonthlyAdjOver100kW = demandOver100kW * prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Monthly.Adjustments.Demand");
			
			var demandMonthlyAdjustmentsTotal = demandMonthlyAdj5kW + demandMonthlyAdjNext95kW + demandMonthlyAdjOver100kW;
	
			
			//Demand Delivery Charges (Tier Pricing)			
			var demandDeliveryCharge5kW = demand5kW * 0;//(isSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delv.Dmd.First.5kw.Summer") + notSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Del.Dmd.First.5kw.Non-Summer"));
			var demandDeliveryChargeNext95kW = demandNext95kW * 0;//(isSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delv.Dmd.Nxt.95kw.Summer") + notSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delv.Dmd.Nxt.95kw.Non-Summer"));
			var demandDeliveryChargeOver100kW = demandOver100kW * 0;//(isSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delv.Dmd.Over.100kw.Summer") + notSummer*prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delv.Dmd.Over.100kw.Non-Summer"));
			
			//var demandDeliveryCharge5kW = demand5kW * (prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Deliv.Chrg.Dmd.1st.5kw.Summer") + prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Deliv.Chrg.Dmd.1st.5kw.Non-Summer"));
			//var demandDeliveryChargeNext95kW = demandNext95kW * (prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Deliv.Chrg.Dmd.nxt.95kw.Summer") + prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Deliv.Chrg.Dmd.nxt.95kw.Non-Summer"));
			//var demandDeliveryChargeOver100kW = demandOver100kW * (prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Deliv.Chrg.Dmd.over100kw.Summer") + prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Deliv.Chrg.Dmd.over100kw.Non-Summe"));

			var demandDeliveryChargeTotal = demandDeliveryCharge5kW + demandDeliveryChargeNext95kW + demandDeliveryChargeOver100kW;
			
			//Demand Totals
			var subTotalLowTensionDemandChargeSupply = demandMarketSupplyTotal;
			var subTotalLowTensionDemandChargeDelivery = demandMonthlyAdjustmentsTotal + demandDeliveryChargeTotal;
			
			var subTotalLowTensionDemandCharge	= subTotalLowTensionDemandChargeSupply + subTotalLowTensionDemandChargeDelivery;		
			

			//Pro-Rated Demand Totals
			var proratedTotalDemandCharge = prorateUsingThirtyDayPolicy(subTotalLowTensionDemandCharge);//proratedTotalDemandDeliveryCharge + proratedTotalDemandSupplyCharge;
			var proratedTotalDemandSupplyCharge = prorateUsingThirtyDayPolicy(subTotalLowTensionDemandChargeSupply);
			var proratedTotalDemandDeliveryCharge = prorateUsingThirtyDayPolicy(subTotalLowTensionDemandChargeDelivery);
			
			var demandGRTCommodityTaxCharge = proratedTotalDemandSupplyCharge * mainSlice.getInputValue("ConEd.WST.EL9-I.GRT.Commodity")/100 ;
			var demandGRTTDTaxCharge = proratedTotalDemandCharge * mainSlice.getInputValue("ConEd.WST.EL9-I.GRT.Delivery")/100;
			var totalDemandSupplyChargeWithGRTTax = proratedTotalDemandSupplyCharge + demandGRTCommodityTaxCharge;
			var totalDemandDeliveryChargeWithGRTTax = proratedTotalDemandDeliveryCharge + demandGRTTDTaxCharge;
			
			var subTotalDemandCharge = totalDemandSupplyChargeWithGRTTax + totalDemandDeliveryChargeWithGRTTax;
			
			var totalDeliveryCharges = totalTDCharge + proratedTotalDemandCharge;
			var grossReceiptsTaxDelivery = grtTDTaxCharge + demandGRTTDTaxCharge;
			var finalSubTotalDelivery = totalDeliveryCharges + grossReceiptsTaxDelivery;
			//var salesTaxDelivery = mainSlice.getinputvalue("ConEd.WST.EL9-I.Sales.Tax") * finalSubTotalDelivery/100;
			//var grandTotalDelivery = finalSubTotalDelivery + salesTaxDelivery;

			var totalSupplyCharges = totalCommodityCharge + proratedTotalDemandSupplyCharge;
			var grossReceiptsTaxSupply = grtCommodityTaxCharge + demandGRTCommodityTaxCharge;
			var finalSubTotalSupply = totalSupplyCharges + grossReceiptsTaxSupply;
			//var salesTaxSupply = mainSlice.getinputvalue("ConEd.WST.EL9-I.Sales.Tax") * finalSubTotalSupply/100;
			//var grandTotalSupply = finalSubTotalSupply + salesTaxSupply;
			
			var totalBillingCharges = finalSubTotalDelivery + finalSubTotalSupply;
		
			//result.addEnergyOutput("A Test", "Market.Supply.Energy", mainSlice.getinputvalue(energyMarketSupplyInputValue));
			//ALL INPUT VALUES
			result.addEnergyOutput("A Test", energyMarketSupplyInputValue, prorateInputValueUsingPortionOfTotalDaysPolicy(energyMarketSupplyInputValue)/100);
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Monthly.Adjustments", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Monthly.Adjustments"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Delivery.Charge.Energy", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delivery.Charge.Energy"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.System.Benefits", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.System.Benefits"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Renewable.Portfolio.Standard.Prog", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Renewable.Portfolio.Standard.Prog"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Merchant.Function.Charge", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Merchant.Function.Charge"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Revenue.Decoupling.Adj", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Revenue.Decoupling.Adj"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Delivery.Revenue.Surcharge", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delivery.Revenue.Surcharge"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Collect.PSL.Sec18a", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Collect.PSL.Sec18a"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Billable.Reactive.Power.Demand", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Billable.Reactive.Power.Demand"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.MSC-I.Adjustment", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.MSC-I.Adjustment"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.MSC-II.Adjustment", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.MSC-II.Adjustment"));
			
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.MAC.Reconciliation.Adjustment", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.MAC.Reconciliation.Adjustment"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.MAC.Uncollectable.Bill.Expense", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.MAC.Uncollectable.Bill.Expense"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.MAC.Transition.Adjustment", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.MAC.Transition.Adjustment"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Revenue.Decoupling.Adj", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Revenue.Decoupling.Adj"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Delivery.Revenue.Surcharge", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delivery.Revenue.Surcharge"));
			
			//TODO: see how to select the right tax for the:
			//ConEd.WST.EL9-I.GRT.Commodity.All.Towns.and.City.B
			//ConEd.WST.EL9-I.GRT.Commodity.Rye
			//ConEd.WST.EL9-I.GRT.Commodity.Tax.All.Other.Villag
			//ConEd.WST.EL9-I.GRT.Commodity.Tax.Mt.Vernon
			//ConEd.WST.EL9-I.GRT.Commodity.Tax.New.Rochelle
			//ConEd.WST.EL9-I.GRT.Commodity.Tax.Peekskill
			//ConEd.WST.EL9-I.GRT.Commodity.Tax.White.Plains
			//ConEd.WST.EL9-I.GRT.Commodity.Tax.Yonkers
			//ConEd.WST.EL9-I.GRT.Delivery.All.Towns.and.City.Bu
			//ConEd.WST.EL9-I.GRT.Delivery.Rye
			//ConEd.WST.EL9-I.GRT.Delivery.Tax.All.Other.Village
			//ConEd.WST.EL9-I.GRT.Delivery.Tax.Mt.Vernon
			//ConEd.WST.EL9-I.GRT.Delivery.Tax.New.Rochelle
			//ConEd.WST.EL9-I.GRT.Delivery.Tax.Peekskill
			//ConEd.WST.EL9-I.GRT.Delivery.Tax.White.Plains
			//ConEd.WST.EL9-I.GRT.Delivery.Tax.Yonkers
			
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.GRT.Commodity", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.GRT.Commodity.Tax.Mt.Vernon"));			
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.GRT.Delivery", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.GRT.Delivery.Tax.Mt.Vernon"));
			
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Market.Supply.Dmd", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Market.Supply.Dmd"));
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Monthly.Adjustments.Demand", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Monthly.Adjustments.Demand"));
			
			//result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Delv.Dmd.First.5kw.Summer", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delv.Dmd.First.5kw.Summer"));
			//result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Del.Dmd.First.5kw.Non-Summer", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Del.Dmd.First.5kw.Non-Summer"));
			//result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Delv.Dmd.Nxt.95kw.Summer", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delv.Dmd.Nxt.95kw.Summer"));
			//result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Delv.Dmd.Nxt.95kw.Non-Summer", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delv.Dmd.Nxt.95kw.Non-Summer"));
			//result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Delv.Dmd.Over.100kw.Summer", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delv.Dmd.Over.100kw.Summer"));
			//result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.Delv.Dmd.Over.100kw.Non-Summer", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delv.Dmd.Over.100kw.Non-Summer"));
		
			/*//INPUT VALUES
			result.addEnergyOutput("A Test", "Market.Supply.Energy", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Market.Supply.Energy")/100);
			result.addEnergyOutput("A Test", "Monthly.Adjustments", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Monthly.Adjustments")/100);
			result.addEnergyOutput("A Test", "Delivery.Charge.Energy", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delivery.Charge.Energy")/100);
			result.addEnergyOutput("A Test", "System.Benefits", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.System.Benefits")/100);
			result.addEnergyOutput("A Test", "Renewable.Portfolio.Standard.Prog", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Renewable.Portfolio.Standard.Prog")/100);
			result.addEnergyOutput("A Test", "Merchant.Function.Charge", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Merchant.Function.Charge")/100);
			result.addEnergyOutput("A Test", "Revenue.Decoupling.Adj", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Revenue.Decoupling.Adj")/100);
			result.addEnergyOutput("A Test", "Delivery.Revenue.Surcharge", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Delivery.Revenue.Surcharge")/100);
			result.addEnergyOutput("A Test", "PSL.Sec18", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Collect.PSL.Sec18a")/100);
			result.addEnergyOutput("A Test", "Billable.Reactive.Power.Demand", prorateInputValueUsingPortionOfTotalDaysPolicy("ConEd.WST.EL9-I.Billable.Reactive.Power.Demand")/100);
			*/
			/*
			result.addEnergyOutput("A Test", "Market Supply", energyMarketsupply);
			result.addEnergyOutput("A Test", "Monthly Adjustments", energyMonthlyadjustments);
			result.addEnergyOutput("A Test", "Delivery Charge", energyDeliverycharge);
			result.addEnergyOutput("A Test", "System Benefits", energySystembenefits);
			result.addEnergyOutput("A Test", "Renewable portfolio", energyRenewableportfoliostandardprogram);
			result.addEnergyOutput("A Test", "Merchant function charge", energyMerchantfunctioncharge);
			result.addEnergyOutput("A Test", "Revenue Decoupling", energyRevenuedecouplingmechanismadjustment);
			result.addEnergyOutput("A Test", "Delivery Revenue", energyDeliveryrevenuesurcharge);
			result.addEnergyOutput("A Test", "Surcharge PSL 18", energySurchargePslSection18aAssessments);
			result.addEnergyOutput("A Test", "Billable Reactive power", energyBillableReactivePowerChargesDemand);
			*/
			/*
			result.addEnergyOutput("A Test", "MSCi", adjMSCi);
			result.addEnergyOutput("A Test", "MSCii", adjMSCii);
			result.addEnergyOutput("A Test", "MSC", adjMSC);
			
			result.addEnergyOutput("A Test", "adjMACreconciliation", adjMACreconciliation);
			result.addEnergyOutput("A Test", "adjMACuncollectiblebillexpense", adjMACuncollectiblebillexpense);
			result.addEnergyOutput("A Test", "adjMACtransitionadjustment", adjMACtransitionadjustment);
			result.addEnergyOutput("A Test", "adjRevenuedecouplingmechanismadjustment", adjRevenuedecouplingmechanismadjustment);
			result.addEnergyOutput("A Test", "adjDeliveryrevenuesurcharge", adjDeliveryrevenuesurcharge);
			result.addEnergyOutput("A Test", "MAC", adjMAC);
			
			result.addEnergyOutput("A Test", "adjustmentFactorMSC", adjustmentFactorMSC);
			result.addEnergyOutput("A Test", "adjustmentFactorMAC", adjustmentFactorMAC);
			result.addEnergyOutput("A Test", "adjustmentFactorTotal", adjustmentFactorTotal);
			*/
			/*
			result.addEnergyOutput("A Test", "totalCommodityCharge", totalCommodityCharge);
			result.addEnergyOutput("A Test", "grtCommodityTaxCharge", grtCommodityTaxCharge);
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.GRT.Commodity", mainSlice.getInputValue("ConEd.WST.EL9-I.GRT.Commodity"));
			result.addEnergyOutput("A Test", "totalEnergyDeliveryCharges", totalEnergyDeliveryCharges);
			result.addEnergyOutput("A Test", "totalSBCRPSCharge", totalSBCRPSCharge);
			
			result.addEnergyOutput("A Test", "totalTDCharge", totalTDCharge);
			result.addEnergyOutput("A Test", "grtTDTaxCharge", grtTDTaxCharge);
			result.addEnergyOutput("A Test", "ConEd.WST.EL9-I.GRT.Delivery", mainSlice.getInputValue("ConEd.WST.EL9-I.GRT.Delivery")/100);
			result.addEnergyOutput("A Test", "totalEnergyFuelUsageBeforeTax", totalEnergyFuelUsageBeforeTax);
			result.addEnergyOutput("A Test", "subTotalEnergyCharge", subTotalEnergyCharge);
			*/
			/*
			result.addEnergyOutput("A Test", "totalDeliveryCharges", totalDeliveryCharges);
			result.addEnergyOutput("A Test", "grossReceiptsTaxDelivery", grossReceiptsTaxDelivery);
			
			result.addEnergyOutput("A Test", "totalSupplyCharges", totalSupplyCharges);
			result.addEnergyOutput("A Test", "grossReceiptsTaxSupply", grossReceiptsTaxSupply);
			
			result.addEnergyOutput("A Test", "finalSubTotalDelivery", finalSubTotalDelivery);
			result.addEnergyOutput("A Test", "finalSubTotalSupply", finalSubTotalSupply);
			*/
		
			//Set Calculator Grand Total
			result.setTotal(totalBillingCharges);
			/*
			result.addOutput("Usage Info","Number of Slices",numslices,"string");
			result.addOutput("Usage Info","Consumption",usage,"string");
			result.addOutput("Usage Info","Slice1 Billing Period",this.timeslices[1].periodStart & ' to ' & this.timeslices[1].periodEnd,"date");
			result.addOutput("Usage Info","Slice2 Billing Period",this.timeslices[2].periodStart & ' to ' & this.timeslices[2].periodEnd,"date");
			result.addOutput("Usage Info","Slice3 Billing Period",this.timeslices[3].periodStart & ' to ' & this.timeslices[3].periodEnd,"date");
			result.addOutput("Usage Info","energyUsage UID",mainSlice.energyUsage.energy_usage_uid,"string");
			result.setTotal(0); */
		</cfscript>

		<cfreturn result>
	</cffunction>

</cfcomponent>