<cfcomponent extends="com.mce.rate.calculator.impl.BaseModelCalculator"
			 implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<cffunction name="getRequiredInputCodes" returntype="string" access="package">
    <cfreturn "">
	</cffunction>
	
	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<!--- Valid answers are "all", "first", "none" --->
	<cffunction name="getSlicesThatNeedIntervalData" access="package" returntype="string">
		<cfreturn "none">
	</cffunction>


	<!--- Perform actual calculations... all retrieved data is available via "this.data" structure --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfscript>
			// Call supermethod to get blank "CalculatorResult" object
			var result = super.calculate();
			var numslices = ArrayLen(this.timeslices); //includes full period slice
			var mainSlice = this.timeslices[1]; // 1 is the full billing period slice

		// Input & Calculations

			//Usage Info
			var period_start = mainSlice.periodStart;
			var period_end = mainSlice.periodEnd;
			var usage = this.params.usage; 
			var property = this.params.property;
			var consumption = mainSlice.energyPropertyUsage["Consumption"][mainSlice.energyPropertyUsage.currentRow];					
			//(Total Electricity Charges $ - (Supply Sales tax $ + Delivery Sales tax $))/Consumption kWh
					
			//Total Cost
			var propertyAvgBillingCharges = IIF(consumption eq 0,0, (mainSlice.energyPropertyUsage["TotalElectricityCharges"][mainSlice.energyPropertyUsage.currentRow] - (mainSlice.energyPropertyUsage["Salestax_Supply"][mainSlice.energyPropertyUsage.currentRow] + mainSlice.energyPropertyUsage["Salestax_Delivery"][mainSlice.energyPropertyUsage.currentRow]))/consumption);
			var TotalBillingCharges = propertyAvgBillingCharges * usage;
			
			
		// Results Output
			
			result.setTotal(totalBillingCharges);
			
		</cfscript>

		<cffile 
   action = "append" 
    file = "D:\cfdata\mceRateModelEngine\Log.txt"
    output = "#totalBillingCharges# #consumption#">
	
		<cfreturn result>
	</cffunction>

</cfcomponent>