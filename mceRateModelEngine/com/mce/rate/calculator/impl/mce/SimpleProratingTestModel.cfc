<cfcomponent
	extends="com.mce.rate.calculator.impl.BaseSimpleThirtyDayProratingModelCalculator"
	implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">

	<!--- Used internally by BaseModelCalculator to determine what data to load; subclasses can answer differently as appropriate --->
	<cffunction name="getRequiredInputCodes" returntype="string" access="package">
		<cfreturn "demandCostRatePerKw">
	</cffunction>

	<!--- Perform actual calculations... all retrieved data is available via "this.data" structure --->
	<cffunction name="calculate" access="public" returntype="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">
		<cfscript>
			// Call supermethod to get blank "CalculatorResult" object
			var result = super.calculate();

			// Example of how to call the pro-rating
			var proratedDemandCostRatePerKw = prorateInputValueUsingPortionOfTotalDaysPolicy("demandCostRatePerKw");

			var proratedSomeOtherValue = prorateUsingThirtyDayPolicy(1000);

			result.addMoneyOutput("Rates", "Pro-rated demand cost, per kW", proratedDemandCostRatePerKw);
			result.addMoneyOutput("Rates", "Some other pro-rated value (#this.timeslices[1].getInputName('demandCostRatePerKw')#)", proratedSomeOtherValue);
		</cfscript>

		<cfreturn result>
	</cffunction>

</cfcomponent>