<cfcomponent output="false"
	implements="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult">

	<cfset this._total = 0>
	<cfset this._output = QueryNew("category,label,value,format,count,meta_type_lookup_code")>
	<cfset this._inputData = StructNew()>
	<cfset this._outputMap = StructNew()>

	<cffunction name="setTotal" access="public" returntype="void">
		<cfargument name="total" type="numeric" required="true">
		<cfset this._total = total>
	</cffunction>

	<cffunction name="getTotal" access="public" returntype="numeric">
		<cfreturn this._total>
	</cffunction>

	<cffunction name="getOutput" access="public" returntype="query">
		<cfreturn this._output>
	</cffunction>

	<cffunction name="addMoneyOutput" access="public" returntype="void">
		<cfargument name="category" type="string" required="true">
		<cfargument name="label" type="string" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfargument name="meta_type_lookup_code" type="string" required="false" default="">
		<cfset addOutput(category, label, value, "money", meta_type_lookup_code)>
	</cffunction>

	<cffunction name="addEnergyOutput" access="public" returntype="void">
		<cfargument name="category" type="string" required="true">
		<cfargument name="label" type="string" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfargument name="meta_type_lookup_code" type="string" required="false" default="">
		<cfset addOutput(category, label, value, "energy", meta_type_lookup_code)>
	</cffunction>

	<cffunction name="addFactorOutput" access="public" returntype="void">
		<cfargument name="category" type="string" required="true">
		<cfargument name="label" type="string" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfargument name="meta_type_lookup_code" type="string" required="false" default="">
		<cfset addOutput(category, label, value, "factor", meta_type_lookup_code)>
	</cffunction>

	<cffunction name="addNumberOutput" access="public" returntype="void">
		<cfargument name="category" type="string" required="true">
		<cfargument name="label" type="string" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfargument name="numberFormatMask" type="string" required="true">
		<cfargument name="meta_type_lookup_code" type="string" required="false" default="">
		<cfset addOutput(category, label, value, "number", meta_type_lookup_code)>
	</cffunction>

	<cffunction name="addOutput" access="public" returntype="void">
		<cfargument name="category" type="string" required="true">
		<cfargument name="label" type="string" required="true">
		<cfargument name="value" type="string" required="true">
		<cfargument name="format" type="string" required="true">
		<cfargument name="meta_type_lookup_code" type="string" required="false" default="">

		<cfset var row = 0>
		<cfset var key = "#category#_#label#_#meta_type_lookup_code#">

		<cfif structKeyExists(this._outputMap, key)>
			<cfset row = this._outputMap[key]>
			<cfset this._output.value[row] = this._output.value[row] + value>
			<cfset this._output.count[row] = this._output.count[row] + 1>
		<cfelse>
			<cfset row = QueryAddRow(this._output)>
			<cfset QuerySetCell(this._output, "category", arguments.category)>
			<cfset QuerySetCell(this._output, "label", arguments.label)>
			<cfset QuerySetCell(this._output, "value", arguments.value)>
			<cfset QuerySetCell(this._output, "format", arguments.format)>
			<cfset QuerySetCell(this._output, "meta_type_lookup_code", arguments.meta_type_lookup_code)>
			<cfset QuerySetCell(this._output, "count", 1)>
			<cfset this._outputMap[key] = row>
		</cfif>

	</cffunction>


	<cffunction name="merge" access="public">
		<cfargument name="that" type="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult" required="true">
		<cfset mergeOutput(that)>
	</cffunction>


	<cffunction name="mergeOutput" access="private">
		<cfargument name="that" type="mceRateModelEngine.com.mce.rate.calculator.ICalculatorResult" required="true">

		<cfset var qThat = that.getOutput()>
		<cfloop query="qThat">
			<cfset this.addOutput(qThat.category, qThat.label, qThat.value, qThat.format)>
		</cfloop>
	</cffunction>



	<cffunction name="getInputData" access="public" returntype="struct">
		<cfreturn this._inputData>
	</cffunction>

	<cffunction name="setInputData" access="public" returntype="void">
		<cfargument name="data" type="struct" required="true">
		<cfset this._inputData = data>
	</cffunction>

	<cffunction name="toSerializableForm" access="public" returntype="struct">
		<cfargument name="intervalDataMaxRows" type="numeric" required="false" default="-1" hint="Optionally gates the amount of interval data returned. Does not affect internal calculations. The default value of -1 will result in all rows being returned.">

		<cfset var result = StructNew()>
		<cfset result.total = getTotal()>
		<cfset result.output = getOutput()>
		<cfset result.inputData = getInputData()>

		<cfif intervalDataMaxRows gt -1 and IsDefined("result.inputData.energyIntervals")>
			<cfset result.inputData.energyIntervals = Application.utils.QueryUtils.queryLimitRows(result.inputData.energyIntervals, intervalDataMaxRows)>
		</cfif>

		<cfreturn result>
	</cffunction>

</cfcomponent>