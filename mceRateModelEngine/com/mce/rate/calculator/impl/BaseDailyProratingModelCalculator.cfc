<cfcomponent
	extends="com.mce.rate.calculator.impl.BaseModelCalculator"
	implements="mceRateModelEngine.com.mce.rate.calculator.IModelCalculator">

	<cffunction name="getTimesliceAdapter" access="public" returntype="mceRateModelEngine.com.mce.rate.timeslice.ITimesliceAdapter">
		<cfreturn CreateObject("component", "mceRateModelEngine.com.mce.rate.timeslice.DailyTimesliceAdapter")>
	</cffunction>

	<cffunction name="prorateInputValueUsingPortionOfTotalDaysPolicy" returntype="numeric">
		<cfargument name="lookupCode" type="string" required="true">

		<cfset var i = "">
		<cfset var myCode = "">
		<cfset var stopit = "">
		<cfset var slice = "">
		<cfset var result = 0>
		<cfset var totalNumberOfDays = this.timeslices[1].numberOfDays>
		
<cfif lookupCode eq "energyRevenuedecouplingmechanismadjustment">
	<cfset myCode = lookupCode>
</cfif>
		<!--- For however many timeslices there are, not including the "main" slice --->
		<!--- (we are starting at position 2 to skip that first slice which in CF is at position 1) --->
<!--- 		<cfloop from="2" to="#ArrayLen(this.timeslices)#" index="i"> --->
		<cfloop from="2" to="#ArrayLen(this.timeslices)#" index="i">			
			<!--- Here is the value of the input as of this timeslice --->
			<cfset slice = this.timeslices[i]>
	<cfif myCode eq lookupCode>
		<cfif i eq 28>
			<cfset stopit = "1" >
			</cfif>
	</cfif>
			<!--- This style of prorating always divides each partial calendar month's value by the total number of days --->
			<cfset result = result + (slice.getInputValue(lookupCode) * (slice.numberOfDays / totalNumberOfDays))>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="prorateUsingThirtyDayPolicy">
		<cfargument name="val" type="numeric" required="true">
		<cfreturn val * (this.timeslices[1].numberOfDays / 30)>
	</cffunction>

</cfcomponent>