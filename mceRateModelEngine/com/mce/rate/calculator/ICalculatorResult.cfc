<cfinterface>
	<cffunction name="setTotal" access="public" returntype="void">
		<cfargument name="total" type="numeric" required="true">
	</cffunction>

	<cffunction name="getTotal" access="public" returntype="numeric">
	</cffunction>

	<cffunction name="getOutput" access="public" returntype="query">
	</cffunction>

	<cffunction name="addMoneyOutput" access="public" returntype="void">
		<cfargument name="category" type="string" required="true">
		<cfargument name="label" type="string" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfargument name="meta_type_lookup_code" type="string" required="false" default="">
	</cffunction>

	<cffunction name="addEnergyOutput" access="public" returntype="void">
		<cfargument name="category" type="string" required="true">
		<cfargument name="label" type="string" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfargument name="meta_type_lookup_code" type="string" required="false" default="">
	</cffunction>

	<cffunction name="addNumberOutput" access="public" returntype="void">
		<cfargument name="category" type="string" required="true">
		<cfargument name="label" type="string" required="true">
		<cfargument name="value" type="numeric" required="true">
		<cfargument name="numberFormatMask" type="string" required="true">
		<cfargument name="meta_type_lookup_code" type="string" required="false" default="">
	</cffunction>

	<cffunction name="addOutput" access="public" returntype="void">
		<cfargument name="category" type="string" required="true">
		<cfargument name="label" type="string" required="true">
		<cfargument name="value" type="string" required="true">
		<cfargument name="format" type="string" required="true">
		<cfargument name="meta_type_lookup_code" type="string" required="false" default="">
	</cffunction>

	<cffunction name="getInputData" access="public" returntype="struct">
	</cffunction>

	<cffunction name="setInputData" access="public" returntype="void">
		<cfargument name="data" type="struct" required="true">
	</cffunction>

</cfinterface>