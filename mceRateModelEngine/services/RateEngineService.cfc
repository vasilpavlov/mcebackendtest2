<cfcomponent displayname="Rate Engine Service"
			 hint="This component is used to expose the rate engine service." output="false">

	<!--- Runs a model, based on a scalar arguments from the wild --->
	<cffunction name="run" returntype="struct" access="remote"
				hint="This method is used to run / execute a rate model lookup using the rate engine service.">
		<cfargument name="modelLookupCode" type="string" required="true" hint="Describes the model lookup code for the rate model being leveraged.">
		<cfargument name="accountId" type="string" required="true" hint="Describes the referenced account identifier for a given rate model lookup.">
		<cfargument name="periodStart" type="string" required="true" hint="Describes the start period / date that will be used by the rate model lookup.">
		<cfargument name="periodEnd" type="string" required="true" hint="Describes the end period / date that will be used by the rate model lookup.">
		<cfargument name="intervalAccountIdentifier" type="string" required="false" default="#accountId#" hint="Describes the primary key of the interval account identifier.">
		<cfargument name="intervalPeriodStart" type="string" required="false" default="#periodStart#" hint="Describes the start period / date that will be used by the rate model lookup.">
		<cfargument name="intervalPeriodEnd" type="string" required="false" default="#periodEnd#" hint="Describes the end period / date that will be used by the rate model lookup.">
		<cfargument name="intervalDataMaxRows" type="numeric" required="false" default="100" hint="Describes the total number of maxiumum data rows that will be returned by the rate model lookup results.">
		<cfargument name="inputValueOverrides" type="struct" required="false" default="#StructNew()#" hint="Describes the overrides for any input values that are processed.">

		<!--- Local variables --->
		<cfset var requestArgs = "">
		<cfset var modelResponse = "">

		<!--- Massage the incoming arguments into standard "RequestArguments" object for the model engine --->
		<cfif Find("Submetering", "#modelLookupCode#") gt 0>
			<cfinvoke
				component="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments"
				method="init"
				returnVariable="requestArgs"

				modelLookupCode="#modelLookupCode#"
				accountIdentifier="#accountId#"
				periodStart="#periodStart#"
				periodEnd="#periodEnd#"
				intervalAccountIdentifier="#intervalAccountIdentifier#"
				intervalPeriodStart="#parseDateTime(intervalPeriodStart)#"
				intervalPeriodEnd="#parseDateTime(intervalPeriodEnd)#"
				inputValueOverrides="#inputValueOverrides#"
				
				property="#inputValueOverrides.mainSliceData.property#"
				usage="#inputValueOverrides.mainSliceData.usage#"
				demand="#inputValueOverrides.mainSliceData.demand#">
		<cfelse>		
			<cfinvoke
				component="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments"
				method="init"
				returnVariable="requestArgs"

				modelLookupCode="#modelLookupCode#"
				accountIdentifier="#accountId#"
				periodStart="#periodStart#"
				periodEnd="#periodEnd#"
				intervalAccountIdentifier="#intervalAccountIdentifier#"
				intervalPeriodStart="#parseDateTime(intervalPeriodStart)#"
				intervalPeriodEnd="#parseDateTime(intervalPeriodEnd)#"
				inputValueOverrides="#inputValueOverrides#">
		</cfif>

		<!--- Run the model --->
		<cfset modelResponse = runModel(requestArgs)>

		<!--- Return the model in a simple serialized form --->
		<cfreturn modelResponse.toSerializableForm(intervalDataMaxRows)>
	</cffunction>

	<!--- Runs a model (for Submetering)--->
	<cffunction name="runSubmeteringModel" returntype="any" access="remote"
				hint="This method is used to run / execute a rate model lookup using the rate engine service.">
		<cfargument name="modelLookupCode" type="string" required="true" hint="Describes the model lookup code for the rate model being leveraged.">
		<cfargument name="accountId" type="string" required="true" hint="Describes the referenced account identifier for a given rate model lookup.">
		<cfargument name="periodStart" type="string" required="true" hint="Describes the start period / date that will be used by the rate model lookup.">
		<cfargument name="periodEnd" type="string" required="true" hint="Describes the end period / date that will be used by the rate model lookup.">
		<cfargument name="intervalDataMaxRows" type="numeric" required="false" default="100" hint="Describes the total number of maxiumum data rows that will be returned by the rate model lookup results.">
		
		<cfargument name="property" type="string" required="false" hint="Property friendly name.">
        <cfargument name="usage" type="numeric" required="false" default="1" hint="Describes the overrides for any input values that are processed.">
        <cfargument name="demand" type="numeric" required="false" default="1" hint="Describes the overrides for any input values that are processed.">
        <cfargument name="tripCode" type="numeric" required="false" default="-1" hint="Describes the overrides for any input values that are processed.">

		<!--- Local variables --->
		<cfset var requestArgs = "">
		<cfset var modelResponse = "">

		<!--- Massage the incoming arguments into standard "RequestArguments" object for the model engine --->
		<cfinvoke
			component="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments"
			method="init"
			returnVariable="requestArgs"

			modelLookupCode="#modelLookupCode#"
			accountIdentifier="#accountId#"
			periodStart="#periodStart#"
			periodEnd="#periodEnd#"
           
			property="#property#"
            usage="#usage#"
            demand="#demand#"
            tripCode="#tripCode#">

		<!--- Run the model --->
		<cfset modelResponse = runModel(requestArgs)>

		<!--- Return the model in a simple serialized form --->
		<cfreturn modelResponse.toSerializableForm(intervalDataMaxRows).result.total>
		
	</cffunction>
	
	<!--- Simplified version of run() with optional arguments omitted --->
	<cffunction name="runSimple" returntype="Any" access="remote"
				hint="This method is used to run / execute a simple rate model lookup using the rate engine service (only returns a total). ">
		<cfargument name="modelLookupCode" type="string" required="true" hint="Describes the model lookup code for the rate model being leveraged.">
		<cfargument name="accountId" type="string" required="true" hint="Describes the referenced account identifier for a given rate model lookup.">
		<cfargument name="periodStart" type="string" required="true" hint="Describes the start period / date that will be used by the rate model lookup.">
		<cfargument name="periodEnd" type="string" required="true" hint="Describes the end period / date that will be used by the rate model lookup.">

		<!--- Return the total calculated by the model --->
		<cfreturn run(argumentCollection=arguments).result.total>
	</cffunction>


	<cffunction name="runBasic" returntype="struct" access="remote"
				hint="This method is used to run / execute a simple rate model lookup using the rate engine service (only returns a total). ">
		<cfargument name="modelLookupCode" type="string" required="true" hint="Describes the model lookup code for the rate model being leveraged.">
		<cfargument name="accountId" type="string" required="true" hint="Describes the referenced account identifier for a given rate model lookup.">
		<cfargument name="periodStart" type="string" required="true" hint="Describes the start period / date that will be used by the rate model lookup.">
		<cfargument name="periodEnd" type="string" required="true" hint="Describes the end period / date that will be used by the rate model lookup.">

		<!--- Return the total calculated by the model --->
		<cfreturn run(argumentCollection=arguments)>
	</cffunction>



	<!--- Runs a model, based on a RateModelRequestArguments object --->
	<cffunction name="runModel" returntype="mceRateModelEngine.com.mce.rate.engine.RateModelResponse" access="remote"
				hint="This method is used to run / execute a rate model lookup using the rate engine service.">
		<cfargument name="requestArgs" type="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments" required="true">

		<!--- Local variables --->
		<cfset var response = "">

		<!--- Run the actual model --->
		<cfinvoke
			method="runModel"
			component="com.mce.rate.RateModelRunner"
			requestArgs="#requestArgs#"
			returnVariable="response">

		<cfreturn response>
	</cffunction>


	<!--- Returns a listing of current rate models --->
	<cffunction name="getKnownModels" access="remote" returntype="query"
				hint="This method is used to return all the available /known / registered rate modelsfrom the rate engine service.">
		<cfreturn CreateObject("component", "com.mce.db.RateModelDelegate").getModels()>
	</cffunction>

	<!--- Returns a listing of accounts for a given property --->
	<cffunction name="getAccountsForProperty" access="remote" returntype="query"
				hint="This method is used to return all the accounts associated to a given property.">
		<cfargument name="propertyUid" type="string" required="true" hint="Describes the primary key / unique identifier for a given property.">
		<cfreturn CreateObject("component", "com.mce.db.EnergyAccountDelegate").getAccountsForProperty(#propertyUid#)>
	</cffunction>

	<!--- Returns a listing of properties --->
	<cffunction name="getAllProperties" access="remote" returntype="query"
				hint="This method is used to return all the properties registered with the a given energy account.">
		<cfreturn CreateObject("component", "com.mce.db.EnergyAccountDelegate").getAllProperties()>
	</cffunction>
	
	<!--- Returns a trip code for a given property as for given date--->
	<cffunction name="getTripCode" access="remote" returntype="numeric"
				hint="This method is used to return trip code for a given property for a given date.">
		<cfargument name="propertyUid" type="string" required="true" hint="Describes the primary key / unique identifier for a given property.">
		<cfargument name="forDate" type="string" required="true" hint="Describes the date we need info for">
		<cfset var startDate = DateFormat(arguments.forDate, "mm/dd/yyyy")>
		<cfreturn #CreateObject("component", "com.mce.db.PropertyMetaDelegate").getTripCodeForProperty(#propertyUid#, #startDate#)#>		
	</cffunction>
	
	<!--- Returns a contract type for a given energy account as for given date--->
	<cffunction name="getContractType" access="remote" returntype="string"
				hint="This method is used to return all the accounts associated to a given property.">
		<cfargument name="accountUid" type="string" required="true" hint="Describes the primary key / unique identifier for a given property.">
		<cfargument name="forDate" type="string" required="true" hint="Describes the date we need info for">
		<cfset var startDate = DateFormat(arguments.forDate, "mm/dd/yyyy")>
		<cfreturn #CreateObject("component", "com.mce.db.EnergyAccountDelegate").getContractType(#accountUid#, #startDate#)#>
		<!---cfreturn CreateObject("component", "com.mce.db.EnergyAccountDelegate").getAccountsForProperty(#propertyUid#)--->
	</cffunction>

</cfcomponent>