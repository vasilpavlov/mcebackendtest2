<cfcomponent output="false"
			 displayname="Rate Model Application Component."
			 hint="This component is the application controller for the Rate Model Engine.">

	<!---
		Set up the application with the following assumptions:
		- This application is for the rate model engine only
		- Is both stateless and "headless" (no UI) and thus doesn't need session management, etc
	--->
	<cfset this.name = "mceRateModelEngineApplication">
	<cfset this.clientManagement = false>
	<cfset this.sessionManagement = false>
	<cfset this.setClientCookies = false>
	<cfset this.scriptProtect = true>

	<cffunction name="onApplicationStart"
				hint="This method is used to launch / execute any application logic when the application is first started.">
	</cffunction>

	<cffunction name="onRequestStart"
				hint="This method is used to launch / execute any applicaiton logic for the rate model engine that must take place at the beginning of a given request.">
		<cfset instantiateSingletons()>
	</cffunction>

	<cffunction name="instantiateSingletons" access="private"
				hint="This method is used to instantiate singleton classes leveraged by the rate model engine.">
		<cfif isDefined("Application.singletons.RateModelRequestBroker")>
			<cfset Application.singletons.RateModelRequestBroker.stop()>
		</cfif>
		<cfset Application.singletons.RateModelRequestBroker = CreateObject("component", "com.mce.rate.engine.RateModelRequestBroker")>
		<cfset Application.singletons.ModelFactory = CreateObject("component", "com.mce.rate.engine.ModelFactory")>
		<cfset Application.singletons.ClassifierFactory = CreateObject("component", "com.mce.rate.engine.ClassifierFactory")>
		<cfset Application.singletons.DataAccessorRegistry = CreateObject("component", "com.mce.rate.dataaccess.DataAccessorRegistry")>
		<cfset Application.singletons.RequestFactory = CreateObject("component", "com.mce.rate.engine.RequestFactory")>
		<cfset Application.utils.CalcUtils = CreateObject("component", "com.mce.util.CalcUtils")>
		<cfset Application.utils.QueryUtils = CreateObject("component", "com.mce.util.QueryUtils")>
		<cfset Application.utils.StructUtils = CreateObject("component", "com.mce.util.StructUtils")>
		<cfset Application.utils.Logger = CreateObject("component", "com.mce.log.SimpleLogger").init("info")>
	</cffunction>

</cfcomponent>