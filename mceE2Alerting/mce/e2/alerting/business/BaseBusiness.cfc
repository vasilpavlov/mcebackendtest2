<cfcomponent name="Base Business Logic Component"
			 output="true"
			 extends="mce.e2.alerting.common.BaseComponent"
			 hint="This component is the base business logic component for all business *.cfc's.">
						
	<!--- Create the *.cfc constructor --->		
	<cffunction name="init"
			 	access="public" returntype="any"
			 	hint="This is the constructor for the base business component.">
		
		<!--- Return an instance of the component --->		
		<cfreturn this>

	</cffunction>

</cfcomponent>