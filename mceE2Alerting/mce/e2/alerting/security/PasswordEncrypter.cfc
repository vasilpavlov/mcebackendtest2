<cfcomponent output="false">

	<cffunction name="init">
		<cfargument name="defaultAlgorithm" type="string" required="true">
		<cfset this.defaultAlgorithm = arguments.defaultAlgorithm>
		<cfreturn this>
	</cffunction>

	<cffunction name="encryptString" returntype="string">
		<cfargument name="str" type="string" required="true">
		<cfargument name="algorithm" type="string" required="false" default="#this.defaultAlgorithm#">
		<cfreturn hash(str, algorithm)>
	</cffunction>
</cfcomponent>