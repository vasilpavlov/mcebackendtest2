<cfcomponent name="Data Generator"
			 output="true"
			 hint="This component is used to generate test data for the application database.">

	<!--- Initialize this component --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>

	<cffunction name="init"
				access="public"
				hint="This method is used to initialize the data generator.">
				
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<cfscript>
		
			// Initialize the testData structure
			this.testData = structNew();

			// Initialize the seed data structure
			this.seedData = structNew();

			// Initialize the common delegates
			this.alertDeliveryMethodDelegate = request.beanFactory.getBean("alertDeliveryMethodDelegate");
			this.alertIntervalDelegate = request.beanFactory.getBean("alertIntervalDelegate");
			this.alertStatusDelegate = request.beanFactory.getBean("alertStatusDelegate");
			this.alertDelegate = request.beanFactory.getBean("alertDelegate");

			// Initialize all the service components
			this.alertDeliveryMethodService = request.beanFactory.getBean("alertDeliveryMethodService");
			this.alertIntervalService = request.beanFactory.getBean("alertIntervalService");
			this.alertStatusService = request.beanFactory.getBean("alertStatusService");
			this.alertService = request.beanFactory.getBean("alertService");
			
			// Setup all the test data in this *.cfc for creation
			setupAlertDeliveryMethodData();
			setupAlertIntervalData();
			setupAlertStatusData();
			
		</cfscript>
				
	</cffunction>

	<cffunction name="createTestData"
				access="public"
				returnType="void"
				hint="This method is used to populate the application database with test data.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>	
		
			// Purge the test data prior to creating any new test data
			purgeTestData();		
		
			// Call the methods used to create / populate the application database
			// Order matters due to constraints
			createAlertDeliveryMethodData();
			createAlertIntervalData();
			createAlertStatusData();
					
		</cfscript>

	</cffunction>

	<cffunction name="purgeTestData"
				access="public"
				returnType="void"
				hint="This method is used to remove all test data from the application database.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>	
			
			// Call the methods used to create / populate the application database
			// Order matters due to constraints
			purgeAlertDeliveryMethodData();
			purgeAlertIntervalData();
			purgeAlertStatusData();
					
		</cfscript>

	</cffunction>

	<!--- AlertDeliveryMethod Data Methods --->
	<cffunction name="setupAlertDeliveryMethodData"
				access="private"
				hint="This method is used to create the data for testing alertDeliveryMethod records.">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>			
	
			// Initialize the test data company structures
			this.testData.alertDeliveryMethods = structNew();

			// Define the total number of records to create / process
			this.testData.alertDeliveryMethods.rowCount = 4;
			
			// Initialize the new alertDeliveryMethod data set
			this.testData.alertDeliveryMethods.new = queryNew('alert_delivery_method_code,friendly_name,is_active,created_by,created_date,modified_by,modified_date');	
				
			// Add a row to the query (instance #1)
			queryAddRow(this.testData.alertDeliveryMethods.new,this.testData.alertDeliveryMethods.rowCount);				
				
			// Loop over the collection and create the query test rows
			for( local.rowIndex = 1; local.rowIndex lte this.testData.alertDeliveryMethods.rowCount; local.rowIndex = local.rowIndex + 1){

				// Set the cell values for this instance of an alert
				querySetCell(this.testData.alertDeliveryMethods.new,'alert_delivery_method_code', 'AlertDeliveryMethod00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.alertDeliveryMethods.new,'friendly_name', 'Alert 00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.alertDeliveryMethods.new,'is_active', 1, local.rowIndex);
				querySetCell(this.testData.alertDeliveryMethods.new,'created_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.alertDeliveryMethods.new,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.testData.alertDeliveryMethods.new,'modified_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.alertDeliveryMethods.new,'modified_date', createOdbcDateTime(now()), local.rowIndex);

			}

			// Populate the seed alert value
			this.seedData.alertDeliveryMethods.alert_delivery_method_code = this.testData.alertDeliveryMethods.new.alert_delivery_method_code[1];

		</cfscript>
				
	</cffunction>		
		
	<cffunction name="createAlertDeliveryMethodData"
				access="public"
				returntype="void"
				hint="This method is used to seed the application database with test / alertDeliveryMethod data.">	
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Build out the array of alert objects --->
		<cfset local.newAlertDeliveryMethods = this.delegate.queryToVoArray(this.testData.alertDeliveryMethods.new, 'mce.e2.alerting.vo.AlertDeliveryMethod')/>
							
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertDeliveryMethods)#" index="local.arrayIndex">	

			<!--- Save the alertDeliveryMethod to the application database --->
			<cfset this.alertDeliveryMethodService.saveNewAlertDeliveryMethod(local.newAlertDeliveryMethods[local.arrayIndex])/>
			
		</cfloop>		
		
	</cffunction>	

	<cffunction name="purgeAlertDeliveryMethodData"
				access="public"
				hint="This method is used to purge / delete any alertDeliveryMethod data related to the test data being used when alertDeliveryMethod unit tests are exersized.">

		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the alertDeliveryMethod data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.alertDeliveryMethods
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.testData.alertDeliveryMethods.new.friendly_name)#">

					)
		
		</cfquery>
			
	</cffunction>

	<!--- AlertInterval Data Methods --->
	<cffunction name="setupAlertIntervalData"
				access="private"
				hint="This method is used to create the data for testing alertInterval records.">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>			
	
			// Initialize the test data company structures
			this.testData.alertIntervals = structNew();

			// Define the total number of records to create / process
			this.testData.alertIntervals.rowCount = 4;
			
			// Initialize the new alertInterval data set
			this.testData.alertIntervals.new = queryNew('alert_interval_code,friendly_name,is_active,created_by,created_date,modified_by,modified_date');	
				
			// Add a row to the query (instance #1)
			queryAddRow(this.testData.alertIntervals.new,this.testData.alertIntervals.rowCount);				
				
			// Loop over the collection and create the query test rows
			for( local.rowIndex = 1; local.rowIndex lte this.testData.alertIntervals.rowCount; local.rowIndex = local.rowIndex + 1){

				// Set the cell values for this instance of an alert
				querySetCell(this.testData.alertIntervals.new,'alert_interval_code', 'AlertInterval00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.alertIntervals.new,'friendly_name', 'Alert 00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.alertIntervals.new,'is_active', 1, local.rowIndex);
				querySetCell(this.testData.alertIntervals.new,'created_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.alertIntervals.new,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.testData.alertIntervals.new,'modified_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.alertIntervals.new,'modified_date', createOdbcDateTime(now()), local.rowIndex);

			}

			// Populate the seed alert value
			this.seedData.alertIntervals.alert_interval_code = this.testData.alertIntervals.new.alert_interval_code[1];

		</cfscript>
				
	</cffunction>		
		
	<cffunction name="createAlertIntervalData"
				access="public"
				returntype="void"
				hint="This method is used to seed the application database with test / alertInterval data.">	
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Build out the array of alert objects --->
		<cfset local.newAlertIntervals = this.delegate.queryToVoArray(this.testData.alertIntervals.new, 'mce.e2.alerting.vo.AlertInterval')/>
							
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertIntervals)#" index="local.arrayIndex">	

			<!--- Save the alertInterval to the application database --->
			<cfset this.alertIntervalService.saveNewAlertInterval(local.newAlertIntervals[local.arrayIndex])/>
			
		</cfloop>		
		
	</cffunction>	

	<cffunction name="purgeAlertIntervalData"
				access="public"
				hint="This method is used to purge / delete any alertInterval data related to the test data being used when alertInterval unit tests are exersized.">

		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the alertInterval data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.alertIntervals
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.testData.alertIntervals.new.friendly_name)#">

					)
		
		</cfquery>
			
	</cffunction>

	<!--- AlertStatus Data Methods --->
	<cffunction name="setupAlertStatusData"
				access="private"
				hint="This method is used to create the data for testing alertStatus records.">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>			
	
			// Initialize the test data company structures
			this.testData.alertStatuses = structNew();

			// Define the total number of records to create / process
			this.testData.alertStatuses.rowCount = 4;
			
			// Initialize the new alertStatus data set
			this.testData.alertStatuses.new = queryNew('alert_status_code,friendly_name,is_active,created_by,created_date,modified_by,modified_date'');	
				
			// Add a row to the query (instance #1)
			queryAddRow(this.testData.alertStatuses.new,this.testData.alertStatuses.rowCount);				
				
			// Loop over the collection and create the query test rows
			for( local.rowIndex = 1; local.rowIndex lte this.testData.alertStatuses.rowCount; local.rowIndex = local.rowIndex + 1){

				// Set the cell values for this instance of an alert
				querySetCell(this.testData.alertStatuses.new,'alert_status_code', 'AlertStatus00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.alertStatuses.new,'friendly_name', 'Alert 00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.alertStatuses.new,'is_active', 1, local.rowIndex);
				querySetCell(this.testData.alertStatuses.new,'created_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.alertStatuses.new,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.testData.alertStatuses.new,'modified_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.alertStatuses.new,'modified_date', createOdbcDateTime(now()), local.rowIndex);

			}

			// Populate the seed alert value
			this.seedData.alertStatuses.alert_status_code = this.testData.alertStatuses.new.alert_status_code[1];

		</cfscript>
				
	</cffunction>		
		
	<cffunction name="createAlertStatusData"
				access="public"
				returntype="void"
				hint="This method is used to seed the application database with test / alertStatus data.">	
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Build out the array of alert objects --->
		<cfset local.newAlertStatuses = this.delegate.queryToVoArray(this.testData.alertStatuses.new, 'mce.e2.alerting.vo.AlertStatus')/>
							
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertStatuses)#" index="local.arrayIndex">	

			<!--- Save the alertStatus to the application database --->
			<cfset this.alertStatusService.saveNewAlertStatus(local.newAlertStatuses[local.arrayIndex])/>
			
		</cfloop>		
		
	</cffunction>	

	<cffunction name="purgeAlertStatusData"
				access="public"
				hint="This method is used to purge / delete any alertStatus data related to the test data being used when alertStatus unit tests are exersized.">

		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the alertStatus data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.alertStatuses
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.testData.alertStatuses.new.friendly_name)#">

					)
		
		</cfquery>
			
	</cffunction>

	<!--- AlertType Data Methods --->
	<cffunction name="setupAlertTypeData"
				access="private"
				hint="This method is used to create the data for testing alertType records.">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>			
	
			// Initialize the test data company structures
			this.testData.alertTypes = structNew();

			// Define the total number of records to create / process
			this.testData.alertTypes.rowCount = 4;
			
			// Initialize the new alertType data set
			this.testData.alertTypes.new = queryNew('alert_type_code,friendly_name,is_active,created_by,created_date,modified_by,modified_date');	
				
			// Add a row to the query (instance #1)
			queryAddRow(this.testData.alertTypes.new,this.testData.alertTypes.rowCount);				
				
			// Loop over the collection and create the query test rows
			for( local.rowIndex = 1; local.rowIndex lte this.testData.alertTypes.rowCount; local.rowIndex = local.rowIndex + 1){

				// Set the cell values for this instance of an alert
				querySetCell(this.testData.alertTypes.new,'alert_type_code', 'AlertType00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.alertTypes.new,'friendly_name', 'Alert 00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.alertTypes.new,'description', 'Alert 00#local.rowIndex# Description', local.rowIndex);
				querySetCell(this.testData.alertTypes.new,'cfc_name', 'AlertType', local.rowIndex);
				querySetCell(this.testData.alertTypes.new,'is_active', 1, local.rowIndex);
				querySetCell(this.testData.alertTypes.new,'created_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.alertTypes.new,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.testData.alertTypes.new,'modified_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.alertTypes.new,'modified_date', createOdbcDateTime(now()), local.rowIndex);

			}

			// Populate the seed alert value
			this.seedData.alertTypes.alert_type_code = this.testData.alertTypes.new.alert_type_code[1];

		</cfscript>
				
	</cffunction>		
		
	<cffunction name="createAlertTypeData"
				access="public"
				returntype="void"
				hint="This method is used to seed the application database with test / alertType data.">	
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Build out the array of alert objects --->
		<cfset local.newAlertTypes = this.delegate.queryToVoArray(this.testData.alertTypes.new, 'mce.e2.alerting.vo.AlertType')/>
							
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertTypes)#" index="local.arrayIndex">	

			<!--- Save the alertType to the application database --->
			<cfset this.alertTypeService.saveNewAlertType(local.newAlertTypes[local.arrayIndex])/>
			
		</cfloop>		
		
	</cffunction>	

	<cffunction name="purgeAlertTypeData"
				access="public"
				hint="This method is used to purge / delete any alertType data related to the test data being used when alertType unit tests are exersized.">

		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the alertType data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.alertTypes
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.testData.alertTypes.new.friendly_name)#">

					)
		
		</cfquery>
			
	</cffunction>

	<!--- Alert Data Methods --->
	<cffunction name="setupAlertData"
				access="private"
				hint="This method is used to create the data for testing alert records.">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>			
	
			// Initialize the test data company structures
			this.testData.alerts = structNew();

			// Define the total number of records to create / process
			this.testData.alerts.rowCount = 4;
			
			// Initialize the new company data set
			this.testData.alerts.new = queryNew('alert_uid,friendly_name,is_active,created_by,created_date,modified_by,modified_date');	
				
			// Add a row to the query (instance #1)
			queryAddRow(this.testData.alerts.new,this.testData.alerts.rowCount);				
				
			// Loop over the collection and create the query test rows
			for( local.rowIndex = 1; local.rowIndex lte this.testData.alerts.rowCount; local.rowIndex = local.rowIndex + 1){

				// Set the cell values for this instance of an alert
				querySetCell(this.testData.alerts.new,'alert_uid', this.delegate.createUniqueIdentifier(), local.rowIndex);
				querySetCell(this.testData.alerts.new,'friendly_name', 'Alert 00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.alerts.new,'is_active', 1, local.rowIndex);
				querySetCell(this.testData.alerts.new,'created_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.alerts.new,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.testData.alerts.new,'modified_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.alerts.new,'modified_date', createOdbcDateTime(now()), local.rowIndex);

			}

			// Populate the seed alert value
			this.seedData.alerts.alert_uid = this.testData.alerts.new.alert_uid[1];

		</cfscript>
				
	</cffunction>		
		
	<cffunction name="createAlertData"
				access="public"
				returntype="void"
				hint="This method is used to seed the application database with test / alert data.">	
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Build out the array of alert objects --->
		<cfset local.newAlerts = this.delegate.queryToVoArray(this.testData.alerts.new, 'mce.e2.alerting.vo.Alert')/>
							
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlerts)#" index="local.arrayIndex">	

			<!--- Save the alert to the application database --->
			<cfset this.alertService.saveNewAlert(local.newAlerts[local.arrayIndex])/>
			
		</cfloop>		
		
	</cffunction>	

	<cffunction name="purgeAlertData"
				access="public"
				hint="This method is used to purge / delete any alert data related to the test data being used when alert unit tests are exersized.">

		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the alert data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.alerts
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.testData.alerts.new.friendly_name)#">

					)
		
		</cfquery>
			
	</cffunction>	
		
</cfcomponent>