<cfcomponent name="Base Alert Subscription Testing Component"
			 extends="mxunit.framework.TestCase" output="true"
			 hint="This component is the base testing component for the alert Subscription delegate / service unit tests.">

	<!--- Private methods / for internal use only --->
	<cffunction name="purgeAlertSubscriptionTestData"
				access="private"
				hint="This method is used to purge / delete any alertSubscription data related to the test data being used when alertSubscription unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="alertSubscriptions" Subscription="query" hint="This argument is used to identify the alertSubscription data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the alertSubscription data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.alertSubscriptions
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newAlertSubscriptions.friendly_name)#">

					)
		
			delete
			from	dbo.alertSubscriptions
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.modifiedAlertSubscriptions.friendly_name)#">

					)		
		
		</cfquery>
			
	</cffunction>			
	
	<cffunction name="setupTestData"
				access="private"
				hint="This method is used to setup test data for the alertSubscription service and delegate tests.">
		
		<cfscript>
		
			//Build out the alertSubscription test data (for create / modify scenarios)
			createAlertSubscriptionTestData();

		</cfscript>		
				
	</cffunction>
	
	<cffunction name="createAlertSubscriptionTestData"
				access="private"
				hint="This method is used to create the test data for testing alertSubscription records (create and update actions).">
	
		<cfscript>			
			// Initialize the foreign key values
			this.testData.alertSubscriptions.client_company_uid = "2355C5B2-EF6B-4E19-B666-7ADFAA6B081C";
			this.testData.alertSubscriptions.property_uid = "5A384FD7-34AA-4BE9-B2B2-000B41E87043";
			this.testData.alertSubscriptions.alert_type_code = "CompletedUsage";
			
			// Initialize a query containing alertSubscription data to be tested with (the query should contain all of the columns of the base table)
			this.newAlertSubscriptions = queryNew('alert_subscription_uid,client_company_uid,property_uid,alert_type_code,friendly_name,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newAlertSubscriptions,4);
		
			// Set the cell values for this instance of a alertSubscription
			querySetCell(this.newAlertSubscriptions,'alert_subscription_uid', this.delegate.createUniqueIdentifier(), 1);
			querySetCell(this.newAlertSubscriptions,'client_company_uid', this.testData.alertSubscriptions.client_company_uid, 1);
			querySetCell(this.newAlertSubscriptions,'property_uid', this.testData.alertSubscriptions.property_uid, 1);
			querySetCell(this.newAlertSubscriptions,'alert_type_code', this.testData.alertSubscriptions.alert_type_code, 1);
			querySetCell(this.newAlertSubscriptions,'friendly_name', 'AlertSubscription 001', 1);
			querySetCell(this.newAlertSubscriptions,'is_active', 1, 1);
			querySetCell(this.newAlertSubscriptions,'created_by', 'MX Unit (created)', 1);
			querySetCell(this.newAlertSubscriptions,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newAlertSubscriptions,'modified_by', 'MX Unit (created)', 1);
			querySetCell(this.newAlertSubscriptions,'modified_date', createOdbcDateTime(now()), 1);
					
			querySetCell(this.newAlertSubscriptions,'alert_subscription_uid', this.delegate.createUniqueIdentifier(), 2);
			querySetCell(this.newAlertSubscriptions,'client_company_uid', this.testData.alertSubscriptions.client_company_uid, 2);
			querySetCell(this.newAlertSubscriptions,'property_uid', this.testData.alertSubscriptions.property_uid, 2);
			querySetCell(this.newAlertSubscriptions,'alert_type_code', this.testData.alertSubscriptions.alert_type_code, 2);
			querySetCell(this.newAlertSubscriptions,'friendly_name', 'AlertSubscription 002', 2);
			querySetCell(this.newAlertSubscriptions,'is_active', 1, 2);
			querySetCell(this.newAlertSubscriptions,'created_by', 'MX Unit (created)', 2);
			querySetCell(this.newAlertSubscriptions,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newAlertSubscriptions,'modified_by', 'MX Unit (created)', 2);
			querySetCell(this.newAlertSubscriptions,'modified_date', createOdbcDateTime(now()), 2);
					
			querySetCell(this.newAlertSubscriptions,'alert_subscription_uid', this.delegate.createUniqueIdentifier(), 3);
			querySetCell(this.newAlertSubscriptions,'client_company_uid', this.testData.alertSubscriptions.client_company_uid, 3);
			querySetCell(this.newAlertSubscriptions,'property_uid', this.testData.alertSubscriptions.property_uid, 3);
			querySetCell(this.newAlertSubscriptions,'alert_type_code', this.testData.alertSubscriptions.alert_type_code, 3);
			querySetCell(this.newAlertSubscriptions,'friendly_name', 'AlertSubscription 003', 3);
			querySetCell(this.newAlertSubscriptions,'is_active', 1, 3);
			querySetCell(this.newAlertSubscriptions,'created_by', 'MX Unit (created)', 3);
			querySetCell(this.newAlertSubscriptions,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newAlertSubscriptions,'modified_by', 'MX Unit (created)', 3);
			querySetCell(this.newAlertSubscriptions,'modified_date', createOdbcDateTime(now()), 3);
					
			querySetCell(this.newAlertSubscriptions,'alert_subscription_uid', this.delegate.createUniqueIdentifier(), 4);
			querySetCell(this.newAlertSubscriptions,'client_company_uid', this.testData.alertSubscriptions.client_company_uid, 4);
			querySetCell(this.newAlertSubscriptions,'property_uid', this.testData.alertSubscriptions.property_uid, 4);
			querySetCell(this.newAlertSubscriptions,'alert_type_code', this.testData.alertSubscriptions.alert_type_code, 4);
			querySetCell(this.newAlertSubscriptions,'friendly_name', 'AlertSubscription 004', 4);
			querySetCell(this.newAlertSubscriptions,'is_active', 1, 4);
			querySetCell(this.newAlertSubscriptions,'created_by', 'MX Unit (created)', 4);
			querySetCell(this.newAlertSubscriptions,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newAlertSubscriptions,'modified_by', 'MX Unit (created)', 4);
			querySetCell(this.newAlertSubscriptions,'modified_date', createOdbcDateTime(now()), 4);
	
			// Build out the test data set of alertSubscriptions without primary keys
			this.newAlertSubscriptionsWithoutPks = duplicate(this.newAlertSubscriptions);
			
			// Remote the primary keys from the alertSubscription test data 
			querySetCell(this.newAlertSubscriptionsWithoutPks,'alert_subscription_uid', '', 1);
			querySetCell(this.newAlertSubscriptionsWithoutPks,'alert_subscription_uid', '', 2);
			querySetCell(this.newAlertSubscriptionsWithoutPks,'alert_subscription_uid', '', 3);
			querySetCell(this.newAlertSubscriptionsWithoutPks,'alert_subscription_uid', '', 4);
					
			// Duplicate the new alertSubscriptions to create the baseline modified alertSubscriptions
			this.modifiedAlertSubscriptions = duplicate(this.newAlertSubscriptions);

			// Update the friendly names for the modified alertSubscriptions
			querySetCell(this.modifiedAlertSubscriptions,'friendly_name', 'AlertSubscription M 001', 1);
			querySetCell(this.modifiedAlertSubscriptions,'friendly_name', 'AlertSubscription M 002', 2);
			querySetCell(this.modifiedAlertSubscriptions,'friendly_name', 'AlertSubscription M 003', 3);			
			querySetCell(this.modifiedAlertSubscriptions,'friendly_name', 'AlertSubscription M 004', 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedAlertSubscriptions,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedAlertSubscriptions,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedAlertSubscriptions,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedAlertSubscriptions,'modified_by', 'MX Unit (modified)', 4);
		
		</cfscript>
				
	</cffunction>

	<cffunction name="compareAlertSubscriptionProperties"
				access="private"
				hint="This method is used to compare the properties for a given alertSubscription vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" Subscription="array" required="true" hint="Describes the array collection of alertSubscription vo's to be compared.">
		<cfargument name="compareAction" Subscription="string" required="true" hint="Describes the Subscription of comparison being performed (create / modify).">
		<cfargument name="compareType" Subscription="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" Subscription="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the alertSubscription from the application database --->
				<cfset local.retrievedAlertSubscription = this.delegate.getAlertSubscriptionAsComponent(arguments.sourceObject[local.arrayIndex].alert_subscription_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the alertSubscription from the application database --->
				<cfset local.retrievedAlertSubscription = this.service.getAlertSubscription(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared alertSubscription VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedAlertSubscription, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved alertSubscription VO component [#arguments.compareAction#].")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
						
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedAlertSubscription[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedAlertSubscription[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>
	
</cfcomponent>