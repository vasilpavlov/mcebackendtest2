<cfcomponent name="Base Alert Type Testing Component"
			 extends="mxunit.framework.TestCase" output="true"
			 hint="This component is the base testing component for the alert Type delegate / service unit tests.">

	<!--- Private methods / for internal use only --->
	<cffunction name="purgeAlertTypeTestData"
				access="private"
				hint="This method is used to purge / delete any alertType data related to the test data being used when alertType unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="alertTypes" type="query" hint="This argument is used to identify the alertType data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the alertType data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.alertTypes
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newAlertTypes.friendly_name)#">

					)
		
			delete
			from	dbo.alertTypes
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.modifiedAlertTypes.friendly_name)#">

					)		
		
		</cfquery>
			
	</cffunction>			
	
	<cffunction name="setupTestData"
				access="private"
				hint="This method is used to setup test data for the alertType service and delegate tests.">
		
		<cfscript>
		
			//Build out the alertType test data (for create / modify scenarios)
			createAlertTypeTestData();

		</cfscript>		
				
	</cffunction>
	
	<cffunction name="createAlertTypeTestData"
				access="private"
				hint="This method is used to create the test data for testing alertType records (create and update actions).">
	
		<cfscript>			
		
			// Initialize a query containing alertType data to be tested with (the query should contain all of the columns of the base table)
			this.newAlertTypes = queryNew('alert_Type_code,friendly_name,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newAlertTypes,4);
		
			// Set the cell values for this instance of a alertType
			querySetCell(this.newAlertTypes,'alert_Type_code', 'AlertType001', 1);
			querySetCell(this.newAlertTypes,'friendly_name', 'AlertType 001', 1);
			querySetCell(this.testData.alertTypes.new,'description', 'Alert 001 Description', 1);
			querySetCell(this.testData.alertTypes.new,'cfc_name', 'AlertType', 1);
			querySetCell(this.newAlertTypes,'is_active', 1, 1);
			querySetCell(this.newAlertTypes,'created_by', 'MX Unit (created)', 1);
			querySetCell(this.newAlertTypes,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newAlertTypes,'modified_by', 'MX Unit (created)', 1);
			querySetCell(this.newAlertTypes,'modified_date', createOdbcDateTime(now()), 1);
					
			querySetCell(this.newAlertTypes,'alert_Type_code', 'AlertType002', 2);
			querySetCell(this.newAlertTypes,'friendly_name', 'AlertType 002', 2);
			querySetCell(this.testData.alertTypes.new,'description', 'Alert 002 Description', 2);
			querySetCell(this.testData.alertTypes.new,'cfc_name', 'AlertType', 2);
			querySetCell(this.newAlertTypes,'is_active', 1, 2);
			querySetCell(this.newAlertTypes,'created_by', 'MX Unit (created)', 2);
			querySetCell(this.newAlertTypes,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newAlertTypes,'modified_by', 'MX Unit (created)', 2);
			querySetCell(this.newAlertTypes,'modified_date', createOdbcDateTime(now()), 2);
					
			querySetCell(this.newAlertTypes,'alert_Type_code', 'AlertType003', 3);
			querySetCell(this.newAlertTypes,'friendly_name', 'AlertType 003', 3);
			querySetCell(this.testData.alertTypes.new,'description', 'Alert 003 Description', 3);
			querySetCell(this.testData.alertTypes.new,'cfc_name', 'AlertType', 3);
			querySetCell(this.newAlertTypes,'is_active', 1, 3);
			querySetCell(this.newAlertTypes,'created_by', 'MX Unit (created)', 3);
			querySetCell(this.newAlertTypes,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newAlertTypes,'modified_by', 'MX Unit (created)', 3);
			querySetCell(this.newAlertTypes,'modified_date', createOdbcDateTime(now()), 3);
					
			querySetCell(this.newAlertTypes,'alert_Type_code', 'AlertType004', 4);
			querySetCell(this.newAlertTypes,'friendly_name', 'AlertType 004', 4);
			querySetCell(this.testData.alertTypes.new,'description', 'Alert 004 Description', 4);
			querySetCell(this.testData.alertTypes.new,'cfc_name', 'AlertType', 4);
			querySetCell(this.newAlertTypes,'is_active', 1, 4);
			querySetCell(this.newAlertTypes,'created_by', 'MX Unit (created)', 4);
			querySetCell(this.newAlertTypes,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newAlertTypes,'modified_by', 'MX Unit (created)', 4);
			querySetCell(this.newAlertTypes,'modified_date', createOdbcDateTime(now()), 4);
	
			// Build out the test data set of alertTypes without primary keys
			this.newAlertTypesWithoutPks = duplicate(this.newAlertTypes);
			
			// Remote the primary keys from the alertType test data 
			querySetCell(this.newAlertTypesWithoutPks,'alert_Type_code', '', 1);
			querySetCell(this.newAlertTypesWithoutPks,'alert_Type_code', '', 2);
			querySetCell(this.newAlertTypesWithoutPks,'alert_Type_code', '', 3);
			querySetCell(this.newAlertTypesWithoutPks,'alert_Type_code', '', 4);
					
			// Duplicate the new alertTypes to create the baseline modified alertTypes
			this.modifiedAlertTypes = duplicate(this.newAlertTypes);

			// Update the friendly names for the modified alertTypes
			querySetCell(this.modifiedAlertTypes,'friendly_name', 'AlertType M 001', 1);
			querySetCell(this.modifiedAlertTypes,'friendly_name', 'AlertType M 002', 2);
			querySetCell(this.modifiedAlertTypes,'friendly_name', 'AlertType M 003', 3);			
			querySetCell(this.modifiedAlertTypes,'friendly_name', 'AlertType M 004', 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedAlertTypes,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedAlertTypes,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedAlertTypes,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedAlertTypes,'modified_by', 'MX Unit (modified)', 4);
		
		</cfscript>
				
	</cffunction>

	<cffunction name="compareAlertTypeProperties"
				access="private"
				hint="This method is used to compare the properties for a given alertType vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of alertType vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the alertType from the application database --->
				<cfset local.retrievedAlertType = this.delegate.getAlertTypeAsComponent(arguments.sourceObject[local.arrayIndex].alert_Type_code)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the alertType from the application database --->
				<cfset local.retrievedAlertType = this.service.getAlertType(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared alertType VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedAlertType, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved alertType VO component [#arguments.compareAction#].")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
						
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedAlertType[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedAlertType[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>
	
</cfcomponent>