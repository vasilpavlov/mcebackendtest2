<cfcomponent name="Test Alert Subscription Service"
			 extends="BaseAlertSubscriptionTestingComponent" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the alertSubscriptionService component.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')>
		<cfset init()>
	</cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returnSubscription="void">
		
		<cfscript>
		
			// Get an instance of the alertSubscription service
			this.service = request.beanFactory.getBean("AlertSubscriptionService");
			this.delegate = request.beanFactory.getBean("AlertSubscriptionDelegate");
	
			// Setup the test data for the unit tests
			setupTestData();
				
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyAlertSubscriptionComponent" 
				output="false" returnSubscription="void"
				hint="This method is used to test the retrieval of an empty alertSubscription component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertSubscription delegate --->
		<cfset local.alertSubscription = this.service.getEmptyAlertSubscriptionVo()>

		<!--- Assert that the alertSubscription object is of the correct Subscription --->
		<cfset assertEquals(isInstanceOf(local.alertSubscription, "mce.e2.alerting.vo.alertSubscription"), true, "local.alertSubscription objectSubscription is not of Subscription mce.e2.alerting.vo.AlertSubscription.")>
			
	</cffunction>

	<cffunction name="getAlertSubscriptions"
				output="false" returnSubscription="void"
				hint="This method is used to test the retrieval of all alertSubscriptions from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alert delegate --->
		<cfset local.alertSubscriptionCollection = this.service.getAlertSubscriptions()>
				
		<!--- Assert that the alertSubscription array collection is an array --->
		<cfset assertTrue(isArray(local.alertSubscriptionCollection), "The value returned by this.service.getAlertSubscriptions() was not an array.")>

		<!--- Compare the alertSubscription properties and excersize retrieving single instances of alertSubscriptions --->
		<cfset comparealertSubscriptionProperties(local.alertSubscriptionCollection,'get','service')>
				
	</cffunction>

	<cffunction name="getAlertSubscription"
				output="false" returnSubscription="void"
				hint="This method is used to test the retrieval of a single alertSubscription from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertSubscription delegate --->
		<cfset local.alertSubscriptionCollection = this.service.getAlertSubscriptions()>
				
		<!--- Loop through the alertSubscription collection, and retrieve each alertSubscription individually --->
		<cfloop from="1" to="#arraylen(local.alertSubscriptionCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the alertSubscription to test --->
			<cfset local.alertSubscription = local.alertSubscriptionCollection[local.arrayIndex]>

				<!--- Retrrive a single instance of a alertSubscription from the service --->
				<cfset local.testAlertSubscription = this.service.getalertSubscription(local.alertSubscription)>

			<!--- Assert that the alertSubscription array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testAlertSubscription, "mce.e2.alerting.vo.AlertSubscription"), true, "local.testAlertSubscription objectSubscription is not of Subscription mce.e2.alerting.vo.AlertSubscription.")>
	
		</cfloop>
				
	</cffunction>

	<!--- Test the creation of alertSubscription data without specifying primary keys --->
	<cffunction name="saveAndValidateNewAlertSubscriptionWithoutPrimaryKeys"
				access="public" returnSubscription="void"
				hint="This method is used to test / validate the creation of alertSubscriptions using the saveNewSubscriptionAlert() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of alert objects --->
		<cfset local.newAlertSubscriptions = this.delegate.queryToVoArray(this.newAlertSubscriptionsWithoutPks, 'mce.e2.alerting.vo.AlertSubscription')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdAlertSubscriptions = arrayNew(1)/>
		
		<!--- Purge / remote the alertSubscription test data --->	
		<cfset purgeAlertSubscriptionTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertSubscriptions)#" index="local.arrayIndex">	

			<!--- Save the alertSubscription to the application database --->
			<cfset local.testAlertSubscriptionPrimaryKey = this.service.saveNewAlertSubscription(local.newAlertSubscriptions[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testAlertSubscriptionPrimaryKey), "The value returned by this.service.saveNewAlertSubscription() was not a unique identifier.")/>
	
			<!--- Add the primary key to the alertSubscription --->
			<cfset local.testAlertSubscription = local.newAlertSubscriptions[local.arrayIndex]>	
			<cfset local.testAlertSubscription.alert_subscription_uid = local.testAlertSubscriptionPrimaryKey>
	
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdAlertSubscriptions, duplicate(local.testAlertSubscription))/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdAlertSubscriptions), arrayLen(local.newAlertSubscriptions), "The total number of records processed does not match the total number of records iterated over when creating alertSubscription records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertSubscriptionProperties(sourceObject=local.createdAlertSubscriptions, compareAction="create", compareSubscription="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alertSubscription test data --->	
		<cfset purgeAlertSubscriptionTestData()> 			
							
	</cffunction>

	<!--- Test the creation of alertSubscription data --->
	<cffunction name="saveAndValidateNewAlertSubscriptions"
				access="public" returnSubscription="void"
				hint="This method is used to test / validate the creation of alerts using the saveNewAlertSubscription() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of alert objects --->
		<cfset local.newAlertSubscriptions = this.delegate.queryToVoArray(this.newAlertSubscriptions, 'mce.e2.alerting.vo.AlertSubscription')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		
		<!--- Purge / remote the alertSubscription test data --->	
		<cfset purgeAlertSubscriptionTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertSubscriptions)#" index="local.arrayIndex">	

			<!--- Save the alertSubscription to the application database --->
			<cfset local.testAlertSubscriptionPrimaryKey = this.service.saveNewAlertSubscription(local.newAlertSubscriptions[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<!---
			Value is a code, not a uid
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testAlertSubscriptionPrimaryKey), "The value returned by this.service.saveNewAlertSubscription() was not a unique identifier.")/>
			--->
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlertSubscriptions), "The total number of records processed does not match the total number of records iterated over when creating alertSubscription records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertSubscriptionProperties(sourceObject=local.newAlertSubscriptions, compareAction="create", compareSubscription="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertSubscriptionTestData()> 			
							
	</cffunction>
	
	<cffunction name="saveAndReturnAndValidateNewAlertSubscriptions"
				access="public" returnSubscription="void"
				hint="This method is used to test / validate the creation of alerts using the saveAndReturnNewAlertSubscription() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
					
		<!--- Build out the test alertSubscriptions array --->			
		<cfset local.testAlertSubscriptions = arrayNew(1)>			
						
		<!--- Build out the array of alertSubscription objects --->
		<cfset local.newAlertSubscriptions = this.delegate.queryToVoArray(this.newAlertSubscriptions, 'mce.e2.alerting.vo.AlertSubscription')/>
		
		<!--- Purge / remote the alertSubscription test data --->	
		<cfset purgeAlertSubscriptionTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertSubscriptions)#" index="local.arrayIndex">	

			<!--- Save the alertSubscription to the application database --->
			<cfset local.testAlertSubscription = this.service.saveAndReturnNewAlertSubscription(local.newAlertSubscriptions[local.arrayIndex])/>
			
			<!--- Assert that the alertSubscription object is of the correct Subscription --->
			<cfset assertEquals(isInstanceOf(local.testAlertSubscription, "mce.e2.alerting.vo.AlertSubscription"), true, "local.alertSubscription objectSubscription is not of Subscription mce.e2.alerting.vo.AlertSubscription.")>
						
			<!--- Append the alertSubscription to the test array --->			
			<cfset arrayAppend(local.testAlertSubscriptions, local.testAlertSubscription)>			

		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.testAlertSubscriptions), arrayLen(local.newAlertSubscriptions), "The total number of records processed does not match the total number of records iterated over when creating alertSubscription records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertSubscriptionProperties(sourceObject=local.testAlertSubscriptions, compareAction="create", compareSubscription="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertSubscriptionTestData()> 			
							
	</cffunction>	
	
	<cffunction name="saveAndValidateExistingAlertSubscriptions"
				access="public" returnSubscription="void"
				hint="This method is used to test / validate the modification of alertSubscriptions.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of alertSubscription objects --->
		<cfset local.newAlertSubscriptions = this.delegate.queryToVoArray(this.newAlertSubscriptions, 'mce.e2.alerting.vo.AlertSubscription')/>
		<cfset local.modifiedAlertSubscriptions = this.delegate.queryToVoArray(this.modifiedAlertSubscriptions, 'mce.e2.alerting.vo.AlertSubscription')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>
				
		<!--- Purge / remote the alertSubscription test data --->	
		<cfset purgeAlertSubscriptionTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertSubscriptions)#" index="local.arrayIndex">	

			<!--- Save the alertSubscription to the application database --->
			<cfset this.service.saveNewAlertSubscription(local.newAlertSubscriptions[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlertSubscriptions), "The total number of records processed does not match the total number of records iterated over when creating alertSubscription records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertSubscriptionProperties(sourceObject=local.newAlertSubscriptions, compareAction="create", compareSubscription="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedAlertSubscriptions)#" index="local.arrayIndex">	

			<!--- Save the alertSubscription to the application database --->
			<cfset this.service.saveExistingAlertSubscription(local.modifiedAlertSubscriptions[local.arrayIndex])/>
			
			<!--- Increment the modified count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedAlertSubscriptions), "The total number of records processed does not match the total number of records iterated over when modifying alertSubscription records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertSubscriptionProperties(sourceObject=local.modifiedAlertSubscriptions, compareAction="modify", compareSubscription="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertSubscriptionTestData()> 			
							
	</cffunction>	

</cfcomponent>