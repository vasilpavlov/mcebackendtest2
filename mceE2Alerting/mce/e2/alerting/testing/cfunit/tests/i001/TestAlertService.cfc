<cfcomponent name="Test Alert Service"
			 extends="BaseAlertTestingComponent"
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the alertService component.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')>
		<cfset init()>
	</cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">

		<cfscript>

			// Get an instance of the alert service
			this.service = request.beanFactory.getBean("alertService");
			this.delegate = request.beanFactory.getBean("alertDelegate");

			// Setup the test data for the unit tests
			setupTestData();

		</cfscript>

	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyAlertComponent"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty alert component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Initialize the alert delegate --->
		<cfset local.alert = this.service.getEmptyAlertVo()>

		<!--- Assert that the alert object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.alert, "mce.e2.alerting.vo.alert"), true, "local.alert objectType is not of type mce.e2.alerting.vo.alert.")>

	</cffunction>

	<cffunction name="getAlerts"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all alerts from the application database.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Initialize the alert delegate --->
		<cfset local.alertCollection = this.service.getAlerts()>

		<!--- Assert that the alert array collection is an array --->
		<cfset assertTrue(isArray(local.alertCollection), "The value returned by this.service.getAlerts() was not an array.")>

		<!--- Compare the alert properties and excersize retrieving single instances of alerts --->
		<cfset comparealertProperties(local.alertCollection,'get','service')>

	</cffunction>

	<cffunction name="getalert"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single alert from the application database.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Initialize the alert delegate --->
		<cfset local.alertCollection = this.service.getAlerts()>

		<!--- Loop through the alert collection, and retrieve each alert individually --->
		<cfloop from="1" to="#arraylen(local.alertCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the alert to test --->
			<cfset local.alert = local.alertCollection[local.arrayIndex]>

				<!--- Retrrive a single instance of a alert from the service --->
				<cfset local.testAlert = this.service.getalert(local.alert)>

			<!--- Assert that the alert array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testAlert, "mce.e2.alerting.vo.alert"), true, "local.testAlert objectType is not of type mce.e2.alerting.vo.alert.")>

		</cfloop>

	</cffunction>

	<!--- Test the creation of alert data without specifying primary keys --->
	<cffunction name="saveAndValidateNewAlertWithoutPrimaryKeys"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of alerts using the saveNewAlert() method.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Build out the array of alert objects --->
		<cfset local.newAlerts = this.delegate.queryToVoArray(this.newAlertsWithoutPks, 'mce.e2.alerting.vo.Alert')/>

		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdAlerts = arrayNew(1)/>

		<!--- Purge / remote the alert test data --->
		<cfset purgeAlertTestData()>

		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlerts)#" index="local.arrayIndex">

			<!--- Save the alert to the application database --->
			<cfset local.testAlertPrimaryKey = this.service.saveNewAlert(local.newAlerts[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testAlertPrimaryKey), "The value returned by this.service.saveNewAlert() was not a unique identifier.")/>

			<!--- Add the primary key to the alert --->
			<cfset local.testAlert = local.newAlerts[local.arrayIndex]>
			<cfset local.testAlert.alert_uid = local.testAlertPrimaryKey>

			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdAlerts, duplicate(local.testAlert))/>

		</cfloop>

		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdAlerts), arrayLen(local.newAlerts), "The total number of records processed does not match the total number of records iterated over when creating alert records.")/>

		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertProperties(sourceObject=local.createdAlerts, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->
		<cfset purgeAlertTestData()>

	</cffunction>

	<!--- Test the creation of alert data --->
	<cffunction name="saveAndValidateNewAlerts"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of alerts using the saveNewAlert() method.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Build out the array of alert objects --->
		<cfset local.newAlerts = this.delegate.queryToVoArray(this.newAlerts, 'mce.e2.alerting.vo.alert')/>

		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>

		<!--- Purge / remote the alert test data --->
		<cfset purgeAlertTestData()>

		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlerts)#" index="local.arrayIndex">

			<!--- Save the alert to the application database --->
			<cfset local.testAlertPrimaryKey = this.service.saveNewAlert(local.newAlerts[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testAlertPrimaryKey), "The value returned by this.service.saveNewAlert() was not a unique identifier.")/>

			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>

		</cfloop>

		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlerts), "The total number of records processed does not match the total number of records iterated over when creating alert records.")/>

		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertProperties(sourceObject=local.newAlerts, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->
		<cfset purgeAlertTestData()>

	</cffunction>

	<cffunction name="saveAndReturnAndValidateNewAlerts"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of alerts using the saveAndReturnNewAlert() method.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Build out the test alerts array --->
		<cfset local.testAlerts = arrayNew(1)>

		<!--- Build out the array of alert objects --->
		<cfset local.newAlerts = this.delegate.queryToVoArray(this.newAlertsWithoutPks, 'mce.e2.alerting.vo.alert')/>

		<!--- Purge / remote the alert test data --->
		<cfset purgeAlertTestData()>

		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlerts)#" index="local.arrayIndex">

			<!--- Save the alert to the application database --->
			<cfset local.testAlert = this.service.saveAndReturnNewAlert(local.newAlerts[local.arrayIndex])/>

			<!--- Assert that the alert object is of the correct type --->
			<cfset assertEquals(isInstanceOf(local.testAlert, "mce.e2.alerting.vo.alert"), true, "local.alert objectType is not of type mce.e2.alerting.vo.alert.")>

			<!--- Append the alert to the test array --->
			<cfset arrayAppend(local.testAlerts, local.testAlert)>

		</cfloop>

		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.testAlerts), arrayLen(local.newAlerts), "The total number of records processed does not match the total number of records iterated over when creating alert records.")/>

		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertProperties(sourceObject=local.testAlerts, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->
		<cfset purgeAlertTestData()>

	</cffunction>

	<cffunction name="saveAndValidateExistingAlerts"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of alerts.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Build out the array of alert objects --->
		<cfset local.newAlerts = this.delegate.queryToVoArray(this.newAlerts, 'mce.e2.alerting.vo.alert')/>
		<cfset local.modifiedAlerts = this.delegate.queryToVoArray(this.modifiedAlerts, 'mce.e2.alerting.vo.alert')/>

		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>

		<!--- Purge / remote the alert test data --->
		<cfset purgeAlertTestData()>

		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlerts)#" index="local.arrayIndex">

			<!--- Save the alert to the application database --->
			<cfset this.service.saveNewAlert(local.newAlerts[local.arrayIndex])/>

			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>

		</cfloop>

		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlerts), "The total number of records processed does not match the total number of records iterated over when creating alert records.")/>

		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertProperties(sourceObject=local.newAlerts, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedAlerts)#" index="local.arrayIndex">

			<!--- Save the alert to the application database --->
			<cfset this.service.saveExistingAlert(local.modifiedAlerts[local.arrayIndex])/>

			<!--- Increment the modified count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>

		</cfloop>

		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedAlerts), "The total number of records processed does not match the total number of records iterated over when modifying alert records.")/>

		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertProperties(sourceObject=local.modifiedAlerts, compareAction="modify", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->
		<cfset purgeAlertTestData()>

	</cffunction>

</cfcomponent>