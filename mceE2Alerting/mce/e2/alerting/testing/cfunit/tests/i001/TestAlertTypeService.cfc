<cfcomponent name="Test Type Service"
			 extends="BaseAlertTypeTestingComponent" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the alertTypeService component.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')>
		<cfset init()>
	</cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Get an instance of the alertType service
			this.service = request.beanFactory.getBean("AlertTypeService");
			this.delegate = request.beanFactory.getBean("AlertTypeDelegate");
	
			// Setup the test data for the unit tests
			setupTestData();
				
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyAlertTypeComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty alertType component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertType delegate --->
		<cfset local.alertType = this.service.getEmptyAlertTypeVo()>

		<!--- Assert that the alertType object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.alertType, "mce.e2.alerting.vo.alertType"), true, "local.alertType objectType is not of type mce.e2.alerting.vo.AlertType.")>
			
	</cffunction>

	<cffunction name="getAlertTypes"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all alertTypes from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alert delegate --->
		<cfset local.alertTypeCollection = this.service.getAlertTypes()>
				
		<!--- Assert that the alertType array collection is an array --->
		<cfset assertTrue(isArray(local.alertTypeCollection), "The value returned by this.service.getAlertTypes() was not an array.")>

		<!--- Compare the alertType properties and excersize retrieving single instances of alertTypes --->
		<cfset comparealertTypeProperties(local.alertTypeCollection,'get','service')>
				
	</cffunction>

	<cffunction name="getAlertType"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single alertType from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the alertType delegate --->
		<cfset local.alertTypeCollection = this.service.getAlertTypes()>
				
		<!--- Loop through the alertType collection, and retrieve each alertType individually --->
		<cfloop from="1" to="#arraylen(local.alertTypeCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the alertType to test --->
			<cfset local.alertType = local.alertTypeCollection[local.arrayIndex]>

				<!--- Retrrive a single instance of a alertType from the service --->
				<cfset local.testAlertType = this.service.getalertType(local.alertType)>

			<!--- Assert that the alertType array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testAlertType, "mce.e2.alerting.vo.AlertType"), true, "local.testAlertType objectType is not of type mce.e2.alerting.vo.AlertType.")>
	
		</cfloop>
				
	</cffunction>

	<!--- Test the creation of alertType data --->
	<cffunction name="saveAndValidateNewAlertTypes"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of alerts using the saveNewAlertType() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of alert objects --->
		<cfset local.newAlertTypes = this.delegate.queryToVoArray(this.newAlertTypes, 'mce.e2.alerting.vo.AlertType')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		
		<!--- Purge / remote the alertType test data --->	
		<cfset purgeAlertTypeTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertTypes)#" index="local.arrayIndex">	

			<!--- Save the alertType to the application database --->
			<cfset local.testAlertTypePrimaryKey = this.service.saveNewAlertType(local.newAlertTypes[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<!---
			Value is a code, not a uid
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testAlertTypePrimaryKey), "The value returned by this.service.saveNewAlertType() was not a unique identifier.")/>
			--->
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlertTypes), "The total number of records processed does not match the total number of records iterated over when creating alertType records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertTypeProperties(sourceObject=local.newAlertTypes, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertTypeTestData()> 			
							
	</cffunction>
	
	<cffunction name="saveAndReturnAndValidateNewAlertTypes"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of alerts using the saveAndReturnNewAlertType() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
					
		<!--- Build out the test alertTypes array --->			
		<cfset local.testAlertTypes = arrayNew(1)>			
						
		<!--- Build out the array of alertType objects --->
		<cfset local.newAlertTypes = this.delegate.queryToVoArray(this.newAlertTypes, 'mce.e2.alerting.vo.AlertType')/>
		
		<!--- Purge / remote the alertType test data --->	
		<cfset purgeAlertTypeTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertTypes)#" index="local.arrayIndex">	

			<!--- Save the alertType to the application database --->
			<cfset local.testAlertType = this.service.saveAndReturnNewAlertType(local.newAlertTypes[local.arrayIndex])/>
			
			<!--- Assert that the alertType object is of the correct type --->
			<cfset assertEquals(isInstanceOf(local.testAlertType, "mce.e2.alerting.vo.AlertType"), true, "local.alertType objectType is not of type mce.e2.alerting.vo.AlertType.")>
						
			<!--- Append the alertType to the test array --->			
			<cfset arrayAppend(local.testAlertTypes, local.testAlertType)>			

		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.testAlertTypes), arrayLen(local.newAlertTypes), "The total number of records processed does not match the total number of records iterated over when creating alertType records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertTypeProperties(sourceObject=local.testAlertTypes, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertTypeTestData()> 			
							
	</cffunction>	
	
	<cffunction name="saveAndValidateExistingAlertTypes"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of alertTypes.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of alertType objects --->
		<cfset local.newAlertTypes = this.delegate.queryToVoArray(this.newAlertTypes, 'mce.e2.alerting.vo.AlertType')/>
		<cfset local.modifiedAlertTypes = this.delegate.queryToVoArray(this.modifiedAlertTypes, 'mce.e2.alerting.vo.AlertType')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>
				
		<!--- Purge / remote the alertType test data --->	
		<cfset purgeAlertTypeTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newAlertTypes)#" index="local.arrayIndex">	

			<!--- Save the alertType to the application database --->
			<cfset this.service.saveNewAlertType(local.newAlertTypes[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newAlertTypes), "The total number of records processed does not match the total number of records iterated over when creating alertType records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertTypeProperties(sourceObject=local.newAlertTypes, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedAlertTypes)#" index="local.arrayIndex">	

			<!--- Save the alertType to the application database --->
			<cfset this.service.saveExistingAlertType(local.modifiedAlertTypes[local.arrayIndex])/>
			
			<!--- Increment the modified count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedAlertTypes), "The total number of records processed does not match the total number of records iterated over when modifying alertType records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareAlertTypeProperties(sourceObject=local.modifiedAlertTypes, compareAction="modify", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the alert test data --->	
		<cfset purgeAlertTypeTestData()> 			
							
	</cffunction>	

</cfcomponent>