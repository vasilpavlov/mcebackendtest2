<cfcomponent displayname="Alert Type Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Alert Type information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setAlertTypeDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage Alert Type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.alerting.db.AlertTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to Alert Type information.">
		<cfset this.AlertTypeDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyAlertTypeVo"
				access="public" returntype="mce.e2.alerting.vo.AlertType"
				hint="This method is used to retrieve / return an empty Alert Type information value object.">
		<cfreturn this.AlertTypeDelegate.getEmptyAlertTypeComponent()>
	</cffunction>

	<cffunction name="getAlertTypes"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Alert Types.">
		<cfreturn this.AlertTypeDelegate.getAlertTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getAlertType"
				access="public" returntype="mce.e2.alerting.vo.AlertType"
				hint="This method is used to return a populated Alert Type vo (value object).">
		<cfargument name="AlertType" type="mce.e2.alerting.vo.AlertType" required="true" hint="Describes the Alert Type VO object containing the properties of the Alert Type to be retrieved." />
		<cfreturn this.AlertTypeDelegate.getAlertTypeAsComponent(alert_type_code=arguments.AlertType.alert_type_code)>
	</cffunction>

	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="saveNewAlertType"
				access="public" returntype="string"
				hint="This method is used to persist a new Alert Type record to the application database.">
		<cfargument name="AlertType" type="mce.e2.alerting.vo.AlertType" required="true" hint="Describes the Alert Type VO object containing the details of the Alert Typeto be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.AlertType = setAuditProperties(arguments.AlertType, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<!---<cfset local.AlertType = setPrimaryKey(local.AlertType)>--->

		<!--- Save the modified AlertType --->
		<cfset this.AlertTypeDelegate.saveNewAlertType(AlertType=local.AlertType)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.AlertType.alert_type_code>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewAlertType"
				access="public" returntype="mce.e2.alerting.vo.AlertType"
				hint="This method is used to persist a new Alert Type record to the application database.">
		<cfargument name="AlertType" type="mce.e2.alerting.vo.AlertType" required="true" hint="Describes the Alert Type VO object containing the details of the Alert Type to be saved and returned." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewAlertType(AlertType=arguments.AlertType)>

		<!--- Set the primary key --->
		<cfset arguments.AlertType.alert_type_code = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.AlertType>

	</cffunction>

	<cffunction name="saveExistingAlertType"
				access="public" returntype="mce.e2.alerting.vo.AlertType"
				hint="This method is used to save an existing Alert Type information to the application database.">
		<cfargument name="AlertType" type="mce.e2.alerting.vo.AlertType" required="true" hint="Describes the Alert Type VO object containing the details of the Alert Type to be modified." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.AlertType = setAuditProperties(arguments.AlertType, "modify")>

		<!--- Save the modified AlertType --->
		<cfset this.AlertTypeDelegate.saveExistingAlertType(AlertType=local.AlertType)/>

		<!--- Return the modified AlertType --->
		<cfreturn local.AlertType>

	</cffunction>

	<cffunction name="saveNewAlertTypeIntervals"
				access="public" returnType="boolean"
				hint="This method is used to persist new Alert Type Interval records to the application database.">
		<cfargument name="alert_type_code" type="String" required="true" hint="Describes the Alert Type VO object containing the properties of the Alert Type to be retrieved." />
		<cfargument name="alert_interval_code" required="true" type="string" hint="Comma-delimeted string of alert_interval_codes to be added to the application database."/>
		<cfreturn this.AlertTypeDelegate.saveNewAlertTypeIntervals(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="deleteAlertTypeIntervals"
				access="public" returnType="boolean"
				hint="This method is used to delete an Alert Type Interval record from the application database.">
		<cfargument name="AlertType" type="mce.e2.alerting.vo.AlertType" required="true" hint="Describes the Alert Type VO object containing the properties of the Alert Type to be retrieved." />
		<cfargument name="alert_interval_code" required="true" type="string" hint="Comma-delimeted string of alert_interval_codes to be deleted from the application database."/>
		<cfreturn this.AlertTypeDelegate.deleteAlertTypeIntervals(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="loadAlertTypes"
				access="public" returntype="void"
				hint="Executes load() method in AlertTypeLoader(). This also happens normally at startup or when ColdSpring is re-initialized.">

		<cfset var loader = application.beanFactory.getBean("AlertTypeLoader")>
		<cfset loader.load()>
	</cffunction>

</cfcomponent>