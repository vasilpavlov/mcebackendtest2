<cfcomponent displayname="Alert Schedule Service"
			 extends="BaseDataService" output="true"
			 hint="The component is used to manage all business logic tied / associated with the scheuling of Alert information.">

	<cffunction name="pollAlerts"
				access="public" returntype="any"
				hint="This method is used to check the Alert Intervals and poll for alerts that need to be sent.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set a flag to indicate if the method executes normally --->
		<cfset local.status = true>

<!---TODO: PUT TRY/CATCH BACK IN 		<cftry> --->
			<!--- Create the business logic to poll the alerts --->
			<cfset local.AssembleAlerts = createObject("component", "mce.e2.alerting.business.AssembleAlerts").init()>
			<cfset local.alertsGeneratedCount = local.AssembleAlerts.assemble()>

		<!--- 	<cfcatch type="any">
				<cfset local.status = false>
			</cfcatch>
		</cftry> --->

		<cfreturn local.alertsGeneratedCount>

	</cffunction>


	<cffunction name="checkRelevancy" returntype="string" access="remote">
		<cfargument name="alert_uid" type="string" required="true">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<cfset local.AssembleAlerts = createObject("component", "mce.e2.alerting.business.AssembleAlerts").init()>
		<cfset local.noLongerRelevant = local.AssembleAlerts.checkRelevancy(arguments.alert_uid)>

		<cfreturn arrayToList(local.noLongerRelevant)>
	</cffunction>


	<cffunction name="sendAlerts"
				access="public" returntype="any"
				hint="This method is used to send pending alerts.">

		<cfargument name="alert_digest_code" type="string" required="true">
		<cfargument name="respectVisibilityDate" type="boolean" required="false" default="true">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set a flag to indicate if the method executes normally --->
		<cfset local.status = true>

		<!---<cftry>--->
			<!--- Create the business logic to send the alerts --->
			<cfset local.SendAlerts = createObject("component", "mce.e2.alerting.business.SendAlerts").init()>
			<cfset local.status = local.SendAlerts.send(alert_digest_code, respectVisibilityDate)>

			<!---<cfcatch type="any">
				<cfset local.status = false>
			</cfcatch>
		</cftry>--->

		<cfreturn local.status>

	</cffunction>

</cfcomponent>