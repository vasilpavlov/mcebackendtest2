<cfcomponent name="Security Service"
			 extends="BaseDataService" output="true"
			 hint="This component is used to manage all security-related requests /interactions.">

	<!--- Define the methods to set the bean / delegates --->
	<cffunction name="setSecurityDelegate"
				hint="This method is used to set / define the security delegate (dependency injection).">
		<cfargument name="bean">
		<cfset this.SecurityDelegate = bean>
	</cffunction>

	<cffunction name="setSessionBeanFactory"
				hint="This method is used to set the session bean factory component (dependency injection).">
		<cfargument name="bean" type="any" hint="Describes the component used to set / define the session bean factory.">
		<cfset this.SessionBeanFactory = bean>
	</cffunction>

	<!--- Define the business logic methods --->
	<cffunction name="attemptLogin"
				returntype="mce.e2.alerting.security.SessionBean"
				hint="This method is used to process login attempts.">
		<cfargument name="username" type="string" required="true" hint="Describes the userName that is used to attempt a login.">
		<cfargument name="password" type="string" required="true" hint="Describes the password that is used to attempt a login.">

		<cfset var key = "">

		<!--- A user is attempting to log in, so we better log the current user out, if there is one --->
		<cfset attemptLogout()>

		<!--- Attempt to login via the database --->
		<cfset key = this.SecurityDelegate.attemptLogin(username, password, request.e2.system_instance_uid)>

		<!--- If the database returns a session GUID, we can consiser ourselves authenticated --->
		<cfif isValidSessionKeyFormat(key)>
			<cfset session.mce.e2.alerting.session.sessionKey = key>
			<cfset session.mce.e2.alerting.session.auditUserLabel = username>
			<cfset session.mce.e2.alerting.session.isLoggedIn = key neq "">
		</cfif>

		<!--- Return the new session info --->
		<cfreturn getSession()>

	</cffunction>

	<cffunction name="attemptLogout"
				returntype="void"
				hint="This method is used to attempt a logout action from the application.  It will remove the active session from the application's session management.">
		<cfif IsDefined("session.mce.e2.alerting.session")>
			<cfset StructDelete(session.mce.e2.alerting, "session")/>
		</cfif>
	</cffunction>

	<cffunction name="getSession"
				returntype="any"
				hint="This method is used to return the active session's sessionBean component.">
		<cfreturn this.SessionBeanFactory.getSession()/>
	</cffunction>

	<!--- Define any methods used to manage / secure property collections for a given user / session --->
	<cffunction name="cleanPropertyCollections"
				returntype="array" output="true"
				hint="This method is used to return an array of available property collections for a given user, filtering out collections that the user does not have access to.">
		<cfargument name="propertyCollectionsArray" type="array" required="true" hint="Describes the array collection of propertyCollection vo obects being evaluated.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Make a copy of the arguments scope --->
		<cfset local.args = duplicate(arguments)>

		<!--- Get the list of available / accessible propertyCollection primary keys --->
		<cfloop from="#arrayLen(local.args.propertyCollectionsArray)#" to="1" step="-1" index="local.arrayElement">

			<!--- Create a reference to the current array element --->
			<cfset local.pcVo = local.args.propertyCollectionsArray[local.arrayElement]>

			<!--- Check and see if the property collection primary key is in the session list of appropriate property collections --->
			<cfif not isPropertyCollectionAllowed(local.pcVo.property_collection_uid)>

				<!--- If it is not, delete the array element --->
				<cfset arrayDeleteAt(local.args.propertyCollectionsArray, local.arrayElement)>

			</cfif>

		</cfloop>

		<!--- Return the completed array --->
		<cfreturn local.args.propertyCollectionsArray>

	</cffunction>

	<cffunction name="isPropertyCollectionAllowed"
				returntype="boolean"
				hint="This method is used to validate if a property collection is allowed for a given user.">

		<!--- Define the arguments for the method --->
		<cfargument name="property_collection_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given property collection record.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get the current session --->
		<cfset local.thisSession = getSession()>

		<!--- Default the output value --->
		<cfset local.returnResult = true>

 		<!--- Is the current property collection primary key in the allowed list of property collections? --->
		<cfif not listFindNoCase(local.thisSession.propertyCollectionsList, arguments.property_collection_uid)>

			<!--- If not, then set the output value to false --->
			<cfset local.returnResult = false>

		</cfif>

		<!--- Return the output value --->
		<cfreturn local.returnResult>

	</cffunction>

	<!--- Private methods --->
	<cffunction name="isValidSessionKeyFormat"
				access="private" returntype="boolean"
				hint="This method is used to determine if the session key is in a valid format.">

		<!--- Define the arguments for this method --->
		<cfargument name="str" type="string" required="true" hint="Describes the session key being evaluated / validated.">

		<!--- We could validate this further, but for now a length check should suffice --->
		<!--- ADL:  The baseDataService has a method to validate uniqueIdentifiers; we could use this instead --->
		<cfreturn len(str) eq 36>

	</cffunction>

</cfcomponent>