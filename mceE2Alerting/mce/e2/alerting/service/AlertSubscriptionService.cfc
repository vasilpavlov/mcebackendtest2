<cfcomponent displayname="Alert Subscription Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Alert Subscription information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setAlertSubscriptionDelegate" 
				access="public" returnType="void" 
				hint="This method is used to set the database delegate used to manage Alert Subscription data (dependency injection).">
		<cfargument name="bean" type="mce.e2.alerting.db.AlertSubscriptionDelegate" hint="Describes the *.cfc used to manage database interactions related to Alert Subscription information.">
		<cfset this.AlertSubscriptionDelegate = arguments.bean>
	</cffunction>
	
	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyAlertSubscriptionVo" 
				access="public" returnType="mce.e2.alerting.vo.AlertSubscription"
				hint="This method is used to retrieve / return an empty Alert Subscription information value object.">
		<cfreturn this.AlertSubscriptionDelegate.getEmptyAlertSubscriptionComponent()>
	</cffunction>

	<cffunction name="getAlertSubscriptions" 
				access="public" returnType="array"
				hint="This method is used to retrieve / return a collection of Alert Subscriptions.">
		<cfreturn this.AlertSubscriptionDelegate.getAlertSubscriptionsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getAlertSubscription" 
				access="public" returnType="mce.e2.alerting.vo.AlertSubscription" 
				hint="This method is used to return a populated Alert Subscription vo (value object)."> 
		<cfargument name="AlertSubscription" type="mce.e2.alerting.vo.AlertSubscription" required="true" hint="Describes the Alert Subscription VO object containing the properties of the Alert Subscription to be retrieved." /> 
		<cfreturn this.AlertSubscriptionDelegate.getAlertSubscriptionAsComponent(alert_subscription_uid=arguments.AlertSubscription.alert_subscription_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewAlertSubscription" 
				access="public" returnType="string" 
				hint="This method is used to persist a new Alert Subscription record to the application database."> 
		<cfargument name="AlertSubscription" type="mce.e2.alerting.vo.AlertSubscription" required="true" hint="Describes the Alert Subscription VO object containing the details of the Alert Subscriptionto be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.AlertSubscription = setAuditProperties(arguments.AlertSubscription, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.AlertSubscription = setPrimaryKey(local.AlertSubscription)>
		
		<!--- Save the modified AlertSubscription --->
		<cfset this.AlertSubscriptionDelegate.saveNewAlertSubscription(AlertSubscription=local.AlertSubscription)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.AlertSubscription.alert_subscription_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewAlertSubscription" 
				access="public" returnType="mce.e2.alerting.vo.AlertSubscription" 
				hint="This method is used to persist a new Alert Subscription record to the application database."> 
		<cfargument name="AlertSubscription" type="mce.e2.alerting.vo.AlertSubscription" required="true" hint="Describes the Alert Subscription VO object containing the details of the Alert Subscription to be saved and returned." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewAlertSubscription(AlertSubscription=arguments.AlertSubscription)>

		<!--- Set the primary key --->
		<cfset arguments.AlertSubscription.alert_subscription_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.AlertSubscription>

	</cffunction>

	<cffunction name="saveExistingAlertSubscription" 
				access="public" returnType="mce.e2.alerting.vo.AlertSubscription" 
				hint="This method is used to save an existing Alert Subscription information to the application database."> 
		<cfargument name="AlertSubscription" type="mce.e2.alerting.vo.AlertSubscription" required="true" hint="Describes the Alert Subscription VO object containing the details of the Alert Subscription to be modified." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.AlertSubscription = setAuditProperties(arguments.AlertSubscription, "modify")>
		
		<!--- Save the modified AlertSubscription --->
		<cfset this.AlertSubscriptionDelegate.saveExistingAlertSubscription(AlertSubscription=local.AlertSubscription)/>

		<!--- Return the modified AlertSubscription --->
		<cfreturn local.AlertSubscription>	

	</cffunction>
	
	<cffunction name="saveNewAlertSubscriptionDeliveryMethods" 
				access="public" returnType="boolean" 
				hint="This method is used to persist new Alert Subscription Delivery Method records to the application database."> 
		<cfargument name="AlertSubscription" type="mce.e2.alerting.vo.AlertSubscription" required="true" hint="Describes the Alert Subscription VO object containing the properties of the Alert Subscription to be retrieved." /> 
		<cfargument name="alert_delivery_method_code" required="true" type="string" hint="Comma-delimeted string of alert_delivery_method_codes to be added to the application database."/>
		<cfreturn this.AlertSubscriptionDelegate.saveNewAlertSubscriptionDeliveryMethods(argumentCollection=arguments)>
	</cffunction>
	
	<cffunction name="deleteAlertSubscriptionDeliveryMethods" 
				access="public" returnType="boolean" 
				hint="This method is used to delete an Alert Subscription Delivery Method record from the application database."> 
		<cfargument name="AlertSubscription" type="mce.e2.alerting.vo.AlertSubscription" required="true" hint="Describes the Alert Subscription VO object containing the properties of the Alert Subscription to be retrieved." /> 
		<cfargument name="alert_delivery_method_code" required="true" type="string" hint="Comma-delimeted string of alert_delivery_method_codes to be deleted from the application database."/>
		<cfreturn this.AlertSubscriptionDelegate.deleteAlertSubscriptionDeliveryMethods(argumentCollection=arguments)>
	</cffunction>
	
</cfcomponent>