<cfcomponent displayname="Alert Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Alert information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setAlertDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage Alert data (dependency injection).">
		<cfargument name="bean" type="mce.e2.alerting.db.AlertDelegate" hint="Describes the *.cfc used to manage database interactions related to Alert information.">
		<cfset this.AlertDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyAlertVo"
				access="public" returntype="mce.e2.alerting.vo.Alert"
				hint="This method is used to retrieve / return an empty Alert information value object.">
		<cfreturn this.AlertDelegate.getEmptyAlertComponent()>
	</cffunction>

	<cffunction name="getAlerts"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Alerts.">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_uid" required="false" type="string"/>
		<cfargument name="alert_subscription_uid" required="false" type="string"/>
		<cfargument name="client_company_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string"/>
		<cfargument name="alert_type_code" required="false" type="string"/>
		<cfargument name="alert_status_code" required="false" type="string"/>

		<cfreturn this.AlertDelegate.getAlertsAsArrayOfComponents(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="getAlert"
				access="public" returntype="mce.e2.alerting.vo.Alert"
				hint="This method is used to return a populated Alert vo (value object).">
		<cfargument name="Alert" type="mce.e2.alerting.vo.Alert" required="true" hint="Describes the Alert VO object containing the properties of the Alert to be retrieved." />
		<cfreturn this.AlertDelegate.getAlertAsComponent(alert_uid=arguments.Alert.alert_uid)>
	</cffunction>

	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="saveNewAlert"
				access="public" returntype="string"
				hint="This method is used to persist a new Alert record to the application database.">
		<cfargument name="Alert" type="mce.e2.alerting.vo.Alert" required="true" hint="Describes the Alert VO object containing the details of the Alert to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.Alert = setAuditProperties(arguments.Alert, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.Alert = setPrimaryKey(local.Alert)>

		<!--- Save the modified Alert --->
		<cfset this.AlertDelegate.saveNewAlert(Alert=local.Alert)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.Alert.alert_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewAlert"
				access="public" returntype="mce.e2.alerting.vo.Alert"
				hint="This method is used to persist a new Alert record to the application database.">
		<cfargument name="Alert" type="mce.e2.alerting.vo.Alert" required="true" hint="Describes the Alert VO object containing the details of the Alert to be saved and returned." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewAlert(Alert=arguments.Alert)>

		<!--- Set the primary key --->
		<cfset arguments.Alert.alert_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.Alert>

	</cffunction>

	<cffunction name="saveExistingAlert"
				access="public" returntype="mce.e2.alerting.vo.Alert"
				hint="This method is used to save an existing Alert information to the application database.">
		<cfargument name="Alert" type="mce.e2.alerting.vo.Alert" required="true" hint="Describes the Alert VO object containing the details of the Alert to be modified." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.Alert = setAuditProperties(arguments.Alert, "modify")>

		<!--- Save the modified Alert --->
		<cfset this.AlertDelegate.saveExistingAlert(Alert=local.Alert)/>

		<!--- Return the modified Alert --->
		<cfreturn local.Alert>

	</cffunction>

</cfcomponent>