<cfcomponent name="Base Database Service Component"
			 output="true"
			 extends="mce.e2.alerting.common.BaseComponent"
			 hint="This component is the base data service component for all service management *.cfc's.">
						
	<!--- Create the *.cfc constructor --->		
	<cffunction name="init"
			 	access="public" returntype="any"
			 	hint="This is the constructor for the base data service component.">
		
		<!--- Return an instance of the component --->		
		<cfreturn this>

	</cffunction>

	<!--- Create any common / shareed service methods --->
	<cffunction name="setAuditProperties"
				access="private" returntype="any"
				hint="This method is used to update the audit properties for any given value object (vo) using information from the active session to audit vo changes.">
				
		<!--- Define the method arguments --->
		<cfargument name="voObject" type="any" required="true" hint="Describes the value object whose audit properties are being updated.">
		<cfargument name="updateType" type="string" required="true" default="create" hint="Describes the type of update being performed (create or modify).">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<!--- Create an instance of the security service --->
		<cfset local.securityService = request.beanFactory.getBean("SecurityService")>				 	

		<!--- Retrieve the current session --->
		<cfset local.session = local.securityService.getSession()>
		
		<!--- If no valid user is defined, then ensure that the auditLabel reflects that --->
		<cfif len(local.session.auditUserLabel) eq 0>
			
			<!--- Set the audit label to the "user not known" audit message --->		
			<cfset local.session.auditUserLabel = local.session.getUnidentifiedUserLabel()>
		
		</cfif>
		
		<!--- Switch / case through the different update types --->
		<cfswitch expression="#arguments.updateType#">
		
			<cfcase value="create">
			
				<!---- Default the active status --->
				<cfset arguments.voObject.is_active = 1>
			
				<!--- Set the creation audit properties --->
				<cfset arguments.voObject.created_date = createOdbcDateTime(now())>
				<cfset arguments.voObject.created_by = local.session.auditUserLabel>
			
				<!--- Set the modification audit properties --->
				<cfset arguments.voObject.modified_date = arguments.voObject.created_date>
				<cfset arguments.voObject.modified_by = arguments.voObject.created_by>

			</cfcase>

			<cfcase value="modify">
			
				<!--- Set the modification audit properties --->
				<cfset arguments.voObject.modified_date = createOdbcDateTime(now())>
				<cfset arguments.voObject.modified_by = local.session.auditUserLabel>
						
			</cfcase>
		
		</cfswitch>

		<!--- Return the value object --->
		<cfreturn arguments.voObject>
				
	</cffunction>			

	<cffunction name="setPrimaryKey"
				access="private" returntype="any"
				hint="This method takes a given value object, and determines if the primary key for the object has been set.  If not, it sets the primary key.">
		<cfargument name="voObject" type="any" required="true" hint="Describes the value object whose primary key will be set.">
		<cfargument name="primaryKey" type="string" required="false" hint="Describes the primary key column / property to be set.">
		
		<!--- Was a primary key property specified? --->
		<cfif structKeyExists(arguments, 'primaryKey')>
			
			<!--- If so, then map this property to be the primary key and set that value --->
			<cfset arguments.voObject.primaryKey = arguments.primaryKey>

		</cfif>
		
		<!--- Only process this if the primary key is defined --->
		<cfif structKeyExists(arguments.voObject, "primaryKey")>

			<!--- Is the primary key not a unique identifier? --->
			<cfif not isUniqueIdentifier(arguments.voObject[arguments.voObject.primaryKey])>

				<!--- If not, then set / create the unique identifier and populate the primary key --->
				<cfset arguments.voObject[arguments.voObject.primaryKey] = createUniqueIdentifier()>

			</cfif>

		</cfif>

		<!--- Return the value object --->
		<cfreturn duplicate(arguments.voObject)>

	</cffunction>

	<cffunction name="deActivate"
				access="private" returntype="any"
				hint="This method takes a given value object, and sets the active status to 0.">
		<cfargument name="voObject" type="any" required="true" hint="Describes the value object that will be deactivated.">
		
		<!--- Only process this if the primary key is defined --->
		<cfif structKeyExists(arguments.voObject, "is_active")>

			<!--- If so, then deactivate the object. --->
			<cfset arguments.voObject.is_active = 0>

		</cfif>

		<!--- Return the value object --->
		<cfreturn duplicate(arguments.voObject)>

	</cffunction>

	<cffunction name="reviewAssociations"
				access="public"
				returnType="struct"
				hint="This method is used to compare processable associations between two object types against the actual associations in the application database.  It returns a structure containing the associations to add, and the associations to remove.">

		<!--- Define the method arguments --->
		<cfargument name="associatedObjects" required="true" type="array" hint="Describes the array representing the actual database / system object associations that will be compared.">
		<cfargument name="associationsToProcess" required="true" type="array" hint="Describes the array representing the associations to compare.">
		<cfargument name="primaryKey" required="true" type="string" hint="Describes the primary key property for a given object that will be used to identify similar objects when performing the object comparison.">
		<cfargument name="compareProperty" required="false" type="string" hint="Describes the the name of a specific property to compare between the objects to determine if an object should be changed / updated.">
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Define the return / output object --->
		<cfset local.output = structNew()>
		
		<!--- Initialize the arrays to store which associations are new / old --->
		<cfset local.output.addArray = arrayNew(1)>
		<cfset local.output.modifyArray = arrayNew(1)>
		<cfset local.output.removeArray = arrayNew(1)>

		<!--- Loop over the associations to process --->
		<cfloop from="1" to="#arraylen(arguments.associationsToProcess)#" index="local.arrayIndex">

			<!--- Create a reference to the current association object --->
			<cfset local.associationToProcess = arguments.associationsToProcess[local.arrayIndex]>
					
			<!--- Default the comparison flags --->		
			<cfset local.associationExists = false>		
					
			<!--- Now, loop over the associated objects and see if the current association is in the associated objects array --->
			<cfloop from="1" to="#arraylen(arguments.associatedObjects)#" index="local.assocArrayIndex">

				<!--- Create a reference to teh current real association --->
				<cfset local.currentAssociation = arguments.associatedObjects[local.assocArrayIndex]>

				<!--- Does this association already exist? --->
				<cfif local.currentAssociation[arguments.primaryKey] eq local.associationToProcess[arguments.primaryKey]>

					<!--- If so, then set the comparative association flag to true --->
					<cfset local.associationExists = true>
					<cfbreak>
										
				</cfif>

			</cfloop>

			<!--- Does the association already exist? --->
			<cfif local.associationExists is true>

				<!--- Was a compare property specified? --->
				<cfif structKeyExists(arguments, 'compareProperty')>

					<!--- Are the comparison properties the same?  If so, then treat the association as new --->
					<cfif local.currentAssociation[arguments.compareProperty] neq local.associationToProcess[arguments.compareProperty]>
						<cfset arrayAppend(local.output.modifyArray, duplicate(local.associationToProcess))>	
					</cfif>

				<!--- Only append the association if a change exists in the association status --->
				<cfelseif local.associationToProcess.isAssociated eq 0 and (local.associationToProcess.isAssociated neq local.currentAssociation.isAssociated)>
					<cfset arrayAppend(local.output.removeArray, duplicate(local.associationToProcess))>	
				</cfif>
			
			<cfelse>

				<!--- Otherwise, append the association to the new association array --->
				<cfif local.associationToProcess.isAssociated eq 1>
					<cfset arrayAppend(local.output.addArray, duplicate(local.associationToProcess))>
				</cfif>
						
			</cfif>

		</cfloop>

		<!--- return the output object containing the two arrays --->
		<cfreturn local.output>

	</cffunction>

	<cffunction name="buildVoPropertyListFromArray"
				access="public"
				returnType="string"
				hint="This method is used to build a list of values for a given property from an array of Vo objects.">

		<!--- Define the method arguments --->
		<cfargument name="voArray" required="true" type="array" hint="Describes the array representing the associations to compare.">
		<cfargument name="propertyName" required="true" type="string" hint="Describes the array representing the actual database / system object associations that will be compared.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the return result --->
		<cfset local.returnResult = ''>

		<!--- Loop over the voArray --->
		<cfloop from="1" to="#arrayLen(arguments.voArray)#" index="local.arrayIndex">

			<!--- Append the property to the return list --->
			<cfset local.returnResult = listAppend(local.returnResult, arguments.voArray[local.arrayIndex][arguments.propertyName])>

		</cfloop>
		
		<!--- Return the list --->
		<cfreturn local.returnResult>
		
	</cffunction>

	<!--- Define any private methods --->
	<cffunction name="validateVo"
				access="private"
				returnType="void"
				hint="This  method is used to validate that the required fields in a given Vo are provided.">
		<cfargument name="voObject" type="any" required="true" hint="Describes the value object whose properties will be validated.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Default the error flag --->
		<cfset local.hasError = false>

		<!--- Are any required properties defined? --->
		<cfif structKeyExists(arguments.voObject, "requiredProperties")>

			<!--- Loop over the required properties --->
			<cfloop list="#structkeyList(arguments.voObject)#" index="local.thisProperty">

				<!--- Was the required property not defined? --->
				<cfif not structKeyExists(arguments.voObject, local.thisProperty)>
	
					<!--- If not, then throw an error --->
					<cfset local.hasError = true>
					<cfset local.errorMessage = "#local.thisProperty# not found in the value object being validated.">
					<cfbreak>
	
				</cfif>

				<!--- Does the required property actually have a value? --->
				<cfif structKeyExists(arguments.voObject, local.thisProperty) and len(arguments.voObject[local.thisProperty]) eq 0>
	
					<!--- If not, then throw an error --->
					<cfset local.hasError = true>
					<cfset local.errorMessage = "#local.thisProperty# was found, but did not contain a value in the value object being validated.">
					<cfbreak>
	
				</cfif>
	
			</cfloop>

			<!--- Was an error found? --->
			<cfif local.hasError eq true>
	
				<!--- Log the error and throw an exception --->
				<cfset request.e2.logger.error("Data Validation Error", local.errorMessage)>
	
			</cfif>
	
		</cfif>

	</cffunction>

	<cffunction name="removeNullPropertiesFromVo"
				access="public"
				returnType="any"
				hint="This  method is used to remove null / empty properties from a given Vo.">
		<cfargument name="voObject" type="any" required="true" hint="Describes the value object whose properties will be evaluated.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Duplicate the vo object --->
		<cfset local.vo = duplicate(arguments.voObject)>

		<!--- Loop over the vo Object and evaluate each property --->
		<cfloop collection="#arguments.voObject#" item="local.thisProperty">

			<!--- Is the current property a simple value? --->
			<cfif isSimplevalue(arguments.voObject[local.thisProperty])>

				<!--- Was there a value for the property, or was a null value provided? --->
				<cfif len(arguments.voObject[local.thisProperty]) eq 0 or arguments.voObject[local.thisProperty] eq 'NaN' or arguments.voObject[local.thisProperty] eq 0>

					<!--- If not, then delete the property from the vo --->
					<cfset structDelete(local.vo, local.thisProperty)>

				</cfif>

			</cfif>

		</cfloop>

		<!---- Return the modified vo --->
		<cfreturn local.vo>

	</cffunction>

</cfcomponent>