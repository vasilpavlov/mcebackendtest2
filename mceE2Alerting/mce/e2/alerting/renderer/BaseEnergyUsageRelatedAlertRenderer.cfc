<cfcomponent
	extends="mce.e2.alerting.renderer.BaseAlertRenderer"
	implements="mce.e2.alerting.renderer.IAlertRenderer"
	output="false">

	<cfset this.lookupDelegate = application.beanFactory.getBean("EnergyUsageRelatedDelegateForAlerts")>


	<!--- Give the renderer an opportunity to skip processing of subscriptions that it knows could not be relevant right now --->
	<cffunction name="preFilterAlertSubscriptionList"
				access="public" returntype="string"
			 	hint="Given a list of subscription uids, filters the list to only include the subscriptions that may be relevant at this time.">
		<cfargument name="alert_subscription_uid_list" type="string" required="true">

		<cfreturn this.lookupDelegate.getSubcriptionsThatHaveChangedEnergyUsageRecordsSinceSubscriptionWasLastPolled(alert_subscription_uid_list)>
	</cffunction>


	<!--- Used internally --->
	<cffunction name="getEnergyUsageData" access="package" returntype="query">
		<cfargument name="fromDate" type="date" required="false">
		<cfargument name="thruDate" type="date" required="false">
		<cfargument name="usage_type_code" type="string" required="false" default="actual">
		<cfargument name="include_budgeted_cost" type="boolean" required="false" default="false">

		<!--- Add list of energy accounts to the named arguments --->
		<cfset arguments.energy_account_uid_list = getFieldListFromVoArray(this.AlertSubscription.accounts, "energy_account_uid")>

		<!--- The profile lookup code is used for emissions calculations, etc --->
		<cfset arguments.profile_lookup_code = this.AlertSubscription.profile_lookup_code>

		<!--- Pass the arguments (which may or may not include the from/thru dates) to the delegate method --->
		<cfreturn this.lookupDelegate.getEnergyUsageDataForUsageAlerts(argumentCollection=arguments)>
	</cffunction>

</cfcomponent>