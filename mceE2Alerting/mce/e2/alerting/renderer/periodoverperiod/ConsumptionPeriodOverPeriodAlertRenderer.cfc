<cfcomponent
	extends="BasePeriodOverPeriodAlertRenderer"
	implements="mce.e2.alerting.renderer.IAlertRenderer"
	output="false">

	<!--- The "column of interest" for calculations; see this renderer's superclass for details --->
	<!---
		Special note for this renderer:
		Note that our "conceptDataField" specifies doing comparisons in BTUs to determine if the alert will be sent.
		If appropriate, our rendering logic herein will use the "native" (non-BTU) recorded_value instead in the text of the alert as presented to the user.
	 --->
	<cfset this.conceptDataField = "recordedValueAsBtu">

	<cffunction name="renderAlerts"
				access="public" returntype="array"
			 	hint="Renders the alert(s) to be sent and persisted.">

		<cfset var result = ArrayNew(1)>
		<cfset var thisAlert = createAlertInstance()>

		<!--- Percentage change could be positive or negative; let's express in human-readable format depending --->
		<cfset var verbString = IIF(this.data.percentDifference gte 0, "'increased'", "'decreased'")>
		<cfset var percentChange = Abs(this.data.percentDifference)>
		<cfset var unitString = "BTUs">

		<!--- The data we have in this.data.recentValue etc is in BTUs; if it's appropriate to show in "native" unit then get that info --->
		<cfif this.data.considerSingleEnergyType>
			<cfset this.data.recentValue = super.queryQuickSum(this.data.recentData, "recorded_value")>
			<cfset this.data.priorValue = super.queryQuickSum(this.data.priorData, "recorded_value")>
			<cfset unitString = this.data.highLevelData.energyUnitFriendlyName>
		</cfif>

		<!--- Render alert "content" --->
		<cfsavecontent variable="thisAlert.alert_content">
			<cfoutput>Consumption has #verbString# by #NumberFormat(percentChange, '9.9')#%</cfoutput>
		</cfsavecontent>

		<!--- Render alert "body" --->
		<cfsavecontent variable="thisAlert.alert_body">
			<cfoutput>
				The newly-entered consumption (for #formatDatePair(this.data.startOfRecentPeriod, this.data.endOfRecentPeriod)#) is #NumberFormat(this.data.recentValue, ",9")# #unitString#.
				The previous consumption (from #formatDatePair(this.data.startOfPriorPeriod, this.data.endOfPriorPeriod)#) was #NumberFormat(this.data.priorValue, ",9")# #unitString#.
			</cfoutput>
		</cfsavecontent>

		<!--- Return the "rendered" alert --->
		<cfset ArrayAppend(result, thisAlert)>
		<cfreturn result>
	</cffunction>


</cfcomponent>