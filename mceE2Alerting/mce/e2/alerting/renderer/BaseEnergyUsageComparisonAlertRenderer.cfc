<cfcomponent
	extends="mce.e2.alerting.renderer.BaseEnergyUsageRelatedAlertRenderer"
	implements="mce.e2.alerting.renderer.IAlertRenderer"
	output="false">

	<!--- Here we are decorating the initial alert instance returned by the base renderer --->
	<cffunction name="createAlertInstance"
				access="package" returntype="mce.e2.alerting.vo.Alert"
			 	hint="Configures a new alert to be sent and persisted.">

		<!--- First call our ancestor's logic --->
		<cfset var alert = super.createAlertInstance()>

		<!--- Get high-level info like number of energy types, number of properties, etc., covered by this subscription --->
		<cfset var qHighLevel = super.getHighLevelAccountInfo()>

		<!--- Keep the high-level info on this alert instance for now (this is not persisted) --->
		<cfset this.data.highLevelData = qHighLevel>

		<!--- Make a note as to whether we want to consider this a "single" energy type --->
		<cfset this.data.considerSingleEnergyType = (qHighLevel.energyTypeCount eq 1) and (qHighLevel.energyUnitCount eq 1)>

		<!--- Remember some common data in the "notes" type fields --->
		<!--- (These are persisted when the alert is persisted, if subclasses don't overwrite these values) --->
		<cfset alert.alert_location_notes = qHighLevel.propertyFriendlyName>
		<cfset alert.alert_context_notes = qHighLevel.energyTypeFriendlyName>
		<cfset alert.alert_subject_notes = qHighLevel.energyProviderFriendlyName>

		<!--- Return the result --->
		<cfreturn alert>
	</cffunction>


	<!--- Return a number of minutes to delay the visibility of this alert --->
	<cffunction name="getAlertVisibilityDelay" access="public" returntype="numeric"	hint="The date when the alert can be considered to be visible to the outside world">
		<cfreturn 30>
	</cffunction>


</cfcomponent>