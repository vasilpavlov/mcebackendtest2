<cfcomponent
	extends="mce.e2.alerting.renderer.BaseEnergyUsageRelatedAlertRenderer"
	implements="mce.e2.alerting.renderer.IAlertRenderer">


	<!--- Determine whether an alert should be spawned, based on the conditions in the subscription --->
	<cffunction name="isAlertConditionsMet"
				access="public" returntype="boolean"
			 	hint="Returns whether the alert subscription's conditions have been met; if false, then the alert should not be spawned at this time.">

		<!--- This type of alert does not have any "conditions" conceptually, so always answer "true" --->
		<cfreturn true>
	</cffunction>


	<!--- Internal function --->
	<cffunction name="getEnergyAccountsDueForDataEntry" returntype="query" access="package">
		<cfargument name="elapsedDays" type="numeric" required="true">
		<cfargument name="energyAccountUidList" type="string" required="false" hint="If not provided, will find relevant accounts on its own">

		<cfset var qRecords = "">
		<cfset var comparisonDate = "">

		<!--- Let's get the absolute value so we don't get tripped up of there is a negative number provided for the "elapsed days" --->
		<cfset arguments.elapsedDays = Abs(arguments.elapsedDays)>

		<cfif not StructKeyExists(arguments, "energyAccountUidList")>
			<cfset comparisonDate = DateAdd("d", -arguments.elapsedDays, now())>
			<cfset arguments.energyAccountUidList = getAccountsByLastGenerated(comparisonDate, "before")>
		</cfif>

		<cfinvoke
			component="#this.lookupDelegate#"
			method="getEnergyAccountsDueForDataEntry"
			energy_account_uid_list="#arguments.energyAccountUidList#"
			elapsed_days="-#arguments.elapsedDays#"
			returnVariable="qRecords">

		<cftrace category="Alerting" text="#qRecords.recordCount# energy account records pulled for subscription #this.AlertSubscription.alert_subscription_uid#">

		<cfreturn qRecords>
	</cffunction>

	<!--- Internal function --->
	<cffunction name="getEnergyAccountsDueForDataEntryE2" returntype="query" access="package">
		<cfargument name="elapsedDays" type="numeric" required="true">
		<cfargument name="energyAccountUidList" type="string" required="false" hint="If not provided, will find relevant accounts on its own">

		<cfset var qRecords = "">
		<cfset var comparisonDate = "">

		<!--- Let's get the absolute value so we don't get tripped up of there is a negative number provided for the "elapsed days" --->
		<cfset arguments.elapsedDays = Abs(arguments.elapsedDays)>

		<cfif not StructKeyExists(arguments, "energyAccountUidList")>
			<cfset comparisonDate = DateAdd("d", -arguments.elapsedDays, now())>
			<cfset arguments.energyAccountUidList = getAccountsByLastGenerated(comparisonDate, "before")>
		</cfif>

		<cfinvoke
			component="#this.lookupDelegate#"
			method="getEnergyAccountsDueForDataEntryE2"
			elapsed_days="#arguments.elapsedDays#"
			returnVariable="qRecords">

		<cftrace category="Alerting" text="#qRecords.recordCount# energy account records pulled for subscription #this.AlertSubscription.alert_subscription_uid#">

		<cfreturn qRecords>
	</cffunction>

	<!--- Internal function --->
	<cffunction name="getAppropriateEnergyUsageRecordsSinceLastTimeSubscriptionWasPolled" returntype="query" access="package">
		<cfargument name="usage_status_code" type="string" required="true">

		<cfset var qRecords = "">
		<cfset var energyAccountUidList = getFieldListFromVoArray(this.AlertSubscription.accounts, "energy_account_uid")>
		<cfset var usageTouchedDateFrom = this.AlertSubscription.last_polled>

		<!--- If there is no last-polled date, this is probably a new subscription so let's use the create-date --->
		<cfif usageTouchedDateFrom eq "">
			<cfset usageTouchedDateFrom = this.AlertSubscription.created_date>
		</cfif>

		<cfinvoke
			component="#this.lookupDelegate#"
			method="getEnergyUsageRecordsForUsageEntryAlerts"
			energy_account_uid_list="#energyAccountUidList#"
			usage_status_code="#usage_status_code#"
			usageTouchedDateFrom="#usageTouchedDateFrom#"
			returnVariable="qRecords">

		<cftrace category="Alerting" text="#qRecords.recordCount# energy usage records pulled for subscription #this.AlertSubscription.alert_subscription_uid#">

		<cfreturn qRecords>
	</cffunction>

</cfcomponent>