<cfcomponent
	extends="mce.e2.alerting.renderer.BaseEnergyUsageComparisonAlertRenderer"
	implements="mce.e2.alerting.renderer.IAlertRenderer"
	output="false">



	<!--- Determine whether an alert should be spawned, based on the conditions in the subscription --->
	<cffunction name="isAlertConditionsMet"
				access="public" returntype="boolean"
			 	hint="Returns whether the alert subscription's conditions have been met; if false, then the alert should not be spawned at this time.">

		<!--- Create "this.data" structure... because it is on "this", the "render" method in the subclasses can access these values without re-querying --->
		<cfset this.data = structNew()>
		<cfset this.data.percentDifference = 0>

		<!--- Dates that we are interested in --->
		<cfset this.data.endOfPeriod   = super.firstOfMonth(now())>
		<cfset this.data.startOfPeriod = DateAdd("m", -getConditionValue("periodLengthMonths"), this.data.endOfPeriod)>

		<!--- Data that we need --->
		<cfset this.data.usageData = getEnergyUsageData(this.data.startOfPeriod, this.data.endOfPeriod, "actual", true)>

		<!--- Do some simple calculations --->
		<cfset this.data.actualValue = super.queryQuickSum(this.data.usageData, this.conceptDataField)>
		<cfset this.data.budgetedValue = super.queryQuickSum(this.data.usageData, this.budgetedDataField)>

		<!--- Trace statement for debugging purposes --->
		<cftrace
			category="Alerting"
			text="Checking conditions for #DateFormat(this.data.startOfPeriod)# to #DateFormat(this.data.endOfPeriod)#, data to compare is #this.data.actualValue# (actual) versus #this.data.budgetedValue# budget.">

		<!--- Stop here if there is no comparison value (logically it doesn't make sense to continue, and also we'd have divide-by-zero problems) --->
		<cfif this.data.budgetedValue eq 0>
			<cfreturn false>
		</cfif>

		<!--- Now we know enough to calculate the difference between the two values --->
		<cfset this.data.percentDifference = ((this.data.actualValue - this.data.budgetedValue) / this.data.budgetedValue) * 100>

		<!--- Now we can decide if the conditions were met --->
		<cfif Abs(this.data.percentDifference) gte getConditionValue("changeThresholdPercentage")>
			<!--- Return "true" to indicate that conditions have been met and we want to move to the rendering phase --->
			<cfreturn true>
		</cfif>

		<!--- If we get here, the conditions must not have been met --->
		<cfreturn false>
	</cffunction>



</cfcomponent>