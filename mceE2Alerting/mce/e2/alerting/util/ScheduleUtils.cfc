<cfcomponent hint="Utility functions to maintain ColdFusion scheduled tasks" output="false">

	<cfproperty name="schedules" type="array" hint="">

	<cffunction name="init" returntype="mce.e2.alerting.util.ScheduleUtils" hint="I am the constructor method; I return an instance of this class.">
		<cfargument name="importedFilename" type="string" required="true" hint="Configuration XML file path">

		<cfset var xml = "">
		<cfset var xmlNode = "">
		<cfset var schedule = "">
		<cfset this.schedules = arrayNew(1)>

		<cfif not fileExists(arguments.importedFilename)>
			<cfset arguments.importedFilename = expandPath(arguments.importedFilename)>
		</cfif>

		<cfif not fileExists(arguments.importedFilename)>
			<cfthrow message="The file #arguments.importedFilename# does not exist!"
				detail="You have tried to use or include a file (#arguments.importedFilename#) that does not exist using either absolute, relative, or mapped paths." />
		</cfif>

		<cfset application.e2.logger.info("[ScheduleUtils.createTask] Updating scheduled task from file: " & arguments.importedFilename)>

		<cffile action="read" file="#arguments.importedFilename#" variable="fileContent" />
		<cfset xml = xmlParse(fileContent)>

		<cfloop from="1" to="#arrayLen(xml.schedules.XmlChildren)#" index="i">
			<cfset xmlNode = xml.schedules.schedule[i]>

			<!--- If the appropriate values don't exist, throw an error --->
			<cfif isDefined("xmlNode.task") and isDefined("xmlNode.interval") and isDefined("xmlNode.url") and
				xmlNode.task neq "" and xmlNode.interval neq "" and xmlNode.url neq "">
				<cfset schedule = structNew()>
				<cfset structInsert(schedule, "task", makeScheduleUnique(xmlNode.task.XmlText))>
				<cfset structInsert(schedule, "interval", xmlNode.interval.XmlText)>
				<cfset structInsert(schedule, "url", xmlNode.url.XmlText)>
				<cfset arrayAppend(this.schedules, schedule)>
			<cfelse>
				<cfthrow message="The file #arguments.importedFilename# does not have the appropriate parameters."
					detail="The XML file must have the following nodes; task, interval, and xml." />
			</cfif>
		</cfloop>

		<cfreturn this>
	</cffunction>

	<cffunction name="createSchedules" returntype="void" hint="Utility function to create ColdFusion scheduled tasks.">
		<!--- Use "update" to create or update the scheduled task --->
		<cfloop from="1" to="#arrayLen(this.schedules)#" index="i">
			<cfset application.e2.logger.info("[ScheduleUtils.createSchedules] Updating scheduled task: #this.schedules[i].task#")>
			<cfschedule
				action="update"
				task="#this.schedules[i].task#"
				interval="#this.schedules[i].interval#"
				operation="HTTPRequest"
				startDate="#DateFormat(Now(), 'mm/dd/yyyy')#"
				startTime="#TimeFormat(Now(), 'h:m tt')#"
				url="#this.schedules[i].url#">
		</cfloop>
	</cffunction>

	<cffunction name="deleteSchedules" returntype="void" hint="Utility function to delete ColdFusion scheduled tasks.">
		<cfloop from="1" to="#arrayLen(this.schedules)#" index="i">
			<cfset application.e2.logger.info("[ScheduleUtils.deleteSchedules] Deleting scheduled task: #this.schedules[i].task#")>
			<cfschedule action="delete" task="#this.schedules[i].task#">
		</cfloop>
	</cffunction>

	<cffunction name="makeScheduleUnique" returntype="string" hint="I make sure only unique schedule names are added.">
		<cfargument name="task" type="string" required="true" hint="Name of the schedule">

		<cfloop from="1" to="#arrayLen(this.schedules)#" index="i">
			<cfif this.schedules[i].task eq arguments.task>
				<cfset arguments.task = arguments.task & " (#i#)">
			</cfif>
		</cfloop>

		<cfreturn arguments.task>
	</cffunction>


	<!--- Utility function to take an array of alert conditions and return them as a "map" (structure) keyed by code --->
	<!--- This just makes it easier to work with conditions. You can then find out if a condition was provided via StructKeyExists(map, "myCode"), etc --->
	<cffunction name="conditionsAsMapByCode" returntype="struct" output="false">
		<cfargument name="conditions" type="array" required="true">

		<cfset var map = StructNew()>
		<cfset var item = "">

		<cfloop array="#conditions#" index="item">
			<cfset map[item.alert_condition_code] = item>
		</cfloop>

		<cfreturn map>
	</cffunction>

</cfcomponent>