<cfcomponent output="false"
			extends="BaseValueObject"
			alias="mce.e2.alerting.vo.AlertConditionsType">
	
	
	<cfproperty name="alert_condition_code" required="true" type="string"/>
	<cfproperty name="alert_type_code" required="true" type="string"/>
	<cfproperty name="friendly_name" required="true" type="string"/>
	<cfproperty name="default_value" required="true" type="numeric"/>
	
	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="alert_condition_code" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "alert_condition_code"/>
	
</cfcomponent>