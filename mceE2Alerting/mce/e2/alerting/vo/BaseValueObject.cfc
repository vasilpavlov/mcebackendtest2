<cfcomponent displayName="Base Value Object"
			 output="false"
			 hint="This component is the base value component for all value objects.">

	<!--- Define any methods leveraged by the value objects --->
	<cffunction name="createInstanceFromQueryRecord" 
				returntype="mce.e2.alerting.vo.BaseValueObject"
				hint="I am a factory method that creates an instance of whatever class I am called on, filled with data from a row of a query. My implementation comes from BaseValueObject, but I could be overloaded by a particular value object to serve a particular need">
		<cfargument name="qRecords" type="query" required="true" hint="The query from which to get the data for the new VO instance">
		<cfargument name="rowNum" type="numeric" required="true" hint="The query row number from which to get the data for the new VO instance">

		<!--- Create a new instance of this value object class, using simple runtime introspection --->
		<cfset var vo = CreateObject("component", getMetaData(this).fullname)>
		<cfset var col = "">

		<!--- For each column in the specified row of the query --->
		<cfloop list="#qRecords.columnList#" index="col">
			<cfset vo[col] = qRecords[col][qRecords.currentRow]>
		</cfloop>

		<!--- Return the new instance --->
		<cfreturn vo>
		
	</cffunction>

	<cffunction name="compareProperties"
				returntype="boolean"
				hint="This method is used to compare properties between the current value object and another value object.  This method will return true if properties match; false if properties do not match.">
		
		<!--- Define the arguments for this method --->
		<cfargument name="objectToCompare" type="any" required="true" hint="Describes the struct / vo whose properties are being compared to the current /active vo / component.">
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Initialize the output variable --->
		<cfset local.output = true>
		
		<!--- Loop over the properties in the present component --->
		<cfloop collection="#this#" item="local.thisKey">
		
			<!--- Was the property found in the target object? --->
			<cfif not structKeyExists(arguments.objectToCompare, local.thisKey)>
		
				<!--- If not, set the output value and exit the function --->
				<cfset local.output = false>
				<cfbreak>
		
			</cfif>
		
		</cfloop>
		
		<!--- Return the output variable --->
		<cfreturn local.output>
		
	</cffunction>

</cfcomponent>