
<!---

  Template Name:  AlertDelegate
     Base Table:  Alerts

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Thursday, July 16, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Thursday, July 16, 2009 - Template Created.

--->

<cfcomponent	displayname="AlertDelegate"
				hint="This CFC manages all data access interactions for the Alerts table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction	name="getEmptyAlertComponent"
				hint="This method is used to retrieve a Alert object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.alerting.vo.Alert"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.alerting.vo.Alert")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction	name="getAlertsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Alert objects for all the Alerts in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_uid" required="false" type="string"/>
		<cfargument name="alert_subscription_uid" required="false" type="string"/>
		<cfargument name="user_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string"/>
		<cfargument name="alert_type_code" required="false" type="string"/>
		<cfargument name="alert_status_code" required="false" type="string"/>
		<cfargument name="alert_digest_code" required="false" type="string"/>
		<cfargument name="alert_visibility_date" required="false" type="date"/>
		<cfargument name="maxRows" required="false" type="numeric" default="999999999"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Alerts and build out the array of components --->
		<cfset local.qResult = getAlert(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.alerting.vo.Alert")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlertAsComponent"
				hint="This method is used to retrieve a single instance of a / an Alert value object representing a single Alert record."
				output="false"
				returnType="mce.e2.alerting.vo.Alert"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_uid" required="true" type="string"/>
		<cfargument name="alert_subscription_uid" required="false" type="string"/>
		<cfargument name="user_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string"/>
		<cfargument name="alert_type_code" required="false" type="string"/>
		<cfargument name="alert_status_code" required="false" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Alert and build out the component --->
		<cfset local.qResult = getAlert(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.alerting.vo.Alert")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlert"
				hint="This method is used to retrieve single / multiple records from the Alerts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_uid" required="false" type="string"/>
		<cfargument name="alert_subscription_uid" required="false" type="string"/>
		<cfargument name="user_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string"/>
		<cfargument name="alert_type_code" required="false" type="string"/>
		<cfargument name="alert_status_code" required="false" type="string"/>
		<cfargument name="alert_digest_code" required="false" type="string"/>
		<cfargument name="recipient_address" required="false" type="string"/>
		<cfargument name="alert_content" required="false" type="string"/>
		<cfargument name="alert_due_date" required="false" type="date"/>
		<cfargument name="alert_visibility_date" required="false" type="date"/>
		<cfargument name="alert_delivery_method_code" required="false" type="string"/>
		<cfargument name="is_active" required="false" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
		<cfargument name="created_by" required="false" type="string" hint="Describes the system user that created a  given record."/>
		<cfargument name="created_date" required="false" type="date" hint="Describes the date that a given record was first created."/>
		<cfargument name="modified_by" required="false" type="string" hint="Describes the system user that last modified a given record."/>
		<cfargument name="modified_date" required="false" type="date" hint="Describes the date that a given record was last modified."/>
		<cfargument name="ORDER_BY" required="false" type="string" default="user-then-visibility-date"/>
		<cfargument name="maxRows" required="false" type="numeric" default="9999999"/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Alert records. --->
		<cfquery timeout="600" name="local.qResult" datasource="#this.datasource#" maxrows="#arguments.maxRows#">

			-- select records from the Alerts table matching the where clause
			select
				alerts.alert_uid,
				alerts.alert_subscription_uid,
				alerts.property_uid,
				alerts.energy_account_uid,
				alerts.alert_type_code,
				alerts.alert_digest_code,
				alerts.alert_status_code,
				alerts.alert_content,
				alerts.alert_body,
				alerts.alert_due_date,
				alerts.alert_visibility_date,
				alerts.alert_generated_date,
				alerts.is_active,
				alerts.alert_location_notes,
				alerts.alert_context_notes,
				alerts.alert_subject_notes,
				alerts.created_by,
				alerts.created_date,
				alerts.modified_by,
				alerts.modified_date,
				users.alert_email as recipient_address,
				subs.friendly_name as alertSubscriptionFriendlyName,
				types.friendly_name,

				-- Property name, or "Multiple Properties" message if the subscription spans properties
				(SELECT
					CASE COUNT(distinct p.property_uid)
						WHEN 0 THEN ''
						WHEN 1 THEN MIN(p.friendly_name)
						ELSE '(Multiple Properties)'
					END
					FROM AlertSubscriptions_EnergyAccounts sea (nolock)
					join EnergyAccounts ea (nolock) on ea.energy_account_uid = sea.energy_account_uid
					join Properties p on p.property_uid = ea.property_uid
					where subs.alert_subscription_uid = sea.alert_subscription_uid
				) AS propertyFriendlyName

			from	dbo.Alerts alerts (nolock)
			inner join dbo.AlertsUsers au (nolock) on au.alert_uid = alerts.alert_uid
			inner join dbo.Users users (nolock) on users.user_uid = au.user_uid
			inner join dbo.AlertSubscriptions subs (nolock) on alerts.alert_subscription_uid = subs.alert_subscription_uid
			inner join dbo.AlertTypes types (nolock) on types.alert_type_code = subs.alert_type_code
			where	1=1

					<cfif structKeyExists(arguments, 'alert_uid')>
						and alerts.alert_uid IN (
							<!--- Dramatic performance benefit from putting the actual list of values in a subquery; Nate unsure of the specific theory behind this but found it via brute force --->
							SELECT list.alert_uid FROM Alerts list WHERE alerts.alert_uid IN (#listQualify(arguments.alert_uid, "'")# )
						)
					</cfif>

					<cfif structKeyExists(arguments, 'alert_subscription_uid')>
					and alerts.alert_subscription_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_subscription_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'user_uid')>
					and au.user_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.user_uid#" list="true" null="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and alerts.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_type_code')>
					and alerts.alert_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_type_code#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_status_code')>
					and alerts.alert_status_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_status_code#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_digest_code')>
					and alerts.alert_digest_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_digest_code#" null="false" list="true"> )
					</cfif>

					<!--- Note that the alert_visibility_date filter is based on a "on or before" test, not an equality test --->
					<cfif structKeyExists(arguments, 'alert_visibility_date')>
					and alerts.alert_visibility_date <= ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.alert_visibility_date#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'recipient_address')>
					and tbl.recipient_address in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.recipient_address#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_content')>
					and alerts.alert_content in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_content#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_due_date')>
					and alerts.alert_due_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.alert_due_date#" list="true" null="false"> )
					</cfif>

					and alerts.is_active in ( 1 )
					

					<cfif structKeyExists(arguments, 'created_by')>
					and alerts.created_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.created_by#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'created_date')>
					and alerts.created_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.created_date#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_by')>
					and alerts.modified_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.modified_by#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_date')>
					and alerts.modified_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_delivery_method_code')>
					and subs.alert_subscription_uid IN (
						select alert_subscription_uid
						FROM AlertSubscriptions_AlertDeliveryMethods meths (nolock)
						where meths.alert_delivery_method_code IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_delivery_method_code#" null="false" list="true">)
					)
					</cfif>

			<cfif isDefined("arguments.ORDER_BY")>
				<cfif arguments.ORDER_BY eq "user-then-visibility-date">
					ORDER BY au.user_uid, alert_visibility_date DESC
				<cfelse>
					<cfthrow message="Unknown value for ORDER_BY argument.">
				</cfif>
			</cfif>
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>



	<!---Describes the methods used to persist / modify Alert data. --->
	<cffunction	name="saveNewAlert"
				hint="This method is used to persist a new Alert record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="Alert" required="true" type="mce.e2.alerting.vo.Alert" hint="Describes the VO containing the Alert record that will be persisted."/>

		<cfset var item = "">
		<cfset var sqlResult = "">

		<!--- Persist the Alert data to the application database --->
		<cfset var insertedAlertUid = this.dataMgr.insertRecord("Alerts", arguments.Alert)/>
		<cftrace category="Alerting" text="Saved new '#Alert.alert_type_code#' alert with id #insertedAlertUid#">

		<!--- Persist the Alert's meta data to the database --->
		<cfif isDefined("arguments.Alert.alertMeta")>
			<cfloop array="#arguments.Alert.alertMeta#" index="item">
				<cfset item.alert_uid = insertedAlertUid>
				<cfset this.dataMgr.insertRecord("AlertsMeta", item)/>
			</cfloop>
		</cfif>

		<!--- Copy associated users from the subscription to the alert record --->
		<cfquery datasource="#this.datasource#" result="sqlResult">
			INSERT INTO
				AlertsUsers(alert_uid, user_uid, alert_email)
			SELECT
				a.alert_uid, u.user_uid, u.alert_email
			FROM
				Users u JOIN
				AlertSubscriptions_Users asu ON u.user_uid = asu.user_uid JOIN
				Alerts a ON a.alert_subscription_uid = asu.alert_subscription_uid
			WHERE
				a.alert_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#insertedAlertUid#">
		</cfquery>

		<cftrace category="Alerting" text="Associated #sqlResult.recordCount# users for alert #insertedAlertUid#">
	</cffunction>


	<cffunction name="getUsersForAlert" returntype="query" output="false">
		<cfargument name="alert_uid" type="string" required="true">

		<cfset var qResult = "">

		<cfquery datasource="#this.datasource#" name="qResult">
			SELECT
				u.user_uid, u.alert_email
			FROM
				AlertsUsers asu (nolock) JOIN
				Users u (nolock) ON asu.user_uid = u.user_uid
			WHERE
				asu.alert_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_uid#">
			ORDER BY
				u.username
		</cfquery>

		<cfreturn qResult>
	</cffunction>


	<cffunction name="updateAlertStatus" returntype="boolean" output="false">
		<cfargument name="alerts" type="array" required="true">
		<cfargument name="alert_status_code" type="string" required="true">

		<cfset var sqlResult = "">
		<cfset var alertUidList = getFieldListFromVoArray(alerts, "alert_uid")>

		<cfquery datasource="#this.datasource#" result="sqlResult">
			UPDATE
				Alerts
			SET
				alert_status_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_status_code#">,
				alert_sent_date = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">
			WHERE
				alert_uid IN ( #ListQualify(alertUidList, "'")# )
		</cfquery>

		<cfreturn sqlResult.recordCount gt 0>
	</cffunction>



	<cffunction	name="saveExistingAlert"
				hint="This method is used to persist an existing Alert record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="Alert" required="true" type="mce.e2.alerting.vo.Alert" hint="Describes the VO containing the Alert record that will be persisted."/>

		<!--- Persist the Alert data to the application database --->
		<cfset this.dataMgr.updateRecord("Alerts", arguments.Alert)/>

	</cffunction>



	<cffunction name="getAlertTypeConditions" returntype="query" access="public">
		<cfargument name="alert_type_code" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				conds.alert_type_code,
				conds.alert_condition_code,
				conds.default_value,
				conds.friendly_name
			FROM
				AlertConditionTypes conds (nolock),
				AlertTypes types (nolock)
			WHERE
				types.alert_type_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.usage_type_code#">
			ORDER BY
				types.friendly_name
		</cfquery>

		<cfreturn qResult>
	</cffunction>

</cfcomponent>