<cfcomponent name="Base Database Delegate"
			 output="false"
			 extends="mce.e2.alerting.common.BaseComponent"
			 hint="This component is the base database delegate component for all data management *.cfc's.">

	<!--- Initialize the properties for the component --->
	<cfproperty name="datasource" type="string" hint="I am the datasource name to use for cfquery calls, etc. I will be set at runtime by the IOC/injection framework.">
	<cfproperty name="dataMgr" type="datamgr.DataMgr" hint="I am an instance of the DataMgr class. I will be set at runtime by the IOC/injection framework.">

	<!--- Constructor --->
	<cffunction name="init" hint="I am the constructor method; I return an instance of this class. I am generally called by the IOC framework." output="false">
		<cfargument name="datasource" type="string" required="true" hint="A CF datasource name">
		<cfset this.datasource = arguments.datasource>
		<cfset this.valueObjectUtils = createObject("component", "mce.e2.util.ValueObjectUtils")>
		<cfreturn this>
	</cffunction>

	<!--- Setter, called via IOC/injection --->
	<cffunction name="setDataMgr" hint="I am a setter, generally called via IOC" output="false">
		<cfargument name="bean" type="datamgr.DataMgr" required="true" hint="An instance of the DataMgr class.">
		<cfset this.dataMgr = bean>
	</cffunction>

	<!--- Utility function to create an instance of a value object --->
	<cffunction name="createValueObject" hint="Convenience method to create an instance of a value object" returntype="mce.e2.alerting.vo.BaseValueObject" output="false">
		<cfargument name="className" type="string" required="true" hint="The class name of the value object to create, generally starting with 'mce.e2.vo.'">
		<cfreturn createObject("component", className)>
	</cffunction>

	<!--- Utility function, can be used to convert a one-row query into a "vo" (value object) --->
	<cffunction name="queryToVo" returntype="any" hint="Convenience method to convert a one-row query into a 'vo' (value object). Use when you have fetched a record from the database and want to return it as a VO instance.">
		<cfargument name="qRecords" type="query" required="true" hint="The query object to convert to a VO">
		<cfargument name="componentName" type="string" required="true" hint="The name of the VO class you want to get back; generaly starting with 'mce.e2.vo'.">
		<!--- Actual processing in utility function --->
		<cfreturn this.valueObjectUtils.queryToVo(argumentCollection=arguments)>
	</cffunction>

	<!--- Utility function, can be used to convert a multi-row query into an array of "vo's" (value objects) --->
	<cffunction name="queryToVoArray" returntype="array">
		<cfargument name="qRecords" type="query" required="true" hint="The query object to convert to VOs">
		<cfargument name="componentName" type="string" required="true" hint="The name of the VO class you want to get back; generaly starting with 'mce.e2.vo'.">
		<cfargument name="maxRows" type="numeric" required="false" default="#qRecords.recordCount#">
		<cfargument name="startRow" type="numeric" required="false" default="1">
		<!--- Actual processing in utility function --->
		<cfreturn this.valueObjectUtils.queryToVoArray(argumentCollection=arguments)>
	</cffunction>


</cfcomponent>