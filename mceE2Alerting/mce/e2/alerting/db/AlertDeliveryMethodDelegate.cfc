
<!---

  Template Name:  AlertDeliveryMethodDelegate
     Base Table:  AlertDeliveryMethods	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, July 16, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, July 16, 2009 - Template Created.		

--->
		
<cfcomponent	displayname="AlertDeliveryMethodDelegate"
				hint="This CFC manages all data access interactions for the AlertDeliveryMethods table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction	name="getEmptyAlertDeliveryMethodComponent"
				hint="This method is used to retrieve a Alert Delivery Method object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.alerting.vo.AlertDeliveryMethod"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.alerting.vo.AlertDeliveryMethod")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction	name="getAlertDeliveryMethodsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Alert Delivery Method objects for all the Alert Delivery Methods in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_delivery_method_code" required="false" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Alert Delivery Methods and build out the array of components --->
		<cfset local.qResult = getAlertDeliveryMethod(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.alerting.vo.AlertDeliveryMethod")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlertDeliveryMethodAsComponent"
				hint="This method is used to retrieve a single instance of a / an Alert Delivery Method value object representing a single Alert Delivery Method record."
				output="false"
				returnType="mce.e2.alerting.vo.AlertDeliveryMethod"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_delivery_method_code" required="true" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Alert Delivery Method and build out the component --->
		<cfset local.qResult = getAlertDeliveryMethod(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.alerting.vo.AlertDeliveryMethod")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlertDeliveryMethod"
				hint="This method is used to retrieve single / multiple records from the AlertDeliveryMethods table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_delivery_method_code" required="false" type="string"/>
		<cfargument name="alert_subscription_uid" required="false" type="string"/>
		<cfargument name="friendly_name" required="false" type="string"/>
		<cfargument name="is_active" required="false" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
		<cfargument name="created_by" required="false" type="string" hint="Describes the system user that created a  given record."/>
		<cfargument name="created_date" required="false" type="date" hint="Describes the date that a given record was first created."/>
		<cfargument name="modified_by" required="false" type="string" hint="Describes the system user that last modified a given record."/>
		<cfargument name="modified_date" required="false" type="date" hint="Describes the date that a given record was last modified."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Alert Delivery Method records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the AlertDeliveryMethods table matching the where clause
			select	tbl.alert_delivery_method_code,
					tbl.friendly_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.AlertDeliveryMethods tbl (nolock)
			where	1=1

					<cfif structKeyExists(arguments, 'alert_delivery_method_code')>
					and tbl.alert_delivery_method_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_delivery_method_code#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'alert_subscription_uid')>
					and tbl.alert_delivery_method_code in 
						( 
							select	alert_delivery_method_code
							from	AlertSubscriptions_AlertDeliveryMethods
							where	alert_subscription_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_subscription_uid#" list="true" null="false">
						) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'is_active')>
					and tbl.is_active in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_active#" list="true" null="false"> ) 
					<cfelse>
					and tbl.is_active in ( 1 )
					</cfif>

					<cfif structKeyExists(arguments, 'created_by')>
					and tbl.created_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.created_by#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'created_date')>
					and tbl.created_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.created_date#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'modified_by')>
					and tbl.modified_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.modified_by#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'modified_date')>
					and tbl.modified_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#" null="false" list="true"> ) 
					</cfif>

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Alert Delivery Method data. --->	
	<cffunction	name="saveNewAlertDeliveryMethod"
				hint="This method is used to persist a new Alert Delivery Method record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertDeliveryMethod" required="true" type="mce.e2.alerting.vo.AlertDeliveryMethod" hint="Describes the VO containing the Alert Delivery Method record that will be persisted."/>

		<!--- Persist the Alert Delivery Method data to the application database --->
		<cfset this.dataMgr.insertRecord("AlertDeliveryMethods", arguments.AlertDeliveryMethod)/>

	</cffunction>

	<cffunction	name="saveExistingAlertDeliveryMethod"
				hint="This method is used to persist an existing Alert Delivery Method record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertDeliveryMethod" required="true" type="mce.e2.alerting.vo.AlertDeliveryMethod" hint="Describes the VO containing the Alert Delivery Method record that will be persisted."/>

		<!--- Persist the Alert Delivery Method data to the application database --->
		<cfset this.dataMgr.updateRecord("AlertDeliveryMethods", arguments.AlertDeliveryMethod)/>

	</cffunction>

</cfcomponent>
