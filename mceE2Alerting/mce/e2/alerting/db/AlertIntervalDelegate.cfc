
<!---

  Template Name:  AlertIntervalDelegate
     Base Table:  AlertIntervals

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Thursday, July 16, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Thursday, July 16, 2009 - Template Created.

--->

<cfcomponent	displayname="AlertIntervalDelegate"
				hint="This CFC manages all data access interactions for the AlertIntervals table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction	name="getEmptyAlertIntervalComponent"
				hint="This method is used to retrieve a Alert Interval object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.alerting.vo.AlertInterval"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.alerting.vo.AlertInterval")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction	name="getAlertIntervalsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Alert Interval objects for all the Alert Intervals in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_interval_code" required="false" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Alert Intervals and build out the array of components --->
		<cfset local.qResult = getAlertInterval(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.alerting.vo.AlertInterval")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlertIntervalAsComponent"
				hint="This method is used to retrieve a single instance of a / an Alert Interval value object representing a single Alert Interval record."
				output="false"
				returnType="mce.e2.alerting.vo.AlertInterval"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_interval_code" required="true" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Alert Interval and build out the component --->
		<cfset local.qResult = getAlertInterval(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.alerting.vo.AlertInterval")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlertInterval"
				hint="This method is used to retrieve single / multiple records from the AlertIntervals table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_interval_code" required="false" type="string"/>
		<cfargument name="alert_type_code" required="false" type="string"/>
		<cfargument name="friendly_name" required="false" type="string"/>
		<cfargument name="is_enabled" required="false" type="boolean"/>
		<cfargument name="queue_priority" required="false" type="any"/>
		<cfargument name="polling_interval" required="false" type="numeric"/>
		<cfargument name="last_polled" required="false" type="date"/>
		<cfargument name="is_active" required="false" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
		<cfargument name="created_by" required="false" type="string" hint="Describes the system user that created a  given record."/>
		<cfargument name="created_date" required="false" type="date" hint="Describes the date that a given record was first created."/>
		<cfargument name="modified_by" required="false" type="string" hint="Describes the system user that last modified a given record."/>
		<cfargument name="modified_date" required="false" type="date" hint="Describes the date that a given record was last modified."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Alert Interval records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the AlertIntervals table matching the where clause
			select	tbl.alert_interval_code,
					tbl.friendly_name,
					tbl.is_enabled,
					tbl.queue_priority,
					tbl.polling_interval,
					tbl.last_polled,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.AlertIntervals tbl (nolock)
			where	1=1

					<cfif structKeyExists(arguments, 'alert_interval_code')>
					and tbl.alert_interval_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_interval_code#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_type_code')>
					and tbl.alert_interval_code in
						(
							select	alert_interval_code
							from	AlertTypes_AlertIntervals
							where	alert_type_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_type_code#" list="true" null="false">
						)
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'is_enabled')>
					and tbl.is_enabled in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_enabled#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'queue_priority')>
					and tbl.queue_priority in ( <cfqueryparam cfsqltype="cf_sql_tinyint" value="#arguments.queue_priority#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'polling_interval')>
					and tbl.polling_interval in ( <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.polling_interval#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'last_polled')>
					and tbl.last_polled in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.last_polled#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'is_active')>
					and tbl.is_active in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_active#" list="true" null="false"> )
					<cfelse>
					and tbl.is_active in ( 1 )
					</cfif>

					<cfif structKeyExists(arguments, 'created_by')>
					and tbl.created_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.created_by#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'created_date')>
					and tbl.created_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.created_date#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_by')>
					and tbl.modified_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.modified_by#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_date')>
					and tbl.modified_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#" null="false" list="true"> )
					</cfif>

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<!---Update the last-polled date for the interval --->
	<cffunction	name="updateLastPolledDate"
				hint="This method is used to update the last-polled date of the interval (to 'now')."
				output="false"
				returnType="boolean"
				access="public">

		<cfargument name="alert_interval_code" required="false" type="string"/>

		<cfset var sqlResult = "">

		<cfquery result="sqlResult" datasource="#this.datasource#">
			UPDATE dbo.AlertIntervals
			SET last_polled = getDate()
			WHERE alert_interval_code IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_interval_code#" list="true">)
		</cfquery>

		<cfreturn sqlResult.recordCount eq 1>
	</cffunction>



	<!---Describes the methods used to persist / modify Alert Interval data. --->
	<cffunction	name="saveNewAlertInterval"
				hint="This method is used to persist a new Alert Interval record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertInterval" required="true" type="mce.e2.alerting.vo.AlertInterval" hint="Describes the VO containing the Alert Interval record that will be persisted."/>

		<!--- Persist the Alert Interval data to the application database --->
		<cfset this.dataMgr.insertRecord("AlertIntervals", arguments.AlertInterval)/>

	</cffunction>

	<cffunction	name="saveExistingAlertInterval"
				hint="This method is used to persist an existing Alert Interval record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertInterval" required="true" type="mce.e2.alerting.vo.AlertInterval" hint="Describes the VO containing the Alert Interval record that will be persisted."/>

		<!--- Persist the Alert Interval data to the application database --->
		<cfset this.dataMgr.updateRecord("AlertIntervals", arguments.AlertInterval)/>

	</cffunction>

</cfcomponent>
