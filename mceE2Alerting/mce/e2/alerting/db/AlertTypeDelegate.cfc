

<!---

  Template Name:  AlertTypeDelegate
     Base Table:  AlertTypes

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Thursday, July 16, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Thursday, July 16, 2009 - Template Created.

--->

<cfcomponent	displayname="AlertTypeDelegate"
				hint="This CFC manages all data access interactions for the AlertTypes table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction	name="getEmptyAlertTypeComponent"
				hint="This method is used to retrieve a Alert Type object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.alerting.vo.AlertType"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.alerting.vo.AlertType")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction	name="getAlertTypesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Alert Type objects for all the Alert Types in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_type_code" required="false" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Alert Types and build out the array of components --->
		<cfset local.qResult = getAlertType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.alerting.vo.AlertType")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlertTypeAsComponent"
				hint="This method is used to retrieve a single instance of a / an Alert Type value object representing a single Alert Type record."
				output="false"
				returnType="mce.e2.alerting.vo.AlertType"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_type_code" required="true" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Alert Type and build out the component --->
		<cfset local.qResult = getAlertType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.alerting.vo.AlertType")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction	name="getAlertType"
				hint="This method is used to retrieve single / multiple records from the AlertTypes table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_type_code" required="false" type="string"/>
		<cfargument name="alert_interval_code" required="false" type="string"/>
		<cfargument name="friendly_name" required="false" type="string"/>
		<cfargument name="description" required="false" type="string"/>
		<cfargument name="cfc_name" required="false" type="string"/>
		<cfargument name="is_active" required="false" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
		<cfargument name="created_by" required="false" type="string" hint="Describes the system user that created a  given record."/>
		<cfargument name="created_date" required="false" type="date" hint="Describes the date that a given record was first created."/>
		<cfargument name="modified_by" required="false" type="string" hint="Describes the system user that last modified a given record."/>
		<cfargument name="modified_date" required="false" type="date" hint="Describes the date that a given record was last modified."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Alert Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the AlertTypes table matching the where clause
			select	tbl.alert_type_code,
					tbl.friendly_name,
					tbl.description,
					tbl.cfc_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.AlertTypes tbl (nolock)
			where	1=1

					<cfif structKeyExists(arguments, 'alert_type_code')>
					and tbl.alert_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_type_code#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'alert_interval_code')>
					and tbl.alert_type_code in
						(
							select	alert_type_code
							from	AlertTypes_AlertIntervals
							where	alert_interval_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_interval_code#" list="true" null="false">
						)
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'description')>
					and tbl.description in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.description#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'cfc_name')>
					and tbl.cfc_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.cfc_name#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'is_active')>
					and tbl.is_active in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_active#" list="true" null="false"> )
					<cfelse>
					and tbl.is_active in ( 1 )
					</cfif>

					<cfif structKeyExists(arguments, 'created_by')>
					and tbl.created_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.created_by#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'created_date')>
					and tbl.created_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.created_date#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_by')>
					and tbl.modified_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.modified_by#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_date')>
					and tbl.modified_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#" list="true" null="false"> )
					</cfif>

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<!---Describes the methods used to persist / modify Alert Type data. --->
	<cffunction	name="saveNewAlertType"
				hint="This method is used to persist a new Alert Type record to the application database."
				output="false"
				returnType="string"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertType" required="true" type="mce.e2.alerting.vo.AlertType" hint="Describes the VO containing the Alert Type record that will be persisted."/>

		<!--- Persist the Alert Type data to the application database --->
		<cfreturn this.dataMgr.insertRecord("AlertTypes", arguments.AlertType)/>
	</cffunction>

	<cffunction	name="saveExistingAlertType"
				hint="This method is used to persist an existing Alert Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertType" required="true" type="mce.e2.alerting.vo.AlertType" hint="Describes the VO containing the Alert Type record that will be persisted."/>

		<!--- Persist the Alert Type data to the application database --->
		<cfset this.dataMgr.updateRecord("AlertTypes", arguments.AlertType)/>

	</cffunction>

	<cffunction	name="saveNewAlertTypeIntervals"
				hint="This method is used to persist new Alert Type Interval records to the application database."
				output="false"
				returnType="boolean"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="alert_type_code" required="true" type="String"/>
		<cfargument name="alert_interval_code" required="true" type="string" hint="Comma-delimeted string of alert_interval_codes to be added to the application database."/>
		<cfargument name="is_sending_enabled" required="false" type="boolean" default="true" hint="Used to populate is_sending_enabled in relationship table"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Persist the Alert Type Interval data to the application database --->
		<cfloop list="#arguments.alert_interval_code#" index="local.i">
			<cfquery datasource="#this.datasource#">
				INSERT INTO AlertTypes_AlertIntervals(alert_type_code, alert_interval_code, is_sending_enabled, relationship_start)
				SELECT <cfqueryparam value="#arguments.alert_type_code#" cfsqltype="cf_sql_varchar">, alert_interval_code,  1, GETDATE()
				FROM AlertIntervals
				WHERE alert_interval_code = <cfqueryparam value="#local.i#" cfsqltype="cf_sql_varchar">
				<!--- Subquery to prevent double-insert --->
				AND alert_interval_code NOT IN
					(SELECT alert_interval_code
					FROM AlertTypes_AlertIntervals
					WHERE alert_type_code = <cfqueryparam value="#arguments.alert_type_code#" cfsqltype="cf_sql_varchar">)
			</cfquery>
		</cfloop>

		<cfreturn true>

	</cffunction>

	<cffunction	name="deleteAlertTypeIntervals"
				hint="This method is used to delete an Alert Type Interval record from the application database."
				output="false"
				returnType="boolean"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertType" required="true" type="mce.e2.alerting.vo.AlertType"/>
		<cfargument name="alert_interval_code" required="true" type="string" hint="Comma-delimeted string of alert_interval_codes to be deleted from the application database."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single / multiple Alert Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- delete records from the AlertTypes_AlertIntervals table matching the where clause
			delete	dbo.AlertTypes_AlertIntervals
			where	alert_type_code = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.AlertType.alert_type_code#" null="false" list="false">
			and		alert_interval_code in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.alert_interval_code#" null="false" list="true"> )

		</cfquery>

		<cfreturn true>

	</cffunction>


	<cffunction name="getAlertTypeConditions" access="public" returntype="query">
		<cfargument name="alert_type_code" type="string" required="true">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
		 	SELECT
				conds.alert_type_code,
				conds.alert_condition_code,
				conds.default_value,
				conds.friendly_name
			FROM
				AlertConditionTypes conds
			WHERE
				conds.alert_type_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.alert_type_code#">
			ORDER BY
				conds.friendly_name
		</cfquery>

		<cfreturn qResult>
	</cffunction>

	<!---Describes the methods used to persist / modify Alert Type data. --->
	<cffunction	name="saveNewAlertConditionType"
				hint="This method is used to persist a new AlertConditionsType record to the application database."
				output="false"
				returnType="string"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AlertConditionsType" required="true" type="mce.e2.alerting.vo.AlertConditionsType"/>

		<!--- Persist the AlertConditionsType data to the application database --->
		<cfreturn this.dataMgr.insertRecord("AlertConditionTypes", arguments.AlertConditionsType)/>
	</cffunction>


</cfcomponent>
