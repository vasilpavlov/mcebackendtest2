<cfcomponent name="Base AlertDelivery Component"
			 output="true"
			 extends="mce.e2.alerting.common.BaseComponent"s
			 hint="This abstract component is the base alert delivery component for all alert delivery *.cfc's - do not implement this component directly.">

	<!--- Create the *.cfc constructor --->
	<cffunction name="init"
			 	access="public" returntype="any"
			 	hint="This is the constructor for the base alert delivery component.">

		<!--- <cfargument name="alertSubscription" required="true" type="mce.e2.alerting.vo.AlertSubscription"> --->
		<cfargument name="alerts" required="true" type="array">
		<cfargument name="alert_email" required="true" type="string">

		<!--- Create the subscription and alerts instance --->
		<!--- <cfset this.alertSubscription = arguments.alertSubscription> --->
		<cfset this.alerts = arguments.alerts>
		<cfset this.alert_email = arguments.alert_email>

		<!--- Return an instance of the component --->
		<cfreturn this>

	</cffunction>

	<cffunction name="alertArrayToQuery" access="package" returntype="query" output="false">
		<cfargument name="alerts" type="array" required="true">
		<cfargument name="columnNames" type="string" required="false"
			default="alertSubscriptionFriendlyName,alert_content,alert_body,alert_location_notes,alert_subject_notes,alert_context_notes,alert_uid,alert_subscription_uid,alert_visibility_date,alert_generated_date">

		<cfset var qAlerts = "">

		<cfinvoke
			component="mce.e2.util.ValueObjectUtils"
			method="objectArrayToQuery"
			objects="#arguments.alerts#"
			columnNames="#arguments.columnNames#"
			returnVariable="qAlerts">

		<cfreturn qAlerts>
	</cffunction>


</cfcomponent>