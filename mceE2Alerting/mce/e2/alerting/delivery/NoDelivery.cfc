<cfcomponent name="Loopback No-Delivery AlertDelivery Component"
			 output="true"
			 extends="BaseDelivery"
			 implements="IDelivery">

	<!--- Delivers the alert with specific information based on the alert delivery method --->
	<cffunction name="deliverAlerts"
				access="public" returntype="any"
			 	hint="Delivers the alerts to their intended destinations.">
		<!--- We simply do nothing here --->
	</cffunction>

</cfcomponent>