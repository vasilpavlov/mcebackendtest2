<cfcomponent name="Delivery Factory Component"
			 output="true"
			 extends="mce.e2.alerting.common.BaseComponent"
			 hint="This component is the delivery factory component for all delivery *.cfc's.">

	<!--- Create the *.cfc constructor --->
	<cffunction name="init"
			 	access="public" returntype="any"
			 	hint="This is the constructor for the delivery factory component.">

		<!--- Return an instance of the component --->
		<cfreturn this>

	</cffunction>

	<!--- Return the appropriate delivery cfc based on the alert delivery type --->
	<cffunction name="getDeliveryMethod"
				access="public" returntype="mce.e2.alerting.delivery.IDelivery"
			 	hint="Returns the proper alert delivery method.">

		<cfargument name="alert_delivery_method_code" required="true" type="string">
		<!--- <cfargument name="alertSubscription" required="true" type="mce.e2.alerting.vo.AlertSubscription"> --->
		<cfargument name="alerts" required="true" type="array">
		<cfargument name="alert_email" required="true" type="string">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- If the delivery method exists, create an instance and return it --->
		<cfset local.path = getDirectoryFromPath(getCurrentTemplatePath()) & "#arguments.alert_delivery_method_code#.cfc">
		<cfif fileExists(local.path)>
			<!--- <cftry> --->
				<cfset local.delivery = createObject("component", arguments.alert_delivery_method_code).init(arguments.alerts, arguments.alert_email)>

				<!--- If there was a problem initializing the component throw an exception - we're relying on the alert component to be properly implemented --->
				<!--- <cfcatch >
					<cfthrow message="The DeliveryFactory component could not create the appropriate delivery method for #arguments.alert_delivery_method_code#.  Please verify the delivery method exists and is created properly.">
				</cfcatch>
			</cftry> --->
		<cfelse>
			<!--- The alert delivery method component didn't exist --->
			<cfthrow errorcode="The DeliveryFactory component could not find the appropriate delivery method for #arguments.alert_delivery_method_code#.  Please verify the delivery method exists.">
		</cfif>

		<cfreturn local.delivery>

	</cffunction>

</cfcomponent>