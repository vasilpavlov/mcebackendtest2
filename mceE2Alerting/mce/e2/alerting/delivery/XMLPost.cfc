<cfcomponent name="XMLPost AlertDelivery Component"
			 output="true"
			 extends="BaseDelivery"
			 implements="IDelivery"
			 hint="This component is the XMLPost alert delivery component.">
	
	<!--- Delivers the alert with specific information based on the alert delivery method --->
	<cffunction name="deliverAlerts"
				access="public" returntype="any"
			 	hint="Delivers the alerts to their intended destinations.">
		
		<!--- Implement functionality to send XML to alert recepient_address --->
		
	</cffunction>
	
</cfcomponent>