<cfcomponent output="false">

	<!--- Constructor --->
	<cffunction name="init" access="public">
		<cfargument name="loggingLevel" type="string" default="info">
		<cfargument name="loggingFile" type="string" default="mceRateModelEngine">
		<cfargument name="defaultExceptionType" type="string" default="mceRateModelException">

		<cfset this.loggingLevel = arguments.loggingLevel>
		<cfset this.loggingFile = arguments.loggingFile>
		<cfset this.defaultExceptionType = arguments.defaultExceptionType>

		<cfset this.levels["info"] = 1>
		<cfset this.levels["warning"] = 2>
		<cfset this.levels["error"] = 3>

		<cfreturn this>
	</cffunction>

	<cffunction name="info" access="public" hint="Use to log an 'info' type message, which is the least severe and generally for developer use in debugging only.">
		<cfargument name="message" type="string" required="true">
		<cftrace text="INFO - #message#">
		<cfset logAsAppropriate("info", message)>
	</cffunction>

	<cffunction name="warning" access="public" hint="Use to log a 'warning' type message, which is the more severe than 'info' but still does not throw an exception.">
		<cfargument name="message" type="string" required="true">
		<cftrace text="WARNING - #message#">
		<cfset logAsAppropriate("warning", message)>
	</cffunction>

	<cffunction name="error" access="public" hint="Use to log an 'error' type message, and also throws a cfml exception">
		<cfargument name="message" type="string" required="true">
		<cfargument name="detail" type="string" required="false" default="">
		<cfargument name="exceptionType" type="string" required="false" default="#this.defaultExceptionType#">
		<cfset logAsAppropriate("error", "#exceptionType# - #message# - #detail#")>
		<cfthrow
			type="#exceptionType#"
			message="#message#"
			detail="#detail#">
	</cffunction>

	<cffunction name="logAsAppropriate" access="private" returntype="void">
		<cfargument name="level" type="string" required="true">
		<cfargument name="message" type="String" required="true">
		<cfif this.levels[level] gte this.levels[this.loggingLevel]>
			<cflog
				text="#message#"
				file="#this.loggingFile#"
				type="#level#">
		</cfif>
	</cffunction>

</cfcomponent>