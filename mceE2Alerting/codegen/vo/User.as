﻿package vo
{
	/**
	* This component is used to define the value object (vo) used to describe the details of a given user
	*/

	[Bindable]
	[RemoteClass(alias="mce.e2.alerting.vo.User")]
	public class User extends BaseValueObject
	{

		/** 	 Fields that directly correspond to database; these should also exist in corresponding actionscript class */

		/** Describes the primary key / unique identifier for a given client company to which each user group is associated to. */
		public var client_company_uid:String;

		/** Describes the primary key / unique identifier for a given user. */
		public var user_uid:String;

		/** Describes the first name of a given user. */
		public var first_name:String;

		/** Describes the last name of a given user. */
		public var last_name:String;

		/** Describes the login / username for a given user. */
		public var username:String;

		/** Describes the password hash method used to encrypt a given user's password. */
		public var password_hash_method:Number;

		/** Describes the primary key of the association record between a user and another / associated object (ex. userGroup, propertyCollection, etc)." */
		public var relationship_id:Number;

		/** 	 Define any non-database table related fields that are needed to render the UI display */

		/** Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)." */
		public var isAssociated:Boolean;

	}

}
