﻿		/**  */

package vo
{
	/**
	* This CFC manages all object/bean/value object instances for the EnergyAccounts table, and is used to pass object instances through the application service layer
	*/

	[Bindable]
	[RemoteClass(alias="mce.e2.vo.EnergyAccount")]
	public class EnergyAccount extends BaseValueObject
	{

		/** 	 Declare and initialize the properties for a given component. */

		/** Describes the primary key / unique identifier for a given energy account definition." */
		public var energy_account_uid:String;

		/** Describes the primary key / unique identifier for an associated energy native account definition." */
		public var native_account_uid:String;

		/** Describes the customer facing / external name for a given energy account." */
		public var friendly_name:String;

		/** Describes the primary key of the property associated to a given energy account." */
		public var property_uid:String;

		/** Describes the primary key of the enery account type associated to a given energy account." */
		public var account_type_uid:String;

		/** Describes the primary key of the enery type associated to a given energy account." */
		public var energy_type_uid:String;

		/** Describes the primary key of the meta group associated to a given energy account." */
		public var meta_group_uid:String;

		/** Describes the primary key of the energy unit associated to a given energy account." */
		public var energy_unit_uid:String;

		/** Describes the primary key of the energy factor alias used to override a given energy account." */
		public var factor_alias_uid_override:String;

		/** Describes the primary key / unique identifier of the associated / parent energy account." */
		public var master_energy_account_uid:String;

		/** 	 Define the secondary / associated properties. */

		/** Describes the account number associated to the energy native account tied to a given energy account." */
		public var native_account_number:String;

		/** Describes the primary key of the energy provider associated to a given energy account." */
		public var energy_provider_uid:String;

		/** Describes the primary key of the rate class associated to a given energy account." */
		public var rate_class_uid:String;

		/** Describes the primary key of the rate model associated to a given energy account." */
		public var rate_model_uid:String;

		/** 	 Define the meta-data for a given value object. */

		/** Describes the primary key for the current object / table. */
		public var primaryKey:String;

		/** Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)." */
		public var isAssociated:Boolean;

		/** 	 Define the friendly names */

		/** Describes the customer facing / external name for a given energy account." */
		public var friendlyEnergyAccountName:String;

		/** Describes the customer facing / external name for a given master / parent energy account." */
		public var friendlyMasterEnergyAccountName:String;

		/** Describes the customer facing / external name for a given property." */
		public var friendlyPropertyName:String;

		/** Describes the customer facing / external name for a given energy account type." */
		public var friendlyEnergyAccountTypeName:String;

		/** Describes the customer facing / external name for a given energy type." */
		public var friendlyEnergyTypeName:String;

		/** Describes the customer facing / external name for a given meta type group." */
		public var friendlyMetaTypeGroupName:String;

		/** Describes the customer facing / external name for a given energy unit." */
		public var friendlyEnergyUnitName:String;

		/** Describes the customer facing / external name for a given energy provider." */
		public var friendlyEnergyProviderName:String;

		/** Describes the customer facing / external name for a given rate class." */
		public var friendlyRateClassName:String;

		/** Describes the customer facing / external name for a given rate factor alias." */
		public var friendlyFactorAliasName:String;

		/** Describes the customer facing / external name for a given rate model." */
		public var friendlyRateModelName:String;

		/** 	 Define the association id values */

		/** Describes the primary key of the energy native account / energy provider rate class association. */
		public var energyNativeAccountEnergyProviderRateClass_relId:Number;

		/** Describes the primary key of the energy provider rate class / rate model association. */
		public var energyProviderRateClassRateModels_relId:Number;

	}

}
