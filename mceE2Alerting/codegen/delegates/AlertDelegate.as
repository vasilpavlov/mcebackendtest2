package delegates
{

	/** The AsyncToken class is a standard class used by system delegates.*/
	import mx.rpc.AsyncToken;

	/** The RemoteObject class is a standard class used by system delegates, and is used to store remoteService method references.*/
	import mx.rpc.remoting.RemoteObject;

	/** The Alert object is used to define an instance of a given alert (value object). */
	import vo.Alert;

	/**
	 * This class is used to manage all remote interactions with the coldFusion alertService.
	 */
	public class AlertDelegate extends BaseServiceDelegate
	{

		/** The alertService defines the coldFusion service that will be interacted with. */
		[Autowire(bean="alertService")]
		public var alertService : RemoteObject;

		/** This method is used to set the database delegate used to manage Alert data (dependency injection).
		 *  @param bean Describes the *.cfc used to manage database interactions related to Alert information.
		 *  @returns bean:AlertDelegate A bean:AlertDelegate value object populated with the properties used to modify the specified bean:alertdelegate instance.
		*/
		public function setAlertDelegate(bean:AlertDelegate) : AsyncToken {
			return alertService.setAlertDelegate(bean);
		}

		/** This method is used to retrieve / return an empty Alert information value object.
		*/
		public function getEmptyAlertVo() : AsyncToken {
			return alertService.getEmptyAlertVo();
		}

		/** This method is used to retrieve / return a collection of Alerts.
		 *  @param alert_uid
		 *  @param alert_subscription_uid
		 *  @param client_company_uid
		 *  @param property_uid
		 *  @param alert_type_code
		 *  @param alert_status_code
		 *  @returns alert_uid:String,alert_subscription_uid:String,client_company_uid:String,property_uid:String,alert_type_code:String,alert_status_code:String A alert_uid:String,alert_subscription_uid:String,client_company_uid:String,property_uid:String,alert_type_code:String,alert_status_code:String value object populated with the properties used to modify the specified alert_uid:string,alert_subscription_uid:string,client_company_uid:string,property_uid:string,alert_type_code:string,alert_status_code:string instance.
		*/
		public function getAlerts(alert_uid:String,alert_subscription_uid:String,client_company_uid:String,property_uid:String,alert_type_code:String,alert_status_code:String) : AsyncToken {
			return alertService.getAlerts(alert_uid,alert_subscription_uid,client_company_uid,property_uid,alert_type_code,alert_status_code);
		}

		/** This method is used to return a populated Alert vo (value object).
		 *  @param Alert Describes the Alert VO object containing the properties of the Alert to be retrieved.
		 *  @returns alert:Alert A alert:Alert value object populated with the properties used to modify the specified alert:alert instance.
		*/
		public function getAlert(alert:Alert) : AsyncToken {
			return alertService.getAlert(alert);
		}

		/** This method is used to persist a new Alert record to the application database.
		 *  @param Alert Describes the Alert VO object containing the details of the Alert to be saved.
		 *  @returns alert:Alert A alert:Alert value object populated with the properties used to modify the specified alert:alert instance.
		*/
		public function saveNewAlert(alert:Alert) : AsyncToken {
			return alertService.saveNewAlert(alert);
		}

		/** This method is used to persist a new Alert record to the application database.
		 *  @param Alert Describes the Alert VO object containing the details of the Alert to be saved and returned.
		 *  @returns alert:Alert A alert:Alert value object populated with the properties used to modify the specified alert:alert instance.
		*/
		public function saveAndReturnNewAlert(alert:Alert) : AsyncToken {
			return alertService.saveAndReturnNewAlert(alert);
		}

		/** This method is used to save an existing Alert information to the application database.
		 *  @param Alert Describes the Alert VO object containing the details of the Alert to be modified.
		 *  @returns alert:Alert A alert:Alert value object populated with the properties used to modify the specified alert:alert instance.
		*/
		public function saveExistingAlert(alert:Alert) : AsyncToken {
			return alertService.saveExistingAlert(alert);
		}

	}
}
