package delegates
{

	/** The AsyncToken class is a standard class used by system delegates.*/
	import mx.rpc.AsyncToken;

	/** The RemoteObject class is a standard class used by system delegates, and is used to store remoteService method references.*/
	import mx.rpc.remoting.RemoteObject;

	/** The AlertSubscription object is used to define an instance of a given alertsubscription (value object). */
	import vo.AlertSubscription;

	/**
	 * This class is used to manage all remote interactions with the coldFusion alertSubscriptionService.
	 */
	public class AlertSubscriptionDelegate extends BaseServiceDelegate
	{

		/** The alertSubscriptionService defines the coldFusion service that will be interacted with. */
		[Autowire(bean="alertSubscriptionService")]
		public var alertSubscriptionService : RemoteObject;

		/** This method is used to set the database delegate used to manage Alert Subscription data (dependency injection).
		 *  @param bean Describes the *.cfc used to manage database interactions related to Alert Subscription information.
		 *  @returns bean:AlertSubscriptionDelegate A bean:AlertSubscriptionDelegate value object populated with the properties used to modify the specified bean:alertsubscriptiondelegate instance.
		*/
		public function setAlertSubscriptionDelegate(bean:AlertSubscriptionDelegate) : AsyncToken {
			return alertSubscriptionService.setAlertSubscriptionDelegate(bean);
		}

		/** This method is used to retrieve / return an empty Alert Subscription information value object.
		*/
		public function getEmptyAlertSubscriptionVo() : AsyncToken {
			return alertSubscriptionService.getEmptyAlertSubscriptionVo();
		}

		/** This method is used to retrieve / return a collection of Alert Subscriptions.
		*/
		public function getAlertSubscriptions() : AsyncToken {
			return alertSubscriptionService.getAlertSubscriptions();
		}

		/** This method is used to return a populated Alert Subscription vo (value object).
		 *  @param AlertSubscription Describes the Alert Subscription VO object containing the properties of the Alert Subscription to be retrieved.
		 *  @returns alertSubscription:AlertSubscription A alertSubscription:AlertSubscription value object populated with the properties used to modify the specified alertsubscription:alertsubscription instance.
		*/
		public function getAlertSubscription(alertSubscription:AlertSubscription) : AsyncToken {
			return alertSubscriptionService.getAlertSubscription(alertSubscription);
		}

		/** This method is used to persist a new Alert Subscription record to the application database.
		 *  @param AlertSubscription Describes the Alert Subscription VO object containing the details of the Alert Subscriptionto be saved.
		 *  @returns alertSubscription:AlertSubscription A alertSubscription:AlertSubscription value object populated with the properties used to modify the specified alertsubscription:alertsubscription instance.
		*/
		public function saveNewAlertSubscription(alertSubscription:AlertSubscription) : AsyncToken {
			return alertSubscriptionService.saveNewAlertSubscription(alertSubscription);
		}

		/** This method is used to persist a new Alert Subscription record to the application database.
		 *  @param AlertSubscription Describes the Alert Subscription VO object containing the details of the Alert Subscription to be saved and returned.
		 *  @returns alertSubscription:AlertSubscription A alertSubscription:AlertSubscription value object populated with the properties used to modify the specified alertsubscription:alertsubscription instance.
		*/
		public function saveAndReturnNewAlertSubscription(alertSubscription:AlertSubscription) : AsyncToken {
			return alertSubscriptionService.saveAndReturnNewAlertSubscription(alertSubscription);
		}

		/** This method is used to save an existing Alert Subscription information to the application database.
		 *  @param AlertSubscription Describes the Alert Subscription VO object containing the details of the Alert Subscription to be modified.
		 *  @returns alertSubscription:AlertSubscription A alertSubscription:AlertSubscription value object populated with the properties used to modify the specified alertsubscription:alertsubscription instance.
		*/
		public function saveExistingAlertSubscription(alertSubscription:AlertSubscription) : AsyncToken {
			return alertSubscriptionService.saveExistingAlertSubscription(alertSubscription);
		}

		/** This method is used to persist new Alert Subscription Delivery Method records to the application database.
		 *  @param AlertSubscription Describes the Alert Subscription VO object containing the properties of the Alert Subscription to be retrieved.
		 *  @param alert_delivery_method_code Comma-delimeted string of alert_delivery_method_codes to be added to the application database.
		 *  @returns alertSubscription:AlertSubscription,alert_delivery_method_code:String A alertSubscription:AlertSubscription,alert_delivery_method_code:String value object populated with the properties used to modify the specified alertsubscription:alertsubscription,alert_delivery_method_code:string instance.
		*/
		public function saveNewAlertSubscriptionDeliveryMethods(alertSubscription:AlertSubscription,alert_delivery_method_code:String) : AsyncToken {
			return alertSubscriptionService.saveNewAlertSubscriptionDeliveryMethods(alertSubscription,alert_delivery_method_code);
		}

		/** This method is used to delete an Alert Subscription Delivery Method record from the application database.
		 *  @param AlertSubscription Describes the Alert Subscription VO object containing the properties of the Alert Subscription to be retrieved.
		 *  @param alert_delivery_method_code Comma-delimeted string of alert_delivery_method_codes to be deleted from the application database.
		 *  @returns alertSubscription:AlertSubscription,alert_delivery_method_code:String A alertSubscription:AlertSubscription,alert_delivery_method_code:String value object populated with the properties used to modify the specified alertsubscription:alertsubscription,alert_delivery_method_code:string instance.
		*/
		public function deleteAlertSubscriptionDeliveryMethods(alertSubscription:AlertSubscription,alert_delivery_method_code:String) : AsyncToken {
			return alertSubscriptionService.deleteAlertSubscriptionDeliveryMethods(alertSubscription,alert_delivery_method_code);
		}

	}
}
