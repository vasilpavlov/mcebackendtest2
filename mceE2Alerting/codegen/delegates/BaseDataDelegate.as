package delegates
{

	/** The AsyncToken class is a standard class used by system delegates.*/
	import mx.rpc.AsyncToken;

	/** The RemoteObject class is a standard class used by system delegates, and is used to store remoteService method references.*/
	import mx.rpc.remoting.RemoteObject;

	/**
	 * This class is used to manage all remote interactions with the coldFusion baseDataService.
	 */
	public class BaseDataDelegate extends BaseServiceDelegate
	{

		/** The baseDataService defines the coldFusion service that will be interacted with. */
		[Autowire(bean="baseDataService")]
		public var baseDataService : RemoteObject;

		/** This is the constructor for the base data service component.
		*/
		public function init() : AsyncToken {
			return baseDataService.init();
		}

		/** This method is used to update the audit properties for any given value object (vo) using information from the active session to audit vo changes.
		 *  @param voObject Describes the value object whose audit properties are being updated.
		 *  @param updateType Describes the type of update being performed (create or modify).
		 *  @returns voObject:Any,updateType:String A voObject:Any,updateType:String value object populated with the properties used to modify the specified voobject:any,updatetype:string instance.
		*/
		public function setAuditProperties(voObject:Any,updateType:String) : AsyncToken {
			return baseDataService.setAuditProperties(voObject,updateType);
		}

		/** This method takes a given value object, and determines if the primary key for the object has been set.  If not, it sets the primary key.
		 *  @param voObject Describes the value object whose primary key will be set.
		 *  @param primaryKey Describes the primary key column  property to be set.
		 *  @returns voObject:Any,primaryKey:String A voObject:Any,primaryKey:String value object populated with the properties used to modify the specified voobject:any,primarykey:string instance.
		*/
		public function setPrimaryKey(voObject:Any,primaryKey:String) : AsyncToken {
			return baseDataService.setPrimaryKey(voObject,primaryKey);
		}

		/** This method takes a given value object, and sets the active status to 0.
		 *  @param voObject Describes the value object that will be deactivated.
		 *  @returns voObject:Any A voObject:Any value object populated with the properties used to modify the specified voobject:any instance.
		*/
		public function deActivate(voObject:Any) : AsyncToken {
			return baseDataService.deActivate(voObject);
		}

		/** This method is used to compare processable associations between two object types against the actual associations in the application database.  It returns a structure containing the associations to add, and the associations to remove.
		 *  @param associatedObjects Describes the array representing the actual database  system object associations that will be compared.
		 *  @param associationsToProcess Describes the array representing the associations to compare.
		 *  @param primaryKey Describes the primary key property for a given object that will be used to identify similar objects when performing the object comparison.
		 *  @param compareProperty Describes the the name of a specific property to compare between the objects to determine if an object should be changed  updated.
		 *  @returns associatedObjects:Array,associationsToProcess:Array,primaryKey:String,compareProperty:String A associatedObjects:Array,associationsToProcess:Array,primaryKey:String,compareProperty:String value object populated with the properties used to modify the specified associatedobjects:array,associationstoprocess:array,primarykey:string,compareproperty:string instance.
		*/
		public function reviewAssociations(associatedObjects:Array,associationsToProcess:Array,primaryKey:String,compareProperty:String) : AsyncToken {
			return baseDataService.reviewAssociations(associatedObjects,associationsToProcess,primaryKey,compareProperty);
		}

		/** This method is used to build a list of values for a given property from an array of Vo objects.
		 *  @param voArray Describes the array representing the associations to compare.
		 *  @param propertyName Describes the array representing the actual database  system object associations that will be compared.
		 *  @returns voArray:Array,propertyName:String A voArray:Array,propertyName:String value object populated with the properties used to modify the specified voarray:array,propertyname:string instance.
		*/
		public function buildVoPropertyListFromArray(voArray:Array,propertyName:String) : AsyncToken {
			return baseDataService.buildVoPropertyListFromArray(voArray,propertyName);
		}

		/** This  method is used to validate that the required fields in a given Vo are provided.
		 *  @param voObject Describes the value object whose properties will be validated.
		 *  @returns voObject:Any A voObject:Any value object populated with the properties used to modify the specified voobject:any instance.
		*/
		public function validateVo(voObject:Any) : AsyncToken {
			return baseDataService.validateVo(voObject);
		}

		/** This  method is used to remove null / empty properties from a given Vo.
		 *  @param voObject Describes the value object whose properties will be evaluated.
		 *  @returns voObject:Any A voObject:Any value object populated with the properties used to modify the specified voobject:any instance.
		*/
		public function removeNullPropertiesFromVo(voObject:Any) : AsyncToken {
			return baseDataService.removeNullPropertiesFromVo(voObject);
		}

	}
}
