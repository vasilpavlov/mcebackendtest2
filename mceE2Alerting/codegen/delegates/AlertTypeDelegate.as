package delegates
{

	/** The AsyncToken class is a standard class used by system delegates.*/
	import mx.rpc.AsyncToken;

	/** The RemoteObject class is a standard class used by system delegates, and is used to store remoteService method references.*/
	import mx.rpc.remoting.RemoteObject;

	/** The AlertType object is used to define an instance of a given alerttype (value object). */
	import vo.AlertType;

	/**
	 * This class is used to manage all remote interactions with the coldFusion alertTypeService.
	 */
	public class AlertTypeDelegate extends BaseServiceDelegate
	{

		/** The alertTypeService defines the coldFusion service that will be interacted with. */
		[Autowire(bean="alertTypeService")]
		public var alertTypeService : RemoteObject;

		/** This method is used to set the database delegate used to manage Alert Type data (dependency injection).
		 *  @param bean Describes the *.cfc used to manage database interactions related to Alert Type information.
		 *  @returns bean:AlertTypeDelegate A bean:AlertTypeDelegate value object populated with the properties used to modify the specified bean:alerttypedelegate instance.
		*/
		public function setAlertTypeDelegate(bean:AlertTypeDelegate) : AsyncToken {
			return alertTypeService.setAlertTypeDelegate(bean);
		}

		/** This method is used to retrieve / return an empty Alert Type information value object.
		*/
		public function getEmptyAlertTypeVo() : AsyncToken {
			return alertTypeService.getEmptyAlertTypeVo();
		}

		/** This method is used to retrieve / return a collection of Alert Types.
		*/
		public function getAlertTypes() : AsyncToken {
			return alertTypeService.getAlertTypes();
		}

		/** This method is used to return a populated Alert Type vo (value object).
		 *  @param AlertType Describes the Alert Type VO object containing the properties of the Alert Type to be retrieved.
		 *  @returns alertType:AlertType A alertType:AlertType value object populated with the properties used to modify the specified alerttype:alerttype instance.
		*/
		public function getAlertType(alertType:AlertType) : AsyncToken {
			return alertTypeService.getAlertType(alertType);
		}

		/** This method is used to persist a new Alert Type record to the application database.
		 *  @param AlertType Describes the Alert Type VO object containing the details of the Alert Typeto be saved.
		 *  @returns alertType:AlertType A alertType:AlertType value object populated with the properties used to modify the specified alerttype:alerttype instance.
		*/
		public function saveNewAlertType(alertType:AlertType) : AsyncToken {
			return alertTypeService.saveNewAlertType(alertType);
		}

		/** This method is used to persist a new Alert Type record to the application database.
		 *  @param AlertType Describes the Alert Type VO object containing the details of the Alert Type to be saved and returned.
		 *  @returns alertType:AlertType A alertType:AlertType value object populated with the properties used to modify the specified alerttype:alerttype instance.
		*/
		public function saveAndReturnNewAlertType(alertType:AlertType) : AsyncToken {
			return alertTypeService.saveAndReturnNewAlertType(alertType);
		}

		/** This method is used to save an existing Alert Type information to the application database.
		 *  @param AlertType Describes the Alert Type VO object containing the details of the Alert Type to be modified.
		 *  @returns alertType:AlertType A alertType:AlertType value object populated with the properties used to modify the specified alerttype:alerttype instance.
		*/
		public function saveExistingAlertType(alertType:AlertType) : AsyncToken {
			return alertTypeService.saveExistingAlertType(alertType);
		}

		/** This method is used to persist new Alert Type Interval records to the application database.
		 *  @param AlertType Describes the Alert Type VO object containing the properties of the Alert Type to be retrieved.
		 *  @param alert_interval_code Comma-delimeted string of alert_interval_codes to be added to the application database.
		 *  @returns alertType:AlertType,alert_interval_code:String A alertType:AlertType,alert_interval_code:String value object populated with the properties used to modify the specified alerttype:alerttype,alert_interval_code:string instance.
		*/
		public function saveNewAlertTypeIntervals(alertType:AlertType,alert_interval_code:String) : AsyncToken {
			return alertTypeService.saveNewAlertTypeIntervals(alertType,alert_interval_code);
		}

		/** This method is used to delete an Alert Type Interval record from the application database.
		 *  @param AlertType Describes the Alert Type VO object containing the properties of the Alert Type to be retrieved.
		 *  @param alert_interval_code Comma-delimeted string of alert_interval_codes to be deleted from the application database.
		 *  @returns alertType:AlertType,alert_interval_code:String A alertType:AlertType,alert_interval_code:String value object populated with the properties used to modify the specified alerttype:alerttype,alert_interval_code:string instance.
		*/
		public function deleteAlertTypeIntervals(alertType:AlertType,alert_interval_code:String) : AsyncToken {
			return alertTypeService.deleteAlertTypeIntervals(alertType,alert_interval_code);
		}

	}
}
