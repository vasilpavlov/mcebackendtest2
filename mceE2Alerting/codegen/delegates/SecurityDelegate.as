package delegates
{

	/** The AsyncToken class is a standard class used by system delegates.*/
	import mx.rpc.AsyncToken;

	/** The RemoteObject class is a standard class used by system delegates, and is used to store remoteService method references.*/
	import mx.rpc.remoting.RemoteObject;

	/**
	 * This class is used to manage all remote interactions with the coldFusion securityService.
	 */
	public class SecurityDelegate extends BaseServiceDelegate
	{

		/** The securityService defines the coldFusion service that will be interacted with. */
		[Autowire(bean="securityService")]
		public var securityService : RemoteObject;

		/** This method is used to set / define the security delegate (dependency injection).
		 *  @param bean
		 *  @returns bean: A bean: value object populated with the properties used to modify the specified bean: instance.
		*/
		public function setSecurityDelegate(bean:) : AsyncToken {
			return securityService.setSecurityDelegate(bean);
		}

		/** This method is used to set the session bean factory component (dependency injection).
		 *  @param bean Describes the component used to set  define the session bean factory.
		 *  @returns bean:Any A bean:Any value object populated with the properties used to modify the specified bean:any instance.
		*/
		public function setSessionBeanFactory(bean:Any) : AsyncToken {
			return securityService.setSessionBeanFactory(bean);
		}

		/** This method is used to process login attempts.
		 *  @param username Describes the userName that is used to attempt a login.
		 *  @param password Describes the password that is used to attempt a login.
		 *  @returns username:String,password:String A username:String,password:String value object populated with the properties used to modify the specified username:string,password:string instance.
		*/
		public function attemptLogin(username:String,password:String) : AsyncToken {
			return securityService.attemptLogin(username,password);
		}

		/** This method is used to attempt a logout action from the application.  It will remove the active session from the application's session management.
		*/
		public function attemptLogout() : AsyncToken {
			return securityService.attemptLogout();
		}

		/** This method is used to return the active session's sessionBean component.
		*/
		public function getSession() : AsyncToken {
			return securityService.getSession();
		}

		/** This method is used to return an array of available property collections for a given user, filtering out collections that the user does not have access to.
		 *  @param propertyCollectionsArray Describes the array collection of propertyCollection vo obects being evaluated.
		 *  @returns propertyCollectionsArray:Array A propertyCollectionsArray:Array value object populated with the properties used to modify the specified propertycollectionsarray:array instance.
		*/
		public function cleanPropertyCollections(propertyCollectionsArray:Array) : AsyncToken {
			return securityService.cleanPropertyCollections(propertyCollectionsArray);
		}

		/** This method is used to validate if a property collection is allowed for a given user.
		 *  @param property_collection_uid Describes the primary key  unique identifier of a given property collection record.
		 *  @returns property_collection_uid:String A property_collection_uid:String value object populated with the properties used to modify the specified property_collection_uid:string instance.
		*/
		public function isPropertyCollectionAllowed(property_collection_uid:String) : AsyncToken {
			return securityService.isPropertyCollectionAllowed(property_collection_uid);
		}

		/** This method is used to determine if the session key is in a valid format.
		 *  @param str Describes the session key being evaluated  validated.
		 *  @returns str:String A str:String value object populated with the properties used to modify the specified str:string instance.
		*/
		public function isValidSessionKeyFormat(str:String) : AsyncToken {
			return securityService.isValidSessionKeyFormat(str);
		}

	}
}
