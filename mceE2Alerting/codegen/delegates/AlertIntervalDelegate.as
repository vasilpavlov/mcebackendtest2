package delegates
{

	/** The AsyncToken class is a standard class used by system delegates.*/
	import mx.rpc.AsyncToken;

	/** The RemoteObject class is a standard class used by system delegates, and is used to store remoteService method references.*/
	import mx.rpc.remoting.RemoteObject;

	/** The AlertInterval object is used to define an instance of a given alertinterval (value object). */
	import vo.AlertInterval;

	/**
	 * This class is used to manage all remote interactions with the coldFusion alertIntervalService.
	 */
	public class AlertIntervalDelegate extends BaseServiceDelegate
	{

		/** The alertIntervalService defines the coldFusion service that will be interacted with. */
		[Autowire(bean="alertIntervalService")]
		public var alertIntervalService : RemoteObject;

		/** This method is used to set the database delegate used to manage Alert Interval data (dependency injection).
		 *  @param bean Describes the *.cfc used to manage database interactions related to Alert Interval information.
		 *  @returns bean:AlertIntervalDelegate A bean:AlertIntervalDelegate value object populated with the properties used to modify the specified bean:alertintervaldelegate instance.
		*/
		public function setAlertIntervalDelegate(bean:AlertIntervalDelegate) : AsyncToken {
			return alertIntervalService.setAlertIntervalDelegate(bean);
		}

		/** This method is used to retrieve / return an empty Alert Interval information value object.
		*/
		public function getEmptyAlertIntervalVo() : AsyncToken {
			return alertIntervalService.getEmptyAlertIntervalVo();
		}

		/** This method is used to retrieve / return a collection of Alert Intervals.
		*/
		public function getAlertIntervals() : AsyncToken {
			return alertIntervalService.getAlertIntervals();
		}

		/** This method is used to return a populated Alert Interval vo (value object).
		 *  @param AlertInterval Describes the Alert Interval VO object containing the properties of the Alert Interval to be retrieved.
		 *  @returns alertInterval:AlertInterval A alertInterval:AlertInterval value object populated with the properties used to modify the specified alertinterval:alertinterval instance.
		*/
		public function getAlertInterval(alertInterval:AlertInterval) : AsyncToken {
			return alertIntervalService.getAlertInterval(alertInterval);
		}

		/** This method is used to persist a new Alert Interval record to the application database.
		 *  @param AlertInterval Describes the Alert Interval VO object containing the details of the Alert Interval to be saved.
		 *  @returns alertInterval:AlertInterval A alertInterval:AlertInterval value object populated with the properties used to modify the specified alertinterval:alertinterval instance.
		*/
		public function saveNewAlertInterval(alertInterval:AlertInterval) : AsyncToken {
			return alertIntervalService.saveNewAlertInterval(alertInterval);
		}

		/** This method is used to persist a new Alert Interval record to the application database.
		 *  @param AlertInterval Describes the Alert Interval VO object containing the details of the Alert Interval to be saved and returned.
		 *  @returns alertInterval:AlertInterval A alertInterval:AlertInterval value object populated with the properties used to modify the specified alertinterval:alertinterval instance.
		*/
		public function saveAndReturnNewAlertInterval(alertInterval:AlertInterval) : AsyncToken {
			return alertIntervalService.saveAndReturnNewAlertInterval(alertInterval);
		}

		/** This method is used to save an existing Alert Interval information to the application database.
		 *  @param AlertInterval Describes the Alert Interval VO object containing the details of the Alert Interval to be modified.
		 *  @returns alertInterval:AlertInterval A alertInterval:AlertInterval value object populated with the properties used to modify the specified alertinterval:alertinterval instance.
		*/
		public function saveExistingAlertInterval(alertInterval:AlertInterval) : AsyncToken {
			return alertIntervalService.saveExistingAlertInterval(alertInterval);
		}

	}
}
