<cfinclude template="../toolsHeader.cfm">

<cfset lookupDelegate = application.beanFactory.getBean("EnergyUsageRelatedDelegateForAlerts")>
<cfset datasource = lookupDelegate.datasource>

<cfinclude template="testingTools_makeVisibleNow.cfm">
<hr>
<cfinclude template="testingTools_resetSentStatus.cfm">
<hr>
<cfinclude template="testingTools_resetLastPolled.cfm">
<hr>
<cfinclude template="testingTools_resetLastGenerated.cfm">
<hr>
<cfinclude template="testingTools_touchUsage.cfm">