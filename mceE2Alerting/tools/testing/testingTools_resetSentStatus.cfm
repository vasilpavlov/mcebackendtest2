<!--- This file includes by testingTools.cfm --->

<cfparam name="form.resetSentMinutesBack" type="numeric" default="10">

<cfif isDefined("form.doResetSentStatus")>
	<cfquery datasource="#datasource#" result="touched">
		update alerts
		set
			alert_sent_date = NULL,
			alert_status_code = 'Pending'
		where
			alert_status_code <> 'Pending'
			and alert_sent_date > DATEADD(n, -#resetSentMinutesBack#, getDate())
	</cfquery>

	<cfoutput><font color="red">#touched.recordCount# records updated.</font></cfoutput>
</cfif>


<cfform method="post">
	<h3>Reset Sent Status (at Alert level)</h3>
	<p>
	This clears the "sent" status for any recently-sent alerts.
	This is useful for testing if you want to resend some alerts repeatedly while tweaking formatting, etc.
	After you run this you will likely want to hit the "Send Alerts" link again.
	</p>

	Number of minutes back to look for sent alerts:
	<cfinput name="resetSentMinutesBack" size="5" type="text" validate="integer" value="#resetSentMinutesBack#"><br/>

	<cfinput name="doResetSentStatus" type="submit" value="Reset Sent Now">
</cfform>