<!--- This file includes by testingTools.cfm --->

<cfparam name="form.lastPolledMinutesBack" type="numeric" default="10">

<cfif isDefined("form.doResetLastPolled")>
	<cfquery datasource="#datasource#" result="touched">
		update AlertSubscriptions
		set
			last_polled = NULL
		where
			last_polled > DATEADD(n, -#lastPolledMinutesBack#, getDate())
			and is_active = 1
	</cfquery>

	<cfoutput><font color="red">#touched.recordCount# records updated.</font></cfoutput>
</cfif>


<cfform method="post">
	<h3>Reset Last-Polled Date (at Subscription level)</h3>
	<p>
	This clears the "last polled" flag for recently-polled alert subscriptions.
	For some subscription types, this will cause the system to be willing to send an alert again.
	</p>

	Number of minutes back to look for recently-polled subscriptions:
	<cfinput name="lastPolledMinutesBack" size="5" type="text" validate="integer" value="#lastPolledMinutesBack#"><br/>

	<cfinput name="doResetLastPolled" type="submit" value="Reset Last-Polled">
</cfform>