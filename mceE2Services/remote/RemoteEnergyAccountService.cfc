<!---

  Copyright (c) 2005, Chris Scott, David Ross, Kurt Wiersma, Sean Corfield
  All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

 $Id: RemoteProxyBean.cfc,v 1.8 2008/03/07 02:25:04 pjf Exp $
 $Log: RemoteProxyBean.cfc,v $
 Revision 1.8  2008/03/07 02:25:04  pjf
 Var'ed and scope variables caught by a var scope checker

 Revision 1.7  2007/09/11 11:41:52  scottc
 Fixed error setting bean factory in the proper scope, moved initialization into setup method in RemoteProxyBean

 Revision 1.6  2007/01/01 17:41:36  scottc
 added support for <alias name="fromName" alias="toName"/> tag

 Revision 1.5  2006/06/25 13:22:43  rossd
 removing debug code

 Revision 1.4  2006/04/04 03:51:27  simb
 removed duplicate local var bfUtils

 Revision 1.3  2006/01/28 21:44:13  scottc
 Another slight tweek, everything refers to beanFactory, not context

 Revision 1.2  2006/01/28 21:39:57  scottc
 Shoot, the RemoteProxyBean was looking for an applicationContext instead of a bean factory. Updated to look for a beanFactory, but I need to test!

 Revision 1.1  2006/01/13 15:00:12  scottc
 CSP-38 - First pass at RemoteProxyBean, creating remote services for CS managed seriveces through AOP


--->

<cfcomponent name="RemoteEnergyAccountService"
			displayname="RemoteEnergyAccountService:RemoteProxyBean"
			hint="Abstract Base Class for Aop Based Remote Proxy Beans"
			output="false">

	<cfset variables.proxyId = CreateUUId() />
	<cfset variables.beanFactoryName = "beanFactory" />
	<cfset variables.beanFactoryScope = "" />
	<cfset variables.constructed = false />
	<cfset setup() />

	<cffunction name="setup" access="public" returntype="void">
		<cfset var bfUtils = 0 />
		<cfset var bf = 0 />
		<cfset var error = false />
		<cfset var remoteFactory = "" />

		<!--- I want to make sure that the proxy id really exists --->
		<cfif not StructKeyExists(variables, "proxyId")>
			<cfset variables.proxyId = CreateUUId() />
		</cfif>

		<cflock name="RemoteProxyBean.#variables.proxyId#.Setup" type="readonly" timeout="5">
			<cfif not StructKeyExists(variables, "constructed") or not variables.constructed>

				<!--- it looks like there is an issue with setting up the variables scope in a static initializer
					  with remote methods, so we will make sure things are set up --->
				<cfif not StructKeyExists(variables, "constructed")>
					<cfset variables.beanFactoryName = "beanFactory" />
					<cfset variables.beanFactoryScope = "" />
					<cfset variables.constructed = false />
				</cfif>
				<!--- make sure scope is setup (could have been set to '', meaning application, default) --->
				<cfif not len(variables.beanFactoryScope)>
					<cfset variables.beanFactoryScope = 'application' />
				</cfif>
					<cfset bfUtils = createObject("component","coldspring.beans.util.BeanFactoryUtils").init()/>
				<cftry>
					<cfif not len(variables.beanFactoryName)>
						<cfset bf = bfUtils.getDefaultFactory(variables.beanFactoryScope) />
					<cfelse>
						<cfset bf = bfUtils.getNamedFactory(variables.beanFactoryScope, variables.beanFactoryName) />
					</cfif>
					<cfset remoteFactory = bf.getBean("&RemoteEnergyAccountService") />
					<cfset variables.target = bf.getBean("RemoteEnergyAccountService") />
					<cfset variables.adviceChains = remoteFactory.getProxyAdviceChains() />
					<cfset variables.constructed = true />
					<cfcatch>
						<cfset error = true />
					</cfcatch>
				</cftry>
			</cfif>
		</cflock>

		<cfif error>
			<cfthrow type="coldspring.remoting.ApplicationContextError"
					 message="Sorry, a ColdSpring BeanFactory named #variables.beanFactoryName# was not found in #variables.beanFactoryScope# scope. Please make sure your bean factory is properly loaded. Perhapse your main application is not running?" />
		</cfif>

	</cffunction>

	<cffunction name="callMethod" access="private" returntype="any">
		<cfargument name="methodName" type="string" required="true" />
		<cfargument name="args" type="struct" required="true" />
		<cfset var adviceChain = 0 />
		<cfset var methodInvocation = 0 />
		<cfset var rtn = 0 />
		<cfset var method = 0 />

		<!--- make sure setup is called --->
		<cfif not StructKeyExists(variables, "constructed") or not variables.constructed>
			<cfset setup() />
		</cfif>

		<!--- if an advice chain was created for this method, retrieve a methodInvocation chain from it and proceed --->
		<cfif StructKeyExists(variables.adviceChains, arguments.methodName)>
			<cfset method = CreateObject('component','coldspring.aop.Method').init(variables.target, arguments.methodName, arguments.args) />
			<cfset adviceChain = variables.adviceChains[arguments.methodName] />
			<cfset methodInvocation = adviceChain.getMethodInvocation(method, arguments.args, variables.target) />
			<cfreturn methodInvocation.proceed() />
		<cfelse>
			<!--- if there's no advice chains to execute, just call the method --->
			<cfinvoke component="#variables.target#"
					  method="#arguments.methodName#"
					  argumentcollection="#arguments.args#"
					  returnvariable="rtn">
			</cfinvoke>
			<cfif isDefined('rtn')>
				<cfreturn rtn />
			</cfif>
		</cfif>

	</cffunction>

	<cffunction name="getEnergyAccountsByProperty" access="remote" returntype="any" > 
<cfargument name="property_uid" type="string" required="true" /> 
<cfset var rtn = callMethod('getEnergyAccountsByProperty', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="insertNativeHistoryRecord" access="remote" returntype="any" > 
<cfargument name="originalEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfargument name="modifiedEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfargument name="insertPosition" type="string" required="true" /> 
<cfargument name="relationship_start" type="string" required="true" /> 
<cfargument name="relationship_end" type="string" required="true" /> 
<cfargument name="precedingRecord" type="any" required="false" default="" /> 
<cfargument name="succedingRecord" type="any" required="false" default="" /> 
<cfset var rtn = callMethod('insertNativeHistoryRecord', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getAvailableEnergyAccountSummaries" access="remote" returntype="any" > 
<cfargument name="propertyArray" type="array" required="false" /> 
<cfargument name="selectMethod" type="string" required="true" default="current" /> 
<cfargument name="relationship_filter_date" type="string" required="true" default="[runtime expression]" /> 
<cfset var rtn = callMethod('getAvailableEnergyAccountSummaries', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getAvailableEnergyAccountSummaryByEnergyType" access="remote" returntype="any" > 
<cfargument name="propertyArray" type="array" required="false" /> 
<cfset var rtn = callMethod('getAvailableEnergyAccountSummaryByEnergyType', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="saveAndReturnNewEnergyAccount" access="remote" returntype="any" > 
<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfset var rtn = callMethod('saveAndReturnNewEnergyAccount', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getEnergyAccounts" access="remote" returntype="any" > 
<cfset var rtn = callMethod('getEnergyAccounts', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="createNativeEnergyAccountObject" access="remote" returntype="any" output="false" > 
<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfset var rtn = callMethod('createNativeEnergyAccountObject', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getEnergyAccountSummariesForDataAnalysis" access="remote" returntype="any" > 
<cfargument name="propertyArray" type="array" required="false" /> 
<cfargument name="selectMethod" type="string" required="true" default="current" /> 
<cfargument name="relationship_filter_date" type="string" required="true" default="[runtime expression]" /> 
<cfset var rtn = callMethod('getEnergyAccountSummariesForDataAnalysis', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getCorporateEmissionsSources" access="remote" returntype="any" > 
<cfargument name="propertyArray" type="array" required="false" /> 
<cfset var rtn = callMethod('getCorporateEmissionsSources', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getEmptyEnergyAccountVo" access="remote" returntype="any" > 
<cfset var rtn = callMethod('getEmptyEnergyAccountVo', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="checkForEnergyUsageRecords" access="remote" returntype="any" > 
<cfargument name="energy_account_uid" type="string" required="true" /> 
<cfargument name="relationship_start" type="string" required="true" /> 
<cfargument name="relationship_end" type="string" required="true" /> 
<cfset var rtn = callMethod('checkForEnergyUsageRecords', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getPropertyMetaDataAnalysis" access="remote" returntype="any" > 
<cfargument name="energyAccountArray" type="array" required="true" /> 
<cfargument name="propertyMetaTypeArray" type="array" required="true" /> 
<cfargument name="dateRollupMode" type="string" required="true" /> 
<cfargument name="period_start" type="date" required="true" /> 
<cfargument name="period_end" type="date" required="true" /> 
<cfset var rtn = callMethod('getPropertyMetaDataAnalysis', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getEnergyAccount" access="remote" returntype="any" > 
<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfset var rtn = callMethod('getEnergyAccount', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getEnergyAccountSummary" access="remote" returntype="any" > 
<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfset var rtn = callMethod('getEnergyAccountSummary', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="deleteNativeAccountHistory" access="remote" returntype="any" > 
<cfargument name="native_account_uid" type="string" required="true" /> 
<cfset var rtn = callMethod('deleteNativeAccountHistory', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="isRelationshipsRecordNeeded" access="remote" returntype="any" > 
<cfargument name="originalEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfargument name="modifiedEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfset var rtn = callMethod('isRelationshipsRecordNeeded', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="isNativeAccountRecordNeeded" access="remote" returntype="any" > 
<cfargument name="originalEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfargument name="modifiedEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfset var rtn = callMethod('isNativeAccountRecordNeeded', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="energyNativeAccountEnergyProviderRateClassAssociationExists" access="remote" returntype="any" output="false" > 
<cfargument name="AssociationObject" type="struct" required="true" /> 
<cfset var rtn = callMethod('energyNativeAccountEnergyProviderRateClassAssociationExists', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="saveNewEnergyAccount" access="remote" returntype="any" > 
<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfset var rtn = callMethod('saveNewEnergyAccount', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="saveExistingEnergyAccount" access="remote" returntype="any" > 
<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfset var rtn = callMethod('saveExistingEnergyAccount', arguments) />
</cffunction>

<cffunction name="getNativeAccountHistory" access="remote" returntype="any" > 
<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfset var rtn = callMethod('getNativeAccountHistory', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="setEnergyAccountDelegate" access="remote" returntype="any" > 
<cfargument name="bean" type="mce.e2.db.EnergyAccountDelegate" /> 
<cfset var rtn = callMethod('setEnergyAccountDelegate', arguments) />
</cffunction>

<cffunction name="createEnergyNativeAccountEnergyProviderRateClassAssociationObject" access="remote" returntype="any" output="false" > 
<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfargument name="EnergyNativeAccount" type="struct" required="true" /> 
<cfargument name="DefaultRateClassRateModelAssociation" type="struct" required="true" /> 
<cfargument name="OriginalEnergyAccount" type="mce.e2.vo.EnergyAccount" required="false" /> 
<cfset var rtn = callMethod('createEnergyNativeAccountEnergyProviderRateClassAssociationObject', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="saveNewEnergyAccountAndReturnEnergyAccountSummary" access="remote" returntype="any" > 
<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" /> 
<cfset var rtn = callMethod('saveNewEnergyAccountAndReturnEnergyAccountSummary', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="buildVoPropertyListFromArray" access="remote" returntype="any" > 
<cfargument name="voArray" type="array" required="true" /> 
<cfargument name="propertyName" type="string" required="true" /> 
<cfset var rtn = callMethod('buildVoPropertyListFromArray', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getUserUid" access="remote" returntype="any" > 
<cfset var rtn = callMethod('getUserUid', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="getPermissions" access="remote" returntype="any" > 
<cfset var rtn = callMethod('getPermissions', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="validateVo" access="remote" returntype="any" > 
<cfargument name="voObject" type="any" required="true" /> 
<cfset var rtn = callMethod('validateVo', arguments) />
</cffunction>

<cffunction name="deActivate" access="remote" returntype="any" > 
<cfargument name="voObject" type="any" required="true" /> 
<cfset var rtn = callMethod('deActivate', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="enforcePermission" access="remote" returntype="any" > 
<cfargument name="permissionCode" type="string" required="true" /> 
<cfset var rtn = callMethod('enforcePermission', arguments) />
</cffunction>

<cffunction name="setAuditProperties" access="remote" returntype="any" > 
<cfargument name="voObject" type="any" required="true" /> 
<cfargument name="updateType" type="string" required="true" default="create" /> 
<cfset var rtn = callMethod('setAuditProperties', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="reviewAssociations" access="remote" returntype="any" > 
<cfargument name="associatedObjects" type="array" required="true" /> 
<cfargument name="associationsToProcess" type="array" required="true" /> 
<cfargument name="primaryKey" type="string" required="true" /> 
<cfargument name="compareProperty" type="string" required="false" /> 
<cfset var rtn = callMethod('reviewAssociations', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="removeNullPropertiesFromVo" access="remote" returntype="any" > 
<cfargument name="voObject" type="any" required="true" /> 
<cfset var rtn = callMethod('removeNullPropertiesFromVo', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="setSecurityService" access="remote" returntype="any" > 
<cfargument name="bean" type="mce.e2.service.SecurityService" /> 
<cfset var rtn = callMethod('setSecurityService', arguments) />
</cffunction>

<cffunction name="getAuditLabel" access="remote" returntype="any" > 
<cfset var rtn = callMethod('getAuditLabel', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="hasPermission" access="remote" returntype="any" > 
<cfargument name="permissionCode" type="string" required="true" /> 
<cfset var rtn = callMethod('hasPermission', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="setPrimaryKey" access="remote" returntype="any" > 
<cfargument name="voObject" type="any" required="true" /> 
<cfargument name="primaryKey" type="string" required="false" /> 
<cfset var rtn = callMethod('setPrimaryKey', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="queryRowToStruct" access="remote" returntype="any" output="false" > 
<cfargument name="query" type="query" required="true" /> 
<cfargument name="row" type="numeric" required="true" default="1" /> 
<cfset var rtn = callMethod('queryRowToStruct', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="createUniqueIdentifierPlaceholder" access="remote" returntype="any" output="false" > 
<cfset var rtn = callMethod('createUniqueIdentifierPlaceholder', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="stringExtract" access="remote" returntype="any" > 
<cfargument name="str" type="string" required="true" /> 
<cfargument name="pattern" type="string" required="true" /> 
<cfargument name="subexpressionIndex" type="numeric" required="false" default="1" /> 
<cfset var rtn = callMethod('stringExtract', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="removeMilisecondsFromDate" access="remote" returntype="any" > 
<cfargument name="date" type="date" required="true" /> 
<cfset var rtn = callMethod('removeMilisecondsFromDate', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="createUniqueIdentifier" access="remote" returntype="any" output="false" > 
<cfset var rtn = callMethod('createUniqueIdentifier', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>

<cffunction name="isUniqueIdentifier" access="remote" returntype="any" output="false" > 
<cfargument name="guid" type="string" required="true" /> 
<cfset var rtn = callMethod('isUniqueIdentifier', arguments) />
<cfif isDefined('rtn')><cfreturn rtn /></cfif>
</cffunction>



</cfcomponent>
