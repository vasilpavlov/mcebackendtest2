
<!--- Initialize the local scope --->
<cfset local = structNew()>

<!--- Initialize the data generation service --->
<cfset local.dataGenerator = createObject("component","mce.e2.testing.cfunit.data.dataGenerator")>

<!--- Purge / remove the test data --->
<cfset local.dataGenerator.createTestData()>

