
<!--- Initialize the local scope --->
<cfset local = structNew()>

<!--- Initialize the services that we need --->
<cfset local.userGroupService = request.beanFactory.getBean("userGroupService")>
<cfset local.securityService = request.beanFactory.getBean("securityService")>

<!--- Log into the user's session --->
<cfset local.session = local.securityService.attemptLogin('username003','password')>

<!--- Retrieve the test user group --->
<cfset local.userGroup = local.userGroupService.getEmptyUserGroupVo()>
<cfset local.userGroup.client_company_uid = local.session.client_company_uid>
<cfset local.userGroup.user_group_uid = 'da97d847-abe8-74cd-1165-ac3eea91fb48'>
<cfset local.userGroup = local.userGroupService.getUserGroup(local.userGroup)>

<!--- Get the associated users --->
<table>
<tr><td colspan="3">User Roles</td></tr>
<tr>
<td valign="top">
GetAssociations
<cfset local.pcArray = local.userGroupService.getUserRoleAssociations(local.userGroup)>
<cfdump var="#local.pcArray#">
</td>
<td valign="top">
GetAssociated
<cfset local.output = local.userGroupService.getAssociatedUserRoles(local.userGroup)>
<cfdump var="#local.output#">
</td>
<td valign="top">
GetUnAssociated
<cfset local.output = local.userGroupService.getUnAssociatedUserRoles(local.userGroup)>
<cfdump var="#local.output#">
</td>
</tr>
</table>