<!--- Initialize local variables --->
<cfset local = structNew()>
				
<!--- Instantiate the reporting service --->
<cfset local.energyEntryService = request.beanFactory.getBean("energyEntryService")>		
			
<!--- Define the energy usage property --->			
<cfset local.energy_usage_uid = '377cdab1-9b00-4129-a69b-20aff2786fba'>			
			
<!--- Get the energy entry object --->			
<cfset local.result = local.energyEntryService.getEnergyUsageVo(
		energy_usage_uid = local.energy_usage_uid)>				
				
<cfdump var="#local.result#">	
			