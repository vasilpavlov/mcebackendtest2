<!--- Initialize local variables --->
<cfset local = structNew()>

<!--- Instantiate the component --->
<cfset local.service = createObject('component', 'remote.remoteEnergyAccountService')>

<cfset local.obj = createObject('component', 'mce.e2.vo.property')>
<cfset local.obj.property_uid = '5e8f5c3e-d64e-4fc7-ba27-53e76b4e9361'>	

<cfset local.array = arrayNew(1)>
<cfset arrayAppend(local.array, local.obj)>

<cfset local.result = local.service.getAvailableEnergyAccountSummaries(propertyArray=local.array)>

<cfdump var="#local.result#">	