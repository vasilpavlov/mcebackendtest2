<cfcomponent displayname="Session Bean Factory"
			 extends="mce.e2.common.BaseComponent"
			 hint="This component is used to produce session beans which keep track of / maintain session information for a given application user.">

	<!--- Define the constructor --->
	<cffunction name="init"
				hint="This method is the constructor for the session bean factory.">
		<cfreturn this>
	</cffunction>

	<!--- Define the set methods for any delegates required by the factory --->
	<cffunction name="setSecurityDelegate"
				hint="This method is used to define the component used to manage database interactions for the session bean factory (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.SecurityDelegate" hint="This component represents the security delegate being associated to the session bean factory.">
		<cfset this.SecurityDelegate = bean>
	</cffunction>
	<!--- This gives us access to the potentially-machine-specific settings in the AppSettingsBean --->
	<cffunction name="setAppSettingsBean" hint="Called by the IOC framework">
		<cfargument name="bean" type="mce.e2.config.AppSettingsBean">
		<cfset this.appSettingsBean = bean>
	</cffunction>

	<!--- Define the methods used to interact with sessions --->
	<cffunction name="getSession"
				returntype="any"
				hint="This method is used to retrieve the active session.">

		<!--- Create an instance of the sessionBean --->
		<cfset var bean = CreateObject("component", "mce.e2.security.SessionBean").init()>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Has a user logged in? --->
		<cfif bean.isLoggedIn>

			<!--- Get the raw user record --->
			<cfset local.qUser = this.securityDelegate.getUserRecord(bean.sessionKey)>

			<!--- Set the user / client company primary key properties --->
			<cfset bean.user_uid = local.qUser.user_uid>
			<cfset bean.client_company_uid = local.qUser.client_company_uid>
			<cfset bean.friendlyName = local.qUser.first_name>
			<cfset bean.image_filename = local.qUser.image_filename>

			<!--- Determine the session's active role list --->
			<cfset bean.rolesList = this.securityDelegate.getCurrentRoles(bean.sessionKey)>

			<!--- Determine the session's active permission list (based on roles) --->
			<cfset bean.permissionsList = this.securityDelegate.getCurrentPermissions(user_uid=bean.user_uid)>

			<!--- Determine the session's active / available / accessible property collection list --->
			<cfset bean.propertyCollectionsList = this.securityDelegate.getValidPropertyCollections(bean.sessionKey)>

			<!--- Determine the session's emissions profile lookup code --->
			<cfset bean.profile_lookup_code = local.qUser.profile_lookup_code>
			<cfset bean.carbon_eq_multiplier_label = local.qUser.carbon_eq_multiplier_label>

			<!--- Determine the session's emissions profile lookup code --->
			<cfset bean.namespace_lookup_code = local.qUser.namespace_lookup_code>


			<cfset bean.reporting = this.appSettingsBean.reporting>
			<cfset bean.propertyImages = this.appSettingsBean.propertyImages>

		</cfif>

		<!--- Return the session bean --->
		<cfreturn bean>

	</cffunction>

</cfcomponent>