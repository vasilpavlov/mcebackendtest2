<cfcomponent
	alias="mce.e2.vo.EnergyContractsMetaChoice"
	style="rpc" 
	extends="BaseValueObject" output="false">

	<cfproperty name="friendly_name" type="string" />
	<cfproperty name="meta_value" type="string" />
</cfcomponent>