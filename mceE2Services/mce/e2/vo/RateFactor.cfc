<cfcomponent displayName="Rate Factor Value Object"
			 output="false"
			 alias="mce.e2.vo.RateFactor" style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the properties of a given rate factor.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="rate_factor_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given rate factor.">
	<cfproperty name="factor_lookup_code" type="string" required="true" hint="Describes the lookup code / internal name for a given rate factor.">
	<cfproperty name="friendly_name" type="string" required="true" hint="Describes the friendly name / customer facing name for a given rate factor.">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="minRelationshipStartDate" type="date" required="false" hint="Describes the minimum rate factor value relationship start date associated to a given rate factor definition.">
	<cfproperty name="maxRelationshipEndDate" type="date" required="false" hint="Describes the maximum rate factor value relationship end date associated to a given rate factor definition.">
	<cfproperty name="currentFactorValue" type="numeric" required="false" hint="Describes the current rate factor numeric value, if any">

	<!--- Define the meta-data for a given value object --->
	<cfproperty name="primaryKey" type="string" required="true" default="rate_factor_uid" hint="Describes the property that acts as the primary key for a given rate factor.">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Set any defaults --->
	<cfset this.rate_factor_uid = "">
	<cfset this.primaryKey = "rate_factor_uid">

</cfcomponent>