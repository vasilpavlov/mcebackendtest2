
<!---

  Template Name:  UserPermission
     Base Table:  UserPermissions	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, June 03, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, June 03, 2009 - Template Created.		

--->
		
<cfcomponent displayname="UserPermissions"
				hint="This CFC manages all object/bean/value object instances for the UserPermissions table, and is used to pass object instances through the applicaiton service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.UserPermissions"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="user_permission_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given user permission."/>
	<cfproperty name="user_permission_code" required="true" type="string" hint="Describes the internal lookup code for a given user permission."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the customer facing / friendly name for a given user permission."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="user_permission_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "user_permission_uid"/>
	<cfset this.user_permission_uid = ""/>

</cfcomponent>
