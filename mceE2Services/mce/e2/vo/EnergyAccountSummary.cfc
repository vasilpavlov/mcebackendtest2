
<!---

  Template Name:  EnergyAccountSummary
     Base Table:  EnergyAccounts

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Sunday, March 22, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Sunday, March 22, 2009 - Template Created.

--->

<cfcomponent displayname="EnergyAccountSummary"
				hint="This CFC manages all object/bean/value object instances for energy account summaries."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyAccountSummary"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
	<cfproperty name="account_type_uid" required="true" type="string" hint="Describes the primary key / unique identifier for the associated energy account type definition."/>
	<cfproperty name="energy_type_uid" required="true" type="string" hint="Describes the primary key / unique identifier for the associated energy type definition."/>
	<cfproperty name="energy_unit_uid" required="true" type="string" hint="Describes the primary key / unique identifier for the associated energy unit definition."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the customer facing / external name for a given energy account."/>
	<cfproperty name="master_energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the associated / parent energy account."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier for the associated property definition.">
	<cfproperty name="native_account_number" required="true" type="string" hint="Describes the external / native account number for a given energy account.">
	<cfproperty name="energy_provider_uid" required="true" type="string" hint="Describes the primary key / unique identifier for the associated property definition.">
	<cfproperty name="td_meta_type_uid" type="string" hint="Describes the primary key for the TD Charges meta type record.">
	<cfproperty name="supply_meta_type_uid" type="string" hint="Describes the primary key for the Supply Charges meta type record.">
	<cfproperty name="account_status" required="true" type="string" hint="Energy Account Status"/>
	<cfproperty name="status_date" type="date" required="true"  hint="Describes the date account status was changed."/>
	
	<!--- Declare the system calculated / defined properties --->
	<cfproperty name="energyUnitFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated energy unit."/>
	<cfproperty name="costTypeFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated energy account type."/>
	<cfproperty name="resourceFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated energy type."/>
	<cfproperty name="propertyFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated property."/>
	<cfproperty name="providerFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated energy provider."/>
	<cfproperty name="masterEnergyAccountFriendlyName" required="true" type="string" hint="Describes the friendly name of the parent / master energy account."/>
	<cfproperty name="energyUsageAvailableFrom" required="true" type="date" hint="Describes the start date of the associated energy usage period."/>
	<cfproperty name="energyUsageAvailableTo" required="true" type="date" hint="Describes the end date of the associated energy usage period."/>
	<cfproperty name="numEnergyUsageRecords" required="true" type="numeric" hint="Describes the end date of the associated energy usage period."/>
	<cfproperty name="input_set_uid" required="true" type="string" hint="Describes the primary key / unique identifier of a given rate model input set."/>
	<cfproperty name="relationship_id" required="true" type="numeric" hint="Describes the primary key / unique identifier for a given energy contract / energy account association."/>

</cfcomponent>