<cfcomponent output="false"
			 extends="BaseValueObject"
			 displayname="Rate Factor Value"
			 alias="mce.e2.vo.RateFactorValue" style="rpc"
			 hint="This component is used to define the value object (vo) used to describe the properties of a given rate factor value.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="rate_factor_uid" type="string" required="true" hint="Describes the primary key / unique identifier associated to a given rate factor.">
	<cfproperty name="factor_value_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given rate factor value.">
	<cfproperty name="factor_value" type="numeric" required="true" hint="Describes rate factor value.">
	<cfproperty name="relationship_start" type="date" required="true" hint="Describes the relationship start date for a given rate factor value.">
	<cfproperty name="relationship_end" type="date" required="true" hint="Describes the relationship end date for a given rate factor value.">

	<!--- Define the meta-data for a given value object --->
	<cfproperty name="primaryKey" type="string" required="true" default="factor_value_uid" hint="Describes the property that acts as the primary key for a given rate factor value.">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Set any defaults --->
	<cfset this.factor_value_uid = "">
	<cfset this.primaryKey = "factor_value_uid">

</cfcomponent>