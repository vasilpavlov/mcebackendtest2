<cfcomponent displayname="EnergyNativeAccount"
				hint="This CFC manages all object/bean/value object instances for the EnergyNativeAccounts table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyNativeAccount"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="native_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
	<cfproperty name="energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
	<cfproperty name="energy_provider_uid" required="true" type="string" hint="Describes the primary key of the energy provider associated to a given energy account."/>	
	<cfproperty name="native_account_number" required="true" type="string" hint="Describes the account number associated to the energy native account tied to a given energy account."/>
	<cfproperty name="meta_group_uid" required="true" type="string" hint="Describes the primary key of the meta group associated to a given energy account."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="native_account_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given rate model record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given rate model record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given rate model record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given rate model record was last modified."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "native_account_uid"/>
	<cfset this.native_account_uid = ""/>

</cfcomponent>
