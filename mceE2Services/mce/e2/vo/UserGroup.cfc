<cfcomponent displayName="User Group Value Object"
			 output="false"
			 alias="mce.e2.vo.UserGroup" style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the details of a given user group.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="user_group_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given user group.">
	<cfproperty name="friendly_name" type="string" required="true" hint="Describes the friendly name / customer facing name for a given user group.">
	<cfproperty name="client_company_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given client company to which each user group is associated to.">
	<cfproperty name="relationship_id" type="numeric" required="false" hint="Describes the primary key of the association record between a user group and another / associated object (ex. userGroup, propertyCollection, user, etc)."/>

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isDeletionPossible" type="boolean" required="true" hint="Describes whether a group can be deleted.">
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Define the meta-data for a given value object --->
	<cfproperty name="primaryKey" type="string" required="true" default="user_group_uid" hint="Describes the property that acts as the primary key for a given user group.">

	<!--- Set any defaults --->
	<cfset this.user_group_uid = "">
	<cfset this.primaryKey = "user_group_uid">

</cfcomponent>