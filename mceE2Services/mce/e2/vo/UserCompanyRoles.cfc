<cfcomponent displayName="User Company Role Value Object"
			 output="false"
			 alias="mce.e2.vo.UserCompanyRoles" style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the details of a given user, their associated company, and related security role lookup codes.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="client_company_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given client company to which each user group is associated to.">
	<cfproperty name="user_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given user.">
	<cfproperty name="first_name" type="string" required="true" hint="Describes the first name of a given user.">
	<cfproperty name="last_name" type="string" required="true" hint="Describes the last name of a given user.">
	<cfproperty name="username" type="string" required="true" hint="Describes the login / username for a given user.">
	
	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="companyFriendlyName" type="string" required="true" hint="Describes the friendly name for a given company.">
	<cfproperty name="companyImageFileName" type="string" required="true" hint="Describes the image file name associated to a given company.">
	<cfproperty name="roleLookupCodes" type="array" required="true" hint="Describes an array of role lookup codes for a given user.">

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>
	
</cfcomponent>