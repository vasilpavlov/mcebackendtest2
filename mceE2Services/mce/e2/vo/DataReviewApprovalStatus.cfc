
<!---

  Template Name:  DataReviewApprovalStatus
     Base Table:  DataReviewApprovalStatuses	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Tuesday, April 28, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Tuesday, April 28, 2009 - Template Created.		

--->
		
<cfcomponent displayname="Data Review Approval Status"
				hint="This CFC manages all object/bean/value object instances for the DataReviewApprovalStatuses table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.DataReviewApprovalStatus"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="review_approval_statuses_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data review approval status."/>
	<cfproperty name="approval_status_code" required="true" type="string" hint="Describes the lookup code for a given approval status."/>
	<cfproperty name="review_namespace_code" required="true" type="string" hint="Describes the review namespace associated to a given approval status."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given approval status."/>
	<cfproperty name="review_role_code" required="true" type="string" hint="Describes whether comments are required for a given approval status / namespace combination."/>
	<cfproperty name="is_comments_required" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="review_approval_statuses_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "review_approval_statuses_uid"/>
	<cfset this.review_approval_statuses_uid = ""/>

</cfcomponent>
