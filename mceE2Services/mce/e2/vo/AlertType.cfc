<cfcomponent	displayname="AlertType"
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.alerting.vo.AlertType"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="alert_type_code" required="true" type="string" hint="Describes the type code / internal identifier for a given alert."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name of a given alert."/>
	<cfproperty name="ui_category" required="true" type="string" hint=""/>
	<cfproperty name="ui_priority" required="true" type="string" hint=""/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<cfproperty name="conditionsTypesList" required="true" type="array"/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="alert_type_code" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "alert_type_code"/>
	<cfset this.alert_type_code = ""/>

</cfcomponent>
