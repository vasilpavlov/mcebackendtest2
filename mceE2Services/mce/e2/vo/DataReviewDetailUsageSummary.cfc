
<!---

  Template Name:  DataReviewDetailUsageSummary
     Base Table:  DataReviewDetail

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, April 22, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, April 22, 2009 - Template Created.		

--->
		
<cfcomponent displayname="DataReviewDetailUsageSummary"
				hint="This CFC manages all object/bean/value object instances for the data review detail table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.DataReviewDetailUsageSummary"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier fora  given property record."/>
	<cfproperty name="energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier fora  given energy account record."/>
	<cfproperty name="period_name" required="true" type="string" hint="Describes the name of the period under which a data review is being executed."/>
	<cfproperty name="detail_period_start" required="true" type="string" hint="Describes the review period start date."/>
	<cfproperty name="detail_period_end" required="true" type="string" hint="Describes the review period end date."/>
	<cfproperty name="energy_type_uid" required="true" type="string" hint="Describes the primary key for a given energy type record."/>
	<cfproperty name="approval_status_code" required="true" type="string" hint="Describes the approval status code for a given data review record."/>
	<cfproperty name="hierarchy_position" required="true" type="string" hint="Describes the hierarchy position of the energy source associated to a given emissions profile."/>
	
	<!--- Derrived properties / Friendly Names --->
	<cfproperty name="approvalStatusFriendlyName" required="true" type="string" hint="Describes the friendly name of the data review approval status for a given record."/>
	<cfproperty name="energyAccountFriendlyName" required="true" type="string" hint="Describes the friendly name of the energy account associated to a given data review record."/>
	<cfproperty name="energyTypeFriendlyName" required="true" type="string" hint="Describes the friendly name of the energy type associated to a given energy account."/>
	<cfproperty name="energyUnitFriendlyName" required="true" type="string" hint="Describes the friendly name of the energy unit associated to a given energy account."/>
	<cfproperty name="conversionEnergyUnitFriendlyName" required="true" type="string" hint="Describes the energy unit that the source energy unit type will be converted to."/>
	<cfproperty name="conversion_factor" required="true" type="numeric" hint="Describes the conversion factor to be applied when converting a given energy unit from one type to another."/>

	<!--- Derrived properties / Energy Usage Calculations --->
	<cfproperty name="energyUsage" required="true" type="numeric" hint="Describes the sum of energy usage for a specified period."/>
	<cfproperty name="firstComparisonEnergyUsage" required="true" type="numeric" hint="Describes sum of energy usage for the previous year starting from the specified period."/>
	<cfproperty name="secondComparisonEnergyUsage" required="true" type="numeric" hint="Describes sum of energy usage for the previous quarter starting from the specified period."/>

	<!--- Derrived properties / Emissions Impact Calculations --->
	<cfproperty name="emissionsImpact" required="true" type="numeric" hint="Describes the emissions impact for a specified period."/>
	<cfproperty name="firstComparisonEmissionsImpact" required="true" type="numeric" hint="Describes the emissions impact for the previous year starting from the specified period."/>
	<cfproperty name="secondComparisonEmissionsImpact" required="true" type="numeric" hint="Describes the emissions impact for the previous quarter starting from the specified period."/>


</cfcomponent>
