
<!---

  Template Name:  StoredDocument
     Base Table:  StoredDocuments	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, April 16, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, April 16, 2009 - Template Created.		

--->
		
<cfcomponent displayname="StoredDocument"
				hint="This CFC manages all object/bean/value object instances for the StoredDocuments table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.StoredDocument"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="stored_document_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given stored document."/>
	<cfproperty name="document_repository_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the repository where a given document will be stored."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given document."/>
	<cfproperty name="original_filename" required="true" type="string" hint="Describes the original filename of a given document."/>
	<cfproperty name="document_keywords" required="true" type="string" hint="Describes the keywords associated to a given document."/>
	<cfproperty name="document_set_uid" required="true" type="string" hint="Describes the primary key / unique identifier representing the document set associated to a given stored document."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>
	<cfproperty name="content_as_filename" required="true" type="string" hint="Describes the content file name for a given docuemtn."/>
	<cfproperty name="storage_date" required="true" type="string" hint="Describes the date that a given document was last stored."/>
	<cfproperty name="folder_name" required="true" type="string" hint="Describes the name of the folder where a given document has been stored."/>
	<cfproperty name="preferred_filename" required="true" type="string" hint="Describes the preferred filename for a given document."/>
	<cfproperty name="mime_type" required="true" type="string" hint="Describes the mime type associated to a given document."/>
	<cfproperty name="permission_needed" required="true" type="string" hint="Describes the permissions needed to retrieve this document."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="stored_document_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "stored_document_uid"/>
	<cfset this.stored_document_uid = ""/>

</cfcomponent>
