
<!---

  Template Name:  UserReport
     Base Table:  UserReports	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Tuesday, June 02, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Tuesday, June 02, 2009 - Template Created.		

--->
		
<cfcomponent displayname="UserReport"
				hint="This CFC manages all object/bean/value object instances for the UserReports table, and is used to pass object instances through the applicaiton service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.UserReport"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="report_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given user report."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given report."/>
	<cfproperty name="group_name" required="true" type="string" hint="Describes the name of the group a given report is associated to."/>
	<cfproperty name="report_lookup_code" required="true" type="string" hint="Describes the internal lookup code for a given report."/>
	<cfproperty name="report_generator" required="true" type="string" hint="Describes the type of generator used to render a given report."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>
	<cfproperty name="report_name" required="true" type="string" hint="Describes the internal name for a given report."/>
	<cfproperty name="report_parameter_ui_class" required="true" type="string" hint="Describes the permission level assigned to a given report."/>
	<cfproperty name="permission_needed" required="true" type="string" hint="Describes the permission level assigned to a given report."/>
	<cfproperty name="report_description" required="true" type="string" hint="Describes a given report."/>
	<cfproperty name="image_filename" required="true" type="string" hint="Describes the filename of an image representing a given report."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="report_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "report_uid"/>
	<cfset this.report_uid = ""/>

</cfcomponent>
