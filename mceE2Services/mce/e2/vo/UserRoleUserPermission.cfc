
<!---

  Template Name:  UserRoleUserPermission
     Base Table:  UserRoles_UserPermissions	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, June 03, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, June 03, 2009 - Template Created.		

--->
		
<cfcomponent displayname="UserRoleUserPermission"
				hint="This CFC manages all object/bean/value object instances for the UserRoles_UserPermissions table, and is used to pass object instances through the applicaiton service layer."
				output="false"
				extends="BaseValueObject"
				alias="UserRoleUserPermission"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="relationship_id" required="true" type="numeric" hint="Describes the primary key / uniqueidentifier for a given user role / user permission association."/>
	<cfproperty name="user_role_uid" required="true" type="string" hint="Describes the primary key of an associated user role."/>
	<cfproperty name="user_permission_code_like" required="true" type="string" hint="Describes the lookup code of the associated user permission."/>
	<cfproperty name="is_granted" required="true" type="boolean" hint="Describes whether a role has been granted to a given / associated user permission (1 = granted, 0 = not granted)."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="relationship_id" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "relationship_id"/>
	<cfset this.relationship_id = ""/>

</cfcomponent>
