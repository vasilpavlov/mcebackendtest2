
<!---

  Template Name:  PropertyClientContactAssociation
     Base Table:  Properties_ClientContacts	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, April 02, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, April 02, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnergyContractAccountAssociation"
				hint="This CFC manages all object/bean/value object instances for the EnergyContracts_EnergyAccounts table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyContractAccountAssociation"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="relationship_id" required="true" type="numeric" hint="Describes the primary key / unique identifier for a given energy contract / energy account association."/>
	<cfproperty name="energy_contract_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given energy contract."/>
	<cfproperty name="energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier of a given energy account."/>
	<cfproperty name="input_set_uid" required="true" type="string" hint="Describes the primary key / unique identifier of a given rate model input set."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="relationship_id" hint="Describes the primary key for the current object / table.">

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "relationship_id"/>
	<cfset this.relationship_id = 0/>

</cfcomponent>
