<cfcomponent displayName="Property Collection Value Object"
			 output="false"
			 alias="mce.e2.vo.PropertyCollection" style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the properties of a given property collection.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="client_company_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given client company.">
	<cfproperty name="property_collection_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given property collection.">
	<cfproperty name="collection_hierarchyid" type="any" required="true" hint="Describes the hierarchy node location for a given property collection within the property collection hierarchy.">
	<cfproperty name="friendly_name" type="string" required="true" hint="Describes the friendly name / customer facing name for a given property collection.">
	<cfproperty name="image_filename" type="string" required="true" hint="Describes the image file name associated to a given property collection.">
	<cfproperty name="collection_type_code" type="string" required="true" hint="Describes the type / category for a given property collection.">
	<cfproperty name="relationship_id" type="numeric" required="false" hint="Describes the primary key of the association record between a property collection and another / associated object (ex. userGroup, propertyCollection, user, etc)."/>

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="propertyCount" type="numeric" required="true" hint="Describes the total number of properties associated to a given property collection.">
	<cfproperty name="properties" type="array" required="false" hint="Describes a collection of the property images associated to a given property collection.">

	<!--- Define the meta-data for a given value object --->
	<cfproperty name="primaryKey" type="string" required="true" default="property_collection_uid" hint="Describes the property that acts as the primary key for a given property collection.">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Set any defaults --->
	<cfset this.property_collection_uid = "">
	<cfset this.propertyCount = 0>
	<cfset this.primaryKey = "property_collection_uid">

</cfcomponent>