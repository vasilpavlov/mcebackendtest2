<cfcomponent displayname="EnergyContractsMetaType"
				hint="This CFC manages all object/bean/value object instances for the EnergyContractsMetaTypes table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyContractsMetaType"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="meta_type_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given energy contract meta type record."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given energy contract meta type."/>
	<cfproperty name="type_lookup_code" required="true" type="string" hint="Describes the internal identifier for a given energy contract meta type."/>
	<cfproperty name="data_type" required="true" type="numeric" hint="Describes the type of data represented by a given energy contract meta type."/>
	<cfproperty name="input_rule_uid" required="true" type="string" hint="Describes input rule for a given energy contract meta type."/>
	<cfproperty name="is_required" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="meta_type_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Additional fields to assist UI, but which do not need to come back to server on updates; these should exist in actionscript class, but should be marked [Transient] there so the Flex app doesn't bother sending them back to the server --->
	<cfproperty name="metaChoices" type="array">
	<cfproperty name="numMetaChoices" type="numeric">

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "meta_type_uid"/>
	<cfset this.meta_type_uid = ""/>

</cfcomponent>
