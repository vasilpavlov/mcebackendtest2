<cfcomponent displayname="PropertySearch"
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.PropertySearch"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
		<cfproperty name="contractEnd" required="true" type="Date" hint=""/>
		<cfproperty name="contractStart" required="true" type="Date" hint=""/>
		<cfproperty name="serviceEnd" required="true" type="Date" hint=""/>
		<cfproperty name="serviceStart" required="true" type="Date" hint=""/>
		<cfproperty name="accountNumber" required="true" type="String" hint=""/>
		<cfproperty name="address" required="true" type="String" hint=""/>
		<cfproperty name="city" required="true" type="String" hint=""/>
		<cfproperty name="companyName" required="true" type="String" hint=""/>
		<cfproperty name="contractEnergyType" required="true" type="String" hint=""/>
		<cfproperty name="energyProvider" required="true" type="String" hint=""/>
		<cfproperty name="energyType" required="true" type="String" hint=""/>
		<cfproperty name="postalCode" required="true" type="String" hint=""/>
		<cfproperty name="profileField" required="true" type="String" hint=""/>
		<cfproperty name="propertyName" required="true" type="String" hint=""/>
		<cfproperty name="serviceName" required="true" type="String" hint=""/>
		<cfproperty name="stateRegion" required="true" type="String" hint=""/>
		<cfproperty name="relStart" required="true" type="Date" hint=""/>
		<cfproperty name="relEnd" required="true" type="Date" hint=""/>
		<cfproperty name="searchOperator" required="true" type="String" hint=""/>
		<cfproperty name="propertyMetaValue" required="true" type="String" hint="">
		<cfproperty name="propertyMetaRangeFrom" required="true" type="String" hint="">
		<cfproperty name="propertyMetaRangeTo" required="true" type="String" hint="">
	<!--- Define the meta-data for a given value object. --->

	<!--- Default any required properties. --->

</cfcomponent>
