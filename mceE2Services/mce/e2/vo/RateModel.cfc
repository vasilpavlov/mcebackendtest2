<cfcomponent displayName="Rate Model Value Object"
			 output="false"
			 alias="mce.e2.vo.RateModel" style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the details of a given rate model.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="rate_model_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given rate model definition.">
	<cfproperty name="model_lookup_code" type="string" required="true" hint="Describes the internal lookup code for a given rate model definition.">
	<cfproperty name="friendly_name" type="string" required="true" hint="Describes the friendly name / customer facing name for a given rate model definition.">
	<cfproperty name="implementation_file" type="string" required="true" hint="Describes the implementation file associated to a given rate model definition.">
	<cfproperty name="defaults_input_set_uid" type="string" required="false" hint="Describes the primary key / unique identifier of the default input set associated to a given rate model definition.">
	<cfproperty name="interval_classifier_uid" type="string" required="false" hint="Describes the primary key / unique identifier of the interval classifier associated to a given rate model.">

	<!--- Define the meta-data for a given value object --->
	<cfproperty name="primaryKey" type="string" required="true" default="rate_model_uid" hint="Describes the property that acts as the primary key for a given rate model.">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Set any defaults --->
	<cfset this.rate_model_uid = "">
	<cfset this.primaryKey = "rate_model_uid">

</cfcomponent>