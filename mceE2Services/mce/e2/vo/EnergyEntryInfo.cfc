<cfcomponent output="false"
				alias="mce.e2.vo.EnergyEntryInfo"
				style="rpc"
				extends="BaseAuditValueObject">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="energy_account_uid" type="string" hint="Describes the primary key / unique identifier for a given energy account record.">
	<cfproperty name="energy_unit_uid" type="string" hint="Describes the primary key / unique identifier for a given energy unit record.">
	<cfproperty name="energy_usage_uid" type="string" hint="Describes the primary key / unique identifier for a given energy usage record.">
	<cfproperty name="native_account_number" type="string" hint="Describes the navtive account number for a given energy account.">
	<cfproperty name="period_end" type="date" hint="Describes the end date for a given energy usage period.">
	<cfproperty name="period_start" type="date" hint="Describes the start date for a given energy usage period.">
	<cfproperty name="report_date" type="date" hint="Describes the date used for reporting purposes.">
	<cfproperty name="recorded_value" type="numeric" hint="Describes the energy usage recorded value.">
	<cfproperty name="usage_status_code" type="string" hint="Describes the energy usage status.">
	<cfproperty name="usage_status_date" type="date" hint="Describes the energy usage status date.">
	<cfproperty name="consider_touched_date" type="date" hint="Describes the date that the record was changed in a meaningful way.">

	<!--- New properties added based on extensions to dbo.energyUsage --->
	<cfproperty name="total_cost" type="numeric" required="true" hint="Describes the total cost associated to a given energyUsage record.">
	<cfproperty name="user_comments" type="string" required="false" hint="Describes any energyUsage comments recorded.">
	<cfproperty name="has_total_cost" type="boolean" required="true" hint="Describes the has_total_cost value of the associated MetaTypeGroup record.">
	<cfproperty name="has_recorded_value" type="boolean" required="true" hint="Describes the has_recorded_value value of the associated MetaTypeGroup record.">
	<cfproperty name="conversionEnergyUnitFriendlyName" type="string" required="false" hint="Describes the friendly name of the associated conversion energy unit.">
	<cfproperty name="conversion_factor" type="numeric" required="false" hint="Describes the factor / value / modified used to convert energy usage data.">
	<cfproperty name="usage_type_code" type="string" hint="Describes the lookup code of the usage type.">
	<cfproperty name="total_utility_cost" type="numeric" required="false" hint="Describes the total utility cost associated to a given energyUsage record.">
	<cfproperty name="total_supply_cost" type="numeric" required="false" hint="Describes the total supply cost associated to a given energyUsage record.">
	<cfproperty name="td_usage_meta_uid" type="string" hint="Describes the primary key for the TD Charges meta record.">
	<cfproperty name="supply_usage_meta_uid" type="string" hint="Describes the primary key for the Supply Charges meta record.">
	<cfproperty name="td_meta_type_uid" type="string" hint="Describes the primary key for the TD Charges meta type record.">
	<cfproperty name="supply_meta_type_uid" type="string" hint="Describes the primary key for the Supply Charges meta type record.">

    <!--- Computed fields which should come back to server on updates; these should also exist in corresonding actionscript class --->
	<cfproperty name="metaFields" type="array" hint="Describes the collection of property meta fields associated to a given energy entry information record.">
	<cfproperty name="priorValues" type="array" hint="Describes the collection of prior period start / period end values to a given energy entry information record.">

	<!--- Additional fields to assist UI, but which do not need to come back to server on updates; these should exist in actionscript class, but should be marked [Transient] there so the Flex app doesn't bother sending them back to the server --->
	<cfproperty name="energyAccountName" type="string" hint="Describes the friendly name of the associated energy account.">
	<cfproperty name="energyAccountNotes" type="string" hint="Notes for the Energy Accounts.">
	<cfproperty name="energyUnitFriendlyName" type="string" hint="Describes the friendly name of the associated energy unit.">
	<cfproperty name="energyTypeFriendlyName" type="string" hint="Describes the friendly name of the associated energy type.">
	<cfproperty name="priorPeriods" type="array" hint="Describes any prior periods associated to an energy record.">
	<cfproperty name="propertyFriendlyName" type="string" hint="Describes the friendly name of the associated property.">
	<cfproperty name="providerFriendlyName" type="string" hint="Describes the friendly name of the associated energy provider.">
	<cfproperty name="usageStatusFriendlyName" type="string" hint="Describes the friendly name of the usage status.">
	<cfproperty name="propertyClientName" type="string" hint="Describes the friendly name of the property's associated company with the 'client' role, if any'.">
	<cfproperty name="is_delivery_based" type="boolean" hint="Whether the energy type is delivery-based (eg, Oil).">
	<cfproperty name="energyContract" type="EnergyContracts" hint="Describes the collection of prior period start / period end values to a given energy entry information record.">
	
	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>
	<cfproperty name="document_set_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given document set."/>
	<cfproperty name="documentSetFriendlyName" required="true" type="string" hint="Describes the friendly name of the documentation set associated to a given energy usage entry.">
	<cfproperty name="documentSetDocumentCount" required="true" type="numeric" default="0" hint="Describes the count of documents associated to the specified document set.">
	<cfproperty name="numberOfDays" required="true" type="numeric" default="0" hint="The number of days that the usage period covers.">

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>
	
	<!---New fields from EnergyUsage--->
	<cfproperty name="is_system_calculated" required="true" type="boolean" hint="Describes if the recorded value was system calculated."/>
	
	<cfproperty name="hasBill" required="false" type="boolean" hint="Describes that this usage is according to the bill"/>
	<cfproperty name="hasScreen" required="false" type="boolean" hint="Describes that this usage is according to the screenshot"/>
	
	<cfproperty name="created_rate_model_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a the first rate model."/>
	<cfproperty name="created_meta_group_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a the first Data Entry Fields (MetaTypeGroup)"/>
	
	<cfproperty name="created_rate_model_name" required="true" type="string" hint="Describes the name for a the first rate model."/>
	<cfproperty name="created_meta_group_name" required="true" type="string" hint="Describes the name for a the first Data Entry Fields (MetaTypeGroup)"/>
	
	<cfproperty name="current_rate_model_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a the first rate model."/>
	<cfproperty name="current_meta_group_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a the first Data Entry Fields (MetaTypeGroup)"/>
	
	<cfproperty name="current_rate_model_name" required="true" type="string" hint="Describes the name for a the first rate model."/>
	<cfproperty name="current_meta_group_name" required="true" type="string" hint="Describes the name for a the first Data Entry Fields (MetaTypeGroup)"/>
</cfcomponent>