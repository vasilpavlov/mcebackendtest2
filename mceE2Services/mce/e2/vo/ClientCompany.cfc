<cfcomponent displayName="Client Company Value Object"
			 output="false"
			 alias="mce.e2.vo.ClientCompany" style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the details of a given client company.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="client_company_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given client company."/>
	<cfproperty name="friendly_name" type="string" required="true" hint="Describes the friendly name / customer facing name for a given client company."/>
	<cfproperty name="image_filename" required="true" type="string" hint="Describes the last name of the client company."/>
	<cfproperty name="company_hierarchyid" type="any" required="true" hint="Describes the hierarchy node location for a given company within the company hierarchy."/>
	<cfproperty name="oldId" type="numeric" required="false" hint="Describes the external / legacy application primary key for this data (used to migrate data from v1 to v2)."/>
	<cfproperty name="document_set_uid" type="string" required="false" hint="Describes primary key / unique identifier for the document set assigned to this client company."/>
	<cfproperty name="company_type_code" type="string" required="true" hint="Describes the company type for a client company."/>

	<!--- Define the meta-data for a given value object --->
	<cfproperty name="primaryKey" type="string" required="true" default="client_company_uid" hint="Describes the property that acts as the primary key for a given client company."/>
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>
	<cfproperty name="companyRoleFriendlyName" type="string" required="true" hint="Describes the friendly name / customer facing name for a given company role."/>
	<cfproperty name="companyTypeFriendlyName" type="string" required="true" hint="Describes the friendly name / customer facing name for a given company type."/>
	<cfproperty name="propertyFriendlyName" type="string" required="true" hint="Describes the friendly name / customer facing name for a given property."/>
	<cfproperty name="relationship_id" type="numeric" required="false" hint="Describes the primary key / unique identifier of a relationship between a client company and property."/>
	<cfproperty name="relationship_start" required="true" type="date" hint="Describes the start date when a given client company / property relationship will begin being respected."/>
	<cfproperty name="relationship_end" required="true" type="date" hint="Describes the end date when a given client company / property relationship will no longer be respected."/>
	<cfproperty name="my_hier" type="string" required="false" hint="Describes hierarchy position for client company"/>
	<cfproperty name="parent_hier" type="string" required="false" hint="Describes parent hierarchy position for client company"/>
	<cfproperty name="is_parent" type="string" required="false" hint="Describes whether or not this is a parent company"/>
		
	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Set any defaults --->
	<cfset this.client_company_uid = "">
	<cfset this.primaryKey = "client_company_uid">

</cfcomponent>