<cfcomponent displayname="CompanyAddress"
				hint="This CFC manages all object/bean/value object instances for the CompanyAddresses table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.CompanyAddress"
				style="rpc">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="company_address_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given company / address association."/>
	<cfproperty name="client_company_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given associated company."/>
	<cfproperty name="address_type_code" required="true" type="string" hint="Describes the primary key / unique identifier of the associated address type."/>
	<cfproperty name="relationship_start" required="true" type="date" hint="Describes the date that the active relationship between a given company and address started."/>
	<cfproperty name="relationship_end" required="true" type="date" hint="Describes the date that the active relationship between a given company and address ended."/>
	<cfproperty name="address_line_1" required="true" type="string" hint="Describes the first address line for a given address."/>
	<cfproperty name="address_line_2" required="true" type="string" hint="Describes the 2nd address line for a given address."/>
	<cfproperty name="city" required="true" type="string" hint="Describes the city associated to a given address."/>
	<cfproperty name="state" required="true" type="string" hint="Describes the state / province associated to a given address."/>
	<cfproperty name="country" required="true" type="string" hint="Describes the country associated to a given address."/>
	<cfproperty name="postal_code" required="true" type="string" hint="Describes the postal code / zip code associated to a given address."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="company_address_uid" hint="Describes the primary key for the current object / table."/>
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>
	<cfproperty name="companyFriendlyName" required="false" type="string" hint="Describes the friendly name for the associated company."/>
	<cfproperty name="addressTypeFriendlyName" required="false" type="string" hint="Describes the friendly name for the associated address type."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>
	
	<!--- Default any required properties. --->
	<cfset this.primaryKey = "company_address_uid"/>
	<cfset this.company_address_uid = ""/>

</cfcomponent>
