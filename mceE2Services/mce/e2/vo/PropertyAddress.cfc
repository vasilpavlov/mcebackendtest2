
<!---

  Template Name:  PropertyAddress
     Base Table:  PropertyAddresses	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Monday, March 30, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Monday, March 30, 2009 - Template Created.		

--->
		
<cfcomponent displayname="PropertyAddress"
				hint="This CFC manages all object/bean/value object instances for the PropertyAddresses table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.PropertyAddress"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="property_address_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given property / address association."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
	<cfproperty name="address_type_code" required="true" type="string" hint="Describes the primary key / unique identifier of the associated address type."/>
	<cfproperty name="relationship_start" required="true" type="date" hint="Describes the date that the active relationship between a given property and address started."/>
	<cfproperty name="relationship_end" required="true" type="date" hint="Describes the date that the active relationship between a given property and address ended."/>
	<cfproperty name="address_line_1" required="true" type="string" hint="Describes the first address line for a given address."/>
	<cfproperty name="address_line_2" required="true" type="string" hint="Describes the 2nd address line for a given address."/>
	<cfproperty name="city" required="true" type="string" hint="Describes the city associated to a given address."/>
	<cfproperty name="state" required="true" type="string" hint="Describes the state / province associated to a given address."/>
	<cfproperty name="country" required="true" type="string" hint="Describes the country associated to a given address."/>
	<cfproperty name="postal_code" required="true" type="string" hint="Describes the postal code / zip code associated to a given address."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given rate model record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given rate model record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given rate model record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given rate model record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="property_address_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>
	<cfproperty name="propertyFriendlyName" required="false" type="string" hint="Describes the friendly name for the associated property.">
	<cfproperty name="addressTypeFriendlyName" required="false" type="string" hint="Describes the friendly name for the associated address type.">

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "property_address_uid"/>
	<cfset this.property_address_uid = ""/>

</cfcomponent>
