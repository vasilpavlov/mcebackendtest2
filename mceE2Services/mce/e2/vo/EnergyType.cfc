
<!---

  Template Name:  EnergyType
     Base Table:  EnergyTypes

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Friday, March 20, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Friday, March 20, 2009 - Template Created.

--->

<cfcomponent displayname="EnergyType"
				hint="This CFC manages all object/bean/value object instances for the EnergyTypes table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyType"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="energy_type_uid" required="true" type="string" hint="Describes the primary key / unique identifierfor a given energy type."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given energy type."/>
	<cfproperty name="energy_type_hierarchy" required="true" type="string" hint="Describes the hierarchy element / node for a given energy type."/>
	<cfproperty name="default_energy_unit_uid" required="true" type="string" hint="Describes the default energy unit associated to a given energy type."/>
	<cfproperty name="oldid" required="true" type="string" hint="Describes the legacy primary key value for a given energy type record."/>
	<cfproperty name="demand" required="true" type="boolean" hint="Describes the demand status of a given energy type record."/>
	<cfproperty name="is_delivery_based" required="true" type="boolean" hint="Describes the delivery-based status of a given energy type record."/>
	<cfproperty name="meta_group_uid" required="true" type="string" hint="Describes the meta group a given energy type is associated to."/>
	<cfproperty name="default_factor_alias_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the default factor alias."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="energy_type_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given rate model record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given rate model record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given rate model record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given rate model record was last modified."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "energy_type_uid"/>
	<cfset this.energy_type_uid = ""/>

</cfcomponent>
