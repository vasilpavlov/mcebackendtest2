
<!---

  Template Name:  PropertyClientCompanyAssociation
     Base Table:  Properties_ClientCompanies	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, April 02, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, April 02, 2009 - Template Created.		

--->
		
<cfcomponent displayname="PropertyClientCompanyAssociation"
				hint="This CFC manages all object/bean/value object instances for the Properties_ClientCompanies table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.PropertyClientCompanyAssociation"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="relationship_id" required="true" type="numeric" hint="Describes the primary key / unique identifier representing the relationship between a property / client company / company role."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier representing an associated property."/>
	<cfproperty name="client_company_uid" required="true" type="string" hint="Describes the primary key / unique identifier representing an associated client company."/>
	<cfproperty name="company_role_uid" required="true" type="string" hint="Describes the primary key / unique identifier representing an associated company role."/>
	<cfproperty name="relationship_start" required="true" type="date" hint="Describes the start date when a given client company / company role / property relationship will begin being respected."/>
	<cfproperty name="relationship_end" required="true" type="date" hint="Describes the end date when a given client company / company role / property relationship will no longer be respected."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="companyRoleFriendlyName" type="string" required="true" hint="Describes the friendly name / customer facing name for a given company role.">
	<cfproperty name="propertyFriendlyName" type="string" required="true" hint="Describes the friendly name / customer facing name for a given property.">
	<cfproperty name="companyTypeFriendlyName" type="string" required="true" hint="Describes the friendly name / customer facing name for a given company type."/>
	<cfproperty name="clientCompanyFriendlyName" required="false" type="string" hint="Describes the external / customer facing friendly name for a given property associated client contact role."/>
	<cfproperty name="primaryKey" required="true" type="string" default="relationship_id" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "relationship_id"/>
	<cfset this.relationship_id = 0/>

</cfcomponent>
