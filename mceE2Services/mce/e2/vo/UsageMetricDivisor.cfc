
<!---

  Template Name:  UsageMetricDivisor
     Base Table:  OptionsForMetricDivisors

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Monday, May 11, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Monday, May 11, 2009 - Template Created.

--->

<cfcomponent displayname="UsageMetricDivisor"
				hint="This CFC manages all object/bean/value object instances for the OptionsForMetricDivisors table, and is used to pass object instances through the applicaiton service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.UsageMetricDivisor"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given metric divisor."/>
	<cfproperty name="divisorValue" required="true" type="numeric" hint="Describes the associated divisor value for a given divisor."/>
	<cfproperty name="property_meta_type_lookup_code" required="true" type="string" hint="Describes the associated divisor value for a given divisor."/>
	<cfproperty name="propertyMetaTypeFriendlyName" required="true" type="string" hint="The friendly name of the associated property meta data type."/>
	<cfproperty name="calculation_operation" required="true" type="string" hint="Describes the associated divisor value for a given divisor."/>

</cfcomponent>