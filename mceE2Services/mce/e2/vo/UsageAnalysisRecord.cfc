<cfcomponent extends="mce.e2.vo.BaseValueObject" output="false">

	<cfproperty name="property_uid" type="string" />
	<cfproperty name="energy_account_uid" type="string" />
	<cfproperty name="emissions_source_uid" type="string" />
	<cfproperty name="emissions_value" type="any" />
	<cfproperty name="relationship_start" type="date" />
	<cfproperty name="relationship_end" type="date" />

	<cfproperty name="propertyFriendlyName" type="any" />
	<cfproperty name="energyAccountFriendlyName" type="any" />
	<cfproperty name="emissionsSourceFriendlyName" type="any" />
</cfcomponent>