
<!---

  Template Name:  DataReviewDetailStatus
     Base Table:  DataReviewDetail

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, April 22, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, April 22, 2009 - Template Created.		

--->
		
<cfcomponent displayname="DataReviewDetailStatus"
				hint="This CFC is used to manage the approval status cod and related notes for a given reviwe detail record, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.DataReviewDetailStatus"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="review_detail_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given review detail record."/>
	<cfproperty name="data_review_uid" required="true" type="string" hint="Describes the primary key / unique identifier fora  given data review record."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the property associated to a given data review detail."/>
	<cfproperty name="approval_status_code" required="true" type="string" hint="Describes the approval status code for a given data review record."/>
	<cfproperty name="approval_status_notes" required="true" type="string" hint="Describes the approval status notes for a given data review record."/>

</cfcomponent>