
<!---

  Template Name:  DataSeriesOption
     Base Table:  OptionsForDataSeries

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Monday, May 11, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Monday, May 11, 2009 - Template Created.

--->

<cfcomponent displayname="DataSeriesOption"
				hint="This CFC manages all object/bean/value object instances for the OptionsForDataSeries table, and is used to pass object instances through the applicaiton service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.DataSeriesOption"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="series_option_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data series option."/>
	<cfproperty name="namespace_lookup_code" required="true" type="string" hint="Describes the namespace lookup code for a given data series option."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the external / customer facing name for a given data series option."/>
	<cfproperty name="energy_type_uid" required="true" type="string" hint="Describes the primary key / unique identifier for an associated energy type."/>
	<cfproperty name="series_area" required="true" type="string" hint="Describes the lookup code / internal identifier for the associated series area type."/>
	<cfproperty name="data_source_type" required="true" type="string" hint="Describes the lookup code / internal identifier for the associated data source type."/>
	<cfproperty name="axis_scale" required="true" type="string" hint="Describes the lookup code / internal identifier for the associated data series axis scale."/>
	<cfproperty name="display_order" required="true" type="any" hint="Describes the sequence / display order of data series options."/>
	<cfproperty name="factor_lookup_code" required="true" type="string" hint="Describes the lookup code / identifier of the associated rate factor lookup code."/>
	<cfproperty name="property_meta_type_code" required="true" type="string" hint="Describes the lookup code for the associated property meta type."/>
	<cfproperty name="usage_meta_type_code" required="true" type="string" hint="Describes the lookup code for the associated property meta type."/>
	<cfproperty name="series_style_name" required="true" type="string"/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="series_option_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "series_option_uid"/>
	<cfset this.series_option_uid = ""/>

</cfcomponent>
