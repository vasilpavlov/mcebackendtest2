<cfcomponent displayName="Rate Model Input Value Object"
			 output="false"
			 alias="mce.e2.vo.RateModelInputValue" style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the details of a given rate model input.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="relationship_id" type="numeric" required="false" hint="Describes the primary key representing the relationship between a rate model and rate model input set.">
	<cfproperty name="model_input_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given rate model input set.">
	<cfproperty name="input_set_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given rate model input set definition.">
	<cfproperty name="factor_lookup_code" type="string" required="true" hint="Describes the internal facing lookup code for the rate factor associated to a given model input set definition.">
	<cfproperty name="input_lookup_code" type="string" required="true" hint="Describes the internal facing lookup code for the rate model input set definition.">
	<cfproperty name="input_value" type="numeric" required="true" hint="Describes the energy value for a given rate model input set definition.">
	<cfproperty name="relationship_start" type="date" required="true" hint="Describes the start date used to enforce the relationship between a rate model and a rate model input set.">
	<cfproperty name="relationship_end" type="date" required="false" hint="Describes the end date used to enforce the relationship between a rate model and a rate model input set.">

	<!--- Define the meta-data for a given value object --->
	<cfproperty name="primaryKey" type="string" required="true" default="relationship_id" hint="Describes the property that acts as the primary key for a given rate model.">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>
	<cfproperty name="inputFriendlyName" type="string" required="false" hint="Describes the friendly name of this particular rate model input." />

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Set any defaults --->
	<cfset this.primaryKey = "">
	<cfset this.primaryKey = "relationship_id">

</cfcomponent>
