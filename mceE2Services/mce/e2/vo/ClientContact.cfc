
<!---

  Template Name:  ClientContact
     Base Table:  ClientContacts	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Tuesday, March 31, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Tuesday, March 31, 2009 - Template Created.		

--->
		
<cfcomponent displayname="ClientContact"
				hint="This CFC manages all object/bean/value object instances for the ClientContacts table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.ClientContact"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="client_contact_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given client contact."/>
	<cfproperty name="contact_hierarchyid" required="true" type="any" hint="Describes the hierarchy location identifier for a given client contact (the place in the hierarchy this contact exists)."/>
	<cfproperty name="user_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the user associated to a given client contact."/>
	<cfproperty name="first_name" required="true" type="string" hint="Describes the first name of the client contact."/>
	<cfproperty name="last_name" required="true" type="string" hint="Describes the image file name associated to a given client contact."/>
	<cfproperty name="image_filename" required="true" type="string" hint="Describes the last name of the client contact."/>
	<cfproperty name="oldid" required="true" type="numeric" hint="Describes the legacy primary key / unique identifier for a given client contact record."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>
	<cfproperty name="external_identifier" required="true" type="numeric" hint="Describes the external system identifier for a given client contact."/>
	<cfproperty name="email" required="true" type="string" hint="Describes the email address of the client contact."/>
	<cfproperty name="phone" required="true" type="string" hint="Describes the phone number of the client contact."/>
	<cfproperty name="comments" required="true" type="string" hint="Describes the comments for the client contact."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="client_contact_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Define the computed columns for this component --->
	<cfproperty name="propertyFriendlyName" required="false" type="string" hint="Describes the external / customer facing friendly name for an associated property."/>
	<cfproperty name="propertyContactRoleFriendlyName" required="false" type="string" hint="Describes the external / customer facing friendly name for a given property associated client contact role."/>
	<cfproperty name="clientCompanyContactRoleFriendlyName" required="false" type="string" hint="Describes the external / customer facing friendly name for a given client company associated client contact role."/>
	<cfproperty name="clientCompanyFriendlyName" required="false" type="string" hint="Describes the external / customer facing friendly name for an associated client company."/>
	<cfproperty name="client_company_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client company."/>
	<cfproperty name="relationship_id" type="numeric" required="false" hint="Describes the primary key / unique identifier of a relationship between a client contact and property.">
	<cfproperty name="relationship_start" required="true" type="date" hint="Describes the start date enforcing when the relationship between a given property / client contact is enforced."/>
	<cfproperty name="relationship_end" required="true" type="date" hint="Describes the end date enforcing when the relationship between a given property / client contact will no longer be enforced."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "client_contact_uid"/>
	<cfset this.client_contact_uid = ""/>

</cfcomponent>