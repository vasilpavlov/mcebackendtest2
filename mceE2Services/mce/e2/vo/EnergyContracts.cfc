<cfcomponent displayname="EnergyContracts"
			hint="This CFC manages all object/bean/value object instances for the EnergyContracts table"
			extends="BaseValueObject"
			alias="mce.e2.vo.EnergyContracts"
			style="rpc"
			output="false">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="energy_contract_uid"  required="true"  type="string" hint="x"/>
	<cfproperty name="friendly_name"  required="false"  type="string" hint="x"/>
	<cfproperty name="contract_start"  required="false"  type="date" hint="x"/>
	<cfproperty name="contract_end"  required="false"  type="date" hint="x"/>
	<cfproperty name="document_set_uid"  required="true"  type="string" hint="x"/>
	<cfproperty name="meta_group_uid"  required="true"  type="string" hint="x"/>
	<cfproperty name="energy_type_uid"  required="true"  type="string" hint="x"/>
	<cfproperty name="contract_id"  required="true"  type="numeric" hint="x"/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey"  required="true"  type="string"  default="energy_contract_uid"  hint="Describes the primary key for the current object / table." >
	<cfproperty name="isAssociated"  required="true"  type="boolean"  default="false"  hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)." />
	<cfproperty name="input_set_uid"  required="true"  type="string" hint="Describes default input set for a particular Energy Contract"/>
	<cfproperty name="client_company_uid"  required="false"  type="string" hint="x"/>
	<cfproperty name="property_uid"  required="false"  type="string" hint="x"/>
	<cfproperty name="energy_account_uid"  required="false"  type="string" hint="x"/>
	<cfproperty name="energyAccountFriendlyName"  required="false"  type="string" hint="x"/>
	<cfproperty name="metaFields" type="array" hint="Describes the collection of property meta fields associated to a given energy contract information record.">
	<cfproperty name="energy_provider_uid"  required="false"  type="string" hint="x"/>
	<cfproperty name="documentCount" required="false" type="numeric" hint="Total number of documents that exist for this energy contract" />
	<cfproperty name="energyTypeFriendlyName"  required="true"  type="string" hint="x"/>
	<cfproperty name="energyProviderFriendlyName"  required="false"  type="string" hint="x"/>
	<cfproperty name="relatedEnergyAccountUids"  required="false"  type="string" hint="x"/>
	<cfproperty name="contractPrice"  required="false"  type="numeric"  hint="x"/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active"  required="false"  type="boolean" hint="x"/>
	<cfproperty name="created_by"  required="true"  type="string" hint="x"/>
	<cfproperty name="created_date"  required="false"  type="date" hint="x"/>
	<cfproperty name="modified_by"  required="true"  type="string" hint="x"/>
	<cfproperty name="modified_date"  required="false"  type="date" hint="x"/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "energy_contract_uid"/>
	<cfset this.energy_contract_uid = ""/>
</cfcomponent>