
<!---

  Template Name:  Energy Unit
     Base Table:  EnergyUnits	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, March 20, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, March 20, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnergyUnit"
				hint="This CFC manages all object/bean/value object instances for the EnergyUnits table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueobject"
				alias="mce.e2.vo.EnergyUnit"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="energy_unit_uid" required="true" type="string" hint="Describes the primary key for a given energy unit record."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the customer facing / external name for a given energy unit."/>
	<cfproperty name="unit_lookup_code" required="true" type="string" hint="Describes the internal lookup code for a given energy unit."/>
	<cfproperty name="btu_conversion_factor" required="true" type="any" hint="Describes the btu conversion factor applied / associated to a given energy unit."/>
	<cfproperty name="oldid" required="true" type="numeric" hint="Describes the legacy primary key value for a given energy unit record."/>
	<cfproperty name="meta_group_uid" required="true" type="string" hint="Describes the primary key of the meta group associated to a given energy unit record."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="energy_unit_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given rate model record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given rate model record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given rate model record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given rate model record was last modified."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "energy_unit_uid"/>
	<cfset this.energy_unit_uid = ""/>

</cfcomponent>
