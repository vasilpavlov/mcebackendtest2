<cfcomponent
	alias="mce.e2.vo.TaxFactor"
	style="rpc" 
	extends="BaseValueObject" output="false">

	<cfproperty name="postal_code" type="string" />
	<cfproperty name="commodity_factor" type="string" />
	<cfproperty name="delivery_factor" type="string" />
	<cfproperty name="sales_factor" type="string" />
</cfcomponent>