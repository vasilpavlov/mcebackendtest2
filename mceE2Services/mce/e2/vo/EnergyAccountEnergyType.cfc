
<!---

  Template Name:  EnergyAccountEnergyType
     Base Table:  EnergyAccount, EnergyType, Property	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Sunday, March 22, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Monday, May 25th 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnergyAccountEnergyType"
				hint="This CFC manages all object/bean/value object instances for energy account / energy type summaries."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyAccountEnergyType"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given energy account definition."/>
	<cfproperty name="energy_type_uid" required="true" type="string" hint="Describes the primary key / unique identifier for the associated energy type definition."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier for the associated property definition.">
	
	<!--- Declare the system calculated / defined properties --->
	<cfproperty name="energyAccountFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated energy account."/>
	<cfproperty name="energyTypeFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated energy type."/>
	<cfproperty name="propertyFriendlyName" required="true" type="string" hint="Describes the friendly name of the associated property."/>

</cfcomponent>