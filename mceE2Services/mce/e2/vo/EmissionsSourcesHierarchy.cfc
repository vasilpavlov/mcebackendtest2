
<!---

  Template Name:  EmissionsSourcesHierarchy
     Base Table:  EmissionsProfile, EmissionsSources	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, May 6th, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, May 6th, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EmissionsSourcesHierarchy"
				hint="This CFC is used to manage / represent energy hierarchy elements from the emissionsProfiles / emissionsSources tables.."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EmissionsSourcesHierarchy"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="emissions_source_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given emissions source / hierarchy node.">
	<cfproperty name="hierarchy_id" required="true" type="string" hint="Describes the hierarchy identifier for a given hierarchy node.">
	<cfproperty name="hierarchy_position" required="true" type="string" hint="Describes the outline position of a given emissions source / hierarchy node within its parent hierarchy.">
	<cfproperty name="parent_position" required="true" type="string" hint="Describes the outline position of a given emissions source / hierarchy node's parent item within its parent hierarchy.">
	<cfproperty name="energy_type_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the energy type associated to a given emissions source / hierarchy node.">

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="emissionsSourceFriendlyName" required="true" type="string" hint="Describes the friendly name of the emissions source / hierarchy level.">
	<cfproperty name="energyTypeFriendlyName" required="true" type="string" hint="Describes the friendly name of the energy type associated to an emissions source / hierarchy level.">

</cfcomponent>