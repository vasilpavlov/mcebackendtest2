<cfcomponent displayname="CompanySearch"
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.CompanySearch"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
		<cfproperty name="serviceEnd" required="true" type="Date" hint=""/>
		<cfproperty name="serviceStart" required="true" type="Date" hint=""/>
		<cfproperty name="address" required="true" type="String" hint=""/>
		<cfproperty name="city" required="true" type="String" hint=""/>
		<cfproperty name="companyName" required="true" type="String" hint=""/>
		<cfproperty name="postalCode" required="true" type="String" hint=""/>
		<cfproperty name="propertyName" required="true" type="String" hint=""/>
		<cfproperty name="serviceName" required="true" type="String" hint=""/>
		<cfproperty name="stateRegion" required="true" type="String" hint=""/>
		<cfproperty name="searchOperator" required="true" type="String" hint=""/>
		<cfproperty name="contactFirstName" required="true" type="String" hint=""/>
		<cfproperty name="contactLastName" required="true" type="String" hint=""/>
		<cfproperty name="contactPhone" required="true" type="String" hint=""/>
		<cfproperty name="contactEmail" required="true" type="String" hint=""/>
		<cfproperty name="contactRole" required="true" type="String" hint=""/>
		<cfproperty name="userFirstName" required="true" type="String" hint=""/>
		<cfproperty name="userLastName" required="true" type="String" hint=""/>
		<cfproperty name="username" required="true" type="String" hint=""/>
		<cfproperty name="userEmail" required="true" type="String" hint=""/>
		<cfproperty name="relatedRole" required="true" type="String" hint=""/>
		<cfproperty name="notRelatedRole" required="true" type="String" hint=""/>
		<cfproperty name="property_uids" required="true" type="String" hint=""/>
		
	<!--- Define the meta-data for a given value object. --->

	<!--- Default any required properties. --->

</cfcomponent>
