
<!---

  Template Name:  EnergyUsage
     Base Table:  EnergyUsage

abe@twintechs.com

       Author:  Abraham Lloyd,Ilian Mihaylov
 Date Created:  Friday, March 20, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Friday, March 20, 2009 - Template Created.
08 Aug 2013 - added new fields to table - hasBill, hasScreen

--->

<cfcomponent displayname="EnergyUsage"
				hint="This CFC manages all object/bean/value object instances for the EnergyUsage table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.EnergyUsage"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="energy_usage_uid" required="true" type="string" hint="Describes the primary key / unique identifier of a given energy usage record."/>
	<cfproperty name="energy_account_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given / associated energy account record."/>
	<cfproperty name="period_start" required="true" type="date" hint="Describes the start date for a given energy usage period."/>
	<cfproperty name="period_end" required="true" type="date" hint="Describes the end date for a given energy usage period."/>
	<cfproperty name="report_date" required="true" type="date" hint="Describes the report date for a given energy usage period."/>
	<cfproperty name="energy_unit_uid" required="true" type="string" hint="Describes the primary key of the related / associated energy unit."/>
	<cfproperty name="classification_code" required="true" type="string" hint="Describes the clasification code for a given energy usage record."/>
	<cfproperty name="recorded_value" required="true" type="any" hint="Describes the recorded energy usage value."/>
	<cfproperty name="is_system_calculated" required="true" type="boolean" hint="Describes if the recorded value was system calculated."/>
	<cfproperty name="document_set_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a related document set."/>
	<cfproperty name="emission_factor_uid" required="true" type="string" hint="Describes the primary key of the related emission factor."/>
	<cfproperty name="total_cost" required="true" type="numeric" hint="Describes the monitary cost associated to a given energy usage record."/>
	<cfproperty name="usage_status_code" type="string" hint="Describes the energy usage status.">
	<cfproperty name="user_comments" type="string" hint="The most recent user comments for the energy usage record.">
	<cfproperty name="usage_status_date" type="date" hint="Describes the energy usage status date.">

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="energy_usage_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>
	<cfproperty name="documentSetFriendlyName" required="true" type="string" hint="Describes the friendly name of the documentation set associated to a given energy usage entry.">
	<cfproperty name="documentSetDocumentCount" required="true" type="numeric" default="0" hint="Describes the count of documents associated to the specified document set.">
	<cfproperty name="usageStatusFriendlyName" type="string" hint="Describes the friendly name of the usage status.">
	<cfproperty name="energyUnitFriendlyName" type="string" hint="Describes the friendly name of the energy unit.">
	<cfproperty name="numberOfDays" type="numeric" hint="Calculated from start and end dates">

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given rate model record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a  given rate model record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given rate model record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given rate model record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given rate model record was last modified."/>
	<cfproperty name="hasBill" required="false" type="boolean" hint="Describes that this usage is according to the bill"/>
	<cfproperty name="hasScreen" required="false" type="boolean" hint="Describes that this usage is according to the screenshot"/>
	
	<cfproperty name="estimated" required="false" type="boolean" hint="Describes that this usage is estimated"/>
	<cfproperty name="estimatedInternal" required="false" type="boolean" hint="Describes that this usage is estimated internal"/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "energy_usage_uid"/>
	<cfset this.energy_usage_uid = ""/>

</cfcomponent>
