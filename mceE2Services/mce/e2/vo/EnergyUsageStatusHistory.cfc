<cfcomponent	output="false" 
				alias="mce.e2.vo.EnergyUsageStatusHistory" 
				style="rpc" 
				extends="BaseAuditValueObject">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="status_history_uid" type="string" hint="Describes the primary key / unique identifier for a given energy usage status history record.">
	<cfproperty name="energy_usage_uid" type="string" hint="Describes the primary key / unique identifier for a given energy usage record.">
	<cfproperty name="user_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given user.">
	<cfproperty name="usage_status_date" type="date" hint="Describes the date for a given usage status.">
	<cfproperty name="user_comments" type="string" hint="Describes the user comments on a given usage history.">
	
	<!--- Additional fields to assist UI, but which do not need to come back to server on updates; these should exist in actionscript class, but should be marked [Transient] there so the Flex app doesn't bother sending them back to the server --->
	<cfproperty name="usageStatusFriendlyName" type="string" hint="Describes the friendly name of the associated usage status.">
	<cfproperty name="userFirstName" type="string" hint="Describes the first name of a given user.">
	<cfproperty name="userLastName" type="string" hint="Describes the last name of a given user.">
	
</cfcomponent>