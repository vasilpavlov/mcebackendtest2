
<!---

  Template Name:  PropertyMeta
     Base Table:  PropertyMeta	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Tuesday, March 31, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Tuesday, March 31, 2009 - Template Created.		

--->
		
<cfcomponent displayname="PropertyMeta"
				hint="This CFC manages all object/bean/value object instances for the PropertyMeta table, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.PropertyMeta"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="property_meta_uid" required="true" type="string" hint="Describes the primary key / unique identifier associated to a given property / meta record."/>
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the property related to a given meta data record."/>
	<cfproperty name="meta_type_uid" required="true" type="string" hint="Describes the primary key / unique identifier of the meta type related to a given property / meta data record."/>
	<cfproperty name="meta_value" required="true" type="string" hint="Describes the meta value associated to a given property."/>
	<cfproperty name="relationship_start" required="true" type="date" hint="Describes the start date for the relationship between a property and the related meta data record."/>
	<cfproperty name="relationship_end" required="true" type="date" hint="Describes the end date for the relationship between a property and the related meta data record."/>
	<cfproperty name="is_active" required="true" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
	<cfproperty name="created_by" required="true" type="string" hint="Describes the system user that created a given record."/>
	<cfproperty name="created_date" required="true" type="date" hint="Describes the date that a given record was first created."/>
	<cfproperty name="modified_by" required="true" type="string" hint="Describes the system user that last modified a given record."/>
	<cfproperty name="modified_date" required="true" type="date" hint="Describes the date that a given record was last modified."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="property_meta_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="isAssociated" required="true" type="boolean" default="false" hint="Describes whether the current object has an association to another object / object type (true = is associated to a parent object, false = is not associated to a parent object)."/>
	<cfproperty name="metaTypeFriendlyName" required="false" type="string" hint="Describes the friendly name for the associated meta data type.">
	<cfproperty name="metaTypeDisplayOrder" required="false" type="numeric" hint="Describes the display order for the associated meta data type'.">
	<cfproperty name="propertyFriendlyName" required="false" type="string" hint="Describes the friendly name for the associated property.">
	<cfproperty name="categoryFriendlyName" required="false" type="string" hint="Describes the friendly name for the associated property meta's category'.">
	<cfproperty name="categoryDisplayOrder" required="false" type="numeric" hint="Describes the display order for the associated property meta's category'.">
	<cfproperty name="hasPermissionToEdit" required="true" type="boolean" hint="Whether the user been granted the permission required at the meta type level."/>
	
	<!--- Additional fields to assist UI, but which do not need to come back to server on updates; these should exist in actionscript class, but should be marked [Transient] there so the Flex app doesn't bother sending them back to the server --->
	<cfproperty name="metaChoices" type="array">
	<cfproperty name="numMetaChoices" type="numeric">
	<cfproperty name="data_type" type="int" hint="Describes the data type for a given energy usage meta type.">
	<cfproperty name="meta_value_before_edit" type="string" hint="Describes the value for a given energy entry meta property, before editing.">
	<cfproperty name="type_lookup_code" type="string" hint="Describes the lookup code for a given energy entry meta  type.">
	
	<!--- Default any required properties. --->
	<cfset this.primaryKey = "property_meta_uid"/>
	<cfset this.property_meta_uid = ""/>

</cfcomponent>
