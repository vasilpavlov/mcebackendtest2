<cfcomponent displayName="Property Collection Type Value Object"
			 output="false"
			 alias="mce.e2.vo.PropertyCollectionType" style="rpc"
		 	 extends="BaseValueObject"
			 hint="This component is used to define the value object (vo) used to describe the properties of a given property collection type.">

	<!--- Fields that directly correspond to database; these should also exist in corresponding actionscript class --->
	<cfproperty name="collection_type_code" type="string" required="true" hint="Describes the type / category for a given property collection.">
	<cfproperty name="friendly_name" type="string" required="true" hint="Describes the friendly name / customer facing name for a given property collection.">

	<!--- Define the meta-data for a given value object --->
	<cfproperty name="primaryKey" type="string" required="true" default="collection_type_code" hint="Describes the property that acts as the primary key for a given property collection type.">

	<!--- Define any non-database table related fields that are needed to render the UI display --->
	<cfproperty name="isAssociated" type="boolean" required="true" default="false" hint="Describes whether a given object is associated to a parent object (true = is related / associated, false = is not related / associated)."/>

	<!--- Define the audit properties for all components --->
	<cfproperty name="is_active" type="boolean" required="true" default="true" hint="Describes whether the component instance is active (true = active, false = inactive)."/>
	<cfproperty name="created_by" type="string" required="true"  hint="Describes the user that created the component instance."/>
	<cfproperty name="created_date" type="date" required="true"  hint="Describes the date that a given component instance was created."/>
	<cfproperty name="modified_by" type="string" required="true" hint="Describes the user that last the component instance."/>
	<cfproperty name="modified_date" type="date" required="true" hint="Describes the date that a given component instance was last modified."/>

	<!--- Set any defaults --->
	<cfset this.collection_type_code = "">
	
	<!--- Define the column that is the primary key for this object --->
	<cfset this.primaryKey = "collection_type_code">

</cfcomponent>