
<!---

  Template Name:  ParticipantProperty
     Base Table:  property, dataReviewParticipant	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Tuesday, April 28, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Tuesday, April 28, 2009 - Template Created.		

--->
		
<cfcomponent displayname="ParticipantProperty"
				hint="This CFC manages all object/bean/value object instances for participant property data, and is used to pass object instances through the application service layer."
				output="false"
				extends="BaseValueObject"
				alias="mce.e2.vo.ParticipantProperty"
				style="rpc">

	<!--- Declare and initialize the properties for a given component. --->
	<cfproperty name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given property record."/>
	<cfproperty name="friendly_name" required="true" type="string" hint="Describes the friendly name for a given property."/>
	<cfproperty name="image_filename" required="true" type="string" hint="Describes the image name associated to a given property."/>
	<cfproperty name="approval_status_code" required="true" type="string" hint="Describes the approval status code for a given property / data review association."/>

	<!--- Define the meta-data for a given value object. --->
	<cfproperty name="primaryKey" required="true" type="string" default="property_uid" hint="Describes the primary key for the current object / table.">
	<cfproperty name="displayPropertyMetaTypeLookupCode" required="true" type="string" hint="Describes the review data detail meta property lookup code associated to a given data review."/>
	<cfproperty name="displayPropertyMetaValue" required="true" type="string" hint="Describes the meta value associated to a given property through a participant / data review association."/>

	<!--- Default any required properties. --->
	<cfset this.primaryKey = "property_uid"/>
	<cfset this.property_uid = ""/>

</cfcomponent>