
<!---

  Template Name:  DataReviewDelegate
     Base Table:  dataReviews	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, April 22, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, April 22, 2009 - Template Created.		

--->
		
<cfcomponent displayname="DataReviewDelegate"
				hint="This CFC manages all data access interactions for the dataReviews table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyDataReviewComponent"
				hint="This method is used to retrieve a Data Review object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.dataReview"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.dataReview")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<cffunction name="getEmptyDataReviewParticipantComponent"
				hint="This method is used to retrieve a Data Review Participant object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.dataReviewParticipant"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.dataReviewParticipant")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<cffunction name="getEmptyDataReviewDetailUsageSummaryComponent"
				hint="This method is used to retrieve a Data Review Detail Usage Summary object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.dataReviewDetailUsageSummary"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.dataReviewDetailUsageSummary")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<cffunction name="getEmptyDataReviewApprovalStatusComponent"
				hint="This method is used to retrieve a Data Review Approval Status object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.dataReviewApprovalStatus"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.dataReviewApprovalStatus")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getDataReviewsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Data Review objects for all the Data Reviews in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review record."/>
		<cfargument name="user_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given user associated to a data review."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Data Reviews and build out the array of components --->
		<cfset local.qResult = getDataReview(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.DataReview")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>
	
	<cffunction name="getDataReviewDetailUsageSummaryAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Data Review Detail Usage Summary objects for a given Data Review."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier fora  given data review record."/>
		<cfargument name="profile_lookup_code" required="true" type="string" default="default" hint="Describes the energy profile lookup code used to retrieve energy hierarchy information."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given property record (filters properties based on this list / collection)."/>
		<cfargument name="display_period_start" required="true" type="date" hint="Describes the start date used to define the period for which energy usage will be calculated."/>
		<cfargument name="display_period_end" required="true" type="date" hint="Describes the end date used to define the period for which energy usage will be calculated."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Data Reviews and build out the array of components --->
		<cfset local.qResult = getDataReviewDetailUsageSummary(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.DataReviewDetailUsageSummary")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>
	
	<cffunction name="getDataReviewParticipantsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Data Review Participant objects for all the Data Reviews in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier fora  given data review record."/>
		<cfargument name="review_participant_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review participant record."/>
		<cfargument name="client_company_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client company record."/>
		<cfargument name="client_contact_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client contact record."/>
		<cfargument name="user_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given user record."/>
		<cfargument name="ownership_level" required="false" type="numeric" hint="Describes the data participant ownership level used to filter this result set."/>
		<cfargument name="review_role_code" required="false" type="string" hint="Describes the data participant role internal lookup code used to filter this result set."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the list of properties that are used to retrieve associated users data review participants."/>
		
		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Data Reviews and build out the array of components --->
		<cfset local.qResult = getDataReviewParticipant(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.DataReviewParticipant")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getDataReviewApprovalStatusesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Data Review Approval Status objects associated to the specified user."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review record."/>
		<cfargument name="review_participant_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review participant record."/>
		<cfargument name="user_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given user record."/>
		<cfargument name="client_contact_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client contact record."/>
		<cfargument name="review_namespace_code" required="false" type="string" hint="Describes the date review namespace lookup code."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all data review approval status information and build out the array of components --->
		<cfset local.qResult = getDataReviewApprovalStatuses(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.DataReviewApprovalStatus")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyUsageforReviewDetailAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of energy usage / data review detail objects."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account record."/>
		<cfargument name="display_period_start" required="true" type="date" hint="Describes the start date used to define the period for which energy usage will be calculated."/>
		<cfargument name="display_period_end" required="true" type="date" hint="Describes the end date used to define the period for which energy usage will be calculated."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all data review approval status information and build out the array of components --->
		<cfset local.qResult = getEnergyUsageforReviewDetail(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyUsageforReviewDetail")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getDataReviewAsComponent"
				hint="This method is used to retrieve a single instance of a / an Data Review value object representing a single Data Review record."
				output="false"
				returnType="mce.e2.vo.dataReview"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier fora  given data review record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Data Review and build out the component --->
		<cfset local.qResult = getDataReview(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.DataReview")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getDataReviewParticipantAsComponent"
				hint="This method is used to retrieve a single instance of a / an Data Review Participant value object representing a single Data Review record."
				output="false"
				returnType="mce.e2.vo.dataReviewParticipant"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier fora  given data review record."/>
		<cfargument name="review_participant_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review participant record."/>
		<cfargument name="client_company_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client company record."/>
		<cfargument name="client_contact_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client contact record."/>
		<cfargument name="user_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given user record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Data Review Participant and build out the component --->
		<cfset local.qResult = getDataReviewParticipant(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.DataReviewParticipant")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getDataReview"
				hint="This method is used to retrieve single / multiple records from the dataReviews table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier fora  given data review record."/>
		<cfargument name="user_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given user associated to a data review."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the friendly name for a given emissions data review."/>
		<cfargument name="review_period_start" required="false" type="date" hint="Describes the start date for a given review period."/>
		<cfargument name="review_period_end" required="false" type="date" hint="Describes the end date for a given review period."/>
		<cfargument name="display_period_start" required="false" type="date" hint="Describes the start date for a given review display period."/>
		<cfargument name="display_period_end" required="false" type="date" hint="Describes the end date for a given review display period."/>
		<cfargument name="display_property_meta_type_lookup_code" required="false" type="string" hint="Describes the lookup code for the associated property meta type."/>
		<cfargument name="review_namespace_code" required="false" type="string" hint="Describes the date review namespace lookup code."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Data Review records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the dataReviews table matching the where clause
			select	tbl.data_review_uid,
					tbl.friendly_name,
					tbl.review_period_start,
					tbl.review_period_end,
					tbl.display_period_start,
					tbl.display_period_end,
					tbl.display_property_meta_type_lookup_code,
					tbl.review_namespace_code,
					tbl.detail_records_length_datepart,
					tbl.detail_records_length,
					tbl.first_comparison_offset_months_start,
					tbl.first_comparison_offset_months_length,
					tbl.second_comparison_offset_months_start,
					tbl.second_comparison_offset_months_length,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.dataReviews tbl (nolock)
			where	1=1
					and tbl.is_active = 1
					
					<cfif structKeyexists(arguments, 'user_uid')>
					and tbl.data_review_uid in (
					
						select	drp.data_review_uid
						from	dbo.dataReviewParticipants drp (nolock)
								join dbo.clientContacts cc (nolock)
									on drp.client_contact_uid = cc.client_contact_uid
								join dbo.users u (nolock)
									on cc.user_uid = u.user_uid
						where	1=1
							
								and drp.is_active = 1
								and cc.is_active = 1
								and u.is_active = 1
								and u.user_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.user_uid#" null="false" list="true"> )
					
					)
					</cfif>

					<cfif structKeyExists(arguments, 'data_review_uid')>
					and tbl.data_review_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.data_review_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'review_period_start')>
					and tbl.review_period_start in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.review_period_start#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'review_period_end')>
					and tbl.review_period_end in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.review_period_end#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'display_period_start')>
					and tbl.display_period_start in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.display_period_start#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'display_period_end')>
					and tbl.display_period_end in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.display_period_end#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'display_property_meta_type_lookup_code')>
					and tbl.display_property_meta_type_lookup_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.display_property_meta_type_lookup_code#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'review_namespace_code')>
					and tbl.review_namespace_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.review_namespace_code#" list="true" null="false"> ) 
					</cfif>

			order
			by		tbl.friendly_name,
					tbl.modified_date

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<cffunction name="getDataReviewParticipant"
				hint="This method is used to retrieve single / multiple records from the dataReviews table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review record."/>
		<cfargument name="review_participant_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review participant record."/>
		<cfargument name="client_company_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client company record."/>
		<cfargument name="client_contact_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client contact record."/>
		<cfargument name="user_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given user record."/>
		<cfargument name="ownership_level" required="false" type="numeric" hint="Describes the data participant ownership level used to filter this result set."/>
		<cfargument name="review_role_code" required="false" type="string" hint="Describes the data participant role internal lookup code used to filter this result set."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the list of properties that are used to retrieve associated users data review participants."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		
		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Data Review records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
		
			-- Get the current user
			select	distinct
					tbl.review_participant_uid,
					tbl.data_review_uid,
					tbl.client_contact_uid,
					tbl.property_collection_uid,
					tbl.review_role_code,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					
					drr.friendly_name as reviewRoleFriendlyName,
					drr.ownership_level as reviewRoleOwnershipLevel,
					cc.first_name as participantFirstName,
					cc.last_name as participantLastName
					
			from	dbo.dataReviewParticipants tbl (nolock)
					join dbo.DataReviewRoles drr (nolock)
						on tbl.review_role_code = drr.review_role_code
					join dbo.clientContacts cc (nolock)
						on tbl.client_contact_uid = cc.client_contact_uid
					join dbo.Users u (nolock)
						on cc.user_uid = u.user_uid
					
			where	1=1
			
					<cfif arguments.selectMethod eq 'current'>
					and tbl.is_active = 1
					and	drr.is_active = 1
					and cc.is_active = 1
					and u.is_active = 1
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and u.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" null="false" list="true"> ) 
					</cfif>						
					

					and (
					
						1=1
					
						<cfif structKeyExists(arguments, 'user_uid')>
						and u.user_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.user_uid#" null="false" list="true"> ) 
						</cfif>		
	
						<cfif structKeyExists(arguments, 'ownership_level')>
						or drr.ownership_level > <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.ownership_level#">
						</cfif>

						<cfif structKeyExists(arguments, 'review_role_code')>
						and drr.review_role_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.review_role_code#"> )
						</cfif>

					)
					
					<cfif structKeyExists(arguments, 'data_review_uid')>
					and tbl.data_review_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.data_review_uid#" null="false" list="true"> ) 
					</cfif>		
		
					<cfif structKeyExists(arguments, 'review_participant_uid')>
					and tbl.review_participant_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.review_participant_uid#" null="false" list="true"> ) 
					</cfif>		

					<cfif structKeyExists(arguments, 'client_contact_uid')>
					and tbl.client_contact_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_contact_uid#" null="false" list="true"> ) 
					</cfif>	
					
					<cfif structKeyexists(arguments, 'property_uid')>
					and tbl.property_collection_uid in (
					
						select 	pc.property_collection_uid
						from	dbo.propertyCollections pc (nolock)
								join dbo.Properties_PropertyCollections ppc (nolock)
									on pc.property_collection_uid = ppc.property_collection_uid
						where	1=1

								<cfif arguments.selectMethod eq 'current'>
								and pc.is_active = 1
								and ppc.is_active = 1
								and (
								
										ppc.is_active = 1
										and ( ppc.relationship_end is null or ppc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
										and ppc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
									
									)		
								</cfif>					

								<cfif structKeyExists(arguments, 'property_uid')>
								-- Only select property collections that have properties owned by the source / parent user					
								and ppc.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> )
								</cfif>

					)
					</cfif>
					
			order
			by		cc.last_name,
					cc.first_name	

		</cfquery>		

		<!--- Return the result set --->
		<cfreturn local.qResult>
		
	</cffunction>

	<cffunction name="getDataReviewDetailUsageSummary"
				hint="This method is used to retrieve single / multiple records from the dataReviews table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given data review record."/>
		<cfargument name="profile_lookup_code" required="true" type="string" default="default" hint="Describes the energy profile lookup code used to retrieve energy hierarchy information."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given property record (filters properties based on this list / collection)."/>
		<cfargument name="display_period_start" required="true" type="date" hint="Describes the start date used to define the period for which energy usage will be calculated."/>
		<cfargument name="display_period_end" required="true" type="date" hint="Describes the end date used to define the period for which energy usage will be calculated."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="as_of_date" type="string" required="true" default="#now()#" hint="Describes the date used to determine the emissions factor value using getEmissionsFactorValueForEnergyAccount(). ">
		
		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Data Review Detail Usage Summary records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">	

			select	distinct
					p.property_uid,
					ea.energy_account_uid,
					dr.display_property_meta_type_lookup_code,
					
					-- New column additions
					es.hierarchy_id.ToString() AS hierarchy_position,
					es.hierarchy_id.GetAncestor(1).ToString() AS parent_position,					
					drd.period_name,
					drd.detail_period_start,
					drd.detail_period_end,
					et.energy_type_uid,
					drd.approval_status_code,
				
					-- Retrieve the friendly name and energy usage data
					dras.friendly_name as approvalStatusFriendlyName,
					ea.friendly_name as energyAccountFriendlyName,
					et.friendly_name as energyTypeFriendlyName,
					eu.friendly_name as energyUnitFriendlyName,

					dbo.getEnergyUsageSum(
						ea.energy_account_uid,
						p.property_uid,
						drd.detail_period_start,
						drd.detail_period_end		
					) as energyUsage,
					
					dbo.getEnergyUsageSum(
						ea.energy_account_uid,
						p.property_uid,
						dateadd(m, dr.first_comparison_offset_months_start, drd.detail_period_start),
						dateadd(m, dr.first_comparison_offset_months_start, drd.detail_period_end)		
					) as firstComparisonEnergyUsage,		
			
					dbo.getEnergyUsageSum(
						ea.energy_account_uid,
						p.property_uid,
						dateadd(m, dr.second_comparison_offset_months_start, drd.detail_period_start),
						dateadd(m, dr.second_comparison_offset_months_length, dateadd(m, dr.second_comparison_offset_months_start, drd.detail_period_start))		
					) as secondComparisonEnergyUsage,	

					-- Retrieve the emissions impact calculations
					(dbo.getEmissionsFactorValueForEnergyAccount(
						ea.energy_account_uid,
						ep.profile_lookup_code,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.as_of_date)#" list="false">) * 
						
						dbo.getEnergyUsageSum(
							ea.energy_account_uid,
							p.property_uid,
							drd.detail_period_start,
							drd.detail_period_end		
						)						
						
					) as emissionsImpact,
					(dbo.getEmissionsFactorValueForEnergyAccount(
						ea.energy_account_uid,
						ep.profile_lookup_code,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.as_of_date)#" list="false">) * 

						dbo.getEnergyUsageSum(
							ea.energy_account_uid,
							p.property_uid,
							dateadd(m, dr.first_comparison_offset_months_start, drd.detail_period_start),
							dateadd(m, dr.first_comparison_offset_months_start, drd.detail_period_end)		
						)

					) as firstComparisonEmissionsImpact,
					(dbo.getEmissionsFactorValueForEnergyAccount(
						ea.energy_account_uid,
						ep.profile_lookup_code,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.as_of_date)#" list="false">) * 

						dbo.getEnergyUsageSum(
							ea.energy_account_uid,
							p.property_uid,
							dateadd(m, dr.second_comparison_offset_months_start, drd.detail_period_start),
							dateadd(m, dr.second_comparison_offset_months_length, dateadd(m, dr.second_comparison_offset_months_start, drd.detail_period_start))		
						)

					) as secondComparisonEmissionsImpact,
						 						
					-- Retrieve the conversion information
					eu1.friendly_name as conversionEnergyUnitFriendlyName,
					euc.conversion_factor
						
			from	dbo.properties p (nolock)
					join dbo.energyAccounts ea (nolock)
						on p.property_uid = ea.property_uid
					join dbo.dataReviewDetail drd (nolock)
						on p.property_uid = drd.property_uid
					join dbo.dataReviews dr (nolock)
						on dr.data_review_uid = drd.data_review_uid	
					join dbo.dataReviewApprovalStatuses dras (nolock)
						on drd.approval_status_code = dras.approval_status_code
					join dbo.energyTypes et (nolock)
						on ea.energy_type_uid = et.energy_type_uid	
					join dbo.energyUnits eu (nolock)
						on ea.energy_unit_uid = eu.energy_unit_uid
					join dbo.EmissionsSources es (nolock)
						on es.energy_type_uid = et.energy_type_uid
					cross join dbo.EmissionsSources esroot (nolock)
					join dbo.EmissionsProfiles ep (nolock)
						on ep.root_emissions_source_uid = esroot.emissions_source_uid
											
					-- Retrieve the conversion information
					left outer join dbo.energyUnitConversions euc (nolock)
						on ea.energy_unit_uid = euc.from_energy_unit_uid
						and euc.normal_metric_conversion = 1
						and euc.is_active = 1
					left outer join dbo.energyUnits eu1 (nolock)
						on euc.to_energy_unit_uid = eu1.energy_unit_uid
						and eu1.is_active = 1
						 
			where	1=1
			
					-- Filter on active data only 
					and p.is_active = 1
					and drd.is_active = 1
					and	dr.is_active = 1
					and ea.is_active = 1
					and dras.is_active = 1
					and	et.is_active = 1
					and eu.is_active = 1
					and ep.is_active = 1
					
					-- Hierarchy filters
					and es.is_active = 1
					and esroot.is_active = 1				   
					and es.hierarchy_id.IsDescendantOf(esroot.hierarchy_id) = 1
	
					-- Select the data review object's properties
					and dr.data_review_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.data_review_uid#">

					-- Filter on a given hierarchy
					and ep.profile_lookup_code = <cfqueryparam cfsqltype="cf_sql_varchar" list="false" value="#arguments.profile_lookup_code#">

					<cfif structKeyExists(arguments, 'property_uid')>
					and p.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.property_uid#"> )
					</cfif>

				group
				by		ep.profile_lookup_code,
						p.property_uid,
						ea.energy_account_uid,
						dr.display_property_meta_type_lookup_code,
						
						-- New column additions
						es.hierarchy_id.ToString(),
						es.hierarchy_id.GetAncestor(1).ToString(),
						drd.period_name,
						drd.detail_period_start,
						drd.detail_period_end,
						et.energy_type_uid,
						drd.approval_status_code,
					
						-- Retrieve the friendly name and energy usage data
						dras.friendly_name,
						ea.friendly_name,
						et.friendly_name,
						eu.friendly_name,
				
						-- Retrieve the conversion information
						eu1.friendly_name,
						euc.conversion_factor,
						dr.display_property_meta_type_lookup_code,
						dr.first_comparison_offset_months_start,
						dr.second_comparison_offset_months_start,
						dr.second_comparison_offset_months_length					
					
					
			order
			by		drd.detail_period_start desc,
					ea.friendly_name,
					es.hierarchy_id.ToString(),
					et.friendly_name

		</cfquery>		

		<!--- Return the result set --->
		<cfreturn local.qResult>
		
	</cffunction>

	<cffunction name="getDataReviewApprovalStatuses"
				hint="This method is used to retrieve single / multiple records from the dataReviewApprovalStatuses table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review record."/>
		<cfargument name="review_participant_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review participant record."/>
		<cfargument name="user_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given user record."/>
		<cfargument name="client_contact_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client contact record."/>
		<cfargument name="review_namespace_code" required="false" type="string" hint="Describes the date review namespace lookup code."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Data Review records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the dataReviews table matching the where clause
			select	distinct
					tbl.review_approval_statuses_uid,
					tbl.approval_status_code,
					tbl.friendly_name,
					tbl.review_namespace_code,
					tbl.review_role_code,
					tbl.is_comments_required,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.dataReviewApprovalStatuses tbl (nolock)
					join dbo.dataReviewParticipants drp (nolock)
						on tbl.review_role_code = drp.review_role_code
					join dbo.dataReviews dr (nolock)
						on drp.data_review_uid = dr.data_review_uid	
						and dr.review_namespace_code = tbl.review_namespace_code
					join dbo.clientContacts cc (nolock)
						on drp.client_contact_uid = cc.client_contact_uid	
					join dbo.users u (nolock)	
						on u.user_uid = cc.user_uid
						
			where	1=1
					and tbl.is_active = 1
					and drp.is_active = 1
					and dr.is_active = 1
					and cc.is_active = 1
					and u.is_active = 1

					<cfif structKeyExists(arguments, 'data_review_uid')>
					and dr.data_review_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.data_review_uid#" null="false" list="true"> ) 
					</cfif>		
		
					<cfif structKeyExists(arguments, 'review_participant_uid')>
					and drp.review_participant_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.review_participant_uid#" null="false" list="true"> ) 
					</cfif>		

					<cfif structKeyExists(arguments, 'client_contact_uid')>
					and cc.client_contact_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_contact_uid#" null="false" list="true"> ) 
					</cfif>		
					
					<cfif structKeyExists(arguments, 'user_uid')>
					and u.user_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.user_uid#" null="false" list="true"> ) 
					</cfif>		

					<cfif structKeyExists(arguments, 'review_namespace_code')>
					and dr.review_namespace_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.review_namespace_code#" list="true" null="false"> ) 
					</cfif>

			order
			by		tbl.friendly_name,
					tbl.modified_date

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>
	
	<cffunction name="getEnergyUsageforReviewDetail"
				hint="This method is used to retrieve single / multiple energy usage / data review records."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account record."/>
		<cfargument name="display_period_start" required="true" type="date" hint="Describes the start date used to define the period for which energy usage will be calculated."/>
		<cfargument name="display_period_end" required="true" type="date" hint="Describes the end date used to define the period for which energy usage will be calculated."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Data Review records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
	
			select	distinct
					ea.energy_account_uid,
					eus.energy_usage_uid,
					eus.user_comments,
					eus.period_start,
					eus.period_end,
					p.property_uid,
					p.friendly_name as propertyFriendlyName,
					et.friendly_name as energyTypeFriendlyName,
					ea.friendly_name as energyAccountFriendlyName,
					-- drd.period_name as dataReviewPeriodName,

					-- Retrieve the property meta value information
					dr.display_property_meta_type_lookup_code as displayPropertyMetaTypeLookupCode,
					pm.meta_value as displayPropertyMetaValue,
				
					-- Retrieve the conversion information
					eu1.friendly_name as conversionEnergyUnitFriendlyName,
					euc.conversion_factor

			from	dbo.energyUsage eus (nolock)
					join dbo.energyAccounts ea (nolock)		
						on ea.energy_account_uid = eus.energy_account_uid	
					join dbo.Properties p (nolock)
						on	p.property_uid = ea.property_uid
					join dbo.energyTypes et (nolock)
						on ea.energy_type_uid = et.energy_type_uid	
					join dbo.dataReviewDetail drd (nolock)
						on p.property_uid = drd.property_uid
					join dbo.dataReviews dr (nolock)
						on dr.data_review_uid = drd.data_review_uid
												
					-- Retrieve the conversion information
					left outer join dbo.energyUnitConversions euc (nolock)
						on ea.energy_unit_uid = euc.from_energy_unit_uid
						and euc.normal_metric_conversion = 1
						and euc.is_active = 1
					left outer join dbo.energyUnits eu1 (nolock)
						on euc.to_energy_unit_uid = eu1.energy_unit_uid
						and eu1.is_active = 1
				
					-- Retrieve the property meta information
					-- Check to make sure that this is being brought back
					left outer join (
					
						select	pm.meta_value,
								pm.property_uid,
								pmt.type_lookup_code
						from	dbo.propertyMeta pm (nolock)
								join dbo.propertyMetaTypes pmt (nolock)
									on pm.meta_type_uid = pmt.meta_type_uid
						where	pm.is_active = 1
								and pmt.is_active = 1
								and ( pm.relationship_end is null or pm.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and pm.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
																
					) pm on
					pm.property_uid = p.property_uid
					and dr.display_property_meta_type_lookup_code = pm.type_lookup_code
							 
			where	1=1
					and ea.is_active = 1
					and p.is_active = 1
					and et.is_active = 1
					and drd.is_active = 1
					and dr.is_active = 1
					and eus.is_active = 1					

					-- Only select energy usage records for the period specified
					and (
						eus.period_end > <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.display_period_start)#">			 
						and eus.period_end <= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.display_period_end)#">			 
					)

					<cfif structKeyExists(arguments, 'energy_account_uid')>
					and ea.energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" null="false" list="true"> ) 
					</cfif>		
				
			order
			by		et.friendly_name,
					p.friendly_name,
					ea.friendly_name,
					eus.period_end

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		
	
	</cffunction>
	
	<cffunction name="getDataReviewRoles"
				hint="This method is used to retrieve the collection of data participant roles supported by the application."
					access="public" output="false"
					returntype="query">
					
		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Data Review records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			select	tbl.review_role_uid,
					tbl.review_role_code,
					tbl.review_namespace_code,
					tbl.friendly_name,
					tbl.ownership_level,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date
					
			from	dbo.dataReviewRoles tbl (nolock)
			where	1=1
			
					-- Only select active data
					and tbl.is_active = 1

					<cfif structKeyExists(arguments, 'review_namespace_code')>
					and tbl.review_namespace_code in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.review_namespace_code#" null="false" list="true"> )
					</cfif>
					
					<cfif structKeyExists(arguments, 'review_role_uid')>
					and tbl.review_role_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.review_role_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'review_role_code')>
					and tbl.review_role_code in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.review_role_code#" null="false" list="true"> )
					</cfif>

			order
			by		tbl.ownership_level asc

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>
	
	<!---Describes the methods used to persist / modify Data Review data. --->	
	<cffunction name="saveNewDataReview"
				hint="This method is used to persist a new Data Review record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="DataReview" required="true" type="mce.e2.vo.dataReview" hint="Describes the VO containing the Data Review record that will be persisted."/>

		<!--- Persist the Data Review data to the application database --->
		<cfset this.dataMgr.insertRecord("dataReviews", arguments.DataReview)/>

	</cffunction>

	<cffunction name="saveExistingDataReview"
				hint="This method is used to persist an existing Data Review record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="DataReview" required="true" type="mce.e2.vo.dataReview" hint="Describes the VO containing the Data Review record that will be persisted."/>

		<!--- Persist the Data Review data to the application database --->
		<cfset this.dataMgr.updateRecord("dataReviews", arguments.DataReview)/>

	</cffunction>

</cfcomponent>