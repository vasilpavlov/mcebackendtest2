
<!---

  Template Name:  DataSeriesOptionDelegate
     Base Table:  OptionsForDataSeries

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Monday, May 11, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Monday, May 11, 2009 - Template Created.

--->

<cfcomponent displayname="DataSeriesOptionDelegate"
				hint="This CFC manages all data access interactions for the OptionsForDataSeries table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyDataSeriesOptionComponent"
				hint="This method is used to retrieve a Option For Data Series object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.DataSeriesOption"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.DataSeriesOption")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getDataSeriesOptionsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Option For Data Series objects for all the Options For Data Series in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="series_option_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data series option."/>
		<cfargument name="namespace_lookup_code" required="false" type="string" hint="Describes the namespace lookup code for a given data series option."/>
		<cfargument name="permission_needed" required="false" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Options For Data Series and build out the array of components --->
		<cfset local.qResult = getDataSeriesOption(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.DataSeriesOption")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getDataSeriesOptionAsComponent"
				hint="This method is used to retrieve a single instance of a / an Option For Data Series value object representing a single Option For Data Series record."
				output="false"
				returnType="mce.e2.vo.OptionsForDataSeries"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="series_option_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data series option."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Option For Data Series and build out the component --->
		<cfset local.qResult = getDataSeriesOption(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.OptionsForDataSeries")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getDataSeriesOption"
				hint="This method is used to retrieve single / multiple records from the OptionsForDataSeries table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="series_option_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data series option."/>
		<cfargument name="namespace_lookup_code" required="false" type="string" hint="Describes the namespace lookup code for a given data series option."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given data series option."/>
		<cfargument name="series_area" required="false" type="string" hint="Describes the lookup code / internal identifier for the associated series area type."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier for an associated energy type."/>
		<cfargument name="data_source_type" required="false" type="string" hint="Describes the lookup code / internal identifier for the associated data source type."/>
		<cfargument name="axis_scale" required="false" type="string" hint="Describes the lookup code / internal identifier for the associated data series axis scale."/>
		<cfargument name="factor_lookup_code" required="false" type="string" hint="Describes the lookup code / identifier of the associated rate factor lookup code."/>
		<cfargument name="property_meta_type_code" required="false" type="string" hint="Describes the lookup code for the associated property meta type."/>
		<cfargument name="usage_meta_type_code" required="false" type="string" hint="Describes the lookup code for the associated property meta type."/>
		<cfargument name="permission_needed" required="false" type="string"/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Option For Data Series records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the OptionsForDataSeries table matching the where clause
			select	tbl.series_option_uid,
					tbl.namespace_lookup_code,
					tbl.friendly_name,
					tbl.series_area,
					tbl.data_source_type,
					tbl.energy_type_uid,
					tbl.axis_scale,
					tbl.display_order,
					tbl.factor_lookup_code,
					tbl.property_meta_type_code,
					tbl.usage_meta_type_code,
					tbl.series_style_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.OptionsForDataSeries tbl (nolock)
			where	1=1
					and tbl.is_active = 1

					<cfif structKeyExists(arguments, 'series_option_uid')>
					and tbl.series_option_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.series_option_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'energy_type_uid')>
					and tbl.energy_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_type_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'namespace_lookup_code')>
					and tbl.namespace_lookup_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.namespace_lookup_code#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'series_area')>
					and tbl.series_area in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.series_area#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'data_source_type')>
					and tbl.data_source_type in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.data_source_type#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'axis_scale')>
					and tbl.axis_scale in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.axis_scale#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'display_order')>
					and tbl.display_order in ( <cfqueryparam cfsqltype="cf_sql_tinyint" value="#arguments.display_order#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'factor_lookup_code')>
					and tbl.factor_lookup_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.factor_lookup_code#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'property_meta_type_code')>
					and tbl.property_meta_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.property_meta_type_code#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'usage_meta_type_code')>
					and tbl.usage_meta_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.usage_meta_type_code#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'permission_needed')>
					and tbl.permission_needed in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.permission_needed#" null="false" list="true"> )
					</cfif>
			order
			by		tbl.display_order

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<!---Describes the methods used to persist / modify Option For Data Series data. --->
	<cffunction name="saveNewDataSeriesOption"
				hint="This method is used to persist a new Option For Data Series record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="DataSeriesOption" required="true" type="mce.e2.vo.OptionsForDataSeries" hint="Describes the VO containing the Option For Data Series record that will be persisted."/>

		<!--- Persist the Option For Data Series data to the application database --->
		<cfset this.dataMgr.insertRecord("OptionsForDataSeries", arguments.DataSeriesOption)/>

	</cffunction>

	<cffunction name="saveExistingDataSeriesOption"
				hint="This method is used to persist an existing Option For Data Series record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="DataSeriesOption" required="true" type="mce.e2.vo.OptionsForDataSeries" hint="Describes the VO containing the Option For Data Series record that will be persisted."/>

		<!--- Persist the Option For Data Series data to the application database --->
		<cfset this.dataMgr.updateRecord("OptionsForDataSeries", arguments.DataSeriesOption)/>

	</cffunction>

</cfcomponent>
