<cfcomponent displayname="ClientServiceBillPerTypeDelegate"
				hint="This CFC manages all data access interactions for the ClientServiceBillPerTypes table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyClientServiceBillPerTypeComponent"
				hint="This method is used to retrieve a Client Service Bill Per Type object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.ClientServiceBillPerType"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.ClientServiceBillPerType")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getClientServiceBillPerTypesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Client Service Bill Per Type objects for all the Client Service Bill Per Types in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="service_bill_per_code" required="false" type="string" hint="Describes the primary key / unique identifierfor a given client service bill per type."/>
		<cfargument name="service_type_uid" required="false" type="string" hint="If provided, returns only those bill-per records that apply to the given service type"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Client Service Bill Per Types and build out the array of components --->
		<cfset local.qResult = getClientServiceBillPerType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.ClientServiceBillPerType")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getClientServiceBillPerTypeAsComponent"
				hint="This method is used to retrieve a single instance of a / an Client Service Bill Per Type value object representing a single Client Service Bill Per Type record."
				output="false"
				returnType="mce.e2.vo.ClientServiceBillPerType"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="service_bill_per_code" required="false" type="string" hint="Describes the primary key / unique identifierfor a given client service bill per type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Client Service Bill Per Type and build out the component --->
		<cfset local.qResult = getClientServiceBillPerType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.ClientServiceBillPerType")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getClientServiceBillPerType"
				hint="This method is used to retrieve single / multiple records from the ClientServiceBillPerTypes table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="service_bill_per_code" required="false" type="string" hint="Describes the primary key / unique identifierfor a given client service bill per type."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given client service bill per type."/>
		<cfargument name="service_type_uid" required="false" type="string" hint="If provided, returns only those bill-per records that apply to the given service type"/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Client Service Bill Per Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the ClientServiceBillPerTypes table matching the where clause
			select	tbl.service_bill_per_code,
					tbl.friendly_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.ClientServiceBillPerTypes tbl (nolock)
			where	1=1
					and tbl.is_active = 1
					
					<cfif structKeyExists(arguments, 'service_bill_per_code')>
					and tbl.service_bill_per_code in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.service_bill_per_code#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>
					
					<cfif structKeyExists(arguments, 'service_type_uid') and arguments.service_type_uid neq "">
					and tbl.service_bill_per_code in ( 
						select service_bill_per_code from ClientServiceTypes_ClientServiceBillPerTypes 
						where service_type_uid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.service_type_uid#" list="true" null="false">) 
					) 
					</cfif>
					
			order by tbl.friendly_name
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Client Service Bill Per Type data. --->	
	<cffunction name="saveNewClientServiceBillPerType"
				hint="This method is used to persist a new Client Service Bill Per Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="ClientServiceBillPerType" required="true" type="mce.e2.vo.ClientServiceBillPerType" hint="Describes the VO containing the Client Service Bill Per Type record that will be persisted."/>

		<!--- Persist the Client Service Bill Per Type data to the application database --->
		<cfset this.dataMgr.insertRecord("ClientServiceBillPerTypes", arguments.ClientServiceBillPerType)/>

	</cffunction>

	<cffunction name="saveExistingClientServiceBillPerType"
				hint="This method is used to persist an existing Client Service Bill Per Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="ClientServiceBillPerType" required="true" type="mce.e2.vo.ClientServiceBillPerType" hint="Describes the VO containing the Client Service Bill Per Type record that will be persisted."/>

		<!--- Persist the Client Service Bill Per Type data to the application database --->
		<cfset this.dataMgr.updateRecord("ClientServiceBillPerTypes", arguments.ClientServiceBillPerType)/>

	</cffunction>

</cfcomponent>