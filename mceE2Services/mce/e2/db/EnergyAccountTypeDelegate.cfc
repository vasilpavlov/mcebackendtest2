
<!---

  Template Name:  EnergyAccountType
     Base Table:  EnergyAccountTypes	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, March 20, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, March 20, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnertyAccountTypeDelegate"
				hint="This CFC manages all data access interactions for the EnergyAccountTypes table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEnergyAccountTypeComponent"
				hint="This method is used to retrieve a Energy Account Type object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EnergyAccountType"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EnergyAccountType")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEnergyAccountTypesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Account Type objects for all the Energy Account Types in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key for an associated energy type record."/>
		<cfargument name="account_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Account Types and build out the array of components --->
		<cfset local.qResult = getEnergyAccountType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyAccountType")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyAccountTypeAsComponent"
				hint="This method is used to retrieve a single instance of a / an Energy Account Type value object representing a single Energy Account Type record."
				output="false"
				returnType="mce.e2.vo.EnergyAccountType"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="account_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Energy Account Type and build out the component --->
		<cfset local.qResult = getEnergyAccountType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EnergyAccountType")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyAccountType"
				hint="This method is used to retrieve single / multiple records from the EnergyAccountTypes table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="account_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy account type."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing friendly name for a given account type."/>
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key for an associated energy type record."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Account Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyAccountTypes table matching the where clause
			select	distinct
					tbl.account_type_uid,
					--tbl.energy_type_uid,
					tbl.friendly_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.EnergyAccountTypes tbl (nolock)
			where	1=1
					and tbl.is_active = 1

					<cfif structKeyExists(arguments, 'account_type_uid')>
					and tbl.account_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.account_type_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" null="false" list="true"> ) 
					</cfif>
					
			order
			by		tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Energy Account Type data. --->	
	<cffunction name="saveNewEnergyAccountType"
				hint="This method is used to persist a new Energy Account Type record to the application database."
				output="false"
				returnType="string"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyAccountType" required="true" type="mce.e2.vo.EnergyAccountType" hint="Describes the VO containing the Energy Account Type record that will be persisted."/>

		<!--- Persist the Energy Account Type data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyAccountTypes", arguments.EnergyAccountType)/>

	</cffunction>

	<cffunction name="saveExistingEnergyAccountType"
				hint="This method is used to persist an existing Energy Account Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyAccountType" required="true" type="mce.e2.vo.EnergyAccountType" hint="Describes the VO containing the Energy Account Type record that will be persisted."/>

		<!--- Persist the Energy Account Type data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyAccountTypes", arguments.EnergyAccountType)/>

	</cffunction>

</cfcomponent>
