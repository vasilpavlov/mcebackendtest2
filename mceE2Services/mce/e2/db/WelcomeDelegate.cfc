
<!---

  Template Name:  AddressTypeDelegate
     Base Table:  AddressTypes	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, April 01, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, April 01, 2009 - Template Created.		

--->
		
<cfcomponent displayname="WelcomeDelegate"
				hint="This CFC manages all data access interactions for the welcome page's mysql tables."
				output="false"
				extends="BaseDatabaseDelegate">


	<cffunction name="getPicks"
				hint="This method is used to retrieve MCE Picks records from the AddressTypes table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="rowLimit" required="false" type="numeric" hint="Describes the number of rows to return."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Address Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
        	SELECT * FROM OPENQUERY(MYSQLVERIO,
			'SELECT 
					ID, 
					PublishDate, 
					Title, 
					URL, 
					Body, 
					Source, 
					LastUpdated, 
					Username 
			FROM 	
					mcene1.marketupdates 
			order by 
					PublishDate DESC, 
					LastUpdated DESC

					LIMIT 6 ')

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<cffunction name="getSparklineSummary"
				hint="This method is used to retrieve sparkline summary records from the AddressTypes table."
				output="false"
				returnType="query"
				access="public">


		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Address Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
        	SELECT * FROM OPENQUERY(MYSQLVERIO,
				'SELECT * 
					FROM 
						mcene1.sparkindices  
					ORDER BY 
						DisplayOrder')

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<cffunction name="getLastUpdate"
				hint="This method is used to retrieve the last updated date from the SparkIndices table."
				output="false"
				returnType="query"
				access="public">


		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>


		<cfquery name="local.qResult" datasource="#this.datasource#">
			SELECT * FROM OPENQUERY(MYSQLVERIO,
				'SELECT 
					date_format(max(LastUpdated), "%a, %b %e %Y, %h:%i %p EDT") as LastUpdated 
				from 
					mcene1.sparkindices')
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>
</cfcomponent>
