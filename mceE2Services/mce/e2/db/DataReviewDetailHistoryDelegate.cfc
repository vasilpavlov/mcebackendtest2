
<!---

  Template Name:  DataReviewDetailHistoryDelegate
     Base Table:  DataReviewDetailHistory	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Tuesday, April 28, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Tuesday, April 28, 2009 - Template Created.		

--->
		
<cfcomponent displayname="DataReviewDetailHistoryDelegate"
				hint="This CFC manages all data access interactions for the DataReviewDetailHistory table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyDataReviewDetailHistoryComponent"
				hint="This method is used to retrieve a Data Review Detail History object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.DataReviewDetailHistory"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.DataReviewDetailHistory")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getDataReviewDetailHistoryAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Data Review Detail History objects for all the Data Review Detail Histories in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review record."/>
		<cfargument name="detail_history_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review detail history record."/>
		<cfargument name="review_detail_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the data review detail record being audited."/>
		<cfargument name="review_participant_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the review participant conducting the approval audit for a given review detail record."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the property by which data review detail history will be filtered."/>
		<cfargument name="includePrior" type="boolean" required="true" default="false" hint="Describes how to return data review detail recordsets; when false, include only the reviw history for the specified data review object instance.  When true, include review history records from other review objects that share the same property." /> 

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />
		
		<!--- Retrieve all Data Review Detail Histories and include the history for additional properties --->
		<cfset local.qResult = getDataReviewDetailHistory(argumentCollection=arguments)/>
		
		<!--- Convert the result set to the array of vo objects --->
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.DataReviewDetailHistory")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getDataReviewDetailHistoryAsComponent"
				hint="This method is used to retrieve a single instance of a / an Data Review Detail History value object representing a single Data Review Detail History record."
				output="false"
				returnType="mce.e2.vo.DataReviewDetailHistory"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="detail_history_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review detail history record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Data Review Detail History and build out the component --->
		<cfset local.qResult = getDataReviewDetailHistory(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.DataReviewDetailHistory")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getDataReviewDetailHistory"
				hint="This method is used to retrieve single / multiple records from the DataReviewDetailHistory table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="data_review_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review record."/>
		<cfargument name="detail_history_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given data review detail history record."/>
		<cfargument name="review_detail_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the data review detail record being audited."/>
		<cfargument name="review_participant_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the review participant conducting the approval audit for a given review detail record."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the property by which data review detail history will be filtered."/>
		<cfargument name="includePrior" type="boolean" required="true" default="false" hint="Describes how to return data review detail recordsets; when false, include only the reviw history for the specified data review object instance.  When true, include review history records from other review objects that share the same property." /> 

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Data Review Detail History records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the DataReviewDetailHistory table matching the where clause
			select	distinct
					tbl.detail_history_uid,
					tbl.review_detail_uid,
					tbl.review_participant_uid,
					tbl.approval_status_date,
					tbl.approval_status_code,
					tbl.approval_status_notes,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					drd.period_name,
					dras.friendly_name as approvalStatusFriendlyName,
					isNull(cc.first_name + ' ','') + cc.last_name as participantFriendlyName

			from	dbo.DataReviewDetailHistory tbl (nolock)
					join dbo.dataReviewApprovalStatuses dras (nolock)
						on tbl.approval_status_code = dras.approval_status_code
					join dbo.dataReviewParticipants drp (nolock)
						on tbl.review_participant_uid = drp.review_participant_uid
					join dbo.clientContacts cc (nolock)
						on drp.client_contact_uid = cc.client_contact_uid
					join dbo.dataReviewDetail drd (nolock)
						on tbl.review_detail_uid = drd.review_detail_uid
					join dbo.dataReviews dr (nolock)
						on dr.data_review_uid = drd.data_review_uid

			where	1=1
					and tbl.is_active = 1
					and dras.is_active = 1
					and drp.is_active = 1
					and cc.is_active = 1
					and drd.is_active = 1
					and dr.is_active = 1

					<cfif structKeyExists(arguments, 'data_review_uid') and arguments.includePrior eq false>
					and dr.data_review_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.data_review_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and drd.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" list="true" null="false"> )
					</cfif>
					
					<cfif structKeyExists(arguments, 'detail_history_uid')>
					and tbl.detail_history_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.detail_history_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'review_detail_uid')>
					and tbl.review_detail_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.review_detail_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'review_participant_uid')>
					and tbl.review_participant_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.review_participant_uid#" list="true" null="false"> ) 
					</cfif>

			order
			by		tbl.approval_status_date desc

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Data Review Detail History data. --->	
	<cffunction name="saveNewDataReviewDetailHistory"
				hint="This method is used to persist a new Data Review Detail History record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="DataReviewDetailHistory" required="true" type="mce.e2.vo.DataReviewDetailHistory" hint="Describes the VO containing the Data Review Detail History record that will be persisted."/>

		<!--- Persist the Data Review Detail History data to the application database --->
		<cfset this.dataMgr.insertRecord("DataReviewDetailHistory", arguments.DataReviewDetailHistory)/>

	</cffunction>

	<cffunction name="saveExistingDataReviewDetailHistory"
				hint="This method is used to persist an existing Data Review Detail History record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="DataReviewDetailHistory" required="true" type="mce.e2.vo.DataReviewDetailHistory" hint="Describes the VO containing the Data Review Detail History record that will be persisted."/>

		<!--- Persist the Data Review Detail History data to the application database --->
		<cfset this.dataMgr.updateRecord("DataReviewDetailHistory", arguments.DataReviewDetailHistory)/>

	</cffunction>

</cfcomponent>
