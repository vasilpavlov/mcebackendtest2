<cfcomponent displayname="User Role Delegate" 
			 output="false" extends="BaseDatabaseDelegate"
			 hint="This component is used to manage all database interactions related to retrieving user role data from the application database.">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyUserRoleComponent" 
				returntype="mce.e2.vo.UserRole"
				hint="This method is used to retrieve an empty userRole object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var UserRoleComponent = createObject("component", "mce.e2.vo.UserRole")/>

		<!--- Return the User component --->
		<cfreturn UserRoleComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getUserRolesAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of user role objects for all the User Roles in the application.">

		<!--- Define the arguments for the method --->
		<cfargument name="user_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given UserGroup record; can also contain a list of comma-delimited User primary keys.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given User record; can also contain a list of comma-delimited user primary keys.">
		<cfargument name="selectMethod" type="string" required="true" default="singular" hint="Describes the method used to retrieve data (inclusive / exclusive / userGroupAssociations).">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Determine the query to execute based on the selectMethod --->
		<cfswitch expression="#arguments.selectMethod#">
		
			<cfcase value="userGroupAssociations">

				<!--- Retrieve a comprehensive list of user group associations --->
				<cfset local.qUserRole = getUserGroupAssociations(argumentCollection=arguments)>

			</cfcase>
			
			<cfdefaultcase>
			
				<!--- Retrieve all user groups --->
				<cfset local.qUserRole = getUserRole(argumentCollection=arguments)>
		
			</cfdefaultcase>
			
		</cfswitch>

		<!--- Retrieve all Users and build out the array of components --->
		<cfset local.returnResult = queryToVoArray(local.qUserRole, "mce.e2.vo.UserRole")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getUserRoleAsComponent"
				returntype="mce.e2.vo.UserRole"
				hint="This method is used to retrieve a user role record from the application database, and populate its properties in a user role object.">

		<!--- Define the arguments for the method --->
		<cfargument name="user_role_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given user role record.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all Users and build out the array of components --->
		<cfset local.qUserRole = getUserRole(user_role_uid=arguments.user_role_uid)>
		<cfset local.returnResult = queryToVo(local.qUserRole, "mce.e2.vo.UserRole")>

		<!--- Return the Vo Object --->
		<cfreturn local.returnResult>

	</cffunction>

	<!--- Define the methods used to interact with user role data in the application database --->
	<cffunction name="getUserGroupAssociations"
				returntype="query"
				hint="This method is used to retrieve comprehensive user group / user role associations.">
			
		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="user_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given UserGroup record; can also contain a list of comma-delimited User primary keys.">
		<cfargument name="user_role_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given user role record.">
		
		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the User Roles in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">
	
					-- Select the base columns	
			select	distinct
					ur.user_role_uid,
					ur.friendly_name,
					ur.role_lookup_code,
					
					-- Select the meta columns
					ugur.is_role_granted as isRoleGranted,
					1 as isAssociated,
					case
					
						when ugur.user_group_uid = ug.user_group_uid then ugur.relationship_id
						else null
						
					end relationship_id,

					-- Select the audit columns
					ur.is_active,
					ur.created_date,
					ur.created_by,
					ur.modified_date,
					ur.modified_by
															
			from	dbo.UserRoles ur
					join dbo.UserGroups_UserRoles ugur
						on ur.user_role_uid = ugur.user_role_uid
						and ugur.is_active = 1						
					join dbo.userGroups ug
						on ugur.user_group_uid = ug.user_group_uid
						and ug.is_active = 1
						and ug.client_company_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.client_company_uid#">
						
						<cfif isUniqueIdentifier(arguments.user_group_uid)>
						and ug.user_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#"> )
						</cfif>
													
			where	1=1 
								
					-- Filter on active user roles
					and ur.is_active = 1					
															
			union 
	
					-- Select the base columns	
			select	distinct
					ur.user_role_uid,
					ur.friendly_name,
					ur.role_lookup_code,

					-- Select the meta columns
					0 as isRoleGranted,
					0 as isAssociated,
					null as relationship_id,

					-- Select the audit columns
					ur.is_active,
					ur.created_date,
					ur.created_by,
					ur.modified_date,
					ur.modified_by
															
			from	dbo.UserRoles ur														
			where	1=1 
								
					-- Filter on active user roles
					and ur.is_active = 1						
					and ur.user_role_uid not in ( 
					
						select	ugur.user_role_uid
						from	dbo.userGroups_userRoles ugur
								join dbo.userGroups ug
									on ugur.user_group_uid = ug.user_group_uid
						where	ugur.is_active = 1
								and ug.is_active = 1
								and ug.client_company_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.client_company_uid#">
								<cfif isUniqueIdentifier(arguments.user_group_uid)>
								and ug.user_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#"> )
								</cfif>
					)
				
			order
			by		ur.friendly_name,
					ur.modified_date
												
		</cfquery>				
				
		<!--- Return the query / result set --->		
		<cfreturn qResult>		
				
	</cffunction>
	
	<cffunction name="getUserRole"
				returntype="query"
				hint="This method is used to retrieve data for a given User Role definition.">
			
		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="user_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given UserGroup record; can also contain a list of comma-delimited User primary keys.">
		<cfargument name="user_role_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given user role record.">
		<cfargument name="selectMethod" type="string" required="true" default="singular" hint="Describes the method used to retrieve user role / user group associations; inclusive = associated; exclusive = not associated; all = all.">
		
		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the User Roles in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">
	
					-- Select the base columns	
			select	distinct
					ur.user_role_uid,
					ur.friendly_name,
					ur.role_lookup_code,

			<cfswitch expression="#arguments.selectMethod#">	
			
				<cfcase value="inclusive">
					-- Select the meta columns
					ugur.is_role_granted as isRoleGranted,
					1 as isAssociated,
					case
					
						when ugur.user_group_uid = ug.user_group_uid then ugur.relationship_id
						else null
						
					end relationship_id,
				</cfcase>
			
				<cfcase value="exclusive">
					-- Select the meta columns
					0 as isRoleGranted,
					0 as isAssociated,
					null as relationship_id,
				</cfcase>
						
			</cfswitch>	

					-- Select the audit columns
					ur.is_active,
					ur.created_date,
					ur.created_by,
					ur.modified_date,
					ur.modified_by

			<cfswitch expression="#arguments.selectMethod#">

				<cfdefaultcase>
			from	dbo.UserRoles ur
			where	ur.is_active = 1							
							
					<cfif structKeyExists(arguments, 'user_role_uid')>
					and ur.user_role_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_role_uid#"> )
					</cfif>		
				</cfdefaultcase>

				<cfcase value="inclusive">
			from 	dbo.UserRoles ur
					join dbo.UserGroups_UserRoles ugur
						on ur.user_role_uid = ugur.user_role_uid
					join dbo.userGroups ug
						on ugur.user_group_uid = ug.user_group_uid
						and ug.client_company_uid in (  <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#"> )
						<cfif structKeyExists(arguments, 'user_group_uid') and isUniqueIdentifier(arguments.user_group_uid)>
						and ug.user_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#"> )
						</cfif>
			where	ur.is_active = 1
					and ug.is_active = 1
					and ugur.is_active = 1
				</cfcase>

				<cfcase value="exclusive">
			from 	dbo.UserRoles ur
			where	ur.is_active = 1
					and ur.user_role_uid not in ( 
					
						-- Exclude the roles already associated to the current group
						select	ugur1.user_role_uid
						from	dbo.UserGroups_UserRoles ugur1
								join dbo.UserGroups ug
									on ugur1.user_group_uid = ug.user_group_uid
						where	ug.is_active = 1 
								and ugur1.is_active = 1 
								and ug.client_company_uid in (  <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#"> )
								<cfif structKeyExists(arguments, 'user_group_uid') and isUniqueIdentifier(arguments.user_group_uid)>
								and ug.user_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#"> )
								</cfif>
								
					)
				</cfcase>					
				
			</cfswitch>	
				
			order
			by		ur.friendly_name,
					ur.modified_date
												
		</cfquery>				
				
		<!--- Return the query / result set --->		
		<cfreturn qResult>		
				
	</cffunction>

	<!--- Define the methods used to persist data to the application database --->
	<cffunction name="saveNewUserRole" 
				access="public" returntype="void"
				hint="This method is used to persist a new UserRole record to the application database.">
		<cfargument name="UserRole" type="mce.e2.vo.UserRole" hint="Describes the VO containing the user role record that will be persisted.">

		<!--- Persist the User Role data to the application database --->
		<cfset this.dataMgr.insertRecord("UserRoles", arguments.UserRole)>
				
	</cffunction>	
	
	<cffunction name="saveExistingUserRole" 
				access="public" returntype="void"
				hint="This method is used to persist changes for an existing user role record to the application database.">
		<cfargument name="UserRole" type="mce.e2.vo.UserRole" hint="Describes the VO containing the user role record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Persist the User Role data to the application database --->
		<cfset this.dataMgr.updateRecord("UserRoles", arguments.UserRole)>
				
	</cffunction>	
	
</cfcomponent>		