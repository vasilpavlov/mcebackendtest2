<cfinterface displayName="IReportGenerator"> 
	<cffunction name="generateReportPdf"> 
		<cfargument name="report_name" type="string" required="true">
		<cfargument name="report_format" type="string" required="true">
		<cfargument name="parameters" type="array" required="true">
		<cfargument name="hdroutput" type="string" required="true">
	</cffunction> 
</cfinterface>