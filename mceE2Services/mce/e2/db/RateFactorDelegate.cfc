<cfcomponent displayname="Rate Factor Delegate"
			 output="false" extends="BaseDatabaseDelegate"
			 hint="This component is used to manage all database interactions related to retrieving rate factor data from the application database.">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyRateFactorComponent"
				returntype="mce.e2.vo.RateFactor"
				hint="This method is used to retrieve a rate factor value object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var rateFactorComponent = createObject("component", "mce.e2.vo.RateFactor")/>

		<!--- Return the rate factor component --->
		<cfreturn rateFactorComponent/>

	</cffunction>

	<cffunction name="getEmptyRateFactorValueComponent"
				returntype="mce.e2.vo.RateFactorValue"
				hint="This method is used to retrieve a rate factor values value object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var rateFactorValueComponent = createObject("component","mce.e2.vo.RateFactorValue")/>

		<!--- Return the rate factor detail component --->
		<cfreturn rateFactorValueComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getRateFactorsAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of rate factor value objects for all the rate factors in the application.">

		<!--- Initialize the method arguments --->
		<cfargument name="rate_factor_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given rate factor record; can also contain a list of comma-delimited rate factor primary keys.">
		<cfargument name="rate_model_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given rate model record; can also contain a list of comma-delimited rate factor primary keys.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all rate factors and build out the array of components --->
		<cfset local.qRateFactors = getRateFactor(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVoArray(local.qRateFactors, "mce.e2.vo.RateFactor")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getRateFactorAsComponent"
				returntype="mce.e2.vo.RateFactor"
				hint="This method is used to retrieve a rate factor record from the application database, and populate its properties in a rate factor value object.">

		<!--- Define the arguments for the method --->
		<cfargument name="rate_factor_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given rate factor record.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all rate factors and build out the array of components --->
		<cfset local.qRateFactor = getRateFactor(rate_factor_uid=arguments.rate_factor_uid)>
		<cfset local.returnResult = queryToVo(local.qRateFactor, "mce.e2.vo.RateFactor")>

		<!--- Return the Vo Object --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getRateFactorValuesAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of rate factor values value objects for a given rate factor.">

		<!--- Initialize the method arguments --->
		<cfargument name="rate_factor_uid" type="string" required="true" hint="Describes the primary key of a given rate factor record.">
		<cfargument name="relationship_start" type="date" required="false" hint="Describes the relation start date that will be used to fiter rate factor value records.">
		<cfargument name="relationship_end" type="date" required="false" hint="Describes the relation end date that will be used to fiter rate factor value records.">
		<cfargument name="rowCount" type="numeric" required="true" default="50" hint="Describes the total number of rows to retrieve when executing this query.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all rate factors and build out the array of components --->
		<cfset local.qRateFactorValues = getRateFactorValue(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVoArray(local.qRateFactorValues, "mce.e2.vo.RateFactorValue")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getRateFactorValueAsComponent"
				access="public"
				returntype="mce.e2.vo.RateFactorValue"
				hint="This method is used to return a rate factor values value object (Vo), populated with rate factor values.">

		<!--- Define the arguments for the method --->
		<cfargument name="rate_factor_uid" type="string" required="false" hint="Describes the primary key of a given rate factor record.">
		<cfargument name="factor_value_uid" type="string" required="false" hint="Describes the primary key of a given rate factor value record.">
		<cfargument name="selectMethod" type="string" required="false" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the rate factor value and apply it to the component /vo --->
		<cfset local.qRateFactorValues = getRateFactorValue(argumentCollection=arguments)>
		<cfset local.returnResult = queryToVo(local.qRateFactorValues, "mce.e2.vo.RateFactorValue")>

		<!--- Return the Vo Object --->
		<cfreturn local.returnResult>

	</cffunction>

	<!--- Define the methods used to interact with rate factor data in the application database --->
	<cffunction name="getRateFactor"
				returntype="query"
				hint="This method is used to retrieve any rate factor record(s) from the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="rate_model_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given rate model record; can also contain a list of comma-delimited rate factor primary keys.">
		<cfargument name="rate_factor_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given rate factor record; can also contain a list of comma-delimited rate factor primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="all" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve all the rate factors in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	rf.rate_factor_uid,
					rf.factor_lookup_code,
					rf.friendly_name,
					rf.is_active,
					rf.created_by,
					rf.created_date,
					rf.modified_by,
					rf.modified_date,

					min(case when rfv.is_active = 0 then null else rfv.relationship_start end) as minRelationshipStartDate,
					nullif(max(case when rfv.is_active = 0 then null else isnull(rfv.relationship_end, '1/1/5000') end), '1/1/5000') as maxRelationshipEndDate,
					dbo.getRateFactorValue(rf.factor_lookup_code, getDate()) as currentFactorValue

			<!---
			from	dbo.rateFactors rf
					left outer join dbo.rateFactorValues rfv on
						rf.rate_factor_uid = rfv.rate_factor_uid

						<cfif arguments.selectMethod eq 'current'>
						and (

								-- Filter on active data while respecting the join relationships
								rfv.is_active = 1
								and ( rfv.relationship_end is null or rfv.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and rfv.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

							)
						</cfif>
			--->

			FROM      RateFactorValues AS rfv RIGHT JOIN
                      RateFactors AS rf LEFT OUTER JOIN
                      RateModelInputSet_RateModelInputs rmis INNER JOIN
                      RateModelInputs rmi ON rmis.model_input_uid = rmi.model_input_uid ON
                      rf.factor_lookup_code = rmis.factor_lookup_code ON rfv.rate_factor_uid = rf.rate_factor_uid
						<cfif arguments.selectMethod eq 'current'>
						and (

								-- Filter on active data while respecting the join relationships
								rfv.is_active = 1
								and ( rfv.relationship_end is null or rfv.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
								and rfv.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

							)
						</cfif>
			where	1=1

					<!--- cfif arguments.selectMethod eq 'current' [For now, always getting only active]]--->
					-- Filter on active data only
					and rf.is_active = 1
					<!---/cfif--->

					<cfif structKeyExists(arguments, 'rate_factor_uid')>
					and rf.rate_factor_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.rate_factor_uid#">)
					</cfif>

					<cfif structKeyExists(arguments, 'rate_model_uid')>
					and rmi.rate_model_uid in( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.rate_model_uid#">)
					</cfif>

			group
			by		rf.rate_factor_uid,
					rf.factor_lookup_code,
					rf.friendly_name,
					rf.is_active,
					rf.created_by,
					rf.created_date,
					rf.modified_by,
					rf.modified_date,
					rmis.factor_lookup_code,
					rmi.rate_model_uid

			order
			by		rf.friendly_name

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getRateFactorValue"
				returntype="query"
				hint="This method is used to retrieve any rate factor value record(s) from the application database.">

		<!--- Define the arguments for the method --->
		<cfargument name="rate_factor_uid" type="string" required="false" hint="Describes the primary key / unique identifier for a given rate factor record, identifying the rate factor definition for which rate factor values will be retrieved.">
		<cfargument name="factor_value_uid" type="string" required="false" hint="Describes the primary key / unique identifier for a given rate factor value record; can be a comma-delimited list of identifiers.">
		<cfargument name="relationship_start" type="date" required="false" hint="Describes the relation start date that will be used to fiter rate factor value records.">
		<cfargument name="relationship_end" type="date" required="false" hint="Describes the relation end date that will be used to fiter rate factor value records.">
		<cfargument name="rowCount" type="numeric" required="true" default="50" hint="Describes the total number of rows to retrieve when executing this query.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the rate factor values in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#" maxrows="#arguments.rowCount#">

			select	rfv.rate_factor_uid,
					rfv.factor_value_uid,
					rfv.factor_value,
					rfv.relationship_start,
					rfv.relationship_end,
					rfv.is_active,
					rfv.created_by,
					rfv.created_date,
					rfv.modified_by,
					rfv.modified_date

			from	dbo.rateFactorValues rfv
			where	1=1

					<cfif arguments.selectMethod eq 'current'>
					-- Filter on active data only
					and (

							rfv.is_active = 1
							and ( rfv.relationship_end is null or rfv.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and rfv.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					<cfelseif structkeyexists(arguments, "relationship_start") and len(arguments.relationship_start) and structkeyexists(arguments, "relationship_end") and len(arguments.relationship_end)>
					-- Filter on active data only
					and (

							rfv.is_active = 1
							and ( rfv.relationship_end is null or rfv.relationship_end <= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_end)#"> )
							and rfv.relationship_start >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_start)#">

						)

					</cfif>


					<cfif structKeyExists(arguments, 'rate_factor_uid')>
					and rfv.rate_factor_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rate_factor_uid#">
					</cfif>

					<cfif structKeyExists(arguments, 'factor_value_uid')>
					and rfv.factor_value_uid in ( <cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#arguments.factor_value_uid#"> )
					</cfif>

			order
			by		rfv.relationship_start DESC,
					rfv.relationship_end DESC

		</cfquery>

		<!--- Return the result set --->
		<cfreturn qResult>

	</cffunction>

	<cffunction name="getRateFactorUsageData"
				returntype="query"
				hint="This method is used to retrieve usage data for a given rate factor definition.">

		<!--- Define the arguments for the method --->
		<cfargument name="rate_factor_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given rate factor record; can also contain a list of comma-delimited rate factor primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the rate factor values in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">

			select	'Rate Model' as type,
					rm.model_lookup_code as dependency,
					rmi2.relationship_start as [from],
					rmi2.relationship_end as [to]
			from	dbo.RateModels rm
					join dbo.RateModelInputs rmi on
						rm.rate_model_uid = rmi.rate_model_uid
					join dbo.RateModelInputSet_RateModelInputs rmi2
						on rmi.model_input_uid = rmi2.model_input_uid
					join dbo.RateFactors rf on
						rmi2.factor_lookup_code = rf.factor_lookup_code
						<cfif arguments.selectMethod eq 'current'>
						and rf.is_active = 1
						</cfif>
			where	1=1

					<cfif arguments.selectMethod eq 'current'>
					and (

							rmi2.is_active = 1
							and ( rmi2.relationship_end is null or rmi2.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and rmi2.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					</cfif>

					<cfif structKeyExists(arguments, 'rate_factor_uid')>
					and rf.rate_factor_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.rate_factor_uid#"> )
					</cfif>

		</cfquery>

		<!--- Return the query / result set --->
		<cfreturn qResult>

	</cffunction>

	<!--- Define the methods used to persist data to the application database --->
	<cffunction name="saveNewRateFactor"
				access="public" returntype="void"
				hint="This method is used to persist a new rate factor record to the application database.">
		<cfargument name="rateFactor" type="mce.e2.vo.RateFactor" hint="Describes the VO containing the rate factor record that will be persisted.">

		<!--- Persist the rateFactor data to the application database --->
		<cfset this.dataMgr.insertRecord("RateFactors", arguments.rateFactor)>

	</cffunction>

	<cffunction name="saveExistingRateFactor"
				access="public" returntype="void"
				hint="This method is used to persist changes for an existing rate factor record to the application database.">
		<cfargument name="rateFactor" type="mce.e2.vo.RateFactor" hint="Describes the VO containing the rate factor record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Persist the rateFactor data to the application database --->
		<cfset local.newUid = this.dataMgr.updateRecord("RateFactors", arguments.rateFactor)>

	</cffunction>

	<cffunction name="saveNewRateFactorValue"
				access="public" returntype="void"
				hint="This method is used to persist a new rate factor value record to the application database.">
		<cfargument name="rateFactorValue" type="mce.e2.vo.RateFactorValue" hint="Describes the VO containing the rate factor value record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Persist the rateFactor value data to the application database --->
		<cfset this.dataMgr.insertRecord("RateFactorValues", arguments.rateFactorValue)>

	</cffunction>

	<cffunction name="saveExistingRateFactorValue"
				access="public" returntype="void"
				hint="This method is used to persist changes for an existing rate factor value record to the application database.">
		<cfargument name="rateFactorValue" type="mce.e2.vo.RateFactorValue" hint="Describes the VO containing the rate factor value record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Persist the rateFactor value data to the application database --->
		<cfset this.dataMgr.updateRecord("RateFactorValues", arguments.rateFactorValue)>

	</cffunction>
	
	<cffunction name="saveTaxFactors" 
				access="public" returntype="boolean" 
				hint="This method is used to save tax factors for given postal codes, it adds or updates the tax factors. The method throws exception if some of the given factor lookup codes is missing."> 
		<cfargument name="postalCode" type="string" required="true" hint="One or a list of postal codes devided with comma" /> 
		<cfargument name="commodityFactor" type="string" required="true" hint="Lookup code for commodity tax factor" /> 
		<cfargument name="deliveryFactor" type="string" required="true" hint="Lookup code for delivery tax factor" /> 
		<cfargument name="salesFactor" type="string" required="true" hint="Lookup code for sales tax factor" /> 

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Save the modified or new tax factors --->
		<cfset local.arr = ListToArray(postalCode)>
		
		<!--- We find all factors uids--->
		<cfquery name="local.qResult" datasource="#this.datasource#">
			SELECT 
				(SELECT rate_factor_uid FROM RateFactors (nolock) WHERE factor_lookup_code = <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.commodityFactor#">) as commodity_factor,
				(SELECT rate_factor_uid FROM RateFactors (nolock) WHERE factor_lookup_code = <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.deliveryFactor#">) as delivery_factor,
				(SELECT rate_factor_uid FROM RateFactors (nolock) WHERE factor_lookup_code = <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.salesFactor#">) as sales_factor
		</cfquery>
		
		<cfset local.commodityFactorUid = local.qResult.commodity_factor>
		<cfset local.deliveryFactorUid = local.qResult.delivery_factor>
		<cfset local.salesFactorUid = local.qResult.sales_factor>
		
		<!---If we don't find some of the factors we throw an exception--->
		<cfif local.commodityFactorUid eq "" OR local.deliveryFactorUid eq "" OR local.salesFactorUid eq "">
			<cfthrow
				type="com.mcenergyinc.RateModel.NoSuchFactor"
				errorcode="NoSuchFactor"
				message="One or more of the factors can't be found!"
				detail="Please verify the factor lookup codes you have entered!">
		</cfif>
		
		<cfloop array=#local.arr# index="name">
		
			<cfquery name="local.uResult" datasource="#this.datasource#">
				begin tran
				update RateModelTaxesByArea with (serializable) 
					set commodity_tax_factor_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#local.commodityFactorUid#">,
					delivery_tax_factor_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#local.deliveryFactorUid#">,
					sales_tax_factor_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#local.salesFactorUid#">,
					modified_date = CURRENT_TIMESTAMP,
					modified_by = <cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#Session.mce.e2.session.AUDITUSERLABEL#">,
					is_active = 1
				where postal_code = <cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#Trim(name)#">

				if @@rowcount = 0
				begin
					insert RateModelTaxesByArea (area_taxes_uid, is_active, created_by, modified_by, postal_code, commodity_tax_factor_uid, delivery_tax_factor_uid, sales_tax_factor_uid, created_date, modified_date) 
					values (newid(), 1, <cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#Session.mce.e2.session.AUDITUSERLABEL#">, <cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#Session.mce.e2.session.AUDITUSERLABEL#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#Trim(name)#">, <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#local.commodityFactorUid#">,
						<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#local.deliveryFactorUid#">, <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#local.salesFactorUid#">,
						CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
				end
				commit tran
			</cfquery>			
		</cfloop>
		
		<cfreturn true>
	</cffunction>
	
	<cffunction name="deleteTaxFactors" 
				access="public" returntype="boolean" 
				hint="This method is used to delete tax factors for given postal codes, it updates the is_active field."> 
		<cfargument name="postalCode" type="string" required="true" hint="One or a list of postal codes devided with comma" />

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Save the modified or new tax factors --->
		<cfset local.arr = ListToArray(postalCode)>
		
		<cfloop array=#local.arr# index="name">
		
			<cfquery name="local.uResult" datasource="#this.datasource#">
				update RateModelTaxesByArea with (serializable) 
					set modified_date = CURRENT_TIMESTAMP,
					modified_by = <cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#Session.mce.e2.session.AUDITUSERLABEL#">,
					is_active = 0
				where postal_code = <cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#Trim(name)#">
			</cfquery>			
		</cfloop>
		
		<cfreturn true>
	</cffunction>
	
	<cffunction name="getAllTaxFactors" 
				access="public" returntype="array" 
				hint="This method is used to get all tax factors for postal codes (cities)"> 
		
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfquery name="local.qResult" datasource="#this.datasource#">
			SELECT rt.postal_code,
			(SELECT factor_lookup_code FROM RateFactors (nolock) WHERE rate_factor_uid = rt.commodity_tax_factor_uid) as commodity_factor,
			(SELECT factor_lookup_code FROM RateFactors (nolock) WHERE rate_factor_uid = rt.delivery_tax_factor_uid) as delivery_factor,
			(SELECT factor_lookup_code FROM RateFactors (nolock) WHERE rate_factor_uid = rt.sales_tax_factor_uid) as sales_factor
			FROM RateModelTaxesByArea rt
			WHERE rt.is_active = 1
			ORDER BY postal_code
		</cfquery>
		
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.TaxFactor")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>
	</cffunction>

</cfcomponent>