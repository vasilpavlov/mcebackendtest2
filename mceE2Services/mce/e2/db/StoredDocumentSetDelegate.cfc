
<!---

  Template Name:  StoredDocumentSetDelegate
     Base Table:  StoredDocumentSet	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, April 16, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, April 16, 2009 - Template Created.		

--->
		
<cfcomponent displayname="StoredDocumentSetDelegate"
				hint="This CFC manages all data access interactions for the StoredDocumentSet table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyStoredDocumentSetComponent"
				hint="This method is used to retrieve a Stored Document Set object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.StoredDocumentSet"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.StoredDocumentSet")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getStoredDocumentSetsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Stored Document Set objects for all the Stored Document Sets in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="document_set_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given document set."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Stored Document Sets and build out the array of components --->
		<cfset local.qResult = getStoredDocumentSet(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.StoredDocumentSet")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getStoredDocumentSetAsComponent"
				hint="This method is used to retrieve a single instance of a / an Stored Document Set value object representing a single Stored Document Set record."
				output="false"
				returnType="mce.e2.vo.StoredDocumentSet"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="document_set_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given document set."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Stored Document Set and build out the component --->
		<cfset local.qResult = getStoredDocumentSet(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.StoredDocumentSet")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getStoredDocumentSet"
				hint="This method is used to retrieve single / multiple records from the StoredDocumentSet table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="document_set_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given document set."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the customer facing / friendly name of the document set."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Stored Document Set records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the StoredDocumentSet table matching the where clause
			select	tbl.document_set_uid,
					tbl.friendly_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.StoredDocumentSet tbl (nolock)
			where	1=1
					and tbl.is_active = 1
					
					<cfif structKeyExists(arguments, 'document_set_uid')>
					and tbl.document_set_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.document_set_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="true"> ) 
					</cfif>

			order
			by		tbl.friendly_name,
					tbl.modified_date

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<cffunction name="getStoredDocumentRepository"
				access="public" returnType="query"
				hint="This method is used to retrieve the properties of a given document repository.">
					
		<!--- Define the arguments for a given document repository.  --->			
		<cfargument name="document_repository_uid" type="string" required="false" hint="Defines the primary key / unique identifier representing a given document repository.">					
		<cfargument name="repository_lookup_code" type="string" required="false" hint="Defines the internal identifier representing a given document repository.">		
				
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Retrieve the repository information --->
		<cfquery name="local.qResult" datasource="#this.datasource#">		
				
			select	tbl.document_repository_uid,
					tbl.friendly_name,
					tbl.absolute_filesystem_path,
					tbl.relative_url_base_path,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					tbl.repository_lookup_code
					
			from	dbo.StoredDocumentRepositories tbl (nolock)
			where	1=1
					and tbl.is_active = 1
					
					<cfif structKeyExists(arguments, 'document_repository_uid')>
					and tbl.document_repository_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.document_repository_uid#" null="false" list="true"> )
					</cfif>
						
					<cfif structKeyExists(arguments, 'repository_lookup_code')>
					and tbl.repository_lookup_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.repository_lookup_code#" null="false" list="true"> )
					</cfif>		
				
		</cfquery>
		
		<!--- Return the result set --->
		<cfreturn local.qResult>		
				
	</cffunction>

	<!---Describes the methods used to persist / modify Stored Document Set data. --->	
	<cffunction name="saveNewStoredDocumentSet"
				hint="This method is used to persist a new Stored Document Set record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="StoredDocumentSet" required="true" type="mce.e2.vo.StoredDocumentSet" hint="Describes the VO containing the Stored Document Set record that will be persisted."/>

		<!--- Persist the Stored Document Set data to the application database --->
		<cfset this.dataMgr.insertRecord("StoredDocumentSet", arguments.StoredDocumentSet)/>

	</cffunction>

	<cffunction name="saveExistingStoredDocumentSet"
				hint="This method is used to persist an existing Stored Document Set record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="StoredDocumentSet" required="true" type="mce.e2.vo.StoredDocumentSet" hint="Describes the VO containing the Stored Document Set record that will be persisted."/>

		<!--- Persist the Stored Document Set data to the application database --->
		<cfset this.dataMgr.updateRecord("StoredDocumentSet", arguments.StoredDocumentSet)/>

	</cffunction>

</cfcomponent>
