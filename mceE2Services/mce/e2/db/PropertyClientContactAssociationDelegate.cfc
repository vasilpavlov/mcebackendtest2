
<!---

  Template Name:  PropertyClientContactAssociationDelegate
     Base Table:  Properties_ClientContacts	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, April 02, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, April 02, 2009 - Template Created.		

--->
		
<cfcomponent displayname="PropertyClientContactAssociationDelegate"
				hint="This CFC manages all data access interactions for the Properties_ClientContacts table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyPropertyClientContactAssociationComponent"
				hint="This method is used to retrieve a Property Client Contact Association object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.PropertyClientContactAssociation"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.PropertyClientContactAssociation")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getPropertyClientContactAssociationsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Property Client Contact Association objects for all the Property Client Contact Associations in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="relationship_id" required="false" type="numeric" hint="Describes the primary key / unique identifier for a given property / client contact association."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given / associated property."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Property Client Contact Associations and build out the array of components --->
		<cfset local.qResult = getPropertyClientContactAssociation(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.PropertyClientContactAssociation")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getPropertyClientContactAssociationAsComponent"
				hint="This method is used to retrieve a single instance of a / an Property Client Contact Association value object representing a single Property Client Contact Association record."
				output="false"
				returnType="mce.e2.vo.PropertyClientContactAssociation"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="relationship_id" required="false" type="numeric" hint="Describes the primary key / unique identifier for a given property / client contact association."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given / associated property."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Property Client Contact Association and build out the component --->
		<cfset local.qResult = getPropertyClientContactAssociation(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.PropertyClientContactAssociation")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getPropertyClientContactAssociation"
				hint="This method is used to retrieve single / multiple records from the Properties_ClientContacts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="relationship_id" required="false" type="numeric" hint="Describes the primary key / unique identifier for a given property / client contact association."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given / associated property."/>
		<cfargument name="client_company_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given / associated client company."/>
		<cfargument name="client_contact_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given / associated client contact."/>
		<cfargument name="contact_role_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given / associated contact role."/>
		<cfargument name="relationship_filter_date" required="true" type="date" default="#createOdbcDateTime(now())#" hint="Describes the date used to filter active records."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Property Client Contact Association records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the Properties_ClientContacts table matching the where clause
			select	tbl.relationship_id,
					tbl.property_uid,
					tbl.client_contact_uid,
					tbl.contact_role_uid,
					tbl.relationship_start,
					tbl.relationship_end,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					tbl.client_company_uid,
					
					ccr.friendly_name as ContactRoleFriendlyName,
					p.friendly_name as propertyFriendlyName,
					cc.friendly_name as clientCompanyFriendlyName,
					cco.first_name,
					cco.last_name,
					cco.email,
					cco.phone,
					cco.comments

			from	dbo.ClientContacts cco (nolock)
					join dbo.Properties_ClientContacts tbl (nolock)
						on cco.client_contact_uid = tbl.client_contact_uid
					join dbo.ClientContactRoles ccr (nolock)
						on tbl.contact_role_uid = ccr.contact_role_uid
					join dbo.Properties p (nolock)
						on tbl.property_uid = p.property_uid
					join dbo.ClientCompanies cc (nolock)
						on tbl.client_company_uid = cc.client_company_uid

			where	1=1
			
					<cfif arguments.selectMethod eq 'current'>	
					and cco.is_active = 1
					and ccr.is_active = 1
					and p.is_active = 1
					and cc.is_active = 1										
					and (

						-- Only retrieve active associations
						tbl.is_active = 1
						and ( tbl.relationship_end is null or tbl.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
						and tbl.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
				
					)	
					</cfif>
					
					<cfif structKeyExists(arguments, 'relationship_id')>
					and tbl.relationship_id in ( <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.relationship_id#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and tbl.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'client_contact_uid')>
					and tbl.client_contact_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_contact_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'contact_role_uid')>
					and tbl.contact_role_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.contact_role_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and cc.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="true" null="false"> ) 
					</cfif>

			order
			by		cc.friendly_name,
					cco.last_name,
					cco.first_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Property Client Contact Association data. --->	
	<cffunction name="saveNewPropertyClientContactAssociation"
				hint="This method is used to persist a new Property Client Contact Association record to the application database."
				output="false"
				returnType="numeric"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="PropertyClientContactAssociation" required="true" type="mce.e2.vo.PropertyClientContactAssociation" hint="Describes the VO containing the Property Client Contact Association record that will be persisted."/>

		<!--- Persist the Property Client Contact Association data to the application database --->
		<cfreturn this.dataMgr.insertRecord("Properties_ClientContacts", arguments.PropertyClientContactAssociation)/>

	</cffunction>

	<cffunction name="saveExistingPropertyClientContactAssociation"
				hint="This method is used to persist an existing Property Client Contact Association record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="PropertyClientContactAssociation" required="true" type="mce.e2.vo.PropertyClientContactAssociation" hint="Describes the VO containing the Property Client Contact Association record that will be persisted."/>

		<!--- Persist the Property Client Contact Association data to the application database --->
		<cfset this.dataMgr.updateRecord("Properties_ClientContacts", arguments.PropertyClientContactAssociation)/>

	</cffunction>

</cfcomponent>
