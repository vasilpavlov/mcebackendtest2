		
<cfcomponent displayname="CompanyAddressDelegate"
				hint="This CFC manages all data access interactions for the CompanyAddresses table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyCompanyAddressComponent"
				hint="This method is used to retrieve a Company Address object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.CompanyAddress"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.CompanyAddress")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getCompanyAddressesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Company Address objects for all the Company Addresses in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="company_address_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given company / address association."/>
		<cfargument name="client_company_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated company."/>
		<cfargument name="address_type_code" required="false" type="string" hint="Describes the primary key / unique identifier of the associated address type."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Company Addresses and build out the array of components --->
		<cfset local.qResult = getCompanyAddress(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.CompanyAddress")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getCompanyAddressAsComponent"
				hint="This method is used to retrieve a single instance of a / an Company Address value object representing a single Company Address record."
				output="false"
				returnType="mce.e2.vo.CompanyAddress"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="company_address_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given company / address association."/>
		<cfargument name="client_company_uid" required="true" type="string" hint="Describes the primary key / unique identifier for a given associated company."/>
		<cfargument name="address_type_code" required="true" type="string" hint="Describes the primary key / unique identifier of the associated address type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Company Address and build out the component --->
		<cfset local.qResult = getCompanyAddress(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.CompanyAddress")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getCompanyAddress"
				hint="This method is used to retrieve single / multiple records from the CompanyAddresses table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="company_address_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given company / address association."/>
		<cfargument name="client_company_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated company."/>
		<cfargument name="address_type_code" required="false" type="string" hint="Describes the primary key / unique identifier of the associated address type."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Company Address records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the CompanyAddresses table matching the where clause
			select	tbl.company_address_uid,
					tbl.client_company_uid,
					tbl.address_type_code,
					tbl.relationship_start,
					tbl.relationship_end,
					tbl.address_line_1,
					tbl.address_line_2,
					tbl.city,
					tbl.state,
					tbl.country,
					tbl.postal_code,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					
					att.friendly_name as addressTypeFriendlyName,
					cc.friendly_name as companyFriendlyName

			from	dbo.CompanyAddresses tbl (nolock)
					join dbo.AddressTypes att (nolock) 
						on tbl.address_type_code = att.address_type_code
					join dbo.ClientCompanies cc (nolock)
						on tbl.client_company_uid = cc.client_company_uid

			where	1=1 
			
					<cfif arguments.selectMethod eq 'current'>												
					and att.is_active = 1
					and	cc.is_active = 1
					and (

							-- Only retrieve active associations
							tbl.is_active = 1
							and ( tbl.relationship_end is null or tbl.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and tbl.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
					
						)
					</cfif>
					
					<cfif structKeyExists(arguments, 'company_address_uid')>
					and tbl.company_address_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.company_address_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and tbl.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'address_type_code')>
					and tbl.address_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.address_type_code#" null="false" list="true"> ) 
					</cfif>

			order
			by		tbl.client_company_uid,
					tbl.state,
					tbl.city,
					tbl.address_line_1

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Company Address data. --->	
	<cffunction name="saveNewCompanyAddress"
				hint="This method is used to persist a new Company Address record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="CompanyAddress" required="true" type="mce.e2.vo.CompanyAddress" hint="Describes the VO containing the Company Address record that will be persisted."/>

		<!--- Persist the Company Address data to the application database --->
		<cfset this.dataMgr.insertRecord("CompanyAddresses", arguments.CompanyAddress)/>

	</cffunction>

	<cffunction name="saveExistingCompanyAddress"
				hint="This method is used to persist an existing Company Address record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="CompanyAddress" required="true" type="mce.e2.vo.CompanyAddress" hint="Describes the VO containing the Company Address record that will be persisted."/>

		<!--- Persist the Company Address data to the application database --->
		<cfset this.dataMgr.updateRecord("CompanyAddresses", arguments.CompanyAddress)/>

	</cffunction>

</cfcomponent>
