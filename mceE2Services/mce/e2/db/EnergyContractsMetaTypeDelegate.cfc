<cfcomponent displayname="EnergyContractsMetaTypeDelegate"
				hint="This CFC manages all data access interactions for the EnergyContractsMetaTypes table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyContractsMetaDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Energy Contracts Meta Data data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyContractsMetaDelegate" hint="Describes the *.cfc used to manage database interactions related to Energy Contracts Meta Data information.">
		<cfset this.EnergyContractsMetaDelegate = arguments.bean>
	</cffunction>

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEnergyContractsMetaTypeComponent"
				hint="This method is used to retrieve a Property Meta Type object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EnergyContractsMetaType"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EnergyContractsMetaType")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEnergyContractsMetaTypesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Property Meta Type objects for all the EnergyContractsMetaTypes in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="meta_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given property meta type record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all EnergyContractsMetaTypes and build out the array of components --->
		<cfset local.qResult = getEnergyContractsMetaType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyContractsMetaType")/>
		<cfset this.EnergyContractsMetaDelegate.applyChoicesToEnergyContractsMeta(local.returnResult)>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyContractsMetaTypeAsComponent"
				hint="This method is used to retrieve a single instance of a / an Property Meta Type value object representing a single Property Meta Type record."
				output="false"
				returnType="mce.e2.vo.EnergyContractsMetaType"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="meta_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given property meta type record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Property Meta Type and build out the component --->
		<cfset local.qResult = getEnergyContractsMetaType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EnergyContractsMetaType")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyContractsMetaType"
				hint="This method is used to retrieve single / multiple records from the EnergyContractsMetaTypes table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="meta_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given property meta type record."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given property meta type."/>
		<cfargument name="type_lookup_code" required="false" type="string" hint="Describes the internal identifier for a given property meta type."/>
		<cfargument name="data_type" required="false" type="numeric" hint="Describes the type of data represented by a given property meta type."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Property Meta Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyContractsMetaTypes table matching the where clause
			select	tbl.meta_type_uid,
					tbl.friendly_name,
					tbl.type_lookup_code,
					tbl.data_type,
					tbl.input_rule_uid,
					tbl.is_required,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					(select COUNT(1) from dbo.EnergyContractsMetaChoices ecmc (nolock) where ecmc.meta_type_uid = tbl.meta_type_uid) as numMetaChoices

			from	dbo.EnergyContractsMetaTypes tbl (nolock)
			where	1=1
					and tbl.is_active = 1
					
					<cfif structKeyExists(arguments, 'meta_type_uid')>
					and tbl.meta_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.meta_type_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'type_lookup_code')>
					and tbl.type_lookup_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.type_lookup_code#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'data_type')>
					and tbl.data_type in ( <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.data_type#" null="false" list="true"> ) 
					</cfif>

			order
			by		tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Property Meta Type data. --->	
	<cffunction name="saveNewEnergyContractsMetaType"
				hint="This method is used to persist a new Property Meta Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyContractsMetaType" required="true" type="mce.e2.vo.EnergyContractsMetaType" hint="Describes the VO containing the Property Meta Type record that will be persisted."/>

		<!--- Persist the Property Meta Type data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyContractsMetaTypes", arguments.EnergyContractsMetaType)/>

	</cffunction>

	<cffunction name="saveExistingEnergyContractsMetaType"
				hint="This method is used to persist an existing Property Meta Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyContractsMetaType" required="true" type="mce.e2.vo.EnergyContractsMetaType" hint="Describes the VO containing the Property Meta Type record that will be persisted."/>

		<!--- Persist the Property Meta Type data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyContractsMetaTypes", arguments.EnergyContractsMetaType)/>

	</cffunction>

</cfcomponent>
