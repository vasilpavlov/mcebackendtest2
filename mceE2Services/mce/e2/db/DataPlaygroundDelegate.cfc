
<!---

  Template Name:  DataPlaygroundDelegate
     Base Table:  OptionsForMetricAdvisors, OptionsNamespaces

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Thursday, April 16, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Thursday, April 16, 2009 - Template Created.

--->

<cfcomponent displayname="DataPlaygroundDelegate"
				hint="This CFC manages all data access interactions for common data playground data including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyUsageMetricDivisor"
				hint="This method is used to retrieve an empty UsageMetricAdvisor object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.UsageMetricDivisor"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.UsageMetricDivisor")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getUsageMetricDivisorsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of UsageMetricDivisor objects for all the data metric divsors in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="metric_divisor_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given metric divisor."/>
		<cfargument name="namespace_lookup" required="false" type="string" hint="Describes the option namespace used to filter metric divisors."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given / associated property."/>
		<cfargument name="as_of_date" required="false" type="string" default="#now()#" hint="Describes the date used to filter metric divisors."/>
		<cfargument name="permission_needed" required="false" type="string"/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Alerts and build out the array of components --->
		<cfset local.qResult = getUsageMetricDivisor(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult,"mce.e2.vo.UsageMetricDivisor")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getUsageMetricDivisorAsComponent"
				hint="This method is used to retrieve a single instance of a / an UsageMetricDivisor value object representing a single optionsForMetricDivisors record."
				output="false"
				returnType="mce.e2.vo.UsageMetricDivisor"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="metric_divisor_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given metric divisor."/>
		<cfargument name="namespace_lookup" required="false" type="string" hint="Describes the option namespace used to filter metric divisors."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given / associated property."/>
		<cfargument name="as_of_date" required="false" type="string" default="#now()#" hint="Describes the date used to filter metric divisors."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Alert and build out the component --->
		<cfset local.qResult = getUsageMetricDivisor(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.UsageMetricDivisor")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getUsageMetricDivisor"
				hint="This method is used to retrieve single / multiple records from the OptionsForMetricsDivisors table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="metric_divisor_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given metric divisor."/>
		<cfargument name="namespace_lookup_code" required="false" type="string" hint="Describes the option namespace used to filter metric divisors."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given / associated property."/>
		<cfargument name="as_of_date" required="false" type="string" default="#now()#" hint="Describes the date used to filter metric divisors."/>
		<cfargument name="permission_needed" required="false" type="string"/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Alert records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			select	'Show Actuals' as friendly_name,
					0 as display_order,
					NULL as property_meta_type_lookup_code,
					NULL as propertyMetaTypeFriendlyName,
					NULL as calculation_operation,
					1 as divisorValue

			union

			select
					tbl.friendly_name,
					tbl.display_order,
					tbl.property_meta_type_lookup_code,
					pmt.friendly_name as propertyMetaTypeFriendlyName,
					tbl.calculation_operation,
					case

						when tbl.calculation_operation = 'SUM' then sum(convert(decimal, isnull(dbo.getPropertyMetaValue(p.property_uid, tbl.property_meta_type_lookup_code, <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.as_of_date)#">), 0)))
						when tbl.calculation_operation = 'AVG' then avg(convert(decimal, isnull(dbo.getPropertyMetaValue(p.property_uid, tbl.property_meta_type_lookup_code, <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.as_of_date)#">), 0)))

					end as divisorValue

			from	dbo.OptionsForMetricDivisors tbl (nolock)
					join dbo.propertyMetaTypes pmt (nolock)
						on tbl.property_meta_type_lookup_code = pmt.type_lookup_code
					join dbo.propertyMeta pm (nolock)
						on pmt.meta_type_uid = pm.meta_type_uid
					join dbo.properties p (nolock)
						on pm.property_uid = p.property_uid

			where	1=1

					and tbl.is_active = 1
					and pmt.is_active = 1
					and pm.is_active = 1
					and p.is_active = 1

					<cfif structKeyExists(arguments, 'metric_divisor_uid')>
					and tbl.metric_divisor_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.metric_divisor_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'namespace_lookup')>
					and tbl.namespace_lookup_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.namespace_lookup_code#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and p.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'permission_needed')>
					and tbl.permission_needed in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.permission_needed#" null="false" list="true"> )
					</cfif>

			group
			by
					tbl.friendly_name,
					tbl.display_order,
					tbl.property_meta_type_lookup_code,
					tbl.calculation_operation,
					pmt.friendly_name

			order
			by
					display_order, friendly_name
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>





	<cffunction name="getPropertyMetaDataAnalysis"
				hint="This method is used to retrieve energy account analysis data filtered by property meta types."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_uid_list" type="string" required="true" hint="The property/properties that you want analysis records for">
		<cfargument name="propertyMetaTypeCode" type="string" required="true" hint="Describes the lookup code for a given property meta type.">
		<cfargument name="period_start" type="date" required="true" hint="Describes the start date that will be used to process period data.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end date that will be used to process period data.">
		<cfargument name="grouping" type="string" required="true" hint="Describes the data roll-up mode that will be implemented (acceptable values are 'daily', 'monthly').">
		<cfargument name="rateFactorViaPropertyMetaCodes" type="string" required="false" default="">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<cfsavecontent variable="local.commonColumns">
			<cfoutput>
				COUNT(*) numTotalRecords
				<cfloop list="#propertyMetaTypeCode#" index="local.typeCode">
					, AVG([#local.typeCode#]) AS [PropertyMeta_#local.typeCode#]
				</cfloop>
				<cfloop list="#arguments.rateFactorViaPropertyMetaCodes#" index="local.typeCode">
					, SUM([RateFactor_#safeCodeFormat(local.typeCode)#]) AS [RateFactor_#safeCodeFormat(local.typeCode)#]
				</cfloop>
			</cfoutput>
		</cfsavecontent>

		<cfquery name="local.qResult" datasource="#this.datasource#">
			WITH RawData(
				property_uid,
				friendly_name,
				rollupPeriodStart
				<cfloop list="#arguments.propertyMetaTypeCode#" index="local.typeCode">
					, [#local.typeCode#]
				</cfloop>
				<cfloop list="#arguments.rateFactorViaPropertyMetaCodes#" index="local.typeCode">
					, [RateFactor_#safeCodeFormat(local.typeCode)#]
				</cfloop>
			) AS (
				SELECT
					p.property_uid,
					p.friendly_name,
					dt.start_date as rollupPeriodStart
					<cfloop list="#propertyMetaTypeCode#" index="local.typeCode">
						, dbo.getPropertyMetaValue(p.property_uid, '#local.typeCode#', dt.start_date) as [#local.typeCode#]
					</cfloop>
					<cfloop list="#arguments.rateFactorViaPropertyMetaCodes#" index="local.typeCode">
						<cfset local.metaTypeLookupCode = stringExtract(local.typeCode, "{(\w+)}", 2)>
						<cfset local.typeCode = Replace(local.typeCode, local.metaTypeLookupCode, "meta")>
						, dbo.getRateFactorAggregatedValueViaPropertyMeta(
							property_uid,
							'#local.metaTypeLookupCode#',
							<!--- simulate max(a,b) ---> case when dt.start_date < #createOdbcDateTime(arguments.period_start)# then #createOdbcDateTime(arguments.period_start)# else dt.start_date end,
							<!--- simulate min(a,b) ---> case when dt.end_date > #createOdbcDateTime(arguments.period_end)# then #createOdbcDateTime(arguments.period_end)# else dt.end_date end,
							'SUM',
							'#local.typeCode#') AS [RateFactor_#safeCodeFormat(local.typeCode)#]
					</cfloop>
				FROM
					Properties p,
					dbo.generateDateTable(
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.period_start)#">,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.period_end)#">,
						'month',
						1) dt
				WHERE
					p.property_uid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#property_uid_list#" list="true">)
			)

			<cfswitch expression="#arguments.grouping#">
				<cfcase value="property">
					SELECT
						property_uid,
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						property_uid
					ORDER BY
						property_uid
				</cfcase>
				<cfcase value="periodMonth">
					SELECT
						rollupPeriodStart as periodEnd,
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						rollupPeriodStart
					ORDER BY
						rollupPeriodStart
				</cfcase>
				<cfcase value="periodQuarter">
					SELECT
						dbo.getDateQuarterName(rollupPeriodStart) as periodEnd,
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						dbo.getDateQuarterName(rollupPeriodStart)
					ORDER BY
						dbo.getDateQuarterName(rollupPeriodStart)
				</cfcase>
				<cfcase value="periodYear">
					SELECT
						YEAR(rollupPeriodStart) as periodEnd,
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						YEAR(rollupPeriodStart)
					ORDER BY
						YEAR(rollupPeriodStart)
				</cfcase>

				<cfdefaultcase>
					<cfthrow
						message="Unknown grouping specfified ('#arguments.grouping#')">
				</cfdefaultcase>
			</cfswitch>
		</cfquery>

		<!--- Return the query result --->
		<cfreturn local.qResult>

	</cffunction>



	<cffunction name="getAvailableBudgetTypes" returntype="query" access="public">
		<cfargument name="energy_account_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given energy account.">
		<cfargument name="period_start" type="date" required="true" hint="Describes the start date that will be used to process period data.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end date that will be used to process period data.">

		<cfset var qResult = "">

		<cfquery name="qResult" datasource="#this.datasource#">
			SELECT
				b.budget_type_code,
				bt.friendly_name,
				COUNT(*) as usageCount
			FROM
				EnergyBudgets b JOIN
				EnergyBudgetTypes bt ON b.budget_type_code = bt.budget_type_code JOIN
				EnergyBudgetDetail bd ON b.energy_budget_uid = bd.energy_budget_uid JOIN
				EnergyUsage eu ON eu.energy_usage_uid = bd.energy_usage_uid
			WHERE
				eu.energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.energy_account_uid#">)
				AND eu.period_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.period_start)#">
				AND eu.period_end <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.period_end)#">
				AND b.is_active = 1
				AND bd.is_active = 1
				AND eu.is_active = 1
				AND eu.usage_type_code = 'budget'
			GROUP BY
				b.budget_type_code,
				bt.friendly_name
			ORDER BY
				bt.friendly_name
		</cfquery>

		<cfreturn qResult>
	</cffunction>



<!---	<cffunction name="getEnergyMetaDataAnalysis"
				hint="This method is used to retrieve energy account analysis data filtered by energy usage meta types."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" type="string" required="true" hint="Describes the primary key / unique identifier for a given energy account.">
		<cfargument name="energyUsageMetaTypeCode" type="string" required="true" hint="Describes the lookup code for a given energy usage meta type.">
		<cfargument name="period_start" type="date" required="true" hint="Describes the start date that will be used to process period data.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end date that will be used to process period data.">
		<cfargument name="dateRollupMode" type="string" required="true" hint="Describes the data roll-up mode that will be implemented (acceptable values are 'daily', 'monthly').">
		<cfargument name="grouping" type="string" required="false" default="periodMonth" hint="Describes the internal identifier / lookup code for a given profile.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve a single / multiple Energy Account records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			WITH RawData(
				property_uid,
				energy_account_uid,
				type_lookup_code,
				metaTypeFriendlyName,
				rollupPeriodStart,
				meta_value
			) AS (

				SELECT
					ea.property_uid,
					ea.energy_account_uid,
					et.energy_type_uid,
					eumt.type_lookup_code,
					eumt.friendly_name AS metaTypeFriendlyName,
					dt.start_date AS rollupPeriodStart,
					(
						SELECT
							SUM(eum.metaValueAsNumeric)
						FROM
							energyUsage eu JOIN
							EnergyUsageMeta eum ON eu.energy_usage_uid = eum.energy_usage_uid
						WHERE
							eu.energy_account_uid = ea.energy_account_uid
							AND eum.meta_type_uid = eumt.meta_type_uid
							AND eu.period_end BETWEEN dt.start_date AND dt.end_date
							AND eu.is_active = 1
							AND eum.is_active = 1
					) AS meta_value
				 FROM
					dbo.EnergyUsageMetaTypes eumt,
					dbo.EnergyTypes et,
					dbo.EnergyAccounts ea,
					dbo.generateDateTable(
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.period_start)#">,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.period_end)#">,
						<cfif arguments.dateRollupMode eq 'monthly'>'month'</cfif>
						<cfif arguments.dateRollupMode eq 'daily'>'day'</cfif>,
						1) dt
				WHERE
					eumt.type_lookup_code IN (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#arguments.energyUsageMetaTypeCode#">)
					AND ea.energy_account_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.energy_account_uid#">)
					AND et.energy_type_uid = ea.energy_type_uid
					AND ea.is_active = 1
					AND eumt.is_active = 1
			)
			<cfswitch expression="#arguments.grouping#">
				<cfcase value="property">
					SELECT
						property_uid,
						type_lookup_code,
						sum(meta_value) as meta_value
					FROM
						RawData
					GROUP BY
						property_uid, type_lookup_code
					ORDER BY
						property_uid, type_lookup_code
				</cfcase>
				<cfcase value="energyAccount">
					SELECT
						energy_account_uid,
						property_uid,
						type_lookup_code,
						sum(meta_value) as meta_value
					FROM
						RawData
					GROUP BY
						energy_account_uid, property_uid, type_lookup_code
					ORDER BY
						energy_account_uid, property_uid, type_lookup_code
				</cfcase>
				<cfcase value="energyType">
					SELECT
						energy_type_uid
						sum(meta_value) as meta_value
					FROM
						RawData
					GROUP BY
						energy_type_uid
					ORDER BY
						energyTypeFriendlyName
				</cfcase>
				<cfcase value="periodMonth">
					SELECT
						rollupPeriodStart as periodEnd,
						type_lookup_code,
						sum(meta_value) as meta_value
					FROM
						RawData
					GROUP BY
						rollupPeriodStart, type_lookup_code
					ORDER BY
						rollupPeriodStart, type_lookup_code
				</cfcase>
				<cfcase value="periodQuarter">
					SELECT
						dbo.getDateQuarterName(rollupPeriodStart) as periodEnd,
						type_lookup_code,
						sum(meta_value) as meta_value
					FROM
						RawData
					GROUP BY
						dbo.getDateQuarterName(rollupPeriodStart), type_lookup_code
					ORDER BY
						dbo.getDateQuarterName(rollupPeriodStart), type_lookup_code
				</cfcase>
				<cfcase value="periodYear">
					SELECT
						YEAR(rollupPeriodStart) as periodEnd,
						type_lookup_code,
						sum(meta_value) as meta_value
					FROM
						RawData
					GROUP BY
						YEAR(rollupPeriodStart), type_lookup_code
					ORDER BY
						YEAR(rollupPeriodStart), type_lookup_code
				</cfcase>

				<cfdefaultcase>
					<cfthrow
						message="Unknown grouping specfified ('#arguments.grouping#')">
				</cfdefaultcase>
			</cfswitch>
		</cfquery>

		<!--- Return the query result --->
		<cfreturn local.qResult>

	</cffunction>--->








<!---	<cffunction name="getRateFactorDataAnalysis"
				hint="This method is used to retrieve energy account analysis data filtered by rate factor meta types."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="factorLookupCodes" type="string" required="true" hint="Describes the lookup code for a given rate factor.">
		<cfargument name="period_start" type="date" required="true" hint="Describes the start date that will be used to process period data.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end date that will be used to process period data.">
		<cfargument name="grouping" type="string" required="true" hint="Describes the data roll-up mode that will be implemented (acceptable values are 'daily', 'monthly').">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>


		<cfsavecontent variable="local.commonColumns">
			<cfoutput>
				COUNT(*) numTotalRecords
				<cfloop list="#factorLookupCodes#" index="local.typeCode">
					, AVG([#local.typeCode#]) AS [RateFactor_#safeCodeFormat(local.typeCode)#]
				</cfloop>
			</cfoutput>
		</cfsavecontent>

		<cfquery name="local.qResult" datasource="#this.datasource#">
			WITH RawData(
				rollupPeriodStart
				<cfloop list="#factorLookupCodes#" index="local.typeCode">
					, [#local.typeCode#]
				</cfloop>
			) AS (
				SELECT
					dt.start_date as rollupPeriodStart
					<cfloop list="#factorLookupCodes#" index="local.typeCode">
						, dbo.getRateFactorValue('#local.typeCode#', dt.start_date) as [#local.typeCode#]
					</cfloop>
				FROM
					RateFactors rf,
					dbo.generateDateTable(
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.period_start)#">,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.period_end)#">,
						'month',
						1) dt
			)

			<cfswitch expression="#arguments.grouping#">
				<cfcase value="periodMonth">
					SELECT
						rollupPeriodStart as periodEnd,
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						rollupPeriodStart
					ORDER BY
						rollupPeriodStart
				</cfcase>
				<cfcase value="periodQuarter">
					SELECT
						dbo.getDateQuarterName(rollupPeriodStart) as periodEnd,
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						dbo.getDateQuarterName(rollupPeriodStart)
					ORDER BY
						dbo.getDateQuarterName(rollupPeriodStart)
				</cfcase>
				<cfcase value="periodYear">
					SELECT
						YEAR(rollupPeriodStart) as periodEnd,
						#local.commonColumns#
					FROM
						RawData
					GROUP BY
						YEAR(rollupPeriodStart)
					ORDER BY
						YEAR(rollupPeriodStart)
				</cfcase>

				<cfdefaultcase>
					<cfthrow
						message="Unknown grouping specfified ('#arguments.grouping#')">
				</cfdefaultcase>
			</cfswitch>
		</cfquery>

		<!--- Return the query result --->
		<cfreturn local.qResult>

	</cffunction>--->

</cfcomponent>