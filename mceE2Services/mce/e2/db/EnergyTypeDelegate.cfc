
<!---

  Template Name:  EnergyType
     Base Table:  EnergyTypes	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, March 20, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, March 20, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnergyTypeDelegate"
				hint="This CFC manages all data access interactions for the EnergyTypes table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEnergyTypeComponent"
				hint="This method is used to retrieve a Energy Type object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EnergyType"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EnergyType")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEnergyTypesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Type objects for all the Energy Types in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key / unique identifierfor a given energy type."/>
		<cfargument name="default_energy_unit_uid" required="false" type="string" hint="Describes the default energy unit associated to a given energy type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Types and build out the array of components --->
		<cfset local.qResult = getEnergyType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyType")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyTypeAsComponent"
				hint="This method is used to retrieve a single instance of a / an Energy Type value object representing a single Energy Type record."
				output="false"
				returnType="mce.e2.vo.EnergyType"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key / unique identifierfor a given energy type."/>
		<cfargument name="default_energy_unit_uid" required="false" type="string" hint="Describes the default energy unit associated to a given energy type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Energy Type and build out the component --->
		<cfset local.qResult = getEnergyType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EnergyType")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyType"
				hint="This method is used to retrieve single / multiple records from the EnergyTypes table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_type_uid" required="false" type="string" hint="Describes the primary key / unique identifierfor a given energy type."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for a given energy type."/>
		<cfargument name="energy_type_hierarchy" required="false" type="string" hint="Describes the hierarchy element / node for a given energy type."/>
		<cfargument name="default_energy_unit_uid" required="false" type="string" hint="Describes the default energy unit associated to a given energy type."/>
		<cfargument name="meta_group_uid" required="false" type="string" hint="Describes the meta group a given energy type is associated to."/>
		<cfargument name="default_factor_alias_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the default factor alias."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyTypes table matching the where clause
			select	tbl.energy_type_uid,
					tbl.friendly_name,
					tbl.energy_type_hierarchy,
					tbl.default_energy_unit_uid,
					tbl.oldid,
					tbl.demand,
					tbl.meta_group_uid,
					tbl.default_factor_alias_uid,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.EnergyTypes tbl (nolock)
			where	1=1
					and tbl.is_active = 1
					
					<cfif structKeyExists(arguments, 'energy_type_uid')>
					and tbl.energy_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_type_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'energy_type_hierarchy')>
					and tbl.energy_type_hierarchy in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.energy_type_hierarchy#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'default_energy_unit_uid')>
					and tbl.default_energy_unit_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.default_energy_unit_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'meta_group_uid')>
					and tbl.meta_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.meta_group_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'default_factor_alias_uid')>
					and tbl.default_factor_alias_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.default_factor_alias_uid#" null="false" list="true"> ) 
					</cfif>
				order by tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Energy Type data. --->	
	<cffunction name="saveNewEnergyType"
				hint="This method is used to persist a new Energy Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyType" required="true" type="mce.e2.vo.EnergyType" hint="Describes the VO containing the Energy Type record that will be persisted."/>

		<!--- Persist the Energy Type data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyTypes", arguments.EnergyType)/>

	</cffunction>

	<cffunction name="saveExistingEnergyType"
				hint="This method is used to persist an existing Energy Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyType" required="true" type="mce.e2.vo.EnergyType" hint="Describes the VO containing the Energy Type record that will be persisted."/>

		<!--- Persist the Energy Type data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyTypes", arguments.EnergyType)/>

	</cffunction>

</cfcomponent>
