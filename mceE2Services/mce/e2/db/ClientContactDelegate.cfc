
<!---

  Template Name:  ClientContactDelegate
     Base Table:  ClientContacts

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Tuesday, March 31, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Tuesday, March 31, 2009 - Template Created.

--->

<cfcomponent displayname="ClientContactDelegate"
				hint="This CFC manages all data access interactions for the ClientContacts table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyClientContactComponent"
				hint="This method is used to retrieve a Client Contact object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.ClientContact"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.ClientContact")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getClientContactsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Client Contact objects for all the Client Contacts in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="client_contact_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client contact."/>
		<cfargument name="user_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the user associated to a given client contact."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Client Contacts and build out the array of components --->
		<cfset local.qResult = getClientContact(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.ClientContact")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getClientContactAsComponent"
				hint="This method is used to retrieve a single instance of a / an Client Contact value object representing a single Client Contact record."
				output="false"
				returnType="mce.e2.vo.ClientContact"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="client_contact_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client contact."/>
		<cfargument name="user_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the user associated to a given client contact."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="contact_role_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Contact Role record; can also contain a list of comma-delimited Client Contact Role primary keys.">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Client Contact and build out the component --->
		<cfset local.qResult = getClientContact(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.ClientContact")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getClientContactCompanyRelationshipId"
				hint="This method is used to retrieve an id for a single client contact company association."
				output="false"
				returnType="Numeric" 
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="client_contact_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client contact."/>
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="contact_role_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Contact Role record; can also contain a list of comma-delimited Client Contact Role primary keys.">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Client Contact records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
		select	relationship_id
		from	dbo.ClientContacts_ClientCompanies
		where	client_contact_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_contact_uid#" list="false" null="false">
				and client_company_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="false" null="false">
				and contact_role_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.contact_role_uid#" list="false" null="false">
		</cfquery>

		<cfreturn local.qResult.relationship_id>
	</cffunction>

	<cffunction name="getClientContact"
				hint="This method is used to retrieve single / multiple records from the ClientContacts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="client_contact_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client contact."/>
		<cfargument name="user_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the user associated to a given client contact."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="external_identifier" type="string" required="false" hint="Describes the external identifier for a given client contact record.">
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="contact_role_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Contact Role record; can also contain a list of comma-delimited Client Contact Role primary keys.">
		<cfargument name="first_name" type="string" required="false" hint="Describes the first name of the client contact.">
		<cfargument name="last_name" type="string" required="false" hint="Describes the last name of the client contact.">


		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Client Contact records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the ClientContacts table matching the where clause
			select	tbl.client_contact_uid,
					tbl.contact_hierarchyid.ToString(),
					tbl.user_uid,
					tbl.first_name,
					tbl.last_name,
					tbl.image_filename,
					tbl.oldid,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					tbl.external_identifier,
					tbl.email,
					tbl.phone,
					tbl.comments

					<cfif structKeyExists(arguments, 'property_uid')>
					,ccr.friendly_name as propertyContactRoleFriendlyName
					,p.friendly_name as propertyFriendlyName
					,pcc.client_company_uid
					,pcc.relationship_id
					,pcc.relationship_start
					,pcc.relationship_end
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid') and not structKeyExists(arguments, 'property_uid')>
					,ccr1.friendly_name as clientCompanyContactRoleFriendlyName
					,cc1.friendly_name as clientCompanyFriendlyName
					,cccc.client_company_uid
					,cccc.relationship_id
					</cfif>

			from	dbo.ClientContacts tbl (nolock)
					<cfif structKeyExists(arguments, 'property_uid')>
					join dbo.Properties_ClientContacts pcc (nolock)
						on tbl.client_contact_uid = pcc.client_contact_uid
					join dbo.ClientContactRoles ccr (nolock)
						on pcc.contact_role_uid = ccr.contact_role_uid
					join dbo.Properties p (nolock)
						on pcc.property_uid = p.property_uid
					join dbo.ClientCompanies cc (nolock)
						on pcc.client_company_uid = cc.client_company_uid
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid') and not structKeyExists(arguments, 'property_uid')>
					join dbo.ClientContacts_ClientCompanies cccc (nolock)
						on cccc.client_contact_uid = tbl.client_contact_uid
					join dbo.ClientCompanies cc1 (nolock)
						on cccc.client_company_uid = cc1.client_company_uid
					join dbo.ClientContactRoles ccr1 (nolock)
						on cccc.contact_role_uid = ccr1.contact_role_uid
					</cfif>

			where	1=1

					<cfif structKeyExists(arguments, 'property_uid')>
					<cfif structKeyExists(arguments, 'contact_role_uid')>
					and ccr.contact_role_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.contact_role_uid#" list="true" null="false"> )
					</cfif>
					<cfif arguments.selectMethod eq 'current'>
					and ccr.is_active = 1
					and cc.is_active = 1
					and p.is_active = 1
					and (

							-- Only retrieve active associations
							pcc.is_active = 1
							and ( pcc.relationship_end is null or pcc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and pcc.relationship_start <= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					</cfif>

					and pcc.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" list="true" null="false"> )
					<cfif structKeyExists(arguments, 'client_company_uid')>
					and pcc.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="true" null="false"> )
					</cfif>
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid') and not structKeyExists(arguments, 'property_uid')>
					<cfif structKeyExists(arguments, 'contact_role_uid')>
					and ccr1.contact_role_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.contact_role_uid#" list="true" null="false"> )
					</cfif>
					<cfif arguments.selectMethod eq 'current'>
					and tbl.is_active = 1
					and cccc.is_active = 1
					and ccr1.is_active = 1
					and cc1.is_active = 1
					and (

							-- Only retrieve active associations
							cccc.is_active = 1
							and ( cccc.relationship_end is null or cccc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
							and cccc.relationship_start <= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

						)
					</cfif>

					and (cc1.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="true" null="false"> )
or cc1.company_hierarchyid.IsDescendantOf((SELECT company_hierarchyid FROM ClientCompanies WHERE client_company_uid=<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)) = 1)
					</cfif>

					<cfif structKeyExists(arguments, 'client_contact_uid')>
					and tbl.client_contact_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_contact_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'user_uid')>
					and tbl.user_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.user_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'external_identifier')>
					and tbl.external_identifier in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.external_identifier#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'first_name') and structKeyExists(arguments, 'last_name')>
					and (
							1 = 0
							<cfif len(arguments.first_name)>
							or tbl.first_name like '%#arguments.first_name#%'
							</cfif>
							<cfif len(arguments.last_name)>
							or tbl.last_name like '%#arguments.last_name#%'
							</cfif>
					)
					</cfif>

			order
			by		<cfif structKeyExists(arguments, 'property_uid')>
					cc.friendly_name,
					</cfif>
					<cfif not structKeyExists(arguments, 'property_uid') and structKeyExists(arguments, 'client_company_uid')>
					cc1.friendly_name,
					</cfif>
					tbl.last_name,
					tbl.first_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getContactsByContext"
				access="public" returntype="query"
				hint="This method is used to retrieve / return a collection of contacts associated to companies or properties.">
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize local variables --->
		<cfset var qResult = structnew()>

		<!--- Retrieve a single / multiple Client Contact records. --->
		<cfquery name="qResult" datasource="#this.datasource#">
		declare @client_company_uid uniqueidentifier = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="false" null="#iif(isUniqueIdentifier(arguments.client_company_uid), 'false', 'true')#">
		declare @property_uid uniqueidentifier = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" list="false" null="#iif(isUniqueIdentifier(arguments.property_uid), 'false', 'true')#">
		declare @relationship_file_date datetime = <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">

		select	*
		from	(
				select	distinct
				<cfif Len(arguments.client_company_uid) neq 0>
						cc.company_hierarchyid,
						<cfelse>
						0 as company_hierarchyid,
						</cfif>
						1 as display_order,
				<cfif Len(arguments.client_company_uid) neq 0>
						cc.client_company_uid,
						</cfif>
						null as property_uid,
						cc.client_company_uid as context_uid,
						cc.friendly_name + ' - ' + ct.friendly_name as context_name,
						tbl.client_contact_uid,	tbl.contact_hierarchyid.ToString() as contact_hierarchyid, tbl.user_uid, tbl.first_name, tbl.last_name, tbl.image_filename, tbl.oldid, cccc.is_active, tbl.created_by, tbl.created_date, tbl.modified_by, tbl.modified_date, tbl.external_identifier, tbl.email, tbl.phone, tbl.comments,
						ccr.friendly_name as role_name
				from	dbo.ClientCompanies cc
							left outer join dbo.ClientContacts_ClientCompanies cccc
								on cccc.client_company_uid = cc.client_company_uid
								<cfif arguments.selectMethod eq 'current' or arguments.selectMethod eq 'active' >
									and cccc.is_active = 1
									and (
										-- Only retrieve active associations
										cccc.is_active = 1
										and ( cccc.relationship_end is null or cccc.relationship_end >= @relationship_file_date )
										and cccc.relationship_start <= @relationship_file_date
									)
								</cfif>
							left outer join dbo.ClientContacts tbl
								on tbl.client_contact_uid = cccc.client_contact_uid
								<cfif arguments.selectMethod eq 'current' or arguments.selectMethod eq 'active' >
									and tbl.is_active = 1
								</cfif>
							left outer join dbo.ClientContactRoles ccr
								on ccr.contact_role_uid = cccc.contact_role_uid
								<cfif arguments.selectMethod eq 'current' or arguments.selectMethod eq 'active' >
									and ccr.is_active = 1
								</cfif>
							left outer join dbo.CompanyTypes ct
								on ct.company_type_code = cc.company_type_code
				where	1 = 1
						and 1 = case when @client_company_uid is null then 0 else 1 end
						<cfif arguments.selectMethod eq 'current'  or arguments.selectMethod eq 'active' >
							and cc.is_active = 1
						</cfif>
						and (
							cc.client_company_uid = @client_company_uid
							or cc.company_hierarchyid.IsDescendantOf((SELECT company_hierarchyid FROM ClientCompanies WHERE client_company_uid=@client_company_uid)) = 1
						)

				union

				select	distinct
				<cfif Len(arguments.client_company_uid) neq 0>
						cc.company_hierarchyid,
						<cfelse>
						0 as company_hierarchyid,
				</cfif>
						2 as display_order,
				<cfif Len(arguments.client_company_uid) neq 0>
						cc.client_company_uid,
				</cfif>
						p.property_uid,
						p.property_uid as context_uid,
						p.friendly_name as context_name,
						tbl.client_contact_uid,	tbl.contact_hierarchyid.ToString() as contact_hierarchyid, tbl.user_uid, tbl.first_name, tbl.last_name, tbl.image_filename, tbl.oldid, pcon.is_active, tbl.created_by, tbl.created_date, tbl.modified_by, tbl.modified_date, tbl.external_identifier, tbl.email, tbl.phone, tbl.comments,
						ccr.friendly_name as role_name
				from	dbo.ClientCompanies cc
							join dbo.Properties_ClientCompanies pcc
								on pcc.client_company_uid = cc.client_company_uid
								<cfif arguments.selectMethod eq 'current'>
									
									and (
										-- Only retrieve active associations
										pcc.is_active = 1
										and ( pcc.relationship_end is null or pcc.relationship_end >= @relationship_file_date )
										and pcc.relationship_start <= @relationship_file_date
									)
								</cfif>
							join dbo.Properties p
								on p.property_uid = pcc.property_uid
								<cfif arguments.selectMethod eq 'current'  or arguments.selectMethod eq 'active' >
								and p.is_active = 1
								</cfif>
							left outer join dbo.Properties_ClientContacts pcon
								on pcon.property_uid = p.property_uid
								<cfif arguments.selectMethod eq 'current' or arguments.selectMethod eq 'active' >
									and pcon.is_active = 1
									and (
										-- Only retrieve active associations
										pcon.is_active = 1
										and ( pcon.relationship_end is null or pcon.relationship_end >= @relationship_file_date )
										and pcon.relationship_start <= @relationship_file_date
									)
								</cfif>
							left outer join dbo.ClientContacts tbl
								on tbl.client_contact_uid = pcon.client_contact_uid
								<cfif arguments.selectMethod eq 'current' or arguments.selectMethod eq 'active' >
									and tbl.is_active = 1
								</cfif>
							left outer join dbo.ClientContactRoles ccr
								on ccr.contact_role_uid = pcon.contact_role_uid
								<cfif arguments.selectMethod eq 'current' or arguments.selectMethod eq 'active' >
									and ccr.is_active = 1
								</cfif>
				where	1 = 1
						and pcc.is_active = 1
						<cfif arguments.selectMethod eq 'current' or arguments.selectMethod eq 'active' >
							and cc.is_active = 1
						</cfif>
						and p.property_uid = isnull(@property_uid, p.property_uid)
						and (
							cc.client_company_uid = isnull(@client_company_uid, cc.client_company_uid)
							--or cc.company_hierarchyid.IsDescendantOf((SELECT company_hierarchyid FROM ClientCompanies WHERE client_company_uid=@client_company_uid)) = 1
						)
		) der
		where	client_contact_uid is not null or context_uid = coalesce(@property_uid, @client_company_uid)
		order by
				company_hierarchyid,
				display_order,
				context_name,
				last_name,
				first_name
		</cfquery>

		<cfreturn scrubDuplicatePlaceholderRecords(qResult)>
	</cffunction>



	<cffunction name="getClientContactAssociations"
				hint="Retrieves all of the active client and property associations for a given contact."
				returntype="Query"
				access="public">

		<cfargument name="client_contact_uid" required="true" type="string" hint="List of the contacts to get associations for." />

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Client Contact records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
		select	roles.friendly_name as role_name,
				cc.friendly_name + ' - ' + ct.friendly_name as company_name,
				null as property_name,
				roles.contact_role_uid,
				cc.client_company_uid,
				null as property_uid,
				con.client_contact_uid,
				con.first_name,
				con.last_name
		from	dbo.ClientContacts con
					left outer join dbo.ClientContacts_ClientCompanies cccc on cccc.client_contact_uid = con.client_contact_uid and cccc.is_active = 1
					left outer join dbo.ClientCompanies cc on cc.client_company_uid = cccc.client_company_uid and cc.is_active = 1
					left outer join dbo.ClientContactRoles roles on roles.contact_role_uid = cccc.contact_role_uid
					left outer join dbo.CompanyTypes ct on ct.company_type_code = cc.company_type_code
		where	roles.contact_role_uid is not null
				and cc.is_active = 1
				and con.client_contact_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_contact_uid#" list="true" null="false"> )
		union
		select	roles.friendly_name as role_name,
				null as company_name,
				p.friendly_name as property_name,
				roles.contact_role_uid,
				pcc.client_company_uid,
				p.property_uid,
				con.client_contact_uid,
				con.first_name,
				con.last_name
		from	dbo.ClientContacts con
					left outer join dbo.Properties_ClientContacts pcc on pcc.client_contact_uid = con.client_contact_uid and pcc.is_active = 1
					left outer join dbo.Properties p on p.property_uid = pcc.property_uid and p.is_active = 1
					left outer join dbo.ClientContactRoles roles on roles.contact_role_uid = pcc.contact_role_uid
		where	roles.contact_role_uid is not null
				and p.is_active = 1
				and con.client_contact_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_contact_uid#" list="true" null="false"> )
		order by
				client_contact_uid
		</cfquery>
		<cfreturn local.qResult>
	</cffunction>

	<!---Describes the methods used to persist / modify Client Contact data. --->
	<cffunction name="saveNewClientContact"
				hint="This method is used to persist a new Client Contact record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="ClientContact" required="true" type="mce.e2.vo.ClientContact" hint="Describes the VO containing the Client Contact record that will be persisted."/>

		<!--- Persist the Client Contact data to the application database --->
		<cfset this.dataMgr.insertRecord("ClientContacts", arguments.ClientContact)/>

	</cffunction>
	
	<cffunction name="inactivateExistingClientContactAssociations"
				hint="This method is used to persist an existing Client Contact record to the application database."
				output="false"
				returnType="void"
				access="public">

		<cfargument name="client_contact_uid" required="true" type="string" hint="Uniqueidentifier of contact that must have it's associations deactivated." />
		<cfargument name="modified_date" required="true" type="date" hint="Date that records were deactivated." />
		<cfargument name="modified_by" required="true" type="string" hint="Person who ultimately deactivated records." />

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>
		
		<!--- Inactivate client company associations --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
		update	dbo.ClientContacts_ClientCompanies
		set		is_active = 0,
				relationship_end = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
				modified_date = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#">,
				modified_by = <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#arguments.modified_by#">
		where	client_contact_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_contact_uid#"> and
				is_active = 1
		</cfquery>

		<!--- Inactivate property associations --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
		update	dbo.Properties_ClientContacts
		set		is_active = 0,
				relationship_end = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#now()#">,
				modified_date = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#">,
				modified_by = <cfqueryparam cfsqltype="cf_sql_longvarchar" value="#arguments.modified_by#">
		where	client_contact_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_contact_uid#"> and
				is_active = 1
		</cfquery>

	</cffunction>

	<cffunction name="saveExistingClientContact"
				hint="This method is used to persist an existing Client Contact record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="ClientContact" required="true" type="mce.e2.vo.ClientContact" hint="Describes the VO containing the Client Contact record that will be persisted."/>

		<!--- Persist the Client Contact data to the application database --->
		<cfset this.dataMgr.updateRecord("ClientContacts", arguments.ClientContact)/>

	</cffunction>



	<!--- The getContactsByContext() method returns "placeholder" rows for contexts that don't have any contacts --->
	<!--- As currently implemented in SQL, there may sometimes be more than one placeholder per context; this method removes "duplcate" placeholders --->
	<!--- This is an ugly, lazy approach to the problem, but am leaving in for now because it works (NMW 4/25/2010) --->
	<cffunction name="scrubDuplicatePlaceholderRecords" returntype="query">
		<cfargument name="q" type="query" required="true">

		<!--- Local variables --->
		<cfset var qResult = arguments.q>
		<cfset var prop = "">
		<cfset var props = "">
		<cfset var i = 0>

		<!--- Create "props" list which remembers all properties for which there is a record for "no contact" --->
		<cfloop from="1" to="#qResult.recordCount#" index="i">
			<cfset prop = qResult.context_name[i]>
			<cfif qResult.client_contact_uid[i] eq "">
				<cfif not listFind(props, prop)>
					<cfset props = listAppend(props, prop)>
				</cfif>
			</cfif>
		</cfloop>

		<!--- Remove from "props" list for any record that is *not* for "no contact" --->
		<cfloop from="1" to="#qResult.recordCount#" index="i">
			<cfset prop = qResult.context_name[i]>
			<cfif qResult.client_contact_uid[i] neq "">
				<cfif listFind(props, prop)>
					<cfset props = listDeleteAt(props, listFind(props, prop))>
				</cfif>
			</cfif>
		</cfloop>

		<!--- Loop through one more time, marking all but the first record for each property with "DELETE-ME" --->
		<cfloop from="1" to="#qResult.recordCount#" index="i">
			<cfset prop = qResult.context_name[i]>

			<cfif qResult.client_contact_uid[i] eq "">
				<cfif listFind(props, prop)>
					<cfset props = listDeleteAt(props, listFind(props, prop))>
				<cfelse>
					<cfset QuerySetCell(qResult, "context_uid", "DELETE-ME", i)>
				</cfif>
			</cfif>
		</cfloop>

		<!--- Finally re-select from the query for all records that we didn't mark "DELETE-ME" --->
		<cfquery dbtype="query" name="qResult">
			SELECT * FROM qResult
			WHERE context_uid != 'DELETE-ME'
		</cfquery>

		<cfreturn qResult>
	</cffunction>

</cfcomponent>
