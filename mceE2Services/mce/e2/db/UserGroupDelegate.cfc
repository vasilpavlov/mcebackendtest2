<cfcomponent displayname="User Group Delegate" 
			 output="false" extends="BaseDatabaseDelegate"
			 hint="This component is used to manage all database interactions related to retrieving user group data from the application database.">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyUserGroupComponent" 
				returntype="mce.e2.vo.UserGroup"
				hint="This method is used to retrieve a user group object (vo) / coldFusion component.">

		<!--- Initialize any local variables --->
		<cfset var UserGroupComponent = createObject("component", "mce.e2.vo.UserGroup")/>

		<!--- Return the user group component --->
		<cfreturn UserGroupComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getUserGroupsAsArrayOfComponents"
				returntype="array"
				hint="This method is used to retrieve an array collection of User Group objects for all the User Groups in the application.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="user_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given user group record.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given User record; can also contain a list of comma-delimited User primary keys.">
		<cfargument name="selectMethod" type="string" required="true" default="singular" hint="Describes the method used to retrieve user group associations; inclusive = associated; exclusive = not associated; userAssocations = all.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Determine the query to execute based on the selectMethod --->
		<cfswitch expression="#arguments.selectMethod#">
		
			<cfcase value="userAssociations">

				<!--- Retrieve a comprehensive list of user associations --->
				<cfset local.qUserGroup = getUserAssociations(argumentCollection=arguments)>
		
			</cfcase>
			
			<cfdefaultcase>
			
				<!--- Retrieve the user groups based on the method arguments --->
				<cfset local.qUserGroup = getUserGroup(argumentCollection=arguments)>
		
			</cfdefaultcase>
			
		</cfswitch>

		<!--- Retrieve all user groups and build out the array of components --->
		<cfset local.returnResult = queryToVoArray(local.qUserGroup, "mce.e2.vo.UserGroup")>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult>

	</cffunction>

	<cffunction name="getUserGroupAsComponent"
				returntype="mce.e2.vo.UserGroup"
				hint="This method is used to retrieve a user group record from the application database, and populate its properties in a user group object.">

		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="user_group_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given user group record.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve all Users and build out the array of components --->
		<cfset local.qUserGroup = getUserGroup(argumentCollection=arguments)>

		<!--- Convert the return result to a query --->
		<cfset local.returnResult = queryToVo(local.qUserGroup, "mce.e2.vo.UserGroup")>

		<!--- Return the Vo Object --->
		<cfreturn local.returnResult>

	</cffunction>

	<!--- Define the methods used to interact with User Group data in the application database --->
	<cffunction name="getUserAssociations"
				returntype="query"
				hint="This method is used to retrieve a comprehensive collection of user / user group associations.">
			
		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given User record; can also contain a list of comma-delimited user primary keys.">
		
		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the User Groups in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">
		
			select	distinct
					-- Get core columns
					ug.client_company_uid,
					ug.user_group_uid,
					ug.friendly_name,
			
					-- Get audit columns
					ug.is_active,
					ug.created_date,
					ug.created_by,
					ug.modified_date,
					ug.modified_by,
					
					-- Select the meta columns
					1 as isAssociated,
					case
					
						when uug.user_group_uid = ug.user_group_uid then uug.relationship_id
						else null
						
					end relationship_id,
					-- Determine if a given group can be deleted
					0 as isDeletionPossible
										
			from	dbo.UserGroups ug
					join dbo.Users_UserGroups uug
						on ug.user_group_uid = uug.user_group_uid
					join dbo.Users u
						on uug.user_uid = u.user_uid
						and ug.client_company_uid = u.client_company_uid
						
			where	ug.is_active = 1
					--and u.is_active = 1
					and uug.is_active = 1
					and u.user_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.user_uid#">
							
			union 
		
			select	-- Get core columns
					ug.client_company_uid,
					ug.user_group_uid,
					ug.friendly_name,
			
					-- Get audit columns
					ug.is_active,
					ug.created_date,
					ug.created_by,
					ug.modified_date,
					ug.modified_by,
					
					-- Select the meta columns
					0 as isAssociated,
					null as relationship_id,
					-- Determine if a given group can be deleted
					1 as isDeletionPossible
										
			from	dbo.UserGroups ug
			where	ug.is_active = 1
					and ug.client_company_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.client_company_uid#">
					and ug.user_group_uid not in (
					
						-- Exclude the groups already associated to the current user
						select	user_group_uid
						from	dbo.Users_UserGroups uug1
								join dbo.Users u
									on uug1.user_uid = u.user_uid
						where	--u.is_active = 1 and
								uug1.is_active = 1 and
								u.user_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.user_uid#">
					
					)
			
			order
			by		ug.friendly_name,
					ug.modified_date
												
		</cfquery>				
				
		<!--- Return the query / result set --->		
		<cfreturn qResult>		
				
	</cffunction>
	
	<cffunction name="getUserGroup"
				returntype="query"
				hint="This method is used to retrieve data for a given User Group definition.">
			
		<!--- Define the arguments for the method --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given client company record; can also contain a list of comma-delimited client company primary keys.">
		<cfargument name="user_group_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given UserGroup record; can also contain a list of comma-delimited User primary keys.">
		<cfargument name="user_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given User record; can also contain a list of comma-delimited user primary keys.">
		<cfargument name="selectMethod" type="string" required="true" default="singular" hint="Describes the method used to retrieve user group associations; inclusive = associated; exclusive = not associated; all = all.">
		
		<!--- Initialize any local variables --->
		<cfset var qResult = ''>

		<!--- Build out the query to retrieve the User Groups in the application database --->
		<cfquery name="qResult" datasource="#this.datasource#">
		
			select	-- Get core columns
					ug.client_company_uid,
					ug.user_group_uid,
					ug.friendly_name,

					<cfswitch expression="#arguments.selectMethod#">	
					
						<cfcase value="inclusive">
					-- Select the meta columns
					1 as isAssociated,
					case
					
						when uug.user_group_uid = ug.user_group_uid then uug.relationship_id
						else null
						
					end relationship_id,
						</cfcase>
					
						<cfcase value="exclusive">
					-- Select the meta columns
					0 as isAssociated,
					null as relationship_id,
						</cfcase>
								
					</cfswitch>
					
					-- Determine if a given group can be deleted
				    case
						when
						    ( select count(*) 
							from dbo.UserGroups_PropertyCollections 
							where user_group_uid = ug.user_group_uid ) = 0
							and
							( select count(*)
							from dbo.UserGroups_UserRoles
							where user_group_uid = ug.user_group_uid ) = 0
						then 1
						else 0
					end isDeletionPossible,										

					-- Get audit columns
					ug.is_active,
					ug.created_date,
					ug.created_by,
					ug.modified_date,
					ug.modified_by											

			<cfswitch expression="#arguments.selectMethod#">

				<cfdefaultcase>
			from	dbo.UserGroups ug
			where	ug.is_active = 1

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and ug.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.client_company_uid#">)
					</cfif>				
				
					<cfif structKeyExists(arguments, 'user_group_uid')>
					and ug.user_group_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#arguments.user_group_uid#">)
					</cfif>				
				</cfdefaultcase>

				<cfcase value="inclusive">
			from	dbo.UserGroups ug
					join dbo.Users_UserGroups uug
						on ug.user_group_uid = uug.user_group_uid
					join dbo.Users u
						on uug.user_uid = u.user_uid
						and ug.client_company_uid = u.client_company_uid
						
			where	ug.is_active = 1
					--and u.is_active = 1
					and uug.is_active = 1
					and u.user_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.user_uid#">
				</cfcase>

				<cfcase value="exclusive">
			from	dbo.UserGroups ug
			where	ug.is_active = 1
					and ug.client_company_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.client_company_uid#">
					and ug.user_group_uid not in (
					
						-- Exclude the groups already associated to the current user
						select	uug1.user_group_uid
						from	dbo.Users_UserGroups uug1
								join dbo.Users u
									on uug1.user_uid = u.user_uid
						where	u.is_active = 1 and
								uug1.is_active = 1 and
								u.user_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" list="false" value="#arguments.user_uid#">
					
					)
				</cfcase>
				
			</cfswitch>

			order
			by		ug.friendly_name,
					ug.modified_date
												
		</cfquery>				
				
		<!--- Return the query / result set --->		
		<cfreturn qResult>		
				
	</cffunction>

	<!--- Define the methods used to persist data to the application database --->
	<cffunction name="saveNewUserGroup" 
				access="public" returntype="void"
				hint="This method is used to persist a new UserGroup record to the application database.">
		<cfargument name="UserGroup" type="mce.e2.vo.UserGroup" hint="Describes the VO containing the UserGroup record that will be persisted.">

		<!--- Persist the User Group data to the application database --->
		<cfset this.dataMgr.insertRecord("UserGroups", arguments.UserGroup)>
				
	</cffunction>	
	
	<cffunction name="saveExistingUserGroup" 
				access="public" returntype="void"
				hint="This method is used to persist changes for an existing UserGroup record to the application database.">
		<cfargument name="UserGroup" type="mce.e2.vo.UserGroup" hint="Describes the VO containing the UserGroup record that will be persisted.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Persist the User Group data to the application database --->
		<cfset local.newUid = this.dataMgr.updateRecord("UserGroups", arguments.UserGroup)>
				
	</cffunction>	
	
	<cffunction name="saveUserToUserGroup" 
				access="public" returntype="void"
				hint="This method is used to persist a new user / user group association to the application database.">
		<cfargument name="assocObject" type="struct" hint="Describes the VO containing the user / user group association record that will be persisted.">

		<!--- Persist the User Group data to the application database --->
		<cfset this.dataMgr.insertRecord("Users_UserGroups", arguments.assocObject)>
				
	</cffunction>		
		
	<cffunction name="removeUserFromUserGroup" 
				access="public" returntype="void"
				hint="This method is used to remove / delete a new user / user group association to the application database.">
		<cfargument name="assocObject" type="struct" hint="Describes the VO containing the user / user group association record that will be removed.">

		<!--- Persist the User Group data to the application database --->
		<cfset this.dataMgr.updateRecord("Users_UserGroups", arguments.assocObject)>
				
	</cffunction>			

	<cffunction name="saveUserRoleToUserGroup" 
				access="public" returntype="void"
				hint="This method is used to persist a new user role / user group association to the application database.">
		<cfargument name="assocObject" type="struct" hint="Describes the VO containing the user role / user group association record that will be persisted.">

		<!--- Persist the User Group data to the application database --->
		<cfset this.dataMgr.insertRecord("UserGroups_UserRoles", arguments.assocObject)>
				
	</cffunction>		
		
	<cffunction name="modifyUserRoleToUserGroupAssociation" 
				access="public" returntype="void"
				hint="This method is used to persist changes to an existing user role / user group association to the application database.">
		<cfargument name="assocObject" type="struct" hint="Describes the VO containing the user role / user group association record that will be persisted.">

		<!--- Persist the User Group data to the application database --->
		<cfset this.dataMgr.updateRecord("UserGroups_UserRoles", arguments.assocObject)>
				
	</cffunction>			
		
	<cffunction name="removeUserRoleFromUserGroup" 
				access="public" returntype="void"
				hint="This method is used to remove / delete a new user role / user group association to the application database.">
		<cfargument name="assocObject" type="struct" hint="Describes the VO containing the user role / user group association record that will be removed.">

		<!--- Persist the User Group data to the application database --->
		<cfset this.dataMgr.updateRecord("UserGroups_UserRoles", arguments.assocObject)>
				
	</cffunction>
		
	<cffunction name="savePropertyCollectionToUserGroup" 
				access="public" returntype="void"
				hint="This method is used to persist a new property collection / user group association to the application database.">
		<cfargument name="assocObject" type="struct" hint="Describes the VO containing the property collection / user group association record that will be persisted.">

		<!--- Persist the user group / property collection data to the application database --->
		<cfset this.dataMgr.insertRecord("UserGroups_PropertyCollections", arguments.assocObject)>
				
	</cffunction>		
		
	<cffunction name="removePropertyCollectionFromUserGroup" 
				access="public" returntype="void"
				hint="This method is used to remove / delete a property collection / user group association to the application database.">
		<cfargument name="assocObject" type="struct" hint="Describes the VO containing the property collection / user group association record that will be removed.">

		<!--- Persist the user group / property collection data to the application database --->
		<cfset this.dataMgr.updateRecord("UserGroups_PropertyCollections", arguments.assocObject)>
				
	</cffunction>		
		
</cfcomponent>		