
<!---

  Template Name:  EnergyProvider
     Base Table:  EnergyProviders	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Friday, March 20, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Friday, March 20, 2009 - Template Created.		

--->
		
<cfcomponent displayname="EnergyProviderDelegate"
				hint="This CFC manages all data access interactions for the EnergyProviders table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEnergyProviderComponent"
				hint="This method is used to retrieve a Energy Provider object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EnergyProvider"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EnergyProvider")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEnergyProvidersAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Provider objects for all the Energy Providers in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_provider_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy provider."/>
		<cfargument name="provider_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing an associated energy provider type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Providers and build out the array of components --->
		<cfset local.qResult = getEnergyProvider(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyProvider")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyProviderAsComponent"
				hint="This method is used to retrieve a single instance of a / an Energy Provider value object representing a single Energy Provider record."
				output="false"
				returnType="mce.e2.vo.EnergyProvider"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_provider_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy provider."/>
		<cfargument name="provider_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing an associated energy provider type."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Energy Provider and build out the component --->
		<cfset local.qResult = getEnergyProvider(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EnergyProvider")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyProvider"
				hint="This method is used to retrieve single / multiple records from the EnergyProviders table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_provider_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy provider."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name for an energy provider."/>
		<cfargument name="provider_type_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing an associated energy provider type."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Provider records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyProviders table matching the where clause
			select	tbl.energy_provider_uid,
					tbl.friendly_name,
					tbl.provider_type_uid,
					tbl.oldid,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					ept.friendly_name as providerTypeFriendlyName

			from	dbo.EnergyProviders tbl (nolock) inner join
					dbo.EnergyProviderTypes ept (nolock)
						on tbl.provider_type_uid = ept.provider_type_uid
			where	1=1
					and tbl.is_active = 1
					
					<cfif structKeyExists(arguments, 'energy_provider_uid')>
					and tbl.energy_provider_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_provider_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'provider_type_uid')>
					and tbl.provider_type_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.provider_type_uid#" null="false" list="true"> ) 
					</cfif>
			order by tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Energy Provider data. --->	
	<cffunction name="saveNewEnergyProvider"
				hint="This method is used to persist a new Energy Provider record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyProvider" required="true" type="mce.e2.vo.EnergyProvider" hint="Describes the VO containing the Energy Provider record that will be persisted."/>

		<!--- Persist the Energy Provider data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyProviders", arguments.EnergyProvider)/>

	</cffunction>

	<cffunction name="saveExistingEnergyProvider"
				hint="This method is used to persist an existing Energy Provider record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyProvider" required="true" type="mce.e2.vo.EnergyProvider" hint="Describes the VO containing the Energy Provider record that will be persisted."/>

		<!--- Persist the Energy Provider data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyProviders", arguments.EnergyProvider)/>

	</cffunction>

</cfcomponent>
