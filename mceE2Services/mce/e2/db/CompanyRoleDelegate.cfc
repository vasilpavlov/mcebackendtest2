
<!---

  Template Name:  CompanyRoleDelegate
     Base Table:  CompanyRoles	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, April 01, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, April 01, 2009 - Template Created.		

--->
		
<cfcomponent displayname="CompanyRoleDelegate"
				hint="This CFC manages all data access interactions for the CompanyRoles table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyCompanyRoleComponent"
				hint="This method is used to retrieve a Company Role object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.CompanyRole"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.CompanyRole")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getCompanyRolesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Company Role objects for all the Company Roles in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="company_role_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given company role record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Company Roles and build out the array of components --->
		<cfset local.qResult = getCompanyRole(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.CompanyRole")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getCompanyRoleAsComponent"
				hint="This method is used to retrieve a single instance of a / an Company Role value object representing a single Company Role record."
				output="false"
				returnType="mce.e2.vo.CompanyRole"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="company_role_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given company role record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Company Role and build out the component --->
		<cfset local.qResult = getCompanyRole(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.CompanyRole")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getCompanyRole"
				hint="This method is used to retrieve single / multiple records from the CompanyRoles table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="company_role_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given company role record."/>
		<cfargument name="client_company_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given client company record."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing friendly name for a given company role."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Company Role records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the CompanyRoles table matching the where clause
			select	tbl.company_role_uid,
					tbl.friendly_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.CompanyRoles tbl (nolock)
<!--- 					<cfif structKeyExists(arguments, 'client_company_uid')>
					join dbo.ClientCompanies_CompanyRoles cccr (nolock)
						on tbl.company_role_uid = cccr.company_role_uid
					</cfif> --->
			
			where	1=1
					and tbl.is_active = 1
<!--- 					<cfif structKeyExists(arguments, 'client_company_uid')>
					and cccr.is_active = 1
					</cfif> --->
					
<!--- 					<cfif structKeyExists(arguments, 'client_company_uid')>
					and cccr.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" null="false" list="true"> ) 
					</cfif> --->

					<cfif structKeyExists(arguments, 'company_role_uid')>
					and tbl.company_role_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.company_role_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>
	
			order
			by		tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Company Role data. --->	
	<cffunction name="saveNewCompanyRole"
				hint="This method is used to persist a new Company Role record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="CompanyRole" required="true" type="mce.e2.vo.CompanyRole" hint="Describes the VO containing the Company Role record that will be persisted."/>

		<!--- Persist the Company Role data to the application database --->
		<cfset this.dataMgr.insertRecord("CompanyRoles", arguments.CompanyRole)/>

	</cffunction>

	<cffunction name="saveExistingCompanyRole"
				hint="This method is used to persist an existing Company Role record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="CompanyRole" required="true" type="mce.e2.vo.CompanyRole" hint="Describes the VO containing the Company Role record that will be persisted."/>

		<!--- Persist the Company Role data to the application database --->
		<cfset this.dataMgr.updateRecord("CompanyRoles", arguments.CompanyRole)/>

	</cffunction>

</cfcomponent>
