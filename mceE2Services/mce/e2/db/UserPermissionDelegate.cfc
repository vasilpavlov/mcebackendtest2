
<!---

  Template Name:  UserPermissionDelegate
     Base Table:  UserPermissions

abe@twintechs.com

       Author:  Abraham Lloyd
 Date Created:  Wednesday, June 03, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Wednesday, June 03, 2009 - Template Created.

--->

<cfcomponent displayname="UserReportDelegate"
				hint="This CFC manages all data access interactions for the UserPermissions table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyUserPermissionComponent"
				hint="This method is used to retrieve a UserPermission object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.UserPermissions"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.UserPermissions")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getUserPermissionsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of UserPermission objects for all the User Permissions in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="user_permission_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given user permission."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all User Permissions and build out the array of components --->
		<cfset local.qResult = getUserPermission(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.UserPermissions")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getUserPermissionAsComponent"
				hint="This method is used to retrieve a single instance of a / an UserPermission value object representing a single UserPermission record."
				output="false"
				returnType="mce.e2.vo.UserPermissions"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="user_permission_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given user permission."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single UserPermission and build out the component --->
		<cfset local.qResult = getUserPermission(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.UserPermissions")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getUserPermission"
				hint="This method is used to retrieve single / multiple records from the UserPermissions table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="user_permission_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given user permission."/>
		<cfargument name="user_permission_code" required="false" type="string" hint="Describes the internal lookup code for a given user permission."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the customer facing / friendly name for a given user permission."/>
		<cfargument name="is_active" required="false" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
		<cfargument name="created_by" required="false" type="string" hint="Describes the system user that created a  given record."/>
		<cfargument name="created_date" required="false" type="date" hint="Describes the date that a given record was first created."/>
		<cfargument name="modified_by" required="false" type="string" hint="Describes the system user that last modified a given record."/>
		<cfargument name="modified_date" required="false" type="date" hint="Describes the date that a given record was last modified."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple UserPermission records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the UserPermissions table matching the where clause
			select	tbl.user_permission_uid,
					tbl.user_permission_code,
					tbl.friendly_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.UserPermissions tbl (nolock)
			where	1=1

					<cfif structKeyExists(arguments, 'user_permission_uid')>
					and tbl.user_permission_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.user_permission_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'user_permission_code')>
					and tbl.user_permission_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.user_permission_code#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'is_active')>
					and tbl.is_active in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_active#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'created_by')>
					and tbl.created_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.created_by#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'created_date')>
					and tbl.created_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.created_date#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_by')>
					and tbl.modified_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.modified_by#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'modified_date')>
					and tbl.modified_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#" list="true" null="false"> )
					</cfif>

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<!---Describes the methods used to persist / modify UserPermission data. --->
	<cffunction name="saveNewUserPermission"
				hint="This method is used to persist a new UserPermission record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="UserPermission" required="true" type="mce.e2.vo.UserPermission" hint="Describes the VO containing the UserPermission record that will be persisted."/>

		<!--- Persist the UserPermission data to the application database --->
		<cfset this.dataMgr.insertRecord("UserPermissions", arguments.UserPermission)/>

	</cffunction>

	<cffunction name="saveExistingUserPermission"
				hint="This method is used to persist an existing UserPermission record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="UserPermission" required="true" type="mce.e2.vo.UserPermission" hint="Describes the VO containing the UserPermission record that will be persisted."/>

		<!--- Persist the UserPermission data to the application database --->
		<cfset this.dataMgr.updateRecord("UserPermissions", arguments.UserPermission)/>

	</cffunction>

</cfcomponent>
