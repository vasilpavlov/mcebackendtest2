
<!---

  Template Name:  PropertyClientCompanyAssociationDelegate
     Base Table:  Properties_ClientCompanies	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Thursday, April 02, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Thursday, April 02, 2009 - Template Created.		

--->
		
<cfcomponent displayname="PropertyClientCompanyAssociationDelegate"
				hint="This CFC manages all data access interactions for the Properties_ClientCompanies table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyPropertyClientCompanyAssociationComponent"
				hint="This method is used to retrieve a Property Client Company Association object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.PropertyClientCompanyAssociation"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.PropertyClientCompanyAssociation")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getPropertyClientCompanyAssociationsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Property Client Company Association objects for all the Property Client Company Associations in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="relationship_id" required="false" type="numeric" hint="Describes the primary key / unique identifier representing the relationship between a property / client company / company role."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing an associated property."/>
		<cfargument name="client_company_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing an associated client company."/>
		<cfargument name="company_role_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing an associated company role."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Property Client Company Associations and build out the array of components --->
		<cfset local.qResult = getPropertyClientCompanyAssociation(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.PropertyClientCompanyAssociation")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getPropertyClientCompanyAssociationAsComponent"
				hint="This method is used to retrieve a single instance of a / an Property Client Company Association value object representing a single Property Client Company Association record."
				output="false"
				returnType="mce.e2.vo.PropertyClientCompanyAssociation"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="relationship_id" required="false" type="numeric" hint="Describes the primary key / unique identifier representing the relationship between a property / client company / company role."/>
		<cfargument name="property_uid" required="true" type="string" hint="Describes the primary key / unique identifier representing an associated property."/>
		<cfargument name="client_company_uid" required="true" type="string" hint="Describes the primary key / unique identifier representing an associated client company."/>
		<cfargument name="company_role_uid" required="true" type="string" hint="Describes the primary key / unique identifier representing an associated company role."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Property Client Company Association and build out the component --->
		<cfset local.qResult = getPropertyClientCompanyAssociation(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.PropertyClientCompanyAssociation")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getPropertyClientCompanyAssociation"
				hint="This method is used to retrieve single / multiple records from the Properties_ClientCompanies table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="relationship_id" required="false" type="numeric" hint="Describes the primary key / unique identifier representing the relationship between a property / client company / company role."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing an associated property."/>
		<cfargument name="client_company_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing an associated client company."/>
		<cfargument name="company_role_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing an associated company role."/>
		<cfargument name="relationship_filter_date" required="true" type="date" default="#createOdbcDateTime(now())#" hint="Describes the date used to filter active records."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Property Client Company Association records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the Properties_ClientCompanies table matching the where clause
			select	tbl.relationship_id,
					tbl.property_uid,
					tbl.client_company_uid,
					tbl.company_role_uid,
					tbl.relationship_start,
					tbl.relationship_end,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					
					cc.friendly_name as clientCompanyFriendlyName,
					ct.friendly_name as companyTypeFriendlyName,
					cr.friendly_name as companyRoleFriendlyName,
					p.friendly_name as propertyFriendlyName

			from	dbo.ClientCompanies cc (nolock)
					join dbo.Properties_ClientCompanies tbl (nolock)
						on cc.client_company_uid = tbl.client_company_uid
						and tbl.company_role_uid = tbl.company_role_uid
					join dbo.CompanyRoles cr (nolock)
						on tbl.company_role_uid = cr.company_role_uid 
						and tbl.company_role_uid = cr.company_role_uid
					join dbo.Properties p (nolock)
						on tbl.property_uid = p.property_uid
					join dbo.CompanyTypes ct (nolock)
						on cc.company_type_code = ct.company_type_code		
			where	1=1
					and tbl.is_active = 1
					<cfif arguments.selectMethod eq 'current'>	
					and cc.is_active = 1
					and cr.is_active = 1 and
					p.is_active = 1
					and (

						-- Only retrieve active associations
						tbl.is_active = 1
						and ( tbl.relationship_end is null or tbl.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
						and tbl.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
				
					)	
					</cfif>
					
					<cfif structKeyExists(arguments, 'relationship_id')>
					and tbl.relationship_id in ( <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.relationship_id#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'property_uid')>
					and tbl.property_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'client_company_uid')>
					and tbl.client_company_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'company_role_uid')>
					and tbl.company_role_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.company_role_uid#" null="false" list="true"> ) 
					</cfif>

		order
		by			cc.friendly_name,
					cc.modified_date

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Property Client Company Association data. --->	
	<cffunction name="saveNewPropertyClientCompanyAssociation"
				hint="This method is used to persist a new Property Client Company Association record to the application database."
				output="false"
				returnType="numeric"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="PropertyClientCompanyAssociation" required="true" type="mce.e2.vo.PropertyClientCompanyAssociation" hint="Describes the VO containing the Property Client Company Association record that will be persisted."/>

		<!--- Persist the Property Client Company Association data to the application database --->
		<cfreturn this.dataMgr.insertRecord("Properties_ClientCompanies", arguments.PropertyClientCompanyAssociation)/>

	</cffunction>

	<cffunction name="saveExistingPropertyClientCompanyAssociation"
				hint="This method is used to persist an existing Property Client Company Association record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="PropertyClientCompanyAssociation" required="true" type="mce.e2.vo.PropertyClientCompanyAssociation" hint="Describes the VO containing the Property Client Company Association record that will be persisted."/>

		<!--- Persist the Property Client Company Association data to the application database --->
		<cfset this.dataMgr.updateRecord("Properties_ClientCompanies", arguments.PropertyClientCompanyAssociation)/>

	</cffunction>

</cfcomponent>