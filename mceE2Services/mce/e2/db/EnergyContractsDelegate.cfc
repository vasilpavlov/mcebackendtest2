<cfcomponent displayname="EnergyContractsDelegate"
				hint="This CFC manages all data access interactions for the EnergyContracts table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEnergyContractsComponent"
				hint="This method is used to retrieve a Energy Contract object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EnergyContracts"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EnergyContracts")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEnergyContractsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Contract objects for all the Energy Contracts in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client record; can also contain a list of comma-delimited Client primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Contracts and build out the array of components --->
		<cfset local.qResult = getEnergyContracts(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyContracts")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyContractsAsArrayOfComponentsUnique"
				hint="This method is used to retrieve an array collection of Energy Contract objects for all the Energy Contracts in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client record; can also contain a list of comma-delimited Client primary keys.">
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated energy account."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="mostRecentFirst" type="boolean" required="false" default="false" hint="If true, orders by contract date in descending order">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Contracts and build out the array of components --->
		<cfset local.qResult = getEnergyContractsUnique(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyContracts")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyContractAssociationsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Contract objects for all the Energy Contracts in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_contract_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy contract."/>
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client record; can also contain a list of comma-delimited Client primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Contracts and build out the array of components --->
		<cfset local.qResult = getEnergyContractAssociations(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyAccountSummary")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyContractAsComponent"
				hint="This method is used to retrieve a single instance of a / an Energy Contract value object representing a single Energy Contract record."
				output="false"
				returnType="mce.e2.vo.EnergyContracts"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client record; can also contain a list of comma-delimited Client primary keys.">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Energy Contract and build out the component --->
		<cfset local.qResult = getEnergyContracts(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EnergyContracts")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyContracts"
				hint="This method is used to retrieve single / multiple records from the EnergyContracts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Contract records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyContracts table matching the where clause
			SELECT distinct tbl.energy_contract_uid,
			tbl.friendly_name,
			tbl.contract_start,
			tbl.contract_end,
			tbl.document_set_uid,
			tbl.energy_provider_uid,
			(SELECT prov.friendly_name FROM EnergyProviders prov WHERE prov.energy_provider_uid = tbl.energy_provider_uid) as energyProviderFriendlyName,
			tbl.energy_type_uid,
			(SELECT types.friendly_name FROM EnergyTypes types WHERE types.energy_type_uid = tbl.energy_type_uid) as energyTypeFriendlyName,
			ecea.energy_account_uid,
			ecea.input_set_uid,
			ea.friendly_name AS EnergyAccountFriendlyName,
			ea.property_uid,
			c.client_company_uid,
			c.friendly_name AS ClientFriendlyName,
			tbl.is_active

			FROM dbo.EnergyContracts tbl (nolock)
				JOIN dbo.EnergyContracts_EnergyAccounts ecea (nolock)
					ON tbl.energy_contract_uid = ecea.energy_contract_uid

				JOIN dbo.EnergyAccounts ea (nolock)
					ON ecea.energy_account_uid = ea.energy_account_uid

				JOIN dbo.Properties_ClientCompanies pcc (nolock)
					ON ea.property_uid = pcc.property_uid
					

				JOIN dbo.ClientCompanies c (nolock)
					ON pcc.client_company_uid = c.client_company_uid


			WHERE 1=1
				and
				tbl.is_active = 1
				and pcc.is_active = 1
				and pcc.relationship_end is null
	<!---			and ( tbl.contract_end is null or tbl.contract_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )

				and tbl.contract_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
 --->

				<cfif structKeyExists(arguments, 'energy_account_uid') and len(arguments.energy_account_uid)>
				and	ecea.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" list="true" null="false">
				</cfif>

				<cfif structKeyExists(arguments, 'property_uid') and len(arguments.property_uid)>
				and	ea.property_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" list="true" null="false">
				</cfif>

				<cfif structKeyExists(arguments, 'client_company_uid') and len(arguments.client_company_uid)>
				and	c.client_company_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="true" null="false">
				</cfif>

			ORDER BY
				tbl.contract_end desc,
				tbl.friendly_name,
				ea.friendly_name
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getEnergyContractAssociations"
				hint="This method is used to retrieve single / multiple records from the EnergyContracts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_contract_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given energy contract."/>
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Contract records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyContracts table matching the where clause
			SELECT distinct
			ecea.relationship_id,
			tbl.energy_contract_uid,
			ea.friendly_name,
			tbl.contract_start,
			tbl.contract_end,
			tbl.document_set_uid,
			tbl.energy_provider_uid,
			ecea.energy_account_uid,
			ecea.input_set_uid,
			ea.property_uid,
			p.friendly_name as propertyFriendlyName,
			ea.energy_type_uid,
			c.client_company_uid,
			c.friendly_name AS ClientFriendlyName,
			et.friendly_name as resourceFriendlyName,
			eu.friendly_name as energyUnitFriendlyName,
			1 as isSelected

			FROM dbo.EnergyContracts tbl (nolock)
				JOIN dbo.EnergyContracts_EnergyAccounts ecea (nolock)
					ON tbl.energy_contract_uid = ecea.energy_contract_uid

				JOIN dbo.EnergyAccounts ea (nolock)
					ON ecea.energy_account_uid = ea.energy_account_uid

				JOIN dbo.EnergyTypes et (nolock)
					ON et.energy_type_uid = ea.energy_type_uid

				JOIN dbo.Properties_ClientCompanies pcc (nolock)
					ON ea.property_uid = pcc.property_uid
					

				JOIN dbo.ClientCompanies c (nolock)
					ON pcc.client_company_uid = c.client_company_uid

				JOIN dbo.Properties p (nolock)
					ON ea.property_uid = p.property_uid
					
				JOIN dbo.EnergyUnits eu (nolock)
					on ea.energy_unit_uid = eu.energy_unit_uid


			WHERE 1=1
				and pcc.is_active = 1
				--and	tbl.is_active = 1
				and ecea.is_active = 1
				and pcc.relationship_end is null
				and pcc.company_role_uid = (Select company_role_uid FROM CompanyRoles WHERE friendly_name = 'Client')
				<cfif structKeyExists(arguments, 'energy_contract_uid') and arguments.energy_contract_uid neq "">
				and	tbl.energy_contract_uid IN (<cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_contract_uid#" list="true" null="false">)
				</cfif>

				<cfif structKeyExists(arguments, 'energy_account_uid')>
				and	ecea.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" list="true" null="false">
				</cfif>

				<cfif structKeyExists(arguments, 'property_uid')>
				and	ea.property_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" list="true" null="false">
				</cfif>

				<cfif structKeyExists(arguments, 'client_company_uid')>
				and	c.client_company_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="true" null="false">
				and	pcc.client_company_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="true" null="false">
				</cfif>

			ORDER BY
				ea.friendly_name
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getEnergyContractsUnique"
				hint="This method is used to retrieve single / multiple records from the EnergyContracts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="client_company_uid" type="string" required="false" hint="Describes the primary key / unique identifier of a given Client Company record; can also contain a list of comma-delimited Client Company primary keys.">
		<cfargument name="property_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated property."/>
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given associated energy account."/>
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="mostRecentFirst" type="boolean" required="false" default="false" hint="If true, orders by contract date in descending order">
		<cfargument name="includeRelatedEnergyAccounts" type="boolean" required="false" default="false">
		<cfargument name="rateModelInputLookupCodeForPrice" type="string" required="false">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>
		<cfset var qResult = "">
		<cfset var qRelatedAccounts = "">

		<!--- Retrieve a single / multiple Energy Contract records. --->
		<cfquery name="qResult" datasource="#this.datasource#">

			-- select records from the EnergyContracts table matching the where clause
			SELECT distinct tbl.energy_contract_uid,
			tbl.contract_id,
			tbl.friendly_name,
			tbl.contract_start,
			tbl.contract_end,
			tbl.document_set_uid,
			tbl.energy_provider_uid,
			(SELECT prov.friendly_name FROM EnergyProviders prov WHERE prov.energy_provider_uid = tbl.energy_provider_uid) as energyProviderFriendlyName,
			c.client_company_uid,
			c.friendly_name AS ClientFriendlyName,
			tbl.is_active,
			tbl.energy_type_uid,
			(SELECT types.friendly_name FROM EnergyTypes types WHERE types.energy_type_uid = tbl.energy_type_uid) as energyTypeFriendlyName,
			(select count(1) from dbo.StoredDocuments where document_set_uid = tbl.document_set_uid and is_active = 1) as documentCount
			
			<!--- Contract price --->
			<cfif isDefined("arguments.rateModelInputLookupCodeForPrice") and len(arguments.energy_account_uid) gt 0>
			, dbo.getEnergyContractRateModelInputValue(tbl.energy_contract_uid, '#listFirst(arguments.energy_account_uid)#', '#arguments.rateModelInputLookupCodeForPrice#') as contractPrice
			</cfif>

			FROM dbo.EnergyContracts tbl (nolock)
				JOIN dbo.EnergyContracts_EnergyAccounts ecea (nolock)
					ON tbl.energy_contract_uid = ecea.energy_contract_uid

				JOIN dbo.EnergyAccounts ea (nolock)
					ON ecea.energy_account_uid = ea.energy_account_uid

				JOIN dbo.Properties_ClientCompanies pcc (nolock)
					ON ea.property_uid = pcc.property_uid

				JOIN dbo.ClientCompanies c (nolock)
					ON pcc.client_company_uid = c.client_company_uid
					
				JOIN dbo.Properties p (nolock)
					ON p.property_uid = pcc.property_uid

			WHERE 1=1
				and pcc.is_active = 1
				and ecea.is_active = 1
				and p.is_active = 1
				and pcc.is_active = 1
				and pcc.relationship_end is null
				<cfif arguments.selectMethod eq "current">
				and	tbl.is_active = 1
				<cfelseif arguments.selectMethod eq "active">
				and	tbl.is_active = 1
				</cfif>
				<cfif arguments.selectMethod eq "all">
				and	tbl.is_active IN(1,0)
				</cfif>

				<cfif structKeyExists(arguments, 'property_uid') and len(arguments.property_uid)>
				and pcc.company_role_uid = (Select company_role_uid FROM CompanyRoles WHERE friendly_name = 'Client')
				and	ea.property_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.property_uid#" list="true" null="false">
				and ( pcc.relationship_end is null or pcc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
				and pcc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
				</cfif>

				<cfif structKeyExists(arguments, 'client_company_uid') and len(arguments.client_company_uid)>
				and	c.client_company_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="true" null="false">
				and	pcc.client_company_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.client_company_uid#" list="true" null="false">
				</cfif>

				<cfif structKeyExists(arguments, 'energy_account_uid') and len(arguments.energy_account_uid)>
				and ea.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" list="true" null="false">
				</cfif>
			ORDER BY
					tbl.contract_end desc,
					tbl.friendly_name

		</cfquery>
		
		<cfif arguments.includeRelatedEnergyAccounts>
			<cfset qRelatedAccounts = getEnergyContractAssociations(energy_contract_uid = ValueList(qResult.energy_contract_uid))> 
			<cfset qRelatedAccounts = queryRollup(qRelatedAccounts, "energy_contract_uid", "energy_account_uid")>
			
			<cfquery dbtype="query" name="qResult">
				SELECT qResult.*, qRelatedAccounts.energy_account_uid as relatedEnergyAccountUids
				FROM qRelatedAccounts, qResult
				WHERE qRelatedAccounts.energy_contract_uid = qResult.energy_contract_uid
				ORDER BY qResult.contract_end desc
			</cfquery>
		</cfif>

		<!--- Return the result set based on the query parameters --->
		<cfreturn qResult>

	</cffunction>
	

	

	<cffunction name="getRateModelInputSets"
				hint="This method is used to retrieve single / multiple records from the EnergyContracts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="set_type_code" required="false" type="string" hint="Describes the lookup code used to identify a rate model input set."/>
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Contract records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
		select	rmis.input_set_uid,
				rmis.friendly_name,
				rmis.set_type_code,
				rmis.is_active,
				rmis.created_by,
				rmis.created_date,
				rmis.modified_by,
				rmis.modified_date
		from	dbo.RateModelInputSets rmis
		where	1 = 1
				<cfif structKeyExists(arguments, 'set_type_code')>
				and	rmis.set_type_code in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.set_type_code#" list="true" null="false"> )
				</cfif>
				<cfif structKeyExists(arguments, 'selectMethod')>
				and rmis.is_active = 1
				</cfif>
		</cfquery>

		<cfreturn local.qResult>
	</cffunction>

	<!---Describes the methods used to persist / modify Energy Contract data. --->
	<cffunction name="saveNewEnergyContracts"
				hint="This method is used to persist a new Energy Contract record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyContracts" required="true" type="mce.e2.vo.EnergyContracts" hint="Describes the VO containing the Energy Contract record that will be persisted."/>

		<!--- Persist the Energy Contract data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyContracts", arguments.EnergyContracts)/>

	</cffunction>

	<cffunction name="saveExistingEnergyContracts"
				hint="This method is used to persist an existing Energy Contract record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyContracts" required="true" type="mce.e2.vo.EnergyContracts" hint="Describes the VO containing the Energy Contract record that will be persisted."/>

		<!--- Persist the Energy Contract data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyContracts", arguments.EnergyContracts)/>

	</cffunction>

	<!---Describes the methods used to persist / modify Energy Contract data. --->
	<cffunction name="saveNewEnergyContractAccountAssociation"
				hint="This method is used to persist a new energy contract account association record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyContractAccountAssociation" required="true" type="mce.e2.vo.EnergyContractAccountAssociation" hint="Describes the VO containing the energy contract account association record that will be persisted."/>

		<!--- Persist the Energy Contract data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyContracts_EnergyAccounts", arguments.EnergyContractAccountAssociation)/>

	</cffunction>

	<cffunction name="saveExistingEnergyContractAccountAssociation"
				hint="This method is used to persist an existing energy contract account association record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyContractAccountAssociation" required="true" type="mce.e2.vo.EnergyContractAccountAssociation" hint="Describes the VO containing the energy contract account association record that will be persisted."/>

		<!--- Persist the Energy Contract data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyContracts_EnergyAccounts", arguments.EnergyContractAccountAssociation)/>

	</cffunction>

</cfcomponent>
