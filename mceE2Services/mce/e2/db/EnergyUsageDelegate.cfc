
<!---

  Template Name:  EnergyUsage
     Base Table:  EnergyUsage

abe@twintechs.com

       Author:  Abraham Lloyd,Ilian Mihaylob
 Date Created:  Friday, March 20, 2009
  Description:

Revision History
Date		Initials		Comments
----------------------------------------------------------
Friday, March 20, 2009 - Template Created.
08 aug 2013, added hasBill, hasScreen columns

--->

<cfcomponent displayname="EnergyUsageDelegate"
				hint="This CFC manages all data access interactions for the EnergyUsage table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyEnergyUsageComponent"
				hint="This method is used to retrieve a Energy Usage object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.EnergyUsage"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.EnergyUsage")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getEnergyUsageAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Energy Usage objects for all the Energy Usage in the application."
				output="false"
				returnType="array" 
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_usage_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given energy usage record."/>
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given / associated energy account record."/>
		<cfargument name="period_start" required="false" type="date" hint="Describes the start date for a given energy usage period."/>
		<cfargument name="period_end" required="false" type="date" hint="Describes the end date for a given energy usage period."/>
		<cfargument name="SelectMethod" type="string" required="true" default="all" hint="Describes the method used to select data (current = select only reportable data, all = select all previous data).">

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Energy Usage and build out the array of components --->
		<cfset local.qResult = getEnergyUsage(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.EnergyUsage")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyUsageAsComponent"
				hint="This method is used to retrieve a single instance of a / an Energy Usage value object representing a single Energy Usage record."
				output="false"
				returnType="mce.e2.vo.EnergyUsage"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_usage_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given energy usage record."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Energy Usage and build out the component --->
		<cfset local.qResult = getEnergyUsage(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.EnergyUsage")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getEnergyUsageDateRangeAsStruct"
				access="public" returntype="struct"
				hint="This method is used to return an energy usage date range for a collection of energy accounts as a structure.">

		<!--- Define the arguments for this method --->
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given / associated energy account record."/>
		<cfargument name="surpressEnergyUsageId" required="true" type="boolean" default="true" hint="Describes whether the energy_account_uid should be returned by the generated result set."/>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get the date range --->
		<cfset local.qDateRange = getEnergyUsageDateRange(argumentCollection=arguments)>

		<!--- Return the result set converted to a structure --->
		<cfreturn QueryRowToStruct(local.qDateRange)>

	</cffunction>

	<cffunction name="getEnergyUsageDateRange"
				hint="This method is used to retrieve the minPeriodStart and maxPeriodEnd for all energy usage records associated to a given energy account."
				access="public"
				returntype="query">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_usage_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given energy usage record."/>
		<cfargument name="surpressEnergyUsageUid" required="true" type="boolean" default="true" hint="Describes whether the energy_account_uid should be returned by the generated result set."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Usage records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyUsage table matching the where clause
			select
					<!--- Surpress the energy account primary key --->
					<cfif not arguments.surpressEnergyUsageUid>
					tbl.energy_account_uid,
					</cfif>

					min(tbl.period_start) as minPeriodStart,
					max(tbl.period_end) as maxPeriodEnd

			from	dbo.EnergyUsage tbl (nolock)
			where	1=1
					and tbl.is_active = 1

					<cfif structKeyExists(arguments, 'energy_account_uid')>
					and tbl.energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" null="false" list="true"> )
					</cfif>
					and tbl.usage_type_code = 'actual'
		</cfquery>

		<!--- Return the result set --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getEnergyUsage"
				hint="This method is used to retrieve single / multiple records from the EnergyUsage table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_usage_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given energy usage record."/>
		<cfargument name="energy_account_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given / associated energy account record."/>
		<cfargument name="period_start" required="false" type="date" hint="Describes the start date for a given energy usage period."/>
		<cfargument name="period_end" required="false" type="date" hint="Describes the end date for a given energy usage period."/>
		<cfargument name="energy_unit_uid" required="false" type="string" hint="Describes the primary key of the related / associated energy unit."/>
		<cfargument name="classification_code" required="false" type="string" hint="Describes the clasification code for a given energy usage record."/>
		<cfargument name="recorded_value" required="false" type="any" hint="Describes the recorded energy usage value."/>
		<cfargument name="is_system_calculated" required="false" type="boolean" hint="Describes if the recorded value was system calculated."/>
		<cfargument name="document_set_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a related document set."/>
		<cfargument name="emission_factor_uid" required="false" type="string" hint="Describes the primary key of the related emission factor."/>
		<cfargument name="total_cost" required="false" type="numeric" hint="Describes the monitary cost associated to a given energy usage record."/>
		<cfargument name="SelectMethod" type="string" required="true" default="all" hint="Describes the method used to select data (current = select only reportable data, all = select all previous data)."/>
		<cfargument name="usage_type_code" type="string" required="false" default="actual"/>
		<cfargument name="isExtended" type="boolean" required="false" />

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Energy Usage records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyUsage table matching the where clause
			select	tbl.energy_usage_uid,
					tbl.energy_account_uid,
					tbl.period_start,
					tbl.period_end,
					tbl.report_date,
					tbl.energy_unit_uid,
					tbl.classification_code,
					tbl.recorded_value,
					tbl.is_system_calculated,
					tbl.document_set_uid,
					tbl.emission_factor_uid,
					tbl.total_cost,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date,
					tbl.usage_status_code,
					tbl.usage_status_date,
					
					dbo.getDaysInUsagePeriod(tbl.period_start, tbl.period_end, 0) as numberOfDays,
					
					eus.friendly_name as usageStatusFriendlyName,

					sds.friendly_name as documentSetFriendlyName,
					units.friendly_name as energyUnitFriendlyName,
					(

						select	count(*)
						from	dbo.storedDocuments sd (nolock)
						where	sd.document_set_uid = tbl.document_set_uid
								and sd.is_active = 1

					) as documentSetDocumentCount
					
		
					,
					tblex.hasBill as hasBill,
					tblex.hasScreen as hasScreen,
					
					CAST(dbo.getEnergyUsageMetaValueByIdAsString(tbl.energy_usage_uid, 'IsEstimatedInternal') as bit) as estimatedInternal,
					CAST(dbo.getEnergyUsageMetaValueByIdAsString(tbl.energy_usage_uid, 'Estimated') as bit) as estimated
		
			from	dbo.EnergyUsage tbl (nolock)
		
					left outer join EnergyUsage_Conedison tblex (nolock)
					on tbl. energy_usage_uid = tblex.energy_usage_uid
		
					left outer join dbo.StoredDocumentSet sds (nolock)
						on tbl.document_set_uid = sds.document_set_uid and
						sds.is_active = 1
					left join dbo.EnergyUsageStatuses eus (nolock)
						on tbl.usage_status_code = eus.usage_status_code
					left join dbo.EnergyUnits units (nolock)
						on tbl.energy_unit_uid = units.energy_unit_uid

			where	1=1
					and tbl.is_active = 1

					<cfif structKeyExists(arguments, 'energy_usage_uid')>
					and tbl.energy_usage_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_usage_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'energy_account_uid')>
					and tbl.energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_account_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'energy_unit_uid')>
					and tbl.energy_unit_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_unit_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'classification_code')>
					and tbl.classification_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.classification_code#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'recorded_value')>
					and tbl.recorded_value in ( <cfqueryparam cfsqltype="cf_sql_float" value="#arguments.recorded_value#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'is_system_calculated')>
					and tbl.is_system_calculated in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_system_calculated#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'document_set_uid')>
					and tbl.document_set_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.document_set_uid#" null="false" list="true"> )
					</cfif>

					<cfif structKeyExists(arguments, 'emission_factor_uid')>
					and tbl.emission_factor_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.emission_factor_uid#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'total_cost')>
					and tbl.total_cost in ( <cfqueryparam cfsqltype="cf_sql_money" value="#arguments.total_cost#" list="true" null="false"> )
					</cfif>

					<cfif structKeyExists(arguments, 'period_start')>
					and tbl.report_date >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.period_start)#" list="false" null="false">
					</cfif>

					<cfif structKeyExists(arguments, 'period_end')>
					and tbl.report_date <= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(arguments.period_end)#" null="false" list="false">
					</cfif>

					<cfif arguments.selectMethod eq 'current'>
						AND eus.data_is_reportable = 1
					</cfif>

					and tbl.usage_type_code = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.usage_type_code#">
				ORDER BY
					tbl.period_end DESC
		</cfquery>		

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getAvailableEnergyUsageStatuses" access="public" returntype="query" >
		<cfargument name="loggedInUser" type="any" required="false" />

		<cfset var local = structNew() />
		<cfquery name="local.qResult" datasource="#this.datasource#">
			select usage_status_code,
				friendly_name,
				importance_level,
				display_order,
				permission_required,
				data_is_reportable
			from EnergyUsageStatuses
			order by display_order ASC
		</cfquery>
		<cfreturn local.qResult />
	</cffunction>

	<cffunction name="getEnergyUsageHistoryOverview"
				hint="This method is used to retrieve single / multiple records from the EnergyUsageStatusHistory table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyAccounts" required="true" type="array" hint="Describes the array of energy accounts to fetch history data on."/>
		<cfargument name="SelectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select only reportable data, all = select all previous data).">
		<cfargument name="includeAccountsFromAnyProvider" type="boolean" required="true" hint="If true, history is found for any/all of the energy accounts specified">
		<cfargument name="includeAccountsFromUserProvider" type="boolean" required="true" hint="If true, history is found for the energy accounts specified that are associated with the EnergyProvider on the user record">
		<cfargument name="user_uid" type="string" required="true" hint="The user uid that will be used to find the user's associated EnergyProvider, if needed.">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Get a comma delimited list of energy_account_uids --->
		<cfset local.accountIDs = arrayNew(1)>
		<cfloop from="1" to="#arrayLen(arguments.EnergyAccounts)#" index="local.i">
			<cfset arrayAppend(local.accountIDs, arguments.EnergyAccounts[local.i].energy_account_uid)>
		</cfloop>

		<!--- Retrieve a single / multiple Energy Usage records. --->
		<!--- TODO: Finish query for usageStatusFriendlyName --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the EnergyAccounts table matching the where clause
			select	distinct(tbl.energy_account_uid) as energy_account_uid,
					tbl.property_uid,
					tbl.friendly_name as energyAccountFriendlyName,
					prop.friendly_name as propertyFriendlyName,
					et.friendly_name as energyTypeFriendlyName,
					(SELECT MAX(period_end)
						FROM EnergyUsage sub
						LEFT JOIN dbo.EnergyUsageStatuses eus on sub.usage_status_code = eus.usage_status_code
						WHERE sub.energy_account_uid = tbl.energy_account_uid
						AND is_active = 1
						<cfif arguments.selectMethod eq 'current'>
							AND eus.data_is_reportable = 1
						</cfif>
						AND usage_type_code = 'actual')
					as mostRecentEntryDate,
					(SELECT TOP 1 eus.friendly_name
						FROM dbo.EnergyUsage eu (nolock)
						JOIN dbo.EnergyUsageStatuses eus (nolock)
							ON eu.usage_status_code = eus.usage_status_code

						WHERE eu.energy_account_uid = tbl.energy_account_uid
						and eu.is_active = 1
						<cfif arguments.selectMethod eq 'current'>
							AND eus.data_is_reportable = 1
						</cfif>
						GROUP BY eus.friendly_name, eu.energy_account_uid
						ORDER BY Max(eus.importance_level) DESC)
					as usageStatusFriendlyName

					--usageStatusFriendlyName (this is the friendly_name from the EnergyUsageStatuses table for the
					--status with the HIGHEST importance_level for any EnergyUsage records associated with this
					--energy account. So, if there are ANY “rejected” entries then this column should be “Rejected”.
					--Some kind of subquery should be able to get this data without too much bother.)

			from	dbo.EnergyAccounts tbl (nolock),
					dbo.EnergyTypes et (nolock),
					dbo.Properties prop (nolock)
					--,dbo.EnergyUsageStatuses eus (nolock)

			where	1=1
					and tbl.is_active = 1
					and tbl.energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arrayToList(local.accountIDs)#" null="false" list="true"> )
					and tbl.energy_type_uid = et.energy_type_uid
					and tbl.property_uid = prop.property_uid


					<cfif not arguments.includeAccountsFromAnyProvider>
						<cfif arguments.includeAccountsFromUserProvider>
							AND tbl.energy_account_uid IN (
								SELECT sena.energy_account_uid
								FROM EnergyNativeAccounts sena (nolock)
								INNER JOIN EnergyProviders sep (nolock) ON sep.energy_provider_uid = sena.energy_provider_uid
								INNER JOIN Users su (nolock) ON su.energy_provider_uid = sep.energy_provider_uid
								WHERE sena.energy_account_uid in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arrayToList(local.accountIDs)#" null="false" list="true"> )
								AND su.user_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.user_uid#">
								AND sena.is_active = 1
							)
						<cfelse>
							and tbl.energy_account_uid IS NULL
						</cfif>
					</cfif>
					order by 
					prop.friendly_name,
					et.friendly_name,
					tbl.friendly_name
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getDefaultComparisonPeriod"
				hint="This method is used to retrieve the default comparison period dates for a given energy usage record."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="energy_usage_uid" required="false" type="string" hint="Describes the primary key / unique identifier of a given energy usage record."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Default the empty data set variable --->
		<cfset local.returnEmptyDataSet = false>

		<!--- Retrieve the source / usage data --->
		<cfquery name="local.qSourceData" datasource="#this.datasource#">

			-- Retrieve the meta type group properties
			select	mtg.comparison_enabled,
					mtg.default_comparison_period_index,
					ea.energy_account_uid,
					convert(varchar, eu.period_end, 0) as period_end
			from	dbo.energyAccounts ea (nolock)
					join dbo.EnergyNativeAccounts ena (nolock)
						on ea.energy_account_uid = ena.energy_account_uid
					join dbo.MetaTypeGroups mtg (nolock)
						on ena.meta_group_uid = mtg.meta_group_uid
					join dbo.energyUsage eu (nolock)
						on ea.energy_account_uid = eu.energy_account_uid

			where	1=1
					and ea.is_active = 1
					and mtg.is_active = 1
					and eu.is_active = 1

					<cfif structKeyExists(arguments, 'energy_usage_uid')>
					and eu.energy_usage_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.energy_usage_uid#" list="true" null="false"> )
					</cfif>

		</cfquery>

		<!--- Was a source comparison enabled? --->
		<cfif local.qSourceData.comparison_enabled eq 1>

			<!--- If so, then retrieve the source comparison data --->
			<cfquery name="local.qResult" datasource="#this.datasource#">

				-- If enabled, then retrieve the filter data
				select	top 1
						dt.energy_usage_uid,
						dt.period_start,
						dt.period_end
				from	(

							select	top #abs(local.qSourceData.default_comparison_period_index)#
									eu.energy_usage_uid,
									eu.period_start,
									eu.period_end
							from	dbo.energyUsage eu (nolock)
							where	eu.is_active = 1
									and eu.energy_account_uid = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#local.qSourceData.energy_account_uid#" list="false" null="false">
									and eu.period_end < <cfqueryparam cfsqltype="cf_sql_timestamp" value="#createOdbcDateTime(local.qSourceData.period_end)#" list="false" null="false">
							order
							by		eu.period_end desc

						) dt
						order
						by		dt.period_end asc


			</cfquery>

			<!--- Was a record returned? --->
			<cfif local.qResult.recordCount gt 0>

				<!--- Calculate the date difference / margin --->
				<cfset local.dateDiff = dateDiff('m', local.qSourceData.period_end, local.qResult.period_end )>
				<cfset local.compareDiff = local.qSourceData.default_comparison_period_index>

				<!--- Check to see if the selected period matches the comparison period specified --->
				<cfif local.dateDiff neq local.compareDiff>

					<!--- If it does not, then return an empty comparison period dataset --->
					<cfset local.qResult = queryNew("energy_usage_uid,period_start,period_end")>

					<!--- Create the empty result set --->
					<cfset queryAddRow(local.qResult, 1)>

					<!--- Seed the energy usage primary key --->
					<cfset querySetCell(local.qResult, 'energy_usage_uid', arguments.energy_usage_uid, 1)>

					<!--- Seed the fake period start and edd --->
					<cfset querySetCell(local.qResult, 'period_start', dateFormat(dateAdd('m',(local.qSourceData.default_comparison_period_index - 1),local.qSourceData.period_end), 'MM/DD/YYYY'))>
					<cfset querySetCell(local.qResult, 'period_end', dateFormat(dateAdd('m', local.qSourceData.default_comparison_period_index,local.qSourceData.period_end), 'MM/DD/YYYY'))>

				</cfif>

			<cfelse>

				<!--- If not, then return an empty comparison period dataset --->
				<cfset local.returnEmptyDataSet = true>

			</cfif>

		<cfelse>

			<!--- Return the empty data set --->
			<cfset local.returnEmptyDataSet = true>

		</cfif>

		<!--- Should an empty data set be returned? --->
		<cfif local.returnEmptyDataSet eq true>

			<!--- Create an empty result set --->
			<cfset local.qResult = queryNew('energy_usage_uid,period_start,period_end')>

		</cfif>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>

	</cffunction>

	<!---Describes the methods used to persist / modify Energy Usage data. --->
	<cffunction name="saveNewEnergyUsage"
				hint="This method is used to persist a new Energy Usage record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyUsage" required="true" type="mce.e2.vo.EnergyUsage" hint="Describes the VO containing the Energy Usage record that will be persisted."/>
		<cfargument name="user_uid" required="false" type="string" default="true" hint="User uid to put on the history record"/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!---TODO: see if we don't need to move this to EnergyEntryDelegate--->
		<cfset local.history = createNewEnergyUsageStatusHistory(arguments.EnergyUsage, arguments.user_uid)>
		<cfset saveEnergyUsageStatusHistory(local.history)>

		<!--- Persist the Energy Usage data to the application database --->
		<cfset local.newId=this.dataMgr.insertRecord("EnergyUsage", arguments.EnergyUsage)>
		<cfset local.euExt = createObject("component", "mce.e2.vo.EnergyUsage")>
		<cfset local.euExt.energy_usage_uid = local.newId > 
		<cfset local.euExt.hasBill = arguments.EnergyUsage.hasBill > 
		<cfset local.euExt.hasScreen = arguments.EnergyUsage.hasScreen> 
		<cfset local.newId=this.dataMgr.insertRecord("EnergyUsage_Conedison", local.euExt)>
			
	</cffunction>

	<cffunction name="saveExistingEnergyUsage"
				hint="This method is used to persist an existing Energy Usage record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyUsage" required="true" type="any" hint="Describes the VO containing the Energy Usage record that will be persisted."/>
		<cfargument name="addHistory" required="false" type="boolean" default="true" hint="Allow internal users to change the status without affecting the history."/>
		<cfargument name="user_uid" required="false" type="string" default="true" hint="User uid to put on the history record"/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- If addHistory, check usage_status_code --->
		<cfif arguments.addHistory>
			<cfquery name="local.qResult" datasource="#this.datasource#">
				select	usage_status_code, user_comments
				from	EnergyUsage
				where	energy_usage_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.EnergyUsage.energy_usage_uid#" null="false" list="false">
			</cfquery>

			<cfif isHistoryStatusChanged(arguments.EnergyUsage)>
				<cfset local.history = createNewEnergyUsageStatusHistory(arguments.EnergyUsage, arguments.user_uid)>
				<cfset saveEnergyUsageStatusHistory(local.history)>
			</cfif>
		</cfif>
		
		<cftry>
		<!--- Persist the Energy Usage data to the application database --->
		<cfset this.dataMgr.updateRecord("EnergyUsage", arguments.EnergyUsage)/>
		<cfset local.euExt = createObject("component", "mce.e2.vo.EnergyUsage")>
		<cfset local.euExt.energy_usage_uid = arguments.EnergyUsage.energy_usage_uid > 
		<cfset local.euExt.hasBill = arguments.EnergyUsage.hasBill > 
		<cfset local.euExt.hasScreen = arguments.EnergyUsage.hasScreen> 
		<cftry>
		<cfset this.dataMgr.updateRecord("EnergyUsage_Conedison", local.euExt)/>
		 <cfcatch type = "DataMgr">
			<cfset local.newId=this.dataMgr.insertRecord("EnergyUsage_Conedison", local.euExt)>
		 </cfcatch>
		</cftry>
		<cfcatch>
			<!---there is situation we might not pass hasBill, hasScreen etc., so we leave them unchanged--->
		</cfcatch>
		</cftry>
		
	</cffunction>

	<cffunction name="createNewEnergyUsageStatusHistory" access="private" returntype="mce.e2.vo.EnergyUsageStatusHistory">
		<cfargument name="EnergyUsage" required="true" type="any" hint="Describes the VO containing the Energy Usage record that will be persisted."/>
		<cfargument name="user_uid" required="false" type="string" default="true" hint="User uid to put on the history record"/>

		<cfset local.history = createObject("component", "mce.e2.vo.EnergyUsageStatusHistory")>
		<cfset local.history.energy_usage_uid = arguments.EnergyUsage.energy_usage_uid>
		<cfset local.history.user_uid = arguments.user_uid>
		<cfset local.history.usage_status_date = now()>
		<cfset local.history.usage_status_code = arguments.EnergyUsage.usage_status_code>
		<cfset local.history.user_comments = arguments.EnergyUsage.user_comments>
		
		<cfreturn local.history>

	</cffunction>

	<cffunction name="isHistoryStatusChanged" access="private" returntype="boolean">
		<cfargument name="EnergyUsage" required="true" type="any" hint="Describes the VO containing the Energy Usage record that will be persisted."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<cfquery name="local.qResult" datasource="#this.datasource#">
			select	usage_status_code, user_comments
			from	EnergyUsage
			where	energy_usage_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.EnergyUsage.energy_usage_uid#" null="false" list="false">
		</cfquery>

		<cfreturn
			local.qResult.recordcount eq 0
			or (arguments.EnergyUsage.usage_status_code neq local.qResult.usage_status_code)>
		    <!--- or (trim(arguments.EnergyUsage.user_comments) neq trim(local.qResult.user_comments)) --->
	</cffunction>

	<cffunction name="saveEnergyUsageStatusHistory"
				hint="This method is used to persist an existing Energy Usage Status History record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyUsageStatusHistory" required="true" type="any" hint="Describes the VO containing the Energy Usage Status History record that will be persisted."/>

		<!--- Persist the Energy Usage Status History data to the application database --->
		<cfset this.dataMgr.insertRecord("EnergyUsageStatusHistory", arguments.EnergyUsageStatusHistory)/>

	</cffunction>

	<cffunction name="getEnergyUsageStatusHistory" access="public" returntype="query" >
		<cfargument name="EnergyUsage" required="true" type="mce.e2.vo.EnergyUsage" hint="Describes the VO containing the Energy Usage record that will be persisted."/>

		<cfset var local = structNew() />
		<cfquery name="local.qResult" datasource="#this.datasource#">
			SELECT     	EnergyUsageStatusHistory.usage_status_date,
							Users.first_name, Users.last_name,
                			EnergyUsageStatuses.friendly_name,
							EnergyUsageStatusHistory.user_comments
			FROM     	EnergyUsageStatusHistory
						INNER JOIN Users ON EnergyUsageStatusHistory.user_uid = Users.user_uid
						INNER JOIN EnergyUsageStatuses ON EnergyUsageStatusHistory.usage_status_code = EnergyUsageStatuses.usage_status_code
			WHERE		energy_usage_uid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.EnergyUsage.energy_usage_uid#" null="false" list="false">

			ORDER BY 	EnergyUsageStatusHistory.usage_status_date DESC
		</cfquery>
		<cfreturn local.qResult />
	</cffunction>


</cfcomponent>