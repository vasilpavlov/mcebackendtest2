
<!---

  Template Name:  AddressTypeDelegate
     Base Table:  AddressTypes	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, April 01, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, April 01, 2009 - Template Created.		

--->
		
<cfcomponent displayname="ClientContactRoleDelegate"
				hint="This CFC manages all data access interactions for the AddressTypes table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyAddressTypeComponent"
				hint="This method is used to retrieve a Address Type object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.AddressType"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.AddressType")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getAddressTypesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Address Type objects for all the Address Types in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="address_type_code" required="false" type="string" hint="Describes the address type code for a given address."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Address Types and build out the array of components --->
		<cfset local.qResult = getAddressType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.AddressType")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getAddressTypeAsComponent"
				hint="This method is used to retrieve a single instance of a / an Address Type value object representing a single Address Type record."
				output="false"
				returnType="mce.e2.vo.AddressType"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="address_type_code" required="false" type="string" hint="Describes the address type code for a given address."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Address Type and build out the component --->
		<cfset local.qResult = getAddressType(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.AddressType")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getAddressType"
				hint="This method is used to retrieve single / multiple records from the AddressTypes table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="address_type_code" required="false" type="string" hint="Describes the address type code for a given address."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the friendly / customer facing name for a given address type."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Address Type records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the AddressTypes table matching the where clause
			select	tbl.address_type_code,
					tbl.friendly_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.AddressTypes tbl (nolock)
			where	1=1

					<cfif structKeyExists(arguments, 'address_type_code')>
					and tbl.address_type_code in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.address_type_code#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" null="false" list="true"> ) 
					</cfif>

			order
			by		tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Address Type data. --->	
	<cffunction name="saveNewAddressType"
				hint="This method is used to persist a new Address Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AddressType" required="true" type="mce.e2.vo.AddressType" hint="Describes the VO containing the Address Type record that will be persisted."/>

		<!--- Persist the Address Type data to the application database --->
		<cfset this.dataMgr.insertRecord("AddressTypes", arguments.AddressType)/>

	</cffunction>

	<cffunction name="saveExistingAddressType"
				hint="This method is used to persist an existing Address Type record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AddressType" required="true" type="mce.e2.vo.AddressType" hint="Describes the VO containing the Address Type record that will be persisted."/>

		<!--- Persist the Address Type data to the application database --->
		<cfset this.dataMgr.updateRecord("AddressTypes", arguments.AddressType)/>

	</cffunction>

</cfcomponent>
