<cfcomponent displayname="AdvancedSearchDelegate"
				hint="This CFC manages all search functionality."
				output="false"
				extends="BaseDatabaseDelegate">


	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getSearchCompaniesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of client company objects for all the companies in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="companySearch" required="true" type="mce.e2.vo.CompanySearch" hint="Describes vo for a given set of company search criteria."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Alerts and build out the array of components --->
		<cfset local.qResult = getCompanyResults(arguments.companySearch)/>
		<cfset local.returnResult = queryToVoArray(local.qResult,"mce.e2.vo.ClientCompany")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getSearchPropertiesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Alert objects for all the Alerts in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="propertySearch" required="true" type="mce.e2.vo.PropertySearch" hint="Describes vo for a given set of property search criteria."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Alerts and build out the array of components --->
		<cfset local.qResult = getPropertyResults(arguments.propertySearch)/>
		<cfset local.returnResult = queryToVoArray(local.qResult,"mce.e2.vo.Property")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getCompanyResults"
				hint="This method is used to retrieve single / multiple records from the ClientCompanies table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="companySearch" required="true" type="mce.e2.vo.CompanySearch" hint="Describes vo for a given set of company search criteria."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">
			select	distinct cc.*
			
			from	(	(	(	(	(	(	(	(	(	(	(
																dbo.ClientCompanies cc (nolock)
																	left outer join dbo.Properties_ClientCompanies pcc (nolock)
																		on pcc.client_company_uid = cc.client_company_uid and pcc.is_active = 1
																		left outer join dbo.CompanyRoles cr (nolock)
																			on cr.company_role_uid = pcc.company_role_uid and cr.is_active = 1
			
															)
															left outer join dbo.Properties tbl (nolock)
																on tbl.property_uid = pcc.property_uid and tbl.is_active = 1
																left outer join dbo.Clients_ClientServices svcs (nolock)
																	on tbl.property_uid = svcs.property_uid and svcs.is_active = 1)
																left outer join dbo.ClientServiceTypes svctypes (nolock)
																	on svcs.client_service_uid = svctypes.service_type_uid and svctypes.is_active = 1
															)
															left outer join dbo.EnergyAccounts ea (nolock)
																on tbl.property_uid = ea.property_uid and ea.is_active = 1
														)
														--left outer join dbo.EnergyNativeAccounts ena (nolock)
														--	on ea.energy_account_uid = ena.energy_account_uid
													)
													left outer join dbo.ClientContacts_ClientCompanies cccc (nolock)
														on cccc.client_company_uid = cc.client_company_uid and cccc.is_active = 1
														left outer join	dbo.ClientContacts con (nolock)
															on con.client_contact_uid = cccc.client_contact_uid and con.is_active = 1
														left outer join dbo.ClientContactRoles ccr (nolock)
															on ccr.contact_role_uid = cccc.contact_role_uid and ccr.is_active = 1
												)
												left outer join dbo.CompanyAddresses ca (nolock)
													on cc.client_company_uid = ca.client_company_uid and ca.is_active = 1
											)
											left outer join dbo.Users u (nolock)
												on u.client_company_uid = cc.client_company_uid and u.is_active = 1
										)
										--left outer join dbo.EnergyTypes et (nolock)
										--	on et.energy_type_uid = ea.energy_type_uid
									)
									--left outer join dbo.PropertyMeta pm (nolock)
									--	on tbl.property_uid = pm.property_uid
								)
								--left outer join dbo.PropertyMetaTypes pmt (nolock)
								--	on pm.meta_type_uid = pmt.meta_type_uid
							)

			where 
			
			cc.is_active = 1 and (
				<cfif arguments.companySearch.searchOperator eq 'or'>
				1=0
				<cfelse>
				1=1
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'serviceStart')>
				#arguments.companySearch.searchOperator# svcs.relationship_start >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.companySearch.serviceStart)#">
				</cfif>
							
				<cfif structKeyExists(arguments.companySearch, 'serviceEnd')>
				#arguments.companySearch.searchOperator# ( svcs.relationship_end <= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.companySearch.serviceEnd)#"> )
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'serviceName') and len(arguments.companySearch.serviceName)>
				#arguments.companySearch.searchOperator# svctypes.friendly_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.serviceName#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'address') and len(arguments.companySearch.address)>
				#arguments.companySearch.searchOperator# ca.address_line_1 LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.address#%" list="false" null="false">
				</cfif>
				
				<cfif structKeyExists(arguments.companySearch, 'city') and len(arguments.companySearch.city)>
				#arguments.companySearch.searchOperator# ca.city LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.city#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'postalCode') and len(arguments.companySearch.postalCode)>
				#arguments.companySearch.searchOperator# ca.postal_code LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.postalCode#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'stateRegion') and len(arguments.companySearch.stateRegion)>
				#arguments.companySearch.searchOperator# ca.state LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.stateRegion#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'contactFirstName') and len(arguments.companySearch.contactFirstName)>
				#arguments.companySearch.searchOperator# con.first_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.contactFirstName#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'contactLastName') and len(arguments.companySearch.contactLastName)>
				#arguments.companySearch.searchOperator# con.last_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.contactLastName#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'contactPhone') and len(arguments.companySearch.contactPhone)>
				#arguments.companySearch.searchOperator# con.phone LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.contactPhone#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'contactEmail') and len(arguments.companySearch.contactEmail)>
				#arguments.companySearch.searchOperator# con.email LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.contactEmail#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'contactRole') and len(arguments.companySearch.contactRole)>
				#arguments.companySearch.searchOperator# ccr.friendly_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.contactRole#%" list="false" null="false">
				</cfif>
										
				<cfif structKeyExists(arguments.companySearch, 'companyName') and len(arguments.companySearch.companyName)>
				#arguments.companySearch.searchOperator# cc.friendly_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.companyName#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'propertyName') and len(arguments.companySearch.propertyName)>
				#arguments.companySearch.searchOperator# tbl.friendly_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.propertyName#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'userFirstName') and len(arguments.companySearch.userFirstName)>
				#arguments.companySearch.searchOperator# u.first_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.userFirstName#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'userLastName') and len(arguments.companySearch.userLastName)>
				#arguments.companySearch.searchOperator# u.last_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.userLastName#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'username') and len(arguments.companySearch.username)>
				#arguments.companySearch.searchOperator# u.username LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.username#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'userEmail') and len(arguments.companySearch.userEmail)>
				#arguments.companySearch.searchOperator# u.alert_email LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.companySearch.userEmail#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'relatedRole') and len(arguments.companySearch.relatedRole) and arguments.companySearch.relatedRole neq "Any Relationship">
				#arguments.companySearch.searchOperator# 
					(
						cr.friendly_name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.companySearch.relatedRole#" list="false" null="false">
						and pcc.property_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.companySearch.property_uids#" list="true" null="false">)
					)
				</cfif>
	
				<cfif structKeyExists(arguments.companySearch, 'notRelatedRole') and len(arguments.companySearch.notRelatedRole) and arguments.companySearch.notRelatedRole neq "Any Relationship">
				#arguments.companySearch.searchOperator# 
					(
						cr.friendly_name != <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.companySearch.notRelatedRole#" list="false" null="false">
						and pcc.property_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.companySearch.property_uids#" list="true" null="false">)
					)
				</cfif>
			)		
			order
			by		cc.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<cffunction name="getPropertyResults"
				hint="This method is used to retrieve single / multiple records from the Alerts table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="propertySearch" required="true" type="mce.e2.vo.PropertySearch" hint="Describes vo for a given set of property search criteria."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>
<!--- 
and ( pcc.relationship_end is null or pcc.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#"> )
and pcc.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.relationship_filter_date)#">
--->
		<!--- Retrieve a single / multiple Alert records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the Properties table matching the where clause
			select	distinct tbl.*,
					tbl.currentClientUID as client_company_uid

			from	(((((((((((dbo.Properties tbl (nolock)
				left outer join dbo.Clients_ClientServices svcs (nolock)
					on tbl.property_uid = svcs.property_uid and svcs.is_active = 1)
				left outer join dbo.ClientServiceTypes svctypes (nolock)
					on svcs.client_service_uid = svctypes.service_type_uid and svctypes.is_active = 1)
				left outer join dbo.EnergyAccounts ea (nolock)
					on tbl.property_uid = ea.property_uid and ea.is_active = 1)
				left outer join dbo.EnergyNativeAccounts ena (nolock)
					on ea.energy_account_uid = ena.energy_account_uid and ena.is_active = 1)
				left outer join (dbo.EnergyContracts ec (nolock)
				right outer join dbo.EnergyContracts_EnergyAccounts ecea (nolock)
					on ec.energy_contract_uid = ecea.energy_contract_uid and ec.is_active = 1)
					on ea.energy_account_uid = ecea.energy_account_uid and ecea.is_active = 1)
				left outer join dbo.PropertyAddresses pa (nolock)
					on tbl.property_uid = pa.property_uid and pa.is_active = 1)
				left outer join dbo.EnergyProviders ep (nolock)
					on ena.energy_provider_uid = ep.energy_provider_uid and ep.is_active = 1)
				left outer join dbo.EnergyTypes et (nolock)
					on et.energy_type_uid = ea.energy_type_uid and et.is_active = 1)
				left outer join dbo.PropertyMeta pm (nolock)
					on tbl.property_uid = pm.property_uid and pm.is_active = 1)
				left outer join dbo.PropertyMetaTypes pmt (nolock)
					on pm.meta_type_uid = pmt.meta_type_uid and pmt.is_active = 1)
				left outer join (dbo.Properties_ClientCompanies pcc (nolock)
				right outer join dbo.ClientCompanies cc (nolock)
					on pcc.client_company_uid = cc.client_company_uid and cc.is_active = 1)
					on tbl.property_uid = pcc.property_uid and pcc.is_active = 1)

			where 
			
			tbl.is_active = 1 and (
			
				<cfif arguments.propertySearch.searchOperator eq 'or'>
				1=0
				<cfelse>
				1=1
				</cfif>
	
				<cfif structKeyExists(arguments.propertySearch, 'contractStart')>
					<!--- changed ec.contract_start to use contract_end --->
				#arguments.propertySearch.searchOperator# ec.contract_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.propertySearch.contractStart)#">
				</cfif>
				
				<cfif structKeyExists(arguments.propertySearch, 'contractEnd')>
				#arguments.propertySearch.searchOperator# ( ec.contract_end <= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.propertySearch.contractEnd)#"> )
				</cfif>
				
				<cfif structKeyExists(arguments.propertySearch, 'serviceStart')>
				#arguments.propertySearch.searchOperator# svcs.relationship_start >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.propertySearch.serviceStart)#">
				</cfif>
							
				<cfif structKeyExists(arguments.propertySearch, 'serviceEnd')>
				#arguments.propertySearch.searchOperator# ( svcs.relationship_end <= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.propertySearch.serviceEnd)#"> )
				</cfif>
	
				<cfif structKeyExists(arguments.propertySearch, 'serviceName') and len(arguments.propertySearch.serviceName)>
				#arguments.propertySearch.searchOperator# svctypes.friendly_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.serviceName#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.propertySearch, 'accountNumber') and len(arguments.propertySearch.accountNumber)>
				#arguments.propertySearch.searchOperator# ena.native_account_number LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.accountNumber#%" list="false" null="false">
				</cfif>
				
				<cfif structKeyExists(arguments.propertySearch, 'address') and len(arguments.propertySearch.address)>
				#arguments.propertySearch.searchOperator# pa.address_line_1 LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.address#%" list="false" null="false">
				</cfif>
				
				<cfif structKeyExists(arguments.propertySearch, 'city') and len(arguments.propertySearch.city)>
				#arguments.propertySearch.searchOperator# pa.city LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.city#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.propertySearch, 'postalCode') and len(arguments.propertySearch.postalCode)>
				#arguments.propertySearch.searchOperator# pa.postal_code LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.postalCode#%" list="false" null="false">
				</cfif>
	
				<cfif structKeyExists(arguments.propertySearch, 'stateRegion') and len(arguments.propertySearch.stateRegion)>
				#arguments.propertySearch.searchOperator# pa.state LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.stateRegion#%" list="false" null="false">
				</cfif>
										
				<cfif structKeyExists(arguments.propertySearch, 'companyName') and len(arguments.propertySearch.companyName)>
				#arguments.propertySearch.searchOperator# cc.friendly_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.companyName#%" list="false" null="false">
				</cfif>
				
				<cfif structKeyExists(arguments.propertySearch, 'contractEnergyType') and len(arguments.propertySearch.contractEnergyType)>
				#arguments.propertySearch.searchOperator# ec.energy_type_uid IN(SELECT energy_type_uid FROM EnergyTypes WHERE EnergyTypes.friendly_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.contractEnergyType#%" list="false" null="false">)
				</cfif>
				
				<cfif structKeyExists(arguments.propertySearch, 'energyProvider') and len(arguments.propertySearch.energyProvider)>
				#arguments.propertySearch.searchOperator# ep.friendly_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.energyProvider#%" list="false" null="false">
				</cfif>
				
				<cfif structKeyExists(arguments.propertySearch, 'energyType') and len(arguments.propertySearch.energyType)>
				#arguments.propertySearch.searchOperator# et.friendly_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.energyType#%" list="false" null="false">
				</cfif>
				
				<cfif structKeyExists(arguments.propertySearch, 'profileField') and len(arguments.propertySearch.profileField)>
					<cfif  structKeyExists(arguments.propertySearch, 'propertyMetaValue') and arguments.propertySearch.propertyMetaValue eq "missing_meta">
						#arguments.propertySearch.searchOperator# tbl.property_uid not in (
							select	property_uid
							from	dbo.PropertyMeta
							where	meta_type_uid in (select meta_type_uid from dbo.PropertyMetaTypes where friendly_name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.profileField#" list="false" null="false">) and
									is_active = 1
						)
					<cfelse>
						#arguments.propertySearch.searchOperator# (pmt.friendly_name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.profileField#" list="false" null="false">
					</cfif>
				</cfif>
				
				<cfif structKeyExists(arguments.propertySearch, 'propertyMetaValue') and len(arguments.propertySearch.propertyMetaValue) and arguments.propertySearch.propertyMetaValue neq "missing_meta">
				and pm.meta_value LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.propertyMetaValue#%" list="false" null="false">
				</cfif>			
				
				<cfif structKeyExists(arguments.propertySearch, 'propertyMetaRangeFrom') and len(arguments.propertySearch.propertyMetaRangeFrom)>
				and pm.meta_value >= case 
										when pmt.data_type = 0 then convert(numeric, <cfqueryparam cfsqltype="cf_sql_varchar" value="#replace(arguments.propertySearch.propertyMetaRangeFrom, ',', '', 'all')#" list="false" null="false">)
										when pmt.data_type = 1 then convert(numeric, <cfqueryparam cfsqltype="cf_sql_varchar" value="#replace(arguments.propertySearch.propertyMetaRangeFrom, ',', '', 'all')#" list="false" null="false">)
										when pmt.data_type = 3 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeFrom#" list="false" null="false">
										when pmt.data_type = 4 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeFrom#" list="false" null="false">
										when pmt.data_type = 5 then convert(numeric, <cfqueryparam cfsqltype="cf_sql_varchar" value="#replace(arguments.propertySearch.propertyMetaRangeFrom, ',', '', 'all')#" list="false" null="false">)
										when pmt.data_type = 10 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeFrom#" list="false" null="false">
										when pmt.data_type = 20 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeFrom#" list="false" null="false">
										when pmt.data_type = 30 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeFrom#" list="false" null="false">
										when pmt.data_type = 31 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeFrom#" list="false" null="false">
									end
				</cfif>			
				
				<cfif structKeyExists(arguments.propertySearch, 'propertyMetaRangeTo') and len(arguments.propertySearch.propertyMetaRangeTo)>
				and pm.meta_value <= case 
										when pmt.data_type = 0 then convert(numeric, <cfqueryparam cfsqltype="cf_sql_varchar" value="#replace(arguments.propertySearch.propertyMetaRangeTo, ',', '', 'all')#" list="false" null="false">)
										when pmt.data_type = 1 then convert(numeric, <cfqueryparam cfsqltype="cf_sql_varchar" value="#replace(arguments.propertySearch.propertyMetaRangeTo, ',', '', 'all')#" list="false" null="false">)
										when pmt.data_type = 3 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeTo#" list="false" null="false">
										when pmt.data_type = 4 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeTo#" list="false" null="false">
										when pmt.data_type = 5 then convert(numeric, <cfqueryparam cfsqltype="cf_sql_varchar" value="#replace(arguments.propertySearch.propertyMetaRangeTo, ',', '', 'all')#" list="false" null="false">)
										when pmt.data_type = 10 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeTo#" list="false" null="false">
										when pmt.data_type = 20 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeTo#" list="false" null="false">
										when pmt.data_type = 30 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeTo#" list="false" null="false">
										when pmt.data_type = 31 then <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.propertySearch.propertyMetaRangeTo#" list="false" null="false">
									end
				</cfif>			
				
				<cfif (structKeyExists(arguments.propertySearch, 'propertyMetaValue') and len(arguments.propertySearch.propertyMetaValue) and arguments.propertySearch.propertyMetaValue neq "missing_meta")
					or (structKeyExists(arguments.propertySearch, 'propertyMetaRangeFrom') and len(arguments.propertySearch.propertyMetaRangeFrom))
					or (structKeyExists(arguments.propertySearch, 'propertyMetaRangeTo') and len(arguments.propertySearch.propertyMetaRangeTo))>
					)
				</cfif>
				
				<cfif structKeyExists(arguments.propertySearch, 'propertyName') and len(arguments.propertySearch.propertyName)>
				#arguments.propertySearch.searchOperator# tbl.friendly_name LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#arguments.propertySearch.propertyName#%" list="false" null="false">
				</cfif>
			
				<cfif structKeyExists(arguments.propertySearch, 'relStart')>
				#arguments.propertySearch.searchOperator# pm.relationship_start < <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.propertySearch.relStart)#">
				</cfif>
				
				<cfif structKeyExists(arguments.propertySearch, 'relEnd')>
				#arguments.propertySearch.searchOperator# ( pm.relationship_end is null or pm.relationship_end >= <cfqueryparam cfsqltype="cf_sql_timestamp" list="false" value="#createOdbcDateTime(arguments.propertySearch.relEnd)#"> )
				</cfif>
				
			)
			
			order
			by		tbl.friendly_name

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

</cfcomponent>
