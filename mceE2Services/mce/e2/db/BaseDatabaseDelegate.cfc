<cfcomponent name="Base Database Delegate"
			 output="false"
			 extends="mce.e2.common.BaseComponent"
			 hint="This component is the base database delegate component for all data management *.cfc's.">

	<!--- Initialize the properties for the component --->
	<cfproperty name="datasource" type="string" hint="I am the datasource name to use for cfquery calls, etc. I will be set at runtime by the IOC/injection framework.">
	<cfproperty name="dataMgr" type="datamgr.DataMgr" hint="I am an instance of the DataMgr class. I will be set at runtime by the IOC/injection framework.">

	<!--- Constructor --->
	<cffunction name="init" hint="I am the constructor method; I return an instance of this class. I am generally called by the IOC framework.">
		<cfargument name="datasource" type="string" required="true" hint="A CF datasource name">
		<cfset this.datasource = arguments.datasource>
		<cfset this.valueObjectUtils = createObject("component", "mce.e2.util.ValueObjectUtils")>
		<cfreturn this>
	</cffunction>

	<!--- Setter, called via IOC/injection --->
	<cffunction name="setDataMgr" hint="I am a setter, generally called via IOC">
		<cfargument name="bean" type="datamgr.DataMgr" required="true" hint="An instance of the DataMgr class.">
		<cfset this.dataMgr = bean>
	</cffunction>

	<!--- Utility function to create an instance of a value object --->
	<cffunction name="createValueObject" hint="Convenience method to create an instance of a value object" returntype="mce.e2.vo.BaseValueObject">
		<cfargument name="className" type="string" required="true" hint="The class name of the value object to create, generally starting with 'mce.e2.vo.'">
		<cfreturn createObject("component", className)>
	</cffunction>

	<!--- Utility function, can be used to convert a one-row query into a "vo" (value object) --->
	<cffunction name="queryToVo" returntype="any" hint="Convenience method to convert a one-row query into a 'vo' (value object). Use when you have fetched a record from the database and want to return it as a VO instance.">
		<cfargument name="qRecords" type="query" required="true" hint="The query object to convert to a VO">
		<cfargument name="componentName" type="string" required="true" hint="The name of the VO class you want to get back; generaly starting with 'mce.e2.vo'.">
		<!--- Actual processing in utility function --->
		<cfreturn this.valueObjectUtils.queryToVo(argumentCollection=arguments)>
	</cffunction>

	<!--- Utility function, can be used to convert a multi-row query into an array of "vo's" (value objects) --->
	<cffunction name="queryToVoArray" returntype="array">
		<cfargument name="qRecords" type="query" required="true" hint="The query object to convert to VOs">
		<cfargument name="componentName" type="string" required="true" hint="The name of the VO class you want to get back; generaly starting with 'mce.e2.vo'.">
		<cfargument name="maxRows" type="numeric" required="false" default="#qRecords.recordCount#">
		<!--- Actual processing in utility function --->
		<cfreturn this.valueObjectUtils.queryToVoArray(argumentCollection=arguments)>
	</cffunction>


	<!--- Similar to queryToVoArray method (above), except returns anonymous structures (objects) rather than VOs --->
	<cffunction name="queryToObjectArray" returntype="array" output="false">
		<cfargument name="qRecords" type="query" required="true" hint="The query object to convert to VOs">
		<!--- Actual processing in utility function --->
		<cfreturn this.valueObjectUtils.queryToObjectArray(argumentCollection=arguments)>
	</cffunction>


	<!--- Utility function that sets a particular field on all items in an array to some simple value --->
	<cffunction name="setValuesForRecords">
		<cfargument name="records" type="any" required="true">
		<cfargument name="fieldName" type="string" required="true">
		<cfargument name="value" type="any" required="true">

		<cfset var item = "">

		<cfif isQuery(arguments.records)>
			<cfloop query="records">
				<cfset arguments.records[arguments.fieldName][arguments.records.currentRow] = arguments.value>
			</cfloop>
		<cfelseif isArray(arguments.records)>
			<cfloop array="#arguments.records#" index="item">
				<cfset item[arguments.fieldName] = arguments.value>
			</cfloop>
		<cfelse>
			<cfthrow
				message="The value passed to the 'records' argument must be a query or array."
				detail="The actual value passed was neither an array nor a query object.">
		</cfif>
	</cffunction>

	<cffunction name="buildVoPropertyListFromArray"
				access="public"
				returnType="string"
				hint="This method is used to build a list of values for a given property from an array of Vo objects.">

		<!--- Define the method arguments --->
		<cfargument name="voArray" required="true" type="array" hint="Describes the array representing the associations to compare.">
		<cfargument name="propertyName" required="true" type="string" hint="Describes the array representing the actual database / system object associations that will be compared.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the return result --->
		<cfset local.returnResult = ''>

		<!--- Loop over the voArray --->
		<cfloop from="1" to="#arrayLen(arguments.voArray)#" index="local.arrayIndex">

			<!--- Append the property to the return list --->
			<cfset local.returnResult = listAppend(local.returnResult, arguments.voArray[local.arrayIndex][arguments.propertyName])>

		</cfloop>

		<!--- Return the list --->
		<cfreturn local.returnResult>

	</cffunction>


	<cffunction name="safeCodeFormat" access="public" returntype="string" output="false">
		<cfargument name="str" type="string" required="true">
		<cfreturn REReplace(str, "[\.\{\}\-]", "_", "all")>
	</cffunction>


	
	<cffunction name="queryRollup" returntype="Query">
		<cfargument name="q" type="query" required="true">
		<cfargument name="groupColumn" type="string" required="true">
		<cfargument name="listColumn" type="string" required="true">
		
		<cfset var s = StructNew()>
		<cfset var groupVal = "">
		<cfset var result = queryNew("#groupColumn#,#listColumn#")>
		
		<cfloop query="arguments.q">
			<cfset groupVal = q[arguments.groupColumn][q.currentRow]>
			<cfif not structKeyExists(s, groupVal)>
				<cfset s[groupVal] = ArrayNew(1)>
			</cfif>
			<cfset ArrayAppend(s[groupVal], q[arguments.listColumn][q.currentRow])>
		</cfloop>
		
		<cfloop list="#structKeyList(s)#" index="groupVal">
			<cfset queryAddRow(result, 1)>
			<cfset querySetCell(result, groupColumn, groupVal)>
			<cfset querySetCell(result, listColumn, ArrayToList(s[groupVal]))>
		</cfloop>
		
		<cfreturn result>
	</cffunction>
</cfcomponent>