
<!---

  Template Name:  ClientContactRoleDelegate
     Base Table:  ClientContactRoles	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, April 01, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, April 01, 2009 - Template Created.		

--->
		
<cfcomponent displayname="ClientContactRoleDelegate"
				hint="This CFC manages all data access interactions for the ClientContactRoles table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyClientContactRoleComponent"
				hint="This method is used to retrieve a Client Contact Role object (vo) / coldFusion component."
				output="false"
				returnType="mce.e2.vo.CompanyRole"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "mce.e2.vo.CompanyRole")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getClientContactRolesAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of Client Contact Role objects for all the Client Contact Roles in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="contact_role_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given contact role."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all Client Contact Roles and build out the array of components --->
		<cfset local.qResult = getClientContactRole(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "mce.e2.vo.ClientContactRole")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getClientContactRoleAsComponent"
				hint="This method is used to retrieve a single instance of a / an Client Contact Role value object representing a single Client Contact Role record."
				output="false"
				returnType="mce.e2.vo.CompanyRole"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="contact_role_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given contact role."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single Client Contact Role and build out the component --->
		<cfset local.qResult = getClientContactRole(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "mce.e2.vo.CompanyRole")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getClientContactRole"
				hint="This method is used to retrieve single / multiple records from the ClientContactRoles table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="contact_role_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given contact role."/>
		<cfargument name="friendly_name" required="false" type="string" hint="Describes the external / customer facing name of a given contact role."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple Client Contact Role records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the ClientContactRoles table matching the where clause
			select	tbl.contact_role_uid,
					tbl.friendly_name,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.ClientContactRoles tbl (nolock)
			where	1=1
					and tbl.is_active = 1

					<cfif structKeyExists(arguments, 'contact_role_uid')>
					and tbl.contact_role_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.contact_role_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'friendly_name')>
					and tbl.friendly_name in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.friendly_name#" list="true" null="false"> ) 
					</cfif>

			order
			by		tbl.friendly_name
			
		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify Client Contact Role data. --->	
	<cffunction name="saveNewClientContactRole"
				hint="This method is used to persist a new Client Contact Role record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="ClientContactRole" required="true" type="mce.e2.vo.CompanyRole" hint="Describes the VO containing the Client Contact Role record that will be persisted."/>

		<!--- Persist the Client Contact Role data to the application database --->
		<cfset this.dataMgr.insertRecord("ClientContactRoles", arguments.ClientContactRole)/>

	</cffunction>

	<cffunction name="saveExistingClientContactRole"
				hint="This method is used to persist an existing Client Contact Role record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="ClientContactRole" required="true" type="mce.e2.vo.CompanyRole" hint="Describes the VO containing the Client Contact Role record that will be persisted."/>

		<!--- Persist the Client Contact Role data to the application database --->
		<cfset this.dataMgr.updateRecord("ClientContactRoles", arguments.ClientContactRole)/>

	</cffunction>

</cfcomponent>