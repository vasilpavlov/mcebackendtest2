
<!---

  Template Name:  UserRoleUserPermissionDelegate
     Base Table:  UserRoles_UserPermissions	

abe@twintechs.com

       Author:  Abraham Lloyd 
 Date Created:  Wednesday, June 03, 2009
  Description:  
 
Revision History
Date		Initials		Comments
---------------------------------------------------------- 
Wednesday, June 03, 2009 - Template Created.		

--->
		
<cfcomponent displayname="UserRoleUserPermissionDelegate"
				hint="This CFC manages all data access interactions for the UserRoles_UserPermissions table including date creation, modification, and association activities."
				output="false"
				extends="BaseDatabaseDelegate">

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyUserRoleUserPermissionComponent"
				hint="This method is used to retrieve a UserRoleUserPermission object (vo) / coldFusion component."
				output="false"
				returnType="UserRoleUserPermission"
				access="public">

		<!--- Initialize any local variables --->
		<cfset var voComponent = createObject("component", "UserRoleUserPermission")/>

		<!--- Return the empty v/o component --->
		<cfreturn voComponent/>

	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getUserRolesUserPermissionsAsArrayOfComponents"
				hint="This method is used to retrieve an array collection of UserRoleUserPermission objects for all the User Roles User Permissions in the application."
				output="false"
				returnType="array"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="relationship_id" required="false" type="numeric" hint="Describes the primary key / uniqueidentifier for a given user role / user permission association."/>
		<cfargument name="user_role_uid" required="true" type="string" hint="Describes the primary key of an associated user role."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve all User Roles User Permissions and build out the array of components --->
		<cfset local.qResult = getUserRoleUserPermission(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVoArray(local.qResult, "UserRoleUserPermission")/>

		<!--- Return the Vo Array --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getUserRoleUserPermissionAsComponent"
				hint="This method is used to retrieve a single instance of a / an UserRoleUserPermission value object representing a single UserRoleUserPermission record."
				output="false"
				returnType="UserRoleUserPermission"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="relationship_id" required="false" type="numeric" hint="Describes the primary key / uniqueidentifier for a given user role / user permission association."/>
		<cfargument name="user_role_uid" required="true" type="string" hint="Describes the primary key of an associated user role."/>

		<!--- Initialize any local variables --->
		<cfset var local = structNew() />

		<!--- Retrieve a single UserRoleUserPermission and build out the component --->
		<cfset local.qResult = getUserRoleUserPermission(argumentCollection=arguments)/>
		<cfset local.returnResult = queryToVo(local.qResult, "UserRoleUserPermission")/>

		<!--- Return the Vo --->
		<cfreturn local.returnResult/>

	</cffunction>

	<cffunction name="getUserRoleUserPermission"
				hint="This method is used to retrieve single / multiple records from the UserRoles_UserPermissions table."
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="relationship_id" required="false" type="numeric" hint="Describes the primary key / uniqueidentifier for a given user role / user permission association."/>
		<cfargument name="user_role_uid" required="false" type="string" hint="Describes the primary key of an associated user role."/>
		<cfargument name="user_permission_code_like" required="false" type="string" hint="Describes the lookup code of the associated user permission."/>
		<cfargument name="is_granted" required="false" type="boolean" hint="Describes whether a role has been granted to a given / associated user permission (1 = granted, 0 = not granted)."/>
		<cfargument name="is_active" required="false" type="boolean" hint="Describes the active status of a given record (1 = active, 0 = inactive)."/>
		<cfargument name="created_by" required="false" type="string" hint="Describes the system user that created a  given record."/>
		<cfargument name="created_date" required="false" type="date" hint="Describes the date that a given record was first created."/>
		<cfargument name="modified_by" required="false" type="string" hint="Describes the system user that last modified a given record."/>
		<cfargument name="modified_date" required="false" type="date" hint="Describes the date that a given record was last modified."/>

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Retrieve a single / multiple UserRoleUserPermission records. --->
		<cfquery name="local.qResult" datasource="#this.datasource#">

			-- select records from the UserRoles_UserPermissions table matching the where clause
			select	tbl.relationship_id,
					tbl.user_role_uid,
					tbl.user_permission_code_like,
					tbl.is_granted,
					tbl.is_active,
					tbl.created_by,
					tbl.created_date,
					tbl.modified_by,
					tbl.modified_date

			from	dbo.UserRoles_UserPermissions tbl (nolock)
			where	1=1

					<cfif structKeyExists(arguments, 'relationship_id')>
					and tbl.relationship_id in ( <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.relationship_id#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'user_role_uid')>
					and tbl.user_role_uid in ( <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.user_role_uid#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'user_permission_code_like')>
					and tbl.user_permission_code_like in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.user_permission_code_like#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'is_granted')>
					and tbl.is_granted in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_granted#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'is_active')>
					and tbl.is_active in ( <cfqueryparam cfsqltype="cf_sql_bit" value="#arguments.is_active#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'created_by')>
					and tbl.created_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.created_by#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'created_date')>
					and tbl.created_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.created_date#" null="false" list="true"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'modified_by')>
					and tbl.modified_by in ( <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.modified_by#" list="true" null="false"> ) 
					</cfif>

					<cfif structKeyExists(arguments, 'modified_date')>
					and tbl.modified_date in ( <cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.modified_date#" null="false" list="true"> ) 
					</cfif>

		</cfquery>

		<!--- Return the result set based on the query parameters --->
		<cfreturn local.qResult>		

	</cffunction>

	<!---Describes the methods used to persist / modify UserRoleUserPermission data. --->	
	<cffunction name="saveNewUserRoleUserPermission"
				hint="This method is used to persist a new UserRoleUserPermission record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="UserRoleUserPermission" required="true" type="UserRoleUserPermission" hint="Describes the VO containing the UserRoleUserPermission record that will be persisted."/>

		<!--- Persist the UserRoleUserPermission data to the application database --->
		<cfset this.dataMgr.insertRecord("UserRoles_UserPermissions", arguments.UserRoleUserPermission)/>

	</cffunction>

	<cffunction name="saveExistingUserRoleUserPermission"
				hint="This method is used to persist an existing UserRoleUserPermission record to the application database."
				output="false"
				returnType="void"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="UserRoleUserPermission" required="true" type="UserRoleUserPermission" hint="Describes the VO containing the UserRoleUserPermission record that will be persisted."/>

		<!--- Persist the UserRoleUserPermission data to the application database --->
		<cfset this.dataMgr.updateRecord("UserRoles_UserPermissions", arguments.UserRoleUserPermission)/>

	</cffunction>

</cfcomponent>
