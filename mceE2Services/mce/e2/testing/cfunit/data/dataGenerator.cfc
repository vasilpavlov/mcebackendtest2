<cfcomponent name="Data Generator"
			 output="true"
			 hint="This component is used to generate test data for the application database.">

	<!--- Initialize this component --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>

	<cffunction name="init"
				access="public"
				hint="This method is used to initialize the data generator.">
				
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<cfscript>
		
			// Initialize the testData structure
			this.testData = structNew();

			// Initialize the seed data structure
			this.seedData = structNew();

			// Initialize the common delegate
			this.delegate = request.beanFactory.getBean("clientCompanyDelegate");

			// Initialize the security service
			this.securityService = request.beanFactory.getBean("SecurityService");

			// Initialize all the service  components
			this.companyService = request.beanFactory.getBean("clientCompanyService");
			this.userService = request.beanFactory.getBean("userService");
			this.userGroupService = request.beanFactory.getBean("userGroupService");
			this.userRoleService = request.beanFactory.getBean("userRoleService");
			this.PropertyCollectionService = request.beanFactory.getBean("propertyCollectionService");
			
			// Setup all the test data in this *.cfc for creation
			setupClientCompanyData();	
			setupUserData();		
			setupUserGroupData();
			setupUserRoleData();			
			setupPropertyCollectionData();
			
		</cfscript>
				
	</cffunction>

	<cffunction name="createTestData"
				access="public"
				returnType="void"
				hint="This method is used to populate the application database with test data.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>	
		
			// Purge the test data prior to creating any new test data
			purgeTestData();		
		
			// Call the methods used to create / populate the application database
			createClientCompanyData();			// Client companies must exist before any other data. 
			createUserData();					// User data is dependent on client company data.
			createUserGroupData();				// User groups are dependent on client company data.
			createUserRoleData();				// User roles are dependent on client company and user group data.
			createPropertyCollectionData();		// Property collections are dependent on client company and user group data.
					
		</cfscript>

	</cffunction>

	<cffunction name="purgeTestData"
				access="public"
				returnType="void"
				hint="This method is used to remove all test data from the application database.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>	
			
			// Call the methods used to create / populate the application database
			purgeUserRoleData();				// User groups must be deleted / removed before client companies. 
			purgePropertyCollectionData();		// Property collections must be deleted / removed before user groups. 
			purgeUserGroupData();				// User groups must be deleted / removed before client companies. 
			purgeUserData();					// Users must be deleted / removed before client companies and after user groups / property collections. 
			purgeClientCompanyData();			// Client companies must be deleted / removed last. 
					
		</cfscript>

	</cffunction>

	<!--- Create Company Data Methods --->
	<cffunction name="setupClientCompanyData"
				access="private"
				hint="This method is used to create the data for testing client company records.">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>			
	
			// Initialize the test data company structures
			this.testData.companies = structNew();

			// Define the total number of records to create / process
			this.testData.companies.rowCount = 4;
			
			// Initialize the new company data set
			this.testData.companies.new = queryNew('client_company_uid,friendly_name,company_hierarchyId,oldId,is_active,created_by,created_date,modified_by,modified_date');	
				
			// Add a row to the query (instance #1)
			queryAddRow(this.testData.companies.new,this.testData.companies.rowCount);				
				
			// Loop over the collection and create the query test rows
			for( local.rowIndex = 1; local.rowIndex lte this.testData.companies.rowCount; local.rowIndex = local.rowIndex + 1){

				// Set the cell values for this instance of a client company
				querySetCell(this.testData.companies.new,'client_company_uid', this.delegate.createUniqueIdentifier(), local.rowIndex);
				querySetCell(this.testData.companies.new,'friendly_name', 'CC00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.companies.new,'company_hierarchyId', '/', local.rowIndex);
				querySetCell(this.testData.companies.new,'oldId', 1, local.rowIndex);
				querySetCell(this.testData.companies.new,'is_active', 1, local.rowIndex);
				querySetCell(this.testData.companies.new,'created_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.companies.new,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.testData.companies.new,'modified_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.companies.new,'modified_date', createOdbcDateTime(now()), local.rowIndex);

			}

			// Populate the seed client company value
			this.seedData.client_company_uid = this.testData.companies.new.client_company_uid[1];

		</cfscript>
				
	</cffunction>		
		
	<cffunction name="createClientCompanyData"
				access="public"
				returntype="void"
				hint="This method is used to seed the application database with test / company data.">	
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Build out the array of client company objects --->
		<cfset local.newClientCompanies = this.delegate.queryToVoArray(this.testData.companies.new, 'mce.e2.vo.ClientCompany')/>
							
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newClientCompanies)#" index="local.arrayIndex">	

			<!--- Save the client company to the application database --->
			<cfset this.companyService.saveNewClientCompany(local.newClientCompanies[local.arrayIndex])/>
			
		</cfloop>		
		
	</cffunction>	

	<cffunction name="purgeClientCompanyData"
				access="public"
				hint="This method is used to purge / delete any client company data related to the test data being used when client company unit tests are exersized.">

		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the client company data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.clientCompanies
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.testData.companies.new.friendly_name)#">

					)
		
		</cfquery>
			
	</cffunction>	
		
	<!--- Create the User Data Methods --->		
	<cffunction name="setupUserData"
				access="private"
				hint="This method is used to setup the data used when testing user records.">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>			
	
			// Initialize the test data company structures
			this.testData.users = structNew();

			// Define the total number of records to create / process
			this.testData.users.rowCount = 5;
			
			// Initialize the new user role data set
			this.testData.users.new = queryNew('client_company_uid,user_uid,username,password,password_hash_code,password_hash_method,first_name,last_name,is_active,created_by,created_date,modified_by,modified_date');
				
			// Add a row to the query (instance #1)
			queryAddRow(this.testData.users.new,this.testData.users.rowCount);				
				
			// Loop over the collection and create the query test rows
			for( local.rowIndex = 1; local.rowIndex lte this.testData.users.rowCount; local.rowIndex = local.rowIndex + 1){

				// Set the cell values for this instance of a user group
				querySetCell(this.testData.users.new,'client_company_uid', this.seedData.client_company_uid, local.rowIndex);
				querySetCell(this.testData.users.new,'user_uid', this.delegate.createUniqueIdentifier(), local.rowIndex);
				querySetCell(this.testData.users.new,'username', 'username00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.users.new,'password', 'password', local.rowIndex);
				querySetCell(this.testData.users.new,'password_hash_code', request.beanFactory.getBean("PasswordEncrypter").encryptString(this.testData.users.new.password[local.rowIndex]), local.rowIndex);
				querySetCell(this.testData.users.new,'password_hash_method', 1, local.rowIndex);
				querySetCell(this.testData.users.new,'first_name', 'firstName00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.users.new,'last_name', 'lastName00#local.rowIndex#', local.rowIndex);												
				querySetCell(this.testData.users.new,'is_active', 1, local.rowIndex);
				querySetCell(this.testData.users.new,'created_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.users.new,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.testData.users.new,'modified_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.users.new,'modified_date', createOdbcDateTime(now()), local.rowIndex);
	
			}

			// Populate the seed user value
			this.seedData.user_uid = this.testData.users.new.user_uid[1];

		</cfscript>
				
	</cffunction>			
	
	<cffunction name="createUserData"
				access="public"
				returntype="void"
				hint="This method is used to seed the application database with test / user data.">	
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Build out the array of user objects --->
		<cfset local.newUsers = this.delegate.queryToVoArray(this.testData.users.new, 'mce.e2.vo.User')/>
							
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUsers)#" index="local.arrayIndex">	

			<!--- Save the user to the application database --->
			<cfset this.userService.saveNewUser(local.newUsers[local.arrayIndex], local.newUsers[local.arrayIndex]["password"])/>
			
		</cfloop>		
		
	</cffunction>	
		
	<cffunction name="purgeUserData"
				access="public"
				hint="This method is used to purge / delete any user data from the application database.">

		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Get the user data --->
		<cfquery name="local.getUsers" datasource="#this.delegate.datasource#">
		
			select	user_uid
			from	dbo.users
			where	username in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.testData.users.new.username)#">)
		
		</cfquery>
		
		<cfif local.getUsers.recordcount gt 0>
		
			<!--- Purge the user data --->
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.Users_UserGroups
				where	user_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getUsers.user_uid)#">)
	
			</cfquery>
	
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.PropertyCollections_Users
				where	user_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getUsers.user_uid)#">)
	
			</cfquery>
	
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.users
				where	user_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getUsers.user_uid)#">)
			
			</cfquery>
			
		</cfif>

	</cffunction>			
						
	<!--- Create User Group Data Methods --->
	<cffunction name="setupUserGroupData"
				access="private"
				hint="This method is used to setup the data used when testing user group records.">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>			
	
			// Initialize the test data company structures
			this.testData.userGroups = structNew();

			// Define the total number of records to create / process
			this.testData.userGroups.rowCount = 5;
			
			// Initialize the new company data set
			this.testData.userGroups.new = queryNew('client_company_uid,user_group_uid,friendly_name,is_active,created_by,created_date,modified_by,modified_date');
				
			// Add a row to the query (instance #1)
			queryAddRow(this.testData.userGroups.new,this.testData.userGroups.rowCount);				
				
			// Loop over the collection and create the query test rows
			for( local.rowIndex = 1; local.rowIndex lte this.testData.userGroups.rowCount; local.rowIndex = local.rowIndex + 1){

				// Set the cell values for this instance of a user group
				querySetCell(this.testData.userGroups.new,'client_company_uid', this.seedData.client_company_uid, local.rowIndex);
				querySetCell(this.testData.userGroups.new,'user_group_uid', this.delegate.createUniqueIdentifier(), local.rowIndex);
				querySetCell(this.testData.userGroups.new,'friendly_name', 'UG00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.userGroups.new,'is_active', 1, local.rowIndex);
				querySetCell(this.testData.userGroups.new,'created_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.userGroups.new,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.testData.userGroups.new,'modified_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.userGroups.new,'modified_date', createOdbcDateTime(now()), local.rowIndex);
			
			}

			// Populate the seed user group value
			this.seedData.user_group_uid = this.testData.userGroups.new.user_group_uid[1];

		</cfscript>
				
	</cffunction>			
	
	<cffunction name="createUserGroupData"
				access="public"
				returntype="void"
				hint="This method is used to seed the application database with test / user group data.">	
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Build out the array of user group objects --->
		<cfset local.newUserGroups = this.delegate.queryToVoArray(this.testData.userGroups.new, 'mce.e2.vo.UserGroup')/>
							
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserGroups)#" index="local.arrayIndex">	

			<!--- Save the user group to the application database --->
			<cfset this.userGroupService.saveNewUserGroup(local.newUserGroups[local.arrayIndex])/>
			
		</cfloop>		
		
	</cffunction>	
		
	<cffunction name="purgeUserGroupData"
				access="public"
				hint="This method is used to purge / delete any user group data from the application database.">

		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<cfquery name="local.getUserGroups" datasource="#this.delegate.datasource#">

			select	user_group_uid
			from	dbo.userGroups
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.testData.userGroups.new.friendly_name)#">

					)
		
		</cfquery>		
		
		<cfif local.getUserGroups.recordCount gt 0>
		
			<!--- Purge the user group data --->
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.Users_UserGroups 
				where	user_group_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getUserGroups.user_group_uid)#">)
	
			</cfquery>
	
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.UserGroups_UserRoles
				where	user_group_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getUserGroups.user_group_uid)#">)
	
			</cfquery>
	
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.UserGroups_PropertyCollections
				where	user_group_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getUserGroups.user_group_uid)#">)
	
			</cfquery>
	
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.userGroups
				where	user_group_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getUserGroups.user_group_uid)#">)
			
			</cfquery>

		</cfif>
			
	</cffunction>			
		
	<!--- Create the Property Collection Data Methods --->
	<cffunction name="setupPropertyCollectionData"
				access="private"
				hint="This method is used to setup the data used when testing property collection records.">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>			
	
			// Initialize the test data company structures
			this.testData.propertyCollections = structNew();

			// Define the total number of records to create / process
			this.testData.propertyCollections.rowCount = 4;
			
			// Initialize the new property collection data set
			this.testData.propertyCollections.new = queryNew('client_company_uid,property_collection_uid,collection_hierarchyid,friendly_name,collection_type_code,is_active,created_by,created_date,modified_by,modified_date');
				
			// Add a row to the query (instance #1)
			queryAddRow(this.testData.propertyCollections.new,this.testData.companies.rowCount);				
				
			// Loop over the collection and create the query test rows
			for( local.rowIndex = 1; local.rowIndex lte this.testData.propertyCollections.rowCount; local.rowIndex = local.rowIndex + 1){

				// Set the cell values for this instance of a client company
				querySetCell(this.testData.propertyCollections.new,'client_company_uid', this.seedData.client_company_uid, local.rowIndex);
				querySetCell(this.testData.propertyCollections.new,'property_collection_uid', this.delegate.createUniqueIdentifier(), local.rowIndex);
				querySetCell(this.testData.propertyCollections.new,'collection_hierarchyid', '/', local.rowIndex);
				querySetCell(this.testData.propertyCollections.new,'collection_type_code', 'PC00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.propertyCollections.new,'friendly_name', 'PC00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.propertyCollections.new,'is_active', 1, local.rowIndex);
				querySetCell(this.testData.propertyCollections.new,'created_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.propertyCollections.new,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.testData.propertyCollections.new,'modified_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.propertyCollections.new,'modified_date', createOdbcDateTime(now()), local.rowIndex);
					
			}

			// Populate the seed property collection value
			this.seedData.property_collection_uid = this.testData.propertyCollections.new.property_collection_uid[1];

		</cfscript>
				
	</cffunction>		
		
	<cffunction name="createPropertyCollectionData"
				access="public"
				returntype="void"
				hint="This method is used to seed the application database with test / property collection data.">	
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Create a session for the current user --->
		<cfset local.session = createSession()>
		
		<!--- Build out the array of property collection objects --->
		<cfset local.newPropertyCollections = this.delegate.queryToVoArray(this.testData.propertyCollections.new, 'mce.e2.vo.PropertyCollection')/>
							
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newPropertyCollections)#" index="local.arrayIndex">	

			<!--- Save the property collection to the application database --->
			<cfset this.propertyCollectionService.saveNewPropertyCollection(local.newPropertyCollections[local.arrayIndex])/>
			
		</cfloop>		
		
	</cffunction>	

	<cffunction name="purgePropertyCollectionData"
				access="public"
				hint="This method is used to purge / delete any property collection data from the application database.">

		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<cfquery name="local.getPropertyCollections" datasource="#this.delegate.datasource#">

			select	property_collection_uid
			from	dbo.propertyCollections
			where	friendly_name in (	<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.testData.propertyCollections.new.friendly_name)#">)
						
		</cfquery>		
		
		<cfif local.getPropertyCollections.recordCount gt 0>	
		
			<!--- Purge the property collection data --->
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.PropertyCollections_Users
				where	property_collection_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getPropertyCollections.property_collection_uid)#">)
	
			</cfquery>			
			
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.UserGroups_PropertyCollections
				where	property_collection_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getPropertyCollections.property_collection_uid)#">)
	
			</cfquery>		
			
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.Properties_PropertyCollections
				where	property_collection_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getPropertyCollections.property_collection_uid)#">)
			
			</cfquery>
				
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.propertyCollections
				where	property_collection_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getPropertyCollections.property_collection_uid)#">)
			
			</cfquery>			
			
		</cfif>

	</cffunction>			
		
	<!--- Create the User Role Data Methods --->		
	<cffunction name="setupUserRoleData"
				access="private"
				hint="This method is used to setup the data used when testing user role records.">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
	
		<cfscript>			
	
			// Initialize the test data company structures
			this.testData.userRoles = structNew();

			// Define the total number of records to create / process
			this.testData.userRoles.rowCount = 5;
			
			// Initialize the new user role data set
			this.testData.userRoles.new = queryNew('user_role_uid,friendly_name,role_lookup_code,is_active,created_by,created_date,modified_by,modified_date');
				
			// Add a row to the query (instance #1)
			queryAddRow(this.testData.userRoles.new,this.testData.userRoles.rowCount);				
				
			// Loop over the collection and create the query test rows
			for( local.rowIndex = 1; local.rowIndex lte this.testData.userRoles.rowCount; local.rowIndex = local.rowIndex + 1){

				// Set the cell values for this instance of a user role
				querySetCell(this.testData.userRoles.new,'user_role_uid', this.delegate.createUniqueIdentifier(), local.rowIndex);
				querySetCell(this.testData.userRoles.new,'friendly_name', 'UR00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.userRoles.new,'role_lookup_code', 'UR00#local.rowIndex#', local.rowIndex);
				querySetCell(this.testData.userRoles.new,'is_active', 1, local.rowIndex);
				querySetCell(this.testData.userRoles.new,'created_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.userRoles.new,'created_date', createOdbcDateTime(now()), local.rowIndex);
				querySetCell(this.testData.userRoles.new,'modified_by', 'Data Generator (created)', local.rowIndex);
				querySetCell(this.testData.userRoles.new,'modified_date', createOdbcDateTime(now()), local.rowIndex);
			
			}

			// Populate the seed user role value
			this.seedData.user_role_uid = this.testData.userRoles.new.user_role_uid[1];

		</cfscript>
				
	</cffunction>			
	
	<cffunction name="createUserRoleData"
				access="public"
				returntype="void"
				hint="This method is used to seed the application database with test / user role data.">	
		
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Build out the array of user role objects --->
		<cfset local.newUserRoles = this.delegate.queryToVoArray(this.testData.userRoles.new, 'mce.e2.vo.UserRole')/>
							
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserRoles)#" index="local.arrayIndex">	

			<!--- Save the user role to the application database --->
			<cfset this.userRoleService.saveNewUserRole(local.newUserRoles[local.arrayIndex])/>
			
		</cfloop>		
		
	</cffunction>	
		
	<cffunction name="purgeUserRoleData"
				access="public"
				hint="This method is used to purge / delete any user role data from the application database.">

		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<cfquery name="local.getUserRoles" datasource="#this.delegate.datasource#">

			select	user_role_uid
			from	dbo.userRoles
			where	friendly_name in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.testData.userRoles.new.friendly_name)#">

					)
		
		</cfquery>		
		
		<cfif local.getUserRoles.recordCount gt 0>
		
			<!--- Purge the user role data --->
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.UserGroups_UserRoles 
				where	user_role_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getUserRoles.user_role_uid)#">)
		
			</cfquery>
	
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
	
				delete
				from	dbo.userRoles
				where	user_role_uid in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(local.getUserRoles.user_role_uid)#">)
			
			</cfquery>
				
		</cfif>		
				
	</cffunction>			
			
	<!--- Define any private / internal methods --->
	<cffunction name="createSession"
				access="public"
				returnType="mce.e2.security.sessionBean"
				hint="This method is used to create a session and spoof a login for a given user.">		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>					
	
		<!--- Build out the array of user objects --->
		<cfset local.user = this.delegate.queryRowToStruct(this.testData.users.new,1)/>

		<!--- Create the session for the current user --->
		<cfset local.session = this.securityService.attemptLogin(local.user.username, local.user.password)>
				
		<!--- Return the session --->		
		<cfreturn local.session>		
				
	</cffunction>
	
</cfcomponent>