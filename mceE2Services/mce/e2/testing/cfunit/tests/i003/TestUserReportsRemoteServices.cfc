<cfcomponent name="Test User Reports Remote Services"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component is used to test the methods related to the user reports remote services.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>
	
	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfscript>
		
			local.services = structNew();
						
		</cfscript>		
				
	</cffunction>

	<cffunction name="getSession" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a session.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('janeemissions','nate')>

		<!--- Retrieve the session --->
		<cfset local.result = local.securityService.getSession()>
					
	</cffunction>

	<cffunction name="getUserReportsViaSession" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of user reports.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('janeemissions','nate')>
		
		<!--- Instantiate the reporting service --->
		<cfset local.reportService = request.beanFactory.getBean("userReportService")>
		
		<!--- No arguments --->
		
		<!--- Retrieve the session --->
		<cfset local.result = local.reportService.getUserReports()>

	</cffunction>

	<cffunction name="getUserReportsViaUser" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of user reports.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('janeemissions','nate')>
		
		<!--- Create the stub objects --->
		<cfset local.user = createObject('component','mce.e2.vo.User')>

		<!--- Instantiate the reporting service --->
		<cfset local.reportService = request.beanFactory.getBean("userReportService")>
				
		<!--- Define arguments (jane emissions)--->
		<cfset local.user.user_uid = '0EB0425C-309E-49AC-B44C-0358FCBF449E'>
		
		<!--- Retrieve the session --->
		<cfset local.result = local.reportService.getUserReports(user = local.user)>
		
	</cffunction>

	<cffunction name="getUserReportsViaUserWithReportProperties" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of user reports.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('janeemissions','nate')>
		
		<!--- Create the stub objects --->
		<cfset local.user = createObject('component','mce.e2.vo.User')>
		<cfset local.userReport = createObject('component','mce.e2.vo.UserReport')>

		<!--- Instantiate the reporting service --->
		<cfset local.reportService = request.beanFactory.getBean("userReportService")>
				
		<!--- Define arguments (jane emissions)--->
		<cfset local.user.user_uid = '0EB0425C-309E-49AC-B44C-0358FCBF449E'>
		<cfset local.userReport.report_uid = 'c4c7d57d-0d1b-de11-9083-00c09f464575'>
		
		<!--- Retrieve the session --->
		<cfset local.result = local.reportService.getUserReports(
				user = local.user,
				userReport = local.userReport)>

	</cffunction>
	
	<cffunction name="getUserReportsViaUserWithNoProperties" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of user reports.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
	
		<!--- Instantiate the object and dummy properties --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('janeemissions','nate')>
		
		<!--- Create the stub objects --->
		<cfset local.user = ''>
		<cfset local.userReport = ''>

		<!--- Instantiate the reporting service --->
		<cfset local.reportService = request.beanFactory.getBean("userReportService")>
						
		<!--- Retrieve the session --->
		<cfset local.result = local.reportService.getUserReports(
				user = local.user,
				userReport = local.userReport)>
	
	</cffunction>

</cfcomponent>