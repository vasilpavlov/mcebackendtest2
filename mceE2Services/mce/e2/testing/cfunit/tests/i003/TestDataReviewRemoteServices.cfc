<cfcomponent name="Test Data Review Remote Services"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component is used to test the methods related to the data review remote services.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>
	
	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfscript>
		
			local.services = structNew();
						
		</cfscript>		
				
	</cffunction>

	<cffunction name="getDataReview" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of data review summary data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.service = request.beanFactory.getBean("dataReviewService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.dataReview')>
		<cfset local.obj.data_review_uid = '100D2B83-1374-48C3-AFFE-215BB64CCEF9'>	
		<cfset local.result = local.service.getDataReview(local.obj)>	
					
	</cffunction>

	<cffunction name="getVisibleDataReviewParticipants" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of data review participant data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
			<!--- Log in and set the session --->
			<cfset local.securityService = request.beanFactory.getBean("securityService")>
			<cfset local.securityService.attemptlogin('mgalle','ets')>					
							
			<!--- Create a reference to the current session --->
			<cfset local.service = request.beanFactory.getBean("dataReviewService")>
			<cfset local.dataReviewObject = createObject('component','mce.e2.vo.dataReview')>
			<cfset local.dataReviewObject.data_review_uid = '100d2b83-1374-48c3-affe-215bb64ccef9'>
			
			<!--- Set the output / test value --->	
			<cfset local.result = local.service.getVisibleDataReviewParticipants(
					dataReview = local.dataReviewObject)>
					
	</cffunction>
	
	<cffunction name="getDataReviewParticipants" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of data review participant data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfset local.service = request.beanFactory.getBean("dataReviewService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.dataReview')>
		<cfset local.obj.data_review_uid = '100D2B83-1374-48C3-AFFE-215BB64CCEF9'>	
		<cfset local.result = local.service.getDataReviewParticipants(local.obj)>	
					
	</cffunction>
	
	<cffunction name="getDataReviewParticipant" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of specific data review participant.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfset local.service = request.beanFactory.getBean("dataReviewService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.dataReviewParticipant')>
		<cfset local.obj.review_participant_uid = '0452C726-F182-40D4-833E-09F8C935C170'>
		<cfset local.result = local.service.getDataReviewParticipant(local.obj)>	
					
	</cffunction>	

	<cffunction name="getDataReviewDetailUsageSummaries" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of data review detail usage summaries.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("dataReviewService")>

		<!--- Instantiate the object and dummy properties --->
		<cfset local.obj = createObject('component', 'mce.e2.vo.dataReview')>
		<cfset local.obj.data_review_uid = '100D2B83-1374-48C3-AFFE-215BB64CCEF9'>	
		<cfset local.obj.display_period_start = '2009-01-01'>	
		<cfset local.obj.display_period_end = '2009-06-01'>	

		<!--- Define the profile lookup code to be used --->
		<cfset local.obj.profile_lookup_code = 'default'>
		
		<!--- Build out the property arrray argument --->
		<cfset local.propertyArray = arrayNew(1)>
		<cfset local.property = createObject('component', 'mce.e2.vo.property')>
		<cfset local.property.property_uid = '6878CEA8-4B1A-460C-91F7-FC230BD9B1A4'>
		<cfset arrayAppend(local.propertyArray, local.property)>
		
		<!--- Pull the data review usage summaries --->
		<cfset local.result = local.service.getDataReviewDetailUsageSummaries(
				local.obj, local.propertyArray)>	
					
	</cffunction>

	<cffunction name="getDataReviewDetailHistory" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of data review detail history data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the object and dummy properties --->
		<cfset local.service = request.beanFactory.getBean("dataReviewService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.dataReview')>
		<cfset local.obj.data_review_uid = '100D2B83-1374-48C3-AFFE-215BB64CCEF9'>	
		<cfset local.result = local.service.getDataReviewDetailHistory(local.obj)>	

	</cffunction>
		
	<cffunction name="getDataReviewApprovalStatuses" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of data review approval status data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Log in and set the session --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('mgalle','ets')>

		<!--- Instantiate the object and dummy properties --->
		<cfset local.service = request.beanFactory.getBean("dataReviewService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.dataReview')>
		<cfset local.obj.data_review_uid = '100D2B83-1374-48C3-AFFE-215BB64CCEF9'>	
		<cfset local.result = local.service.getDataReviewApprovalStatuses(local.obj)>	

	</cffunction>

	<cffunction name="getEnergyUsageforReviewDetail" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of energy usage / detailed review data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the object and dummy properties --->
		<cfset local.service = request.beanFactory.getBean("dataReviewService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.energyAccount')>
		<cfset local.obj.energy_account_uid = 'BE40C5DA-9699-40CA-B69C-D38F5DC7B5BA'>
		
		<!--- Initialize the array --->
		<cfset local.array = arrayNew(1)>
		<cfset arrayAppend(local.array, local.obj)>
			
		<!--- Retrieve the results --->	
		<cfset local.result = local.service.getEnergyUsageforReviewDetail(
				local.array, '2008-01-01', '2009-05-01')>	

	</cffunction>
	
	<cffunction name="getEmissionsSourcesHierarchy"
				output="false" returntype="void"
				hint="This method is used to test the retrieval of energy hierarchy data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the service and retrieve the hierarchy --->				
		<cfset local.service = request.beanFactory.getBean("emissionsAnalysisService")>
		<cfset local.result = local.service.getEmissionsSourcesHierarchy()>	
				
	</cffunction>
	
	<cffunction name="changeDataReviewDetailStatus"
				output="false" returntype="void"
				hint="This method is used to test changing and creating data review detail status records.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Log in and set the session --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('janeemissions','nate')>
		
		<!--- Create a new status update --->
		<cfset local.service = request.beanFactory.getBean("dataReviewService")>
		<cfset local.statusUpdate = createObject('component','mce.e2.vo.dataReviewDetailStatus')>
		<cfset local.statusUpdate.property_uid = 'AA1ED5C3-2D26-42D2-AE23-034C949593FA'>
		<cfset local.statusUpdate.data_review_uid = '100D2B83-1374-48C3-AFFE-215BB64CCEF9'>	
		<cfset local.statusUpdate.approval_status_code = 'none'>
		<cfset local.statusUpdate.approval_status_notes = 'Blah Blah Blah - Test Note (update).'>
		<cfset local.service.changeDataReviewDetailStatus(local.statusUpdate)>		
		
		<!--- Create append a status update for a detail that already exists --->
		<cfset local.statusUpdate = createObject('component','mce.e2.vo.dataReviewDetailStatus')>
		<cfset local.statusUpdate.review_detail_uid = '75CDA363-425B-DE11-B30C-00C09F464575'>
		<cfset local.statusUpdate.approval_status_code = 'none'>
		<cfset local.statusUpdate.approval_status_notes = 'Blah Blah Blah - Test Note (2nd update).'>
		<cfset local.service.changeDataReviewDetailStatus(local.statusUpdate)>		

	</cffunction>

	<cffunction name="getDefaultComparisonPeriod"
				output="false" returntype="void"
				hint="This method is used to test changing and creating data review detail status records.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Create a new status update --->
		<cfset local.service = request.beanFactory.getBean("energyUsageService")>
	
		<!--- Initialize the energy usage record --->
		<cfset local.energyUsage = structNew()>
		<cfset local.energyUsage.energy_usage_uid = '253E26AC-9452-4458-87D5-0FA203F5BF93'>
		
		<!--- Get the data comparison periods --->
		<cfset local.service.getDefaultComparisonPeriod(local.energyUsage)>		

	</cffunction>	

	<cffunction name="getAvailableDataReviews"
				access="public" returnType="void"
				hint="This method is used to test retrieving data review for a specific user.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Log in and set the session --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('mgalle','ets')>					
						
		<!--- Create a reference to the current session --->
		<cfset local.service = request.beanFactory.getBean("dataReviewService")>
		
		<!--- Set the output / test value --->	
		<cfset local.result = local.service.getAvailableDataReviews()>
				
	</cffunction>

</cfcomponent>