<cfcomponent name="Pre-Alpha Defect Validation Remote Services"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component is used to test any pre-alpha defects that are addressed through the service layer.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>
	
	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfscript>
		
			local.services = structNew();
						
		</cfscript>		
				
	</cffunction>

	<cffunction name="getAvailableEmissionsFactorAliases" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of available emissions factor alias records.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the reporting service --->
		<cfset local.service = request.beanFactory.getBean("EmissionsFactorAliasService")>		
					
		<!--- Define the energy type property --->			
		<cfset local.energyType = createObject('component','mce.e2.vo.energyType')>
		<cfset local.energyType.energy_type_uid = '603876ef-0eb9-44f9-8f95-4a91b13309a9'>			
					
		<!--- Get the energy entry object --->			
		<cfset local.result = local.service.getAvailableEmissionsFactorAliases(
				energyType = local.energyType)>		
							
	</cffunction>

</cfcomponent>