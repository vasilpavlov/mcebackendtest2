<cfcomponent name="Test Alerting Remote Services"
			 extends="mxunit.framework.TestCase"
			 output="true"
			 hint="This component is used to test the methods related to AlertingService for the UI (not the alert sending framework).">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>


	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">

		<cfset this.e2user = 'mgalle'>
		<cfset this.e2password = 'ets'>

		<cfset this.subscriptionAccountUids = "17F9869B-A377-4D11-A197-297F8E21FE16">
		<cfset this.subscriptionCompanyUid = "24890C20-C609-495A-8B28-5E99493F8B74">
	</cffunction>


	<cffunction name="getSession"
				output="false" returnType="any"
				hint="This method is used to test the retrieval of a session.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the object and dummy properties --->
		<cfset local.securityService = request.beanFactory.getBean("securityService")>
		<cfset local.securityService.attemptlogin('mgalle','ets')>

		<!--- Retrieve the session --->
		<cfset local.result = local.securityService.getSession()>

		<cfreturn local.result>
	</cffunction>


	<cffunction name="getCurrentAlertsForUser"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of user reports.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the alerting service --->
		<cfset local.alertingService = request.beanFactory.getBean("alertingService")>

		<!--- Retrieve the alerts --->
		<cfset local.session = getSession()>
		<cfset local.result = local.alertingService.getCurrentAlertsForUser("Alert", true)>

		<cfset assertTrue(local.result.recordCount gt 0, "Should get at least one alert, but got none.")>
	</cffunction>



	<cffunction name="getCurrentAlertCategoryCountsForUser"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of user reports.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the alerting service --->
		<cfset local.alertingService = request.beanFactory.getBean("alertingService")>

		<!--- Retrieve the alerts --->
		<cfset local.session = getSession()>
		<cfset local.result = local.alertingService.getCurrentAlertCategoryCountsForUser("Alert", true)>

		<cfset assertTrue(local.result.recordCount gt 0, "Should get at least one alert category count, but got none.")>
	</cffunction>


	<cffunction name="getCurrentAlertsForUserByDateRange"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of user reports.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the alerting service --->
		<cfset local.alertingService = request.beanFactory.getBean("alertingService")>

		<!--- Retrieve the alerts --->
		<cfset local.session = getSession()>

		<cfset local.result1 = local.alertingService.getAlertsForUserByDateRange("Alert", true, "8/1/3008", "10/1/3008")>
		<cfset assertTrue(local.result1.recordCount EQ 0, "Should have gotten no alerts because date range is way in the future.")>

		<cfset local.result2 = local.alertingService.getAlertsForUserByDateRange("Alert", true, "8/1/2009", "10/1/2009")>
		<cfset assertTrue(local.result2.recordCount GT 0, "Should get at least one alert, but got none.")>

	</cffunction>

	<cffunction name="setAlertIsActive"
				output="false" returnType="void"
				hint="This method is used to test the setting of an alert's active status.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the alerting service --->
		<cfset local.alertingService = request.beanFactory.getBean("alertingService")>

		<!--- Retrieve the alerts --->
		<cfset local.session = getSession()>
		<cfset local.result = local.alertingService.getCurrentAlertsForUser("Alert", true)>

		<cfset local.success1 = local.alertingService.setAlertIsActive(local.result.alert_uid[1], false)>
		<cfset local.success2 = local.alertingService.setAlertIsActive(local.result.alert_uid[1], true)>

		<cfset assertTrue(local.success2, "Should have gotten success message from changing alert status to 'active'")>
	</cffunction>


	<cffunction name="getAlertMeta"
				output="true" returnType="void"
				hint="This method is used to test the retrieval of alert meta data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the alerting service --->
		<cfset local.alertingService = request.beanFactory.getBean("alertingService")>

		<!--- Retrieve the alerts --->
		<cfset local.session = getSession()>
		<cfset local.alerts = local.alertingService.getCurrentAlertsForUser("Alert", true)>
		<cfset local.alert_uid = local.alerts.alert_uid[1]>

		<cfset local.result = local.alertingService.getAlertMeta(local.alert_uid, true)>

		<cfset assertTrue(local.result.recordCount GT 0, "Not really a true failure: ideally would have been nice to get a meta record for the first alert...")>
	</cffunction>



	<cffunction name="getAlertSubscriptions"
				output="true" returnType="void"
				hint="This method is used to test the retrieval of alert subscriptions.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the alerting service --->
		<cfset local.alertingService = request.beanFactory.getBean("alertingService")>

		<!--- Retrieve the alerts --->
		<cfset local.session = getSession()>
		<cfset local.alertsByAccount = local.alertingService.getAlertSubscriptionsForAccounts(this.subscriptionAccountUids, true)>
		<cfset local.alertsByCompany = local.alertingService.getAlertSubscriptionsForCompany(this.subscriptionCompanyUid, true)>

		<cfset assertTrue(ArrayLen(local.alertsByAccount) GT 0, "Expected to get at least one subscription using the 'by account' method")>
		<cfset assertTrue(ArrayLen(local.alertsByCompany) GT 0, "Expected to get at least one subscription using the 'by company' method")>
	</cffunction>



 	<cffunction name="saveExistingAlertSubscription"
				output="true" returnType="void"
				hint="This method is used to test the saving of alert subscriptions.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the alerting service --->
		<cfset local.alertingService = request.beanFactory.getBean("alertingService")>

		<!--- Grab an existing subscription --->
		<cfset local.session = getSession()>
		<cfset local.alertsByCompany = local.alertingService.getAlertSubscriptionsForCompany(this.subscriptionCompanyUid, true)>
		<cfset assertTrue(ArrayLen(local.alertsByCompany) GT 0, "Expected to get at least one subscription using the 'by company' method")>

		<!--- Let's use the first one for our test --->
		<cfset local.subscription = local.alertsByCompany[1]>

		<!--- Attempt to save the subscription --->
		<cfset local.alertingService.saveExistingAlertSubscription(local.subscription)>
	</cffunction>

	<cffunction name="saveNewAlertSubscription"
				output="true" returnType="void"
				hint="This method is used to test the saving of alert subscriptions.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the alerting service --->
		<cfset local.alertingService = request.beanFactory.getBean("alertingService")>

		<!--- Grab an existing subscription --->
		<cfset local.session = getSession()>
		<cfset local.alertsByCompany = local.alertingService.getAlertSubscriptionsForCompany(this.subscriptionCompanyUid, true)>
		<cfset assertTrue(ArrayLen(local.alertsByCompany) GT 0, "Expected to get at least one subscription using the 'by company' method")>

		<!--- Let's use the first one for our test --->
		<cfset local.subscription = local.alertsByCompany[1]>
		<cfset local.subscription.conditions[1].numeric_value = -1>

		<!--- Attempt to save the subscription --->
		<cfset local.alertingService.saveNewAlertSubscription(local.subscription)>
	</cffunction>

	<cffunction name="getAlertTypes"
				output="true" returnType="void"
				hint="This method is used to test the retrieval of alert type VOs.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the alerting service --->
		<cfset local.alertingService = request.beanFactory.getBean("alertingService")>

		<cfset local.types = local.alertingService.getAlertTypes()>

		<cfset assertTrue(ArrayLen(local.types) GT 0, "Expected to get at least one alert type")>
	</cffunction>

	<cffunction name="getAlertDeliveryMethods"
				output="true" returnType="void"
				hint="This method is used to test the retrieval of alert type VOs.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the alerting service --->
		<cfset local.alertingService = request.beanFactory.getBean("alertingService")>

		<cfset local.types = local.alertingService.getAlertDeliveryMethods()>

		<cfset assertTrue(local.types.recordCount GT 0, "Expected to get at least one alert delivery method")>
	</cffunction>

	<cffunction name="getAlertDigests"
				output="true" returnType="void"
				hint="This method is used to test the retrieval of alert type VOs.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the alerting service --->
		<cfset local.alertingService = request.beanFactory.getBean("alertingService")>

		<cfset local.types = local.alertingService.getAlertDigests()>

		<cfset assertTrue(local.types.recordCount GT 0, "Expected to get at least one alert digest record")>
	</cffunction>

</cfcomponent>