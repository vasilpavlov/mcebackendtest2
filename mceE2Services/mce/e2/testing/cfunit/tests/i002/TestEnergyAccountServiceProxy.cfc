<cfcomponent name="Test Energy Account Service Proxy Services"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component is used to test the methods related to energy account services.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>
	
	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<cfscript>
		
			local.services = structNew();
						
		</cfscript>		
				
	</cffunction>

	<cffunction name="getEnergyAccount" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of energy account data.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("energyAccountService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.EnergyAccount')>
		<cfset local.obj.energy_account_uid = 'D747ADD1-2769-4F56-BA5D-001DF2EFC64B'>	
		<cfset local.result = local.service.getEnergyAccount(local.obj)>	
			
	</cffunction>

	<cffunction name="getAvailableEmissionsFactorAliases"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of available emissions factor alias.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("emissionsFactorAliasService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.EnergyType')>
		<cfset local.obj.energy_type_uid = '61880202-e456-4751-a06f-88662931e7d8'>	
		<cfset local.result = local.service.getAvailableEmissionsFactorAliases(local.obj)>	
			
	</cffunction>

	<cffunction name="getAvailableMetaTypeGroups"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of available meta type groups.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("metaTypeGroupService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.EnergyProviderRateClass')>
		<cfset local.obj.rate_class_uid = '61880202-e456-4751-a06f-88662931e7d8'>	
		<cfset local.result = local.service.getAvailableMetaTypeGroups(local.obj)>	

	</cffunction>
	
	<cffunction name="getAvailableRateModels"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of available rate models.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("rateModelService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.EnergyProviderRateClass')>
		<cfset local.obj.rate_class_uid = '61880202-e456-4751-a06f-88662931e7d8'>	
		<cfset local.result = local.service.getAvailableRateModels(local.obj)>	

	</cffunction>
	
	<cffunction name="getAvailableRateClasses"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of available rate classes.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("energyProviderRateClassService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.EnergyProvider')>
		<cfset local.obj.energy_provider_uid = '72F22801-C631-4743-B173-00F66CC1ACA5'>	
		<cfset local.result = local.service.getAvailableEnergyProviderRateClasses(local.obj)>	

	</cffunction>
	
	<cffunction name="getAvailableEnergyAccountTypes"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of available energy account types.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("energyAccountTypeService")>
		<cfset local.obj = createObject('component', 'mce.e2.vo.EnergyType')>
		<cfset local.obj.energy_type_uid = '0B6C61C8-92D3-40D8-9A15-520E1DF91A29'>	
		<cfset local.result = local.service.getAvailableEnergyAccountTypes(local.obj)>	

	</cffunction>	
	
	<cffunction name="getAvailableEnergyTypes"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of available energy types.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>

		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("energyTypeService")>
		<cfset local.result = local.service.getAvailableEnergyTypes()>	

	</cffunction>	
	
	<cffunction name="getAvailableEnergyAccountSummaries"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of available energy types.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()>	
	
		<!--- Instantiate the component --->
		<cfset local.service = request.beanFactory.getBean("energyAccountService")>
		
		<cfset local.array = arrayNew(1)>
		
		<cfset local.obj = createObject('component', 'mce.e2.vo.Property')>
		<cfset local.obj.property_uid = 'A892074D-848F-44A6-A9A6-20D269248FBD'>	
		<cfset arrayAppend(local.array, duplicate(local.obj))>

		<cfset local.obj = createObject('component', 'mce.e2.vo.Property')>
		<cfset local.obj.property_uid = '150D4112-7177-412C-8743-20ACAB7AD130'>	
		<cfset arrayAppend(local.array, duplicate(local.obj))>

		<cfset local.obj = createObject('component', 'mce.e2.vo.Property')>
		<cfset local.obj.property_uid = '8E472A49-8E06-4468-AC67-91292179D60D'>	
		<cfset arrayAppend(local.array, duplicate(local.obj))>
		
		<cfset local.result = local.service.getAvailableEnergyAccountSummaries(propertyArray=local.array)>					
				
	</cffunction>	
	
	<cffunction name="getAvailableEnergyUsage"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of available energy usage records.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()>	
	
		<cfset local.accountService = request.beanFactory.getBean("energyAccountService")>
		<cfset local.energyUsageService = request.beanFactory.getBean("energyUsageService")>

		<cfset local.array = arrayNew(1)>
		<cfset local.obj = createObject('component', 'mce.e2.vo.EnergyAccount')>
		<cfset local.obj.energy_account_uid = 'D747ADD1-2769-4F56-BA5D-001DF2EFC64B'>	
		<cfset local.obj = local.accountService.getEnergyAccount(local.obj)>
		<cfset arrayAppend(local.array, duplicate(local.obj))>

		<cfset local.energyUsage = local.energyUsageService.getAvailableEnergyUsage(
				local.array, '01-01-2007', '01-01-2010')>				
					
	</cffunction>
	
	<cffunction name="SaveAndReturnNewEnergyAccount"
				output="false" returnType="void"
				hint="This method is used to test the creation of a new energy account.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()>	

		<!--- Create the service --->	
		<cfset local.accountService = request.beanFactory.getBean("energyAccountService")>
		
		<!--- Get the energy account --->
		<cfset local.obj = createObject('component', 'mce.e2.vo.EnergyAccount')>
		<cfset local.obj.energy_account_uid = 'D747ADD1-2769-4F56-BA5D-001DF2EFC64B'>	
		<cfset local.obj = local.accountService.getEnergyAccount(local.obj)>
		
		<!--- modify the account --->
		<cfset local.obj.energy_account_uid = ''>
		<cfset local.obj.friendly_name = 'My Test Account'>
		
		<!--- Save the account --->
		<cfset local.obj = local.accountService.SaveAndReturnNewEnergyAccount(local.obj)>
					
	</cffunction>	

	<cffunction name="SaveExistingEnergyAccount"
				output="false" returnType="void"
				hint="This method is used to test saving changes to an existing energy account.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()>	
		
		<!--- Create the service --->	
		<cfset local.accountService = request.beanFactory.getBean("energyAccountService")>
		
		<!--- Get the energy account --->
		<cfset local.obj = createObject('component', 'mce.e2.vo.EnergyAccount')>
		<cfset local.obj.energy_account_uid = 'D747ADD1-2769-4F56-BA5D-001DF2EFC64B'>	
		<cfset local.obj = local.accountService.getEnergyAccount(local.obj)>
		
		<!--- Modify the account --->
		<cfset local.obj.friendly_name = 'My Updated Test Account'>
		<cfset local.accountService.SaveExistingEnergyAccount(local.obj)>
		<cfset local.obj = local.accountService.getEnergyAccount(local.obj)>
		
	</cffunction>	
		
</cfcomponent>