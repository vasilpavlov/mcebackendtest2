<cfcomponent name="Base Rate Model Testing Component"
			 extends="mxunit.framework.TestCase" output="true"
			 hint="This component is the base testing component for the rateModel delegate / service unit tests.">

	<!--- Private methods / for internal use only --->
	<cffunction name="purgeRateModelTestData"
				access="private"
				hint="This method is used to purge / delete any rate model data related to the test data being used when rate model unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="rateModels" type="query" hint="This argument is used to identify the rate model data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Create a query to purge data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.RateModelInputSets
			where	input_set_uid not in (
			
						select	defaults_input_set_uid
						from	dbo.RateModels
			
					)

		</cfquery>
		
		<cfquery name="local.getInputSets" datasource="#this.delegate.datasource#">
		
			select	input_set_uid
			from	dbo.RateModelInputSets
			where	input_set_uid in (
			
						select	defaults_input_set_uid
						from	dbo.RateModels
						where	model_lookup_code in (			
						
									<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newRateModels.model_lookup_code)#">
			
								)
			
					)

		</cfquery>
		
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.rateModels
			where	model_lookup_code in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newRateModels.model_lookup_code)#">

					)
		
		</cfquery>
		
		<!--- Only process this query if valid id's were found / identified --->
		<cfif local.getInputSets.recordCount gt 0>
		
			<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">		
			
				delete
				from	dbo.RateModelInputSet_RateModelInputs			
				where	input_set_uid in (			
							
							<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#valuelist(local.getInputSets.input_set_uid)#">
				
						)
						
				delete	
				from	dbo.RateModelInputSets
				where	input_set_uid in (			
							
							<cfqueryparam cfsqltype="cf_sql_idstamp" list="true" value="#valuelist(local.getInputSets.input_set_uid)#">
				
						)
	
			</cfquery>
				
		</cfif>
	
	</cffunction>			
	
	<cffunction name="setupTestData"
				access="private"
				hint="This method is used to setup test data for the rateFactor service and delegate tests.">
		
		<cfscript>
		
			//Build out the rate model test data (for create / modify scenarios)
			createRateModelTestData();
			
			// Purge any related rate model data
			purgeRateModelTestData();
		
		</cfscript>		
				
	</cffunction>
	
	<cffunction name="createRateModelTestData"
				access="private"
				hint="This method is used to create the test data for testing rate model records (create and update actions).">
	
		<cfscript>			
		
			// Initialize a query containing rate model data to be tested with (the query should contain all of the columns of the base table)
			this.newRateModels = queryNew('rate_model_uid,model_lookup_code,friendly_name,implementation_file,defaults_input_set_uid,interval_classifier_uid,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newRateModels,4);
		
			// Set the cell values for this instance of a rate model
			querySetCell(this.newRateModels,'rate_model_uid', this.delegate.createUniqueIdentifier(), 1);
			querySetCell(this.newRateModels,'model_lookup_code', 'rm_001', 1);
			querySetCell(this.newRateModels,'friendly_name', 'RM 001', 1);
			querySetCell(this.newRateModels,'implementation_file', '', 1);
			querySetCell(this.newRateModels,'defaults_input_set_uid', this.delegate.createUniqueIdentifier(), 1);
			querySetCell(this.newRateModels,'interval_classifier_uid', '', 1);
			querySetCell(this.newRateModels,'is_active', 1, 1);
			querySetCell(this.newRateModels,'created_by', 'MX Unit (created)', 1);
			querySetCell(this.newRateModels,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newRateModels,'modified_by', 'MX Unit (created)', 1);
			querySetCell(this.newRateModels,'modified_date', createOdbcDateTime(now()), 1);
					
			querySetCell(this.newRateModels,'rate_model_uid', this.delegate.createUniqueIdentifier(), 2);
			querySetCell(this.newRateModels,'model_lookup_code', 'rm_002', 2);
			querySetCell(this.newRateModels,'friendly_name', 'RM 002', 2);
			querySetCell(this.newRateModels,'implementation_file', '', 2);
			querySetCell(this.newRateModels,'defaults_input_set_uid', this.delegate.createUniqueIdentifier(), 2);
			querySetCell(this.newRateModels,'interval_classifier_uid', '', 2);
			querySetCell(this.newRateModels,'is_active', 1, 2);
			querySetCell(this.newRateModels,'created_by', 'MX Unit (created)', 2);
			querySetCell(this.newRateModels,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newRateModels,'modified_by', 'MX Unit (created)', 2);
			querySetCell(this.newRateModels,'modified_date', createOdbcDateTime(now()), 2);
					
			querySetCell(this.newRateModels,'rate_model_uid', this.delegate.createUniqueIdentifier(), 3);
			querySetCell(this.newRateModels,'model_lookup_code', 'rm_003', 3);
			querySetCell(this.newRateModels,'friendly_name', 'RM 003', 3);
			querySetCell(this.newRateModels,'implementation_file', '', 3);
			querySetCell(this.newRateModels,'defaults_input_set_uid', this.delegate.createUniqueIdentifier(), 3);
			querySetCell(this.newRateModels,'interval_classifier_uid', '', 3);
			querySetCell(this.newRateModels,'is_active', 1, 3);
			querySetCell(this.newRateModels,'created_by', 'MX Unit (created)', 3);
			querySetCell(this.newRateModels,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newRateModels,'modified_by', 'MX Unit (created)', 3);
			querySetCell(this.newRateModels,'modified_date', createOdbcDateTime(now()), 3);
					
			querySetCell(this.newRateModels,'rate_model_uid', this.delegate.createUniqueIdentifier(), 4);
			querySetCell(this.newRateModels,'model_lookup_code', 'r4_001', 4);
			querySetCell(this.newRateModels,'friendly_name', 'RM 004', 4);
			querySetCell(this.newRateModels,'implementation_file', '', 4);
			querySetCell(this.newRateModels,'defaults_input_set_uid', this.delegate.createUniqueIdentifier(), 4);
			querySetCell(this.newRateModels,'interval_classifier_uid', '', 4);
			querySetCell(this.newRateModels,'is_active', 1, 4);
			querySetCell(this.newRateModels,'created_by', 'MX Unit (created)', 4);
			querySetCell(this.newRateModels,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newRateModels,'modified_by', 'MX Unit (created)', 4);
			querySetCell(this.newRateModels,'modified_date', createOdbcDateTime(now()), 4);
			
			// Build out the test data set of rateModels without primary keys
			this.newRateModelsWithoutPks = duplicate(this.newRateModels);
			
			// Remote the primary keys from the rate model test data 
			querySetCell(this.newRateModelsWithoutPks,'rate_model_uid', '', 1);
			querySetCell(this.newRateModelsWithoutPks,'rate_model_uid', '', 2);
			querySetCell(this.newRateModelsWithoutPks,'rate_model_uid', '', 3);
			querySetCell(this.newRateModelsWithoutPks,'rate_model_uid', '', 4);
					
			// Duplicate the new rate models to create the baseline modified rate models
			this.modifiedRateModels = duplicate(this.newRateModels);

			// Update the friendly names for the modified rate models
			querySetCell(this.modifiedRateModels,'friendly_name', 'RMM 001', 1);
			querySetCell(this.modifiedRateModels,'friendly_name', 'RMM 002', 2);
			querySetCell(this.modifiedRateModels,'friendly_name', 'RMM 003', 3);			
			querySetCell(this.modifiedRateModels,'friendly_name', 'RMM 004', 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedRateModels,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedRateModels,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedRateModels,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedRateModels,'modified_by', 'MX Unit (modified)', 4);
		
		</cfscript>
				
	</cffunction>

	<!--- ToDo: [ADL] This method (createRateModelInputValueTestData) must be tailored
				to create a valid rateModelInputValue test data set.  This is being put on
				hold due to the nature / relationships involved with the data. --->
	
	<cffunction name="compareRateModelProperties"
				access="private"
				hint="This method is used to compare the properties for a given rate model vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of rate model vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the rate model from the application database --->
				<cfset local.retrievedRateModel = this.delegate.getRateModelAsComponent(arguments.sourceObject[local.arrayIndex].rate_model_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the rate model from the application database --->
				<cfset local.retrievedRateModel = this.service.getRateModel(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared rateFactor VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedRateModel, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved rateFactor VO component [#arguments.compareAction#].")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
						
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedRateModel[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedRateModel[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>
	
	<!--- ToDo: [ADL] This method (compareRateModelInputValueProperties) must be modified to account for
				what needs to be validated and compared specific to rate model input values.  Additionally,
				this method should inherit the changes introduced to the compareRateModelProperties() method. --->
	
	<cffunction name="compareRateModelInputValueProperties"
				access="private"
				hint="This method is used to compare the properties for a given rate model input value vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of rate model vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the rate model from the application database --->
				<cfset 	local.retrievedRateModelInputValue = this.delegate.getRateModelInputValueAsComponent(
						rate_factor_uid=arguments.sourceObject[local.arrayIndex].rate_factor_uid,
						factor_value_uid=arguments.sourceObject[local.arrayIndex].factor_value_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the rate model from the application database --->
				<cfset 	local.retrievedRateModelInputValue = this.service.getRateModelInputValue(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>
			
			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>
			
					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared rateFactorValue VO's [#arguments.compareAction#].">
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedRateModelInputValue, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved rateFactorValue VO component.")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date' or local.thisKey eq 'relationship_start' or local.thisKey eq 'relationship_end'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
									
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedRateModelInputValue[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedRateModelInputValue[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>
	
</cfcomponent>