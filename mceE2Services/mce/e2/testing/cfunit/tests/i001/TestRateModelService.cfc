<cfcomponent name="Test Rate Model Service"
			 extends="baseRateModelTestingComponent" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the rateModelService component.">

	<!--- ToDo: [ADL] Extend the rate model testing service to test rate model input value
				methods (creating, updating, and retrieval).  To support these tests, the
				baseRateModelTestingComponent will need to be extended to include creating
				custom data, data validation, etc. --->

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Get an instance of the rate model service
			this.service = request.beanFactory.getBean("RateModelService");
			this.delegate = request.beanFactory.getBean("RateModelDelegate");
	
			// Setup the test data for the unit tests
			setupTestData();
				
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyRateModelComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty rate model component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the rate model delegate --->
		<cfset local.rateModel = this.service.getEmptyRateModelVo()>

		<!--- Assert that the rateModel object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.rateModel, "mce.e2.vo.RateModel"), true, "local.rateModel objectType is not of type mce.e2.vo.RateModel.")>
			
	</cffunction>

	<cffunction name="getEmptyRateModelInputValueComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty rate model input value component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the rate model delegate --->
		<cfset local.rateModel = this.service.getEmptyRateModelInputValueVo()>

		<!--- Assert that the rateModel object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.rateModel, "mce.e2.vo.RateModelInputValue"), true, "local.rateModel objectType is not of type mce.e2.vo.RateModelValue.")>
			
	</cffunction>

	<cffunction name="getAllRateModels"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all rate models from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the rate model delegate --->
		<cfset local.rateModelCollection = this.service.getAllRateModels()>
				
		<!--- Assert that the rateModel array collection is an array --->
		<cfset assertTrue(isArray(local.rateModelCollection), "The value returned by this.service.getAllRateModels() was not an array.")>

		<!--- Compare the rate model properties and excersize retrieving single instances of rate models --->
		<cfset compareRateModelProperties(local.rateModelCollection,'get','service')>
				
	</cffunction>

	<cffunction name="getRateModel"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single rate model from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the rate model delegate --->
		<cfset local.rateModelCollection = this.service.getAllRateModels()>
				
		<!--- Loop through the rate model collection, and retrieve each rate model individually --->
		<cfloop from="1" to="#arraylen(local.rateModelCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the rateModel to test --->
			<cfset local.rateModel = local.rateModelCollection[local.arrayIndex]>

				<!--- Retrrive a single instance of a rate model from the service --->
				<cfset local.testRateModel = this.service.getRateModel(local.rateModel)>

			<!--- Assert that the rateModel array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testRateModel, "mce.e2.vo.RateModel"), true, "local.testRateModel objectType is not of type mce.e2.vo.RateModel.")>
	
		</cfloop>
				
	</cffunction>

	<cffunction name="getRateModelInputValues"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all rate models input values for a given rate model from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the rate model delegate --->
		<cfset local.rateModelCollection = this.service.getAllRateModels()>
				
		<!--- Assert that the rateModel array collection is an array --->
		<cfset assertTrue(isArray(local.rateModelCollection), "The value returned by this.service.getAllRateModels() was not an array.")>

		<!--- Compare the rate model properties and excersize retrieving single instances of rate models --->
		<cfset compareRateModelProperties(local.rateModelCollection,'create','service')>

		<!--- Loop over the collection of rate models, and retrieve the rate model input values --->
		<cfloop from="1" to="#arrayLen(local.rateModelCollection)#" index="local.arrayIndex">
		
			<!--- Create a reference to the current rate model --->
			<cfset local.thisRateModel = local.rateModelCollection[local.arrayIndex]>
		
			<!--- Retrieve the rate model input values for the current rate model --->
			<cfset local.rateModelInputValuesCollection = this.service.getRateModelInputValues(local.thisRateModel)>

			<!--- Assert that the rateModel input value array collection is an array --->
			<cfset assertTrue(isArray(local.rateModelInputValuesCollection), "The value returned by this.service.getRateModelInputValues() was not an array.")>

			<!--- Compare the rate model input value properties and excersize retrieving single instances of rate models --->
			<cfset compareRateModelInputValueProperties(local.rateModelInputValuesCollection,'get','service')>
		
		</cfloop>
		
	</cffunction>

	<!--- Test the creation of rate model data without specifying primary keys --->
	<cffunction name="saveAndValidateNewRateModelsWithoutPrimaryKeys"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of rate models using the saveNewRateModel() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of rate model objects --->
		<cfset local.newRateModels = this.delegate.queryToVoArray(this.newRateModelsWithoutPks, 'mce.e2.vo.RateModel')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdRateModels = arrayNew(1)/>
		
		<!--- Purge / remote the rate model test data --->	
		<cfset purgeRateModelTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newRateModels)#" index="local.arrayIndex">	

			<!--- Save the rate model to the application database --->
			<cfset local.testRateModelPrimaryKey = this.service.saveNewRateModel(local.newRateModels[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testRateModelPrimaryKey), "The value returned by this.service.saveNewRateModel() was not a unique identifier.")/>
	
			<!--- Add the primary key to the rate model --->
			<cfset local.testRateModel = local.newRateModels[local.arrayIndex]>	
			<cfset local.testRateModel.rate_model_uid = local.testRateModelPrimaryKey>
	
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdRateModels, duplicate(local.testRateModel))/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdRateModels), arrayLen(local.newRateModels), "The total number of records processed does not match the total number of records iterated over when creating rate model records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareRateModelProperties(sourceObject=local.createdRateModels, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the rate model test data --->	
		<cfset purgeRateModelTestData()> 			
							
	</cffunction>

	<!--- Test the creation of rate model data --->
	<cffunction name="saveAndValidateNewRateModels"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of rate models using the saveNewRateModel() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of rate model objects --->
		<cfset local.newRateModels = this.delegate.queryToVoArray(this.newRateModels, 'mce.e2.vo.RateModel')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		
		<!--- Purge / remote the rate model test data --->	
		<cfset purgeRateModelTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newRateModels)#" index="local.arrayIndex">	

			<!--- Save the rate model to the application database --->
			<cfset local.testRateModelPrimaryKey = this.service.saveNewRateModel(local.newRateModels[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testRateModelPrimaryKey), "The value returned by this.service.saveNewRateModel() was not a unique identifier.")/>
	
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newRateModels), "The total number of records processed does not match the total number of records iterated over when creating rate model records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareRateModelProperties(sourceObject=local.newRateModels, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the rate model test data --->	
		<cfset purgeRateModelTestData()> 			
							
	</cffunction>
	
	<cffunction name="saveAndReturnAndValidateNewRateModels"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of rate models using the saveAndReturnNewRateModel() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
					
		<!--- Build out the test rate models array --->			
		<cfset local.testRateModels = arrayNew(1)>			
						
		<!--- Build out the array of rate model objects --->
		<cfset local.newRateModels = this.delegate.queryToVoArray(this.newRateModelsWithoutPks, 'mce.e2.vo.RateModel')/>
		
		<!--- Purge / remote the rate model test data --->	
		<cfset purgeRateModelTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newRateModels)#" index="local.arrayIndex">	

			<!--- Save the rate model to the application database --->
			<cfset local.testRateModel = this.service.saveAndReturnNewRateModel(local.newRateModels[local.arrayIndex])/>
			
			<!--- Assert that the rateModel object is of the correct type --->
			<cfset assertEquals(isInstanceOf(local.testRateModel, "mce.e2.vo.RateModel"), true, "local.rateModel objectType is not of type mce.e2.vo.RateModel.")>
						
			<!--- Append the rate model to the test array --->			
			<cfset arrayAppend(local.testRateModels, local.testRateModel)>			

		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.testRateModels), arrayLen(local.newRateModels), "The total number of records processed does not match the total number of records iterated over when creating rate model records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareRateModelProperties(sourceObject=local.testRateModels, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the rate model test data --->	
		<cfset purgeRateModelTestData()> 			
							
	</cffunction>	
	
	<cffunction name="saveAndValidateExistingRateModels"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of rate models.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of rate model objects --->
		<cfset local.newRateModels = this.delegate.queryToVoArray(this.newRateModels, 'mce.e2.vo.RateModel')/>
		<cfset local.modifiedRateModels = this.delegate.queryToVoArray(this.modifiedRateModels, 'mce.e2.vo.RateModel')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>
				
		<!--- Purge / remote the rate model test data --->	
		<cfset purgeRateModelTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newRateModels)#" index="local.arrayIndex">	

			<!--- Save the rate model to the application database --->
			<cfset this.service.saveNewRateModel(local.newRateModels[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newRateModels), "The total number of records processed does not match the total number of records iterated over when creating rate model records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareRateModelProperties(sourceObject=local.newRateModels, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedRateModels)#" index="local.arrayIndex">	

			<!--- Save the rate model to the application database --->
			<cfset this.service.saveExistingRateModel(local.modifiedRateModels[local.arrayIndex])/>
			
			<!--- Increment the modified count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedRateModels), "The total number of records processed does not match the total number of records iterated over when modifying rate model records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareRateModelProperties(sourceObject=local.modifiedRateModels, compareAction="modify", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the rate model test data --->	
		<cfset purgeRateModelTestData()> 			
							
	</cffunction>	

</cfcomponent>