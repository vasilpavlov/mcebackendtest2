<cfcomponent extends="mxunit.framework.TestCase" output="true">
	<cfset accountUid = "011C1D38-A4C1-4277-B9E9-022340F06C45">
	<cfset periodStart = "2008-11-26">
	<cfset periodEnd = "2008-12-30">

	<cffunction name="getEmptyEnergyUsageVo" output="true">
		<cfset var facade = request.beanFactory.getBean("EnergyEntryService")>
		<cfset var info = facade.getEmptyEnergyUsageVo(accountUid, periodStart, periodEnd)>

		<cfset assertEquals( info.propertyFriendlyName, "150 East 42nd Street",
			"Returned structure did not include the expected property name.")>
		<cfset assertEquals( ArrayLen(info.metaFields), 18,
			"Did not contain the expected number of meta fields.")>
	</cffunction>

</cfcomponent>