<cfcomponent extends="mxunit.framework.TestCase">

	<cffunction name="testAttemptGoodLogin">
		<cfset var facade = request.beanFactory.getBean("SecurityService")>
		<cfset facade.attemptLogin("nate", "nate")>
		<cfset assertTrue(facade.getSession().isLoggedIn,
			"Expected to be logged in successfully.")>
	</cffunction>

	<cffunction name="testAttemptBadLogin">
		<cfset var facade = request.beanFactory.getBean("SecurityService")>
		<cfset facade.attemptLogin("nate", "notnate")>
		<cfset assertFalse(facade.getSession().isLoggedIn,
			"Expected to NOT be logged in.")>
	</cffunction>

</cfcomponent>