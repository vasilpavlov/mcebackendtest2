<cfcomponent name="Base User Role Testing Component"
			 extends="mxunit.framework.TestCase" output="true"
			 hint="This component is the base testing component for the userRole delegate / service unit tests.">

	<!--- Private methods / for internal use only --->
	<cffunction name="purgeUserRoleTestData"
				access="private"
				hint="This method is used to purge / delete any user role data related to the test data being used when user role unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="userRoles" type="query" hint="This argument is used to identify the user role data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<!--- Purge the user role data --->
		<cfquery name="local.purgeData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.userRoles
			where	role_lookup_code in (			
			
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newUserRoles.role_lookup_code)#">

					)
					
		</cfquery>
			
	</cffunction>			
	
	<cffunction name="setupTestData"
				access="private"
				hint="This method is used to setup test data for the user role service and delegate tests.">
		
		<cfscript>
		
			//Build out the user role test data (for create / modify scenarios)
			createUserRoleTestData();
			
			// Purge any related user role data
			purgeUserRoleTestData();
		
		</cfscript>		
				
	</cffunction>
	
	<cffunction name="createUserRoleTestData"
				access="private"
				hint="This method is used to create the test data for testing user role records (create and update actions).">
	
		<cfscript>			
		
			// Initialize a query containing user role data to be tested with (the query should contain all of the columns of the base table)
			this.newUserRoles = queryNew('user_role_uid,friendly_name,role_lookup_code,is_active,created_by,created_date,modified_by,modified_date');	
			
			// Add a row to the query (instance #1)
			queryAddRow(this.newUserRoles,4);
		
			// Set the cell values for this instance of a user role
			querySetCell(this.newUserRoles,'user_role_uid', this.delegate.createUniqueIdentifier(), 1);
			querySetCell(this.newUserRoles,'friendly_name', 'UR_001', 1);
			querySetCell(this.newUserRoles,'role_lookup_code', 'ur.lookup.001', 1);
			querySetCell(this.newUserRoles,'is_active', 1, 1);
			querySetCell(this.newUserRoles,'created_by', 'MX Unit (created)', 1);
			querySetCell(this.newUserRoles,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newUserRoles,'modified_by', 'MX Unit (created)', 1);
			querySetCell(this.newUserRoles,'modified_date', createOdbcDateTime(now()), 1);
					
			querySetCell(this.newUserRoles,'user_role_uid', this.delegate.createUniqueIdentifier(), 2);
			querySetCell(this.newUserRoles,'friendly_name', 'UR_002', 2);
			querySetCell(this.newUserRoles,'role_lookup_code', 'ur.lookup.002', 2);
			querySetCell(this.newUserRoles,'is_active', 1, 2);
			querySetCell(this.newUserRoles,'created_by', 'MX Unit (created)', 2);
			querySetCell(this.newUserRoles,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newUserRoles,'modified_by', 'MX Unit (created)', 2);
			querySetCell(this.newUserRoles,'modified_date', createOdbcDateTime(now()), 2);
					
			querySetCell(this.newUserRoles,'user_role_uid', this.delegate.createUniqueIdentifier(), 3);
			querySetCell(this.newUserRoles,'friendly_name', 'UR_003', 3);
			querySetCell(this.newUserRoles,'role_lookup_code', 'ur.lookup.003', 3);
			querySetCell(this.newUserRoles,'is_active', 1, 3);
			querySetCell(this.newUserRoles,'created_by', 'MX Unit (created)', 3);
			querySetCell(this.newUserRoles,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newUserRoles,'modified_by', 'MX Unit (created)', 3);
			querySetCell(this.newUserRoles,'modified_date', createOdbcDateTime(now()), 3);
					
			querySetCell(this.newUserRoles,'user_role_uid', this.delegate.createUniqueIdentifier(), 4);
			querySetCell(this.newUserRoles,'friendly_name', 'UR_004', 4);
			querySetCell(this.newUserRoles,'role_lookup_code', 'ur.lookup.004', 4);
			querySetCell(this.newUserRoles,'is_active', 1, 4);
			querySetCell(this.newUserRoles,'created_by', 'MX Unit (created)', 4);
			querySetCell(this.newUserRoles,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newUserRoles,'modified_by', 'MX Unit (created)', 4);
			querySetCell(this.newUserRoles,'modified_date', createOdbcDateTime(now()), 4);
	
			// Build out the test data set of userRoles without primary keys
			this.newUserRolesWithoutPks = duplicate(this.newUserRoles);
			
			// Remote the primary keys from the user role test data 
			querySetCell(this.newUserRolesWithoutPks,'user_role_uid', '', 1);
			querySetCell(this.newUserRolesWithoutPks,'user_role_uid', '', 2);
			querySetCell(this.newUserRolesWithoutPks,'user_role_uid', '', 3);
			querySetCell(this.newUserRolesWithoutPks,'user_role_uid', '', 4);
					
			// Duplicate the new user roles to create the baseline modified user roles
			this.modifiedUserRoles = duplicate(this.newUserRoles);

			// Update the friendly names for the modified user roles
			querySetCell(this.modifiedUserRoles,'friendly_name', 'URM_001', 1);
			querySetCell(this.modifiedUserRoles,'friendly_name', 'URM_002', 2);
			querySetCell(this.modifiedUserRoles,'friendly_name', 'URM_003', 3);			
			querySetCell(this.modifiedUserRoles,'friendly_name', 'URM_004', 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedUserRoles,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedUserRoles,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedUserRoles,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedUserRoles,'modified_by', 'MX Unit (modified)', 4);
		
		</cfscript>
				
	</cffunction>

	<cffunction name="compareUserRoleProperties"
				access="private"
				hint="This method is used to compare the properties for a given user role vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of user role vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the user role from the application database --->
				<cfset local.retrievedUserRole = this.delegate.getUserRoleAsComponent(arguments.sourceObject[local.arrayIndex].user_role_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the user role from the application database --->
				<cfset local.retrievedUserRole = this.service.getUserRole(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>

			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared user role VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedUserRole, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved userRole VO component [#arguments.compareAction#].")/>
		
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>							
						
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedUserRole[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedUserRole[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>
											
					</cfif>

				</cfif>

			</cfloop>
			
		</cfloop>
								
	</cffunction>
	
</cfcomponent>