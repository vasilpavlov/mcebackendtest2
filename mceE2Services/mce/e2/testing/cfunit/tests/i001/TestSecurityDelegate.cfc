<cfcomponent extends="mxunit.framework.TestCase">
	<cfset this.systemInstanceId = request.e2.system_instance_uid>


	<cffunction name="testEncryption">
		<cfset var string = "testPassword">
		<cfset var result = request.beanFactory.getBean("PasswordEncrypter").encryptString(string)>

		<cfset assertEquals( result, "82F8809F42D911D1BD5199021D69D15EA91D1FAD",
			"The input string '#string#' did not encrypt as expected.")>
	</cffunction>


	<cffunction name="testLogin">
		<cfset var delegate = request.beanFactory.getBean("securityDelegate")>
		<cfset var session_key = delegate.attemptLogin("nate", "nate", this.systemInstanceId)>

		<cfset assertEquals( len(session_key), 36,
			"We expect to get back a session key, which as a GUID should be 36 characters.")>
	</cffunction>


	<cffunction name="testGetRoles">
		<cfset var delegate = request.beanFactory.getBean("securityDelegate")>
		<cfset var session_key = delegate.attemptLogin("nate", "nate", this.systemInstanceId)>
		<cfset var rolesList = delegate.getCurrentRoles(session_key)>

		<cfset assertTrue( ListFind(rolesList, "energyentry.view"),
			"Expected to be granted the 'energyentry.view' role.")>
		<cfset assertEquals( ListLen(rolesList), 1,
			"Did not get back the expected number of roles.")>
	</cffunction>
</cfcomponent>