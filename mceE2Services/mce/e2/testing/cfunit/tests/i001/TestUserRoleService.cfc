<cfcomponent name="Test User Role Service"
			 extends="baseUserRoleTestingComponent" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the userRoleService component.">

	<!--- ToDo: [ADL] Add unit tests to test / validate adding roles to user groups. --->

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>

	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Get an instance of the user role service
			this.service = request.beanFactory.getBean("UserRoleService");
			this.delegate = request.beanFactory.getBean("UserRoleDelegate");
	
			// Setup the test data for the unit tests
			setupTestData();
				
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyUserRoleComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty user role component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the user role delegate --->
		<cfset local.userRole = this.service.getEmptyUserRoleVo()>

		<!--- Assert that the userRole object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.userRole, "mce.e2.vo.UserRole"), true, "local.userRole objectType is not of type mce.e2.vo.UserRole.")>
			
	</cffunction>

	<cffunction name="getApplicationUserRoles"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of all user roles from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the user role delegate --->
		<cfset local.userRoleCollection = this.service.getApplicationUserRoles()>
				
		<!--- Assert that the userRole array collection is an array --->
		<cfset assertTrue(isArray(local.userRoleCollection), "The value returned by this.service.getApplicationUserRoles() was not an array.")>

		<!--- Compare the user role properties and excersize retrieving single instances of user roles --->
		<cfset compareUserRoleProperties(local.userRoleCollection,'get','service')>
				
	</cffunction>

	<cffunction name="getUserRole"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single user role from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the user role delegate --->
		<cfset local.userRoleCollection = this.service.getApplicationUserRoles()>
				
		<!--- Loop through the user role collection, and retrieve each user role individually --->
		<cfloop from="1" to="#arraylen(local.userRoleCollection)#" index="local.arrayIndex">

			<!--- Create a local instance of the userRole to test --->
			<cfset local.userRole = local.userRoleCollection[local.arrayIndex]>

				<!--- Retrrive a single instance of a user role from the service --->
				<cfset local.testUserRole = this.service.getUserRole(local.userRole)>

			<!--- Assert that the userRole array collection is an array --->
			<cfset assertEquals(isInstanceOf(local.testUserRole, "mce.e2.vo.UserRole"), true, "local.testUserRole objectType is not of type mce.e2.vo.UserRole.")>
	
		</cfloop>
				
	</cffunction>

	<!--- Test the creation of user role data without specifying primary keys --->
	<cffunction name="saveAndValidateNewUserRolesWithoutPrimaryKeys"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of user roles using the saveNewUserRole() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user role objects --->
		<cfset local.newUserRoles = this.delegate.queryToVoArray(this.newUserRolesWithoutPks, 'mce.e2.vo.UserRole')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdUserRoles = arrayNew(1)/>
		
		<!--- Purge / remote the user role test data --->	
		<cfset purgeUserRoleTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserRoles)#" index="local.arrayIndex">	

			<!--- Save the user role to the application database --->
			<cfset local.testUserRolePrimaryKey = this.service.saveNewUserRole(local.newUserRoles[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testUserRolePrimaryKey), "The value returned by this.service.saveNewUserRole() was not a unique identifier.")/>
	
			<!--- Add the primary key to the user role --->
			<cfset local.testUserRole = local.newUserRoles[local.arrayIndex]>	
			<cfset local.testUserRole.user_role_uid = local.testUserRolePrimaryKey>
	
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdUserRoles, duplicate(local.testUserRole))/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdUserRoles), arrayLen(local.newUserRoles), "The total number of records processed does not match the total number of records iterated over when creating user role records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserRoleProperties(sourceObject=local.createdUserRoles, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the user role test data --->	
		<cfset purgeUserRoleTestData()> 			
							
	</cffunction>

	<!--- Test the creation of user role data --->
	<cffunction name="saveAndValidateNewUserRoles"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of user roles using the saveNewUserRole() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user role objects --->
		<cfset local.newUserRoles = this.delegate.queryToVoArray(this.newUserRoles, 'mce.e2.vo.UserRole')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		
		<!--- Purge / remote the user role test data --->	
		<cfset purgeUserRoleTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserRoles)#" index="local.arrayIndex">	

			<!--- Save the user role to the application database --->
			<cfset local.testUserRolePrimaryKey = this.service.saveNewUserRole(local.newUserRoles[local.arrayIndex])/>

			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.testUserRolePrimaryKey), "The value returned by this.service.saveNewUserRole() was not a unique identifier.")/>
	
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newUserRoles), "The total number of records processed does not match the total number of records iterated over when creating user role records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserRoleProperties(sourceObject=local.newUserRoles, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the user role test data --->	
		<cfset purgeUserRoleTestData()> 			
							
	</cffunction>
	
	<cffunction name="saveAndReturnAndValidateNewUserRoles"
				access="public" returnType="void"
				hint="This method is used to test / validate the creation of user roles using the saveAndReturnNewUserRole() method.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
					
		<!--- Build out the test user roles array --->			
		<cfset local.testUserRoles = arrayNew(1)>			
						
		<!--- Build out the array of user role objects --->
		<cfset local.newUserRoles = this.delegate.queryToVoArray(this.newUserRolesWithoutPks, 'mce.e2.vo.UserRole')/>
		
		<!--- Purge / remote the user role test data --->	
		<cfset purgeUserRoleTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserRoles)#" index="local.arrayIndex">	

			<!--- Save the user role to the application database --->
			<cfset local.testUserRole = this.service.saveAndReturnNewUserRole(local.newUserRoles[local.arrayIndex])/>
			
			<!--- Assert that the userRole object is of the correct type --->
			<cfset assertEquals(isInstanceOf(local.testUserRole, "mce.e2.vo.UserRole"), true, "local.userRole objectType is not of type mce.e2.vo.UserRole.")>
						
			<!--- Append the user role to the test array --->			
			<cfset arrayAppend(local.testUserRoles, local.testUserRole)>			

		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.testUserRoles), arrayLen(local.newUserRoles), "The total number of records processed does not match the total number of records iterated over when creating user role records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserRoleProperties(sourceObject=local.testUserRoles, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the user role test data --->	
		<cfset purgeUserRoleTestData()> 			
							
	</cffunction>	
	
	<cffunction name="saveAndValidateExistingUserRoles"
				access="public" returnType="void"
				hint="This method is used to test / validate the modification of user roles.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>			
						
		<!--- Build out the array of user role objects --->
		<cfset local.newUserRoles = this.delegate.queryToVoArray(this.newUserRoles, 'mce.e2.vo.UserRole')/>
		<cfset local.modifiedUserRoles = this.delegate.queryToVoArray(this.modifiedUserRoles, 'mce.e2.vo.UserRole')/>
					
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdCount = 0/>
		<cfset local.modifiedCount = 0/>
				
		<!--- Purge / remote the user role test data --->	
		<cfset purgeUserRoleTestData()>				
		
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newUserRoles)#" index="local.arrayIndex">	

			<!--- Save the user role to the application database --->
			<cfset this.service.saveNewUserRole(local.newUserRoles[local.arrayIndex])/>
			
			<!--- Increment the created count by one --->
			<cfset local.createdCount = local.createdCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(local.createdCount, arrayLen(local.newUserRoles), "The total number of records processed does not match the total number of records iterated over when creating user role records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserRoleProperties(sourceObject=local.newUserRoles, compareAction="create", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.modifiedUserRoles)#" index="local.arrayIndex">	

			<!--- Save the user role to the application database --->
			<cfset this.service.saveExistingUserRole(local.modifiedUserRoles[local.arrayIndex])/>
			
			<!--- Increment the modified count by one --->
			<cfset local.modifiedCount = local.modifiedCount + 1/>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(local.modifiedCount, arrayLen(local.modifiedUserRoles), "The total number of records processed does not match the total number of records iterated over when modifying user role records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset compareUserRoleProperties(sourceObject=local.modifiedUserRoles, compareAction="modify", compareType="service", ignorePropertyList="created_date,modified_date,created_by,modified_by")>

		<!--- Purge / remote the user role test data --->	
		<cfset purgeUserRoleTestData()> 			
							
	</cffunction>	

</cfcomponent>