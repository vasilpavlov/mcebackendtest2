<cfcomponent name="Test Property Collection Service"
			 extends="mxunit.framework.TestCase" 
			 output="true"
			 hint="This component contains the MX Unit tests used to test / validate the PropertyCollectionService component.">

	<!--- Kick off the constructor --->
	<cfif structkeyexists(request, 'beanFactory')><cfset init()></cfif>
	
	<!--- Initialize any setup / helper functions --->
	<cffunction name="init"
				hint="This method is used to setup the data that will be used during the execution of unit tests."
				access="private" returntype="void">
		
		<cfscript>
		
			// Create an instance of the baseClientCompany testing component (for company seed data)
			this.companyQAComponent = createObject('component', 'mce.e2.testing.cfunit.tests.TestClientCompanyService').init();

			// Create an instance of the user testing component (for property test data)	
			this.userQAComponent = createObject('component', 'mce.e2.testing.cfunit.tests.TestUserService').init();

			// Get an instance of the Client Company / user delegates
			this.companyDelegate = request.beanFactory.getBean("ClientCompanyDelegate");
			this.userDelegate = request.beanFactory.getBean("UserDelegate");
				
			// Create an instance of the property testing component (for property test data)	
			this.propertyQAComponent = createObject('component', 'mce.e2.testing.cfunit.tests.TestPropertyService').init();
				
			// Get an instance of the property service
			this.propertyService = request.beanFactory.getBean("propertyService");
			
			// Get an instance of the security service
			this.securityService = request.beanFactory.getBean("securityService");			
					
			// Get an instance of the Property Collection service
			this.service = request.beanFactory.getBean("PropertyCollectionService");
			this.delegate = request.beanFactory.getBean("PropertyCollectionDelegate");
	
			//Build out the Property Collection test data (for create / modify scenarios)
			createPropertyCollectionTestData();
				
		</cfscript>		
				
	</cffunction>

	<!--- Test Retrieval of the empty value objects --->
	<cffunction name="getEmptyPropertyCollectionComponent" 
				output="false" returnType="void"
				hint="This method is used to test the retrieval of an empty Property Collection component.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Initialize the Property Collection delegate --->
		<cfset local.PropertyCollection = this.service.getEmptyPropertyCollectionVo()>

		<!--- Assert that the PropertyCollection object is of the correct type --->
		<cfset assertEquals(isInstanceOf(local.PropertyCollection, "mce.e2.vo.PropertyCollection"), true, "local.PropertyCollection objectType is not of type mce.e2.vo.PropertyCollection.")>
			
	</cffunction>

	<cffunction name="getAssociatedPropertyCollections"
				access="public" returntype="void"
				hint="This method is used to test the retrieval of associated property collections.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Purge / remote the property collection test data --->	
		<cfset purgePropertyCollectionTestData()>		
	
		<!--- Create the root / seed client company record --->
		<cfset createSeedClientCompany()>			
				
		<!--- Build out the array of Property Collection value objects --->
		<cfset local.newPropertyCollections = this.delegate.queryToVoArray(this.newPropertyCollections, 'mce.e2.vo.PropertyCollection')/>
				
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdPropertyCollections = arrayNew(1)/>
				
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newPropertyCollections)#" index="local.arrayIndex">	

			<!--- Save the Property Collection to the application database --->
			<cfset this.service.saveNewPropertyCollection(local.newPropertyCollections[local.arrayIndex])/>
						
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdPropertyCollections, duplicate(local.newPropertyCollections[local.arrayIndex]))>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdPropertyCollections), arrayLen(local.newPropertyCollections), "The total number of records processed does not match the total number of records iterated over when creating Property Collection records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyCollectionProperties(sourceObject=local.createdPropertyCollections, compareAction="create", compareType="service", ignorePropertyList="created_by,created_date,modified_by,modified_date")>
				
		<!--- Retrieve the collections --->
		<cfset local.propertyCollections = this.service.getAssociatedPropertyCollections()>
				
		<!--- Assert that the property array collection is an array --->
		<cfset assertTrue(isArray(local.propertyCollections), "The value returned by this.service.getPropertyCollections() was not an array.")>

		<!--- Compare the property properties and excersize retrieving single instances of properties --->
		<cfset comparePropertyCollectionProperties(local.propertyCollections,'get','service')>	
				
		<!--- Purge / remote the user group test data --->	
		<cfset purgePropertyCollectionTestData()>		
						
	</cffunction>

	<cffunction name="getUnAssociatedPropertyCollections"
				access="public" returntype="void"
				hint="This method is used to test the retrieval of unassociated property collections.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Purge / remote the property collection test data --->	
		<cfset purgePropertyCollectionTestData()>		
	
		<!--- Create the root / seed client company record --->
		<cfset createSeedClientCompany()>			
				
		<!--- Build out the array of Property Collection value objects --->
		<cfset local.newPropertyCollections = this.delegate.queryToVoArray(this.newPropertyCollections, 'mce.e2.vo.PropertyCollection')/>
				
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdPropertyCollections = arrayNew(1)/>
				
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newPropertyCollections)#" index="local.arrayIndex">	

			<!--- Save the Property Collection to the application database --->
			<cfset this.service.saveNewPropertyCollection(local.newPropertyCollections[local.arrayIndex])/>
						
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdPropertyCollections, duplicate(local.newPropertyCollections[local.arrayIndex]))>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdPropertyCollections), arrayLen(local.newPropertyCollections), "The total number of records processed does not match the total number of records iterated over when creating Property Collection records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyCollectionProperties(sourceObject=local.createdPropertyCollections, compareAction="create", compareType="service", ignorePropertyList="created_by,created_date,modified_by,modified_date")>
				
		<!--- Retrieve the collections --->
		<cfset local.propertyCollections = this.service.getUnAssociatedPropertyCollections()>
				
		<!--- Assert that the property array collection is an array --->
		<cfset assertTrue(isArray(local.propertyCollections), "The value returned by this.service.getPropertyCollections() was not an array.")>

		<!--- Compare the property properties and excersize retrieving single instances of properties --->
		<cfset comparePropertyCollectionProperties(local.propertyCollections,'get','service')>	
				
		<!--- Purge / remote the user group test data --->	
		<cfset purgePropertyCollectionTestData()>		
						
	</cffunction>

	<cffunction name="getPropertyCollection"
				output="false" returnType="void"
				hint="This method is used to test the retrieval of a single property collection from the application database.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()>
		
		<!--- Purge / remote the property collection test data --->	
		<cfset purgePropertyCollectionTestData()>		
		
		<!--- Create the root / seed client company record --->
		<cfset createSeedClientCompany()>			
		
		<!--- Build out the array of Property Collection value objects --->
		<cfset local.newPropertyCollections = this.delegate.queryToVoArray(this.newPropertyCollections, 'mce.e2.vo.PropertyCollection')/>
				
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdPropertyCollections = arrayNew(1)/>
				
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newPropertyCollections)#" index="local.arrayIndex">	

			<!--- Save the Property Collection to the application database --->
			<cfset this.service.saveNewPropertyCollection(local.newPropertyCollections[local.arrayIndex])/>
									
		</cfloop>		
		
		<!--- Retrieve the collection of objects --->
		<cfset local.propertyCollections = this.service.getClientCompanyPropertyCollections(this.seedCompany)>
				
		<!--- Loop through the property collection, and retrieve each object individually --->
		<cfloop from="1" to="#arraylen(local.propertyCollections)#" index="local.arrayIndex">

			<!--- Create a local instance of the object to test --->
			<cfset local.property = local.propertyCollections[local.arrayIndex]>

			<!--- Retrrive a single instance of the object from the service --->
			<cfset local.testPropertyCollection = this.service.getPropertyCollection(local.property)>

			<!--- Assert that the object type matches / is accurate --->
			<cfset assertEquals(isInstanceOf(local.testPropertyCollection, "mce.e2.vo.PropertyCollection"), true, "local.testPropertyCollection objectType is not of type mce.e2.vo.PropertyCollection.")>
	
		</cfloop>
				
	</cffunction>

	<!--- Test data creation / modification / retrieval actions involving the application database --->	
	<cffunction name="saveAndValidateNewPropertyCollections"
				access="public" returntype="void"
				hint="This method is used to create new property collections using the method saveNewPropertyCollection(), save them, and then validate that they were saved using the component-defined Property Collection data.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
		
		<!--- Remove the test data --->
		<cfset purgePropertyCollectionTestData()>		
		
		<!--- Create the root / seed client company record --->
		<cfset createSeedClientCompany()>
				
		<!--- Build out the array of Property Collection value objects --->
		<cfset local.newPropertyCollections = this.delegate.queryToVoArray(this.newPropertyCollections, 'mce.e2.vo.PropertyCollection')/>
				
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdPropertyCollections = arrayNew(1)/>
				
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newPropertyCollections)#" index="local.arrayIndex">	

			<!--- Save the Property Collection to the application database --->
			<cfset this.service.saveNewPropertyCollection(local.newPropertyCollections[local.arrayIndex])/>
						
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdPropertyCollections, duplicate(local.newPropertyCollections[local.arrayIndex]))>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdPropertyCollections), arrayLen(local.newPropertyCollections), "The total number of records processed does not match the total number of records iterated over when creating Property Collection records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyCollectionProperties(sourceObject=local.createdPropertyCollections, compareAction="create", compareType="service", ignorePropertyList="created_by,created_date,modified_by,modified_date")>

		<!--- Remove the test data --->
		<cfset purgePropertyCollectionTestData()>

	</cffunction>	
	
	<cffunction name="saveAndValidateNewPropertyCollectionsWithoutPrimaryKeys"
				access="public" returntype="void"
				hint="This method is used to create new property collections without specifying a primary key using the method saveNewPropertyCollection(), save them, and then validate that they were saved using the component-defined Property Collection data.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>

		<!--- Remove the test data --->
		<cfset purgePropertyCollectionTestData()>

		<!--- Create the root / seed client company record --->
		<cfset createSeedClientCompany()>

		<!--- Build out the array of Property Collection value objects --->
		<cfset local.newPropertyCollections = this.delegate.queryToVoArray(this.newPropertyCollectionsWithoutPks, 'mce.e2.vo.PropertyCollection')/>
				
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdPropertyCollections = arrayNew(1)/>
				
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newPropertyCollections)#" index="local.arrayIndex">	

			<!--- Save the Property Collection to the application database --->
			<cfset local.thisPrimaryKey = this.service.saveNewPropertyCollection(local.newPropertyCollections[local.arrayIndex])/>
			
			<!--- Validate that the returned value is a unique identifier --->
			<cfset assertTrue(this.delegate.isUniqueIdentifier(local.thisPrimaryKey), "The value returned by this.service.saveNewPropertyCollection() was not a unique identifier.")/>
						
			<!--- Set the primary key for the object --->
			<cfset local.thisPropertyCollection = local.newPropertyCollections[local.arrayIndex]>
			<cfset local.thisPropertyCollection.property_collection_uid = local.thisPrimaryKey>
			
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdPropertyCollections, duplicate(local.thisPropertyCollection))>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdPropertyCollections), arrayLen(local.newPropertyCollections), "The total number of records processed does not match the total number of records iterated over when creating Property Collection records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyCollectionProperties(sourceObject=local.createdPropertyCollections, compareAction="create", compareType="service", ignorePropertyList="created_by,created_date,modified_by,modified_date")>

		<!--- Remove the test data --->
		<!--- <cfset purgePropertyCollectionTestData()> --->

	</cffunction>
		
	<cffunction name="saveAndReturnAndValidateNewPropertyCollections"
				access="public" returntype="void"
				hint="This method is used to create new property collections using the method saveAndReturnNewPropertyCollection(), save them, and then validate that they were saved using the component-defined Property Collection data.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
		
		<!--- Remove the test data --->
		<cfset purgePropertyCollectionTestData()>		
		
		<!--- Create the root / seed client company record --->
		<cfset createSeedClientCompany()>		
		
		<!--- Build out the array of Property Collection value objects --->
		<cfset local.newPropertyCollections = this.delegate.queryToVoArray(this.newPropertyCollections, 'mce.e2.vo.PropertyCollection')/>
				
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdPropertyCollections = arrayNew(1)/>
				
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newPropertyCollections)#" index="local.arrayIndex">	

			<!--- Save the Property Collection to the application database --->
			<cfset local.thisPropertyCollection = this.service.saveAndReturnNewPropertyCollection(local.newPropertyCollections[local.arrayIndex])/>
						
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdPropertyCollections, duplicate(local.thisPropertyCollection))>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdPropertyCollections), arrayLen(local.newPropertyCollections), "The total number of records processed does not match the total number of records iterated over when creating Property Collection records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyCollectionProperties(sourceObject=local.createdPropertyCollections, compareAction="create", compareType="service", ignorePropertyList="created_by,created_date,modified_by,modified_date")>

		<!--- Remove the test data --->
		<cfset purgePropertyCollectionTestData()>

	</cffunction>		
	
	<cffunction name="saveAndValidateExistingPropertyCollections"
				access="public" returntype="void"
				hint="This method is used to create new property collections using the method saveNewPropertyCollection(), save them, and then validate that they were saved using the component-defined Property Collection data.  Once saved, they will be modified using the method saveExistingPropertyCollection(), and then validated.">
				
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
		
		<!--- Remove the test data --->
		<cfset purgePropertyCollectionTestData()>		
		
		<!--- Create the root / seed client company record --->
		<cfset createSeedClientCompany()>		
		
		<!--- Build out the array of Property Collection value objects --->
		<cfset local.newPropertyCollections = this.delegate.queryToVoArray(this.newPropertyCollections, 'mce.e2.vo.PropertyCollection')/>
		<cfset local.editedPropertyCollections = this.delegate.queryToVoArray(this.modifiedPropertyCollections, 'mce.e2.vo.PropertyCollection')/>
				
		<!--- Set the counts to measure how many records were modified / verified --->
		<cfset local.createdPropertyCollections = arrayNew(1)/>
		<cfset local.modifiedPropertyCollections = arrayNew(1)/>
				
		<!--- Step 1:  Create the data --->
		<!--- Loop over the array of value objects, and insert each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.newPropertyCollections)#" index="local.arrayIndex">	

			<!--- Save the Property Collection to the application database --->
			<cfset this.service.saveNewPropertyCollection(local.newPropertyCollections[local.arrayIndex])/>
						
			<!--- Increment the created count by one --->
			<cfset arrayAppend(local.createdPropertyCollections, duplicate(local.newPropertyCollections[local.arrayIndex]))>
			
		</cfloop>
		
		<!--- Validate that the total records created equals the records processed --->
		<cfset assertEquals(arrayLen(local.createdPropertyCollections), arrayLen(local.newPropertyCollections), "The total number of records processed does not match the total number of records iterated over when creating Property Collection records.")/>
				
		<!--- Step 2:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyCollectionProperties(sourceObject=local.createdPropertyCollections, compareAction="create", compareType="service", ignorePropertyList="created_by,created_date,modified_by,modified_date")>

		<!--- Step 3:  Modify the data --->
		<!--- Loop over the array of value objects, and modify each object instance into the application database --->
		<cfloop from="1" to="#arrayLen(local.editedPropertyCollections)#" index="local.arrayIndex">	

			<!--- Save the Property Collection to the application database --->
			<cfset this.service.saveExistingPropertyCollection(local.editedPropertyCollections[local.arrayIndex])/>
						
			<!--- Increment the modified count by one --->
			<cfset arrayAppend(local.modifiedPropertyCollections, duplicate(local.editedPropertyCollections[local.arrayIndex]))>
			
		</cfloop>
		
		<!--- Validate that the total records modified equals the records processed --->
		<cfset assertEquals(arrayLen(local.modifiedPropertyCollections), arrayLen(local.editedPropertyCollections), "The total number of records processed does not match the total number of records iterated over when creating Property Collection records.")/>
				
		<!--- Step 4:  Validate the data for each vo *.cfc and compare source / target properties --->
		<cfset comparePropertyCollectionProperties(sourceObject=local.modifiedPropertyCollections, compareAction="create", compareType="service", ignorePropertyList="created_by,created_date,modified_by,modified_date")>

		 Remove the test data --->
		 <cfset purgePropertyCollectionTestData()> 

	</cffunction>		

	<cffunction name="saveNewPropertyToPropertyCollection"
				access="public"
				returnType="void"
				hint="This method is used to test the creation of associations between properties and property collections.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()/>

		<!--- Remove the test data --->
		<cfset purgePropertyCollectionTestData()>	

		<!--- Create the root / seed client company record --->
		<cfset createSeedClientCompany()>
				
		<!--- Build out the array of Property Collection value objects --->
		<cfset local.newPropertyCollections = this.delegate.queryToVoArray(this.newPropertyCollections, 'mce.e2.vo.PropertyCollection')/>

		<!--- Build out the array of properties to be associated to the selected property collection --->
		<cfset local.newProperties = this.delegate.queryToVoArray(this.propertyQAComponent.newProperties, 'mce.e2.vo.Property')>
				
		<!--- Save the Property Collection to the application database --->
		<cfset this.service.saveNewPropertyCollection(local.newPropertyCollections[1])/>

		<!--- loop over the new properties --->
		<cfloop from="1" to="#arrayLen(local.newProperties)#" index="local.rowIndex">
		
			<!--- Save the properties --->
			<cfset this.propertyService.saveNewProperty(local.newProperties[local.rowIndex])>
		
			<!--- Save the property collection --->
			<cfset this.service.associatePropertyToPropertyCollection(
					propertyCollection=local.newPropertyCollections[1],
					property=local.newProperties[local.rowIndex])>

		</cfloop>						

		<!--- Retrieve the properties in a given property collection --->
		<cfset local.retrievedProperties = this.service.getPropertiesInACollection(
					propertyCollection=local.newPropertyCollections[1])>

		<!--- Validate that the count of associated properties equals the count of created properties --->
		<cfset assertEquals(arrayLen(local.newProperties), arrayLen(local.retrievedProperties), "The count of created properties does not equal the count of associated properties.")>

		<!--- Remove the test data --->
		<cfset purgePropertyCollectionTestData()>	
		<cfset this.propertyQAComponent.purgePropertyTestData()>

	</cffunction>
	
	<cffunction name="RemovePropertyFromPropertyCollection"
				access="public"
				returnType="void"
				hint="This method is used to test the removal / deletion of associations between properties and property collections.">

		<!--- Initialize local variables --->
		<cfset var local = structNew()/>

		<!--- Remove the test data --->
		<cfset purgePropertyCollectionTestData()>	

		<!--- Create the root / seed client company record --->
		<cfset createSeedClientCompany()>
				
		<!--- Build out the array of Property Collection value objects --->
		<cfset local.newPropertyCollections = this.delegate.queryToVoArray(this.newPropertyCollections, 'mce.e2.vo.PropertyCollection')/>

		<!--- Build out the array of properties to be associated to the selected property collection --->
		<cfset local.newProperties = this.delegate.queryToVoArray(this.propertyQAComponent.newProperties, 'mce.e2.vo.Property')>
				
		<!--- Save the Property Collection to the application database --->
		<cfset this.service.saveNewPropertyCollection(local.newPropertyCollections[1])/>

		<!--- loop over the new properties --->
		<cfloop from="1" to="#arrayLen(local.newProperties)#" index="local.rowIndex">
		
			<!--- Save the properties --->
			<cfset this.propertyService.saveNewProperty(local.newProperties[local.rowIndex])>
		
			<!--- Save the property collection --->
			<cfset this.service.associatePropertyToPropertyCollection(
					propertyCollection=local.newPropertyCollections[1],
					property=local.newProperties[local.rowIndex])>		
		
		</cfloop>						

		<!--- Retrieve the properties in a given property collection --->
		<cfset local.testProperties = this.service.getPropertiesInACollection(
					propertyCollection=local.newPropertyCollections[1])>

		<!--- loop over the new properties --->
		<cfloop from="1" to="#arrayLen(local.testProperties)#" index="local.rowIndex">

			<!--- Remove the property collection --->
			<cfset this.service.disAssociatePropertyFromPropertyCollection(
					propertyCollection=local.newPropertyCollections[1],
					property=local.newProperties[local.rowIndex])>

		</cfloop>						

		<!--- Retrieve the properties in a given property collection --->
		<cfset local.retrievedProperties = this.service.getPropertiesInACollection(
					propertyCollection=local.newPropertyCollections[1])>

		<!--- Validate that the count of associated properties equals the count of created properties --->
		<cfset assertEquals(arrayLen(local.retrievedProperties), 0, "The count of removed properties does not equal zero (all properties were added and then removed; no property associations should exist).")>

		<!--- Remove the test data --->
		<cfset purgePropertyCollectionTestData()>	
		<cfset this.propertyQAComponent.purgePropertyTestData()>

	</cffunction>	

	<!--- Private methods / for internal use only --->
	<cffunction name="purgePropertyCollectionTestData"
				access="private"
				hint="This method is used to purge / delete any PropertyCollection data related to the test data being used when Property Collection unit tests are exersized.">
				
		<!--- Define the arguments for the current method --->		
		<cfargument name="PropertyCollections" type="query" hint="This argument is used to identify the Property Collection data that will be purged from the application database." >		
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>		
		
		<cfquery name="local.purgePropertyCollectionData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.UserGroups_PropertyCollections
			where	property_collection_uid in ( 
		
					select	property_collection_uid
					from	dbo.PropertyCollections
					where	collection_type_code in ( 
				
								<!--- Create a value list of the collection type codes that are in use by this component --->	
								<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newPropertyCollections.collection_type_code)#">
					
							)

					)

		</cfquery>		
		
		<cfquery name="local.purgePropertyCollectionData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.propertyCollections_Users
			where	property_collection_uid in ( 
		
					select	property_collection_uid
					from	dbo.PropertyCollections
					where	collection_type_code in ( 
				
								<!--- Create a value list of the collection type codes that are in use by this component --->	
								<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newPropertyCollections.collection_type_code)#">
					
							)

					)

		</cfquery>					
			
		<cfquery name="local.purgePropertyCollectionData" datasource="#this.delegate.datasource#">

			delete
			from	dbo.Properties_PropertyCollections
			where	property_collection_uid in ( 
		
					select	property_collection_uid
					from	dbo.PropertyCollections
					where	collection_type_code in ( 
				
								<!--- Create a value list of the collection type codes that are in use by this component --->	
								<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newPropertyCollections.collection_type_code)#">
					
							)

					)

		</cfquery>			

		<!--- Create a query to purge Property Collection value data --->
		<cfquery name="local.purgePropertyCollectionData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.properties
			where	property_uid in (
			
				select	property_uid
				from	dbo.Properties_PropertyCollections
				where	property_collection_uid in ( 
		
						select	property_collection_uid
						from	dbo.PropertyCollections
						where	collection_type_code in ( 
					
									<!--- Create a value list of the collection type codes that are in use by this component --->	
									<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newPropertyCollections.collection_type_code)#">
						
								)
				
					)
			
			)

		</cfquery>

		<cfquery name="local.purgePropertyCollectionData" datasource="#this.delegate.datasource#">
							
			delete
			from	dbo.PropertyCollections
			where	collection_type_code in ( 
		
						<!--- Create a value list of the collection type codes that are in use by this component --->	
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.newPropertyCollections.collection_type_code)#">
			
					)

		</cfquery>

		<cfquery name="local.purgePropertyCollectionData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.UserSessions
			where	user_uid in (
			
							select	user_uid
							from	dbo.users
							where	username in (
				
								<!--- Create a value list of the client company friendly names --->	
								<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.userQAComponent.newUsers.username)#">

						)
						
					)
		
		</cfquery>

		<cfquery name="local.purgePropertyCollectionData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.Users
			where	username in (
			
						<!--- Create a value list of the client company friendly names --->	
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.userQAComponent.newUsers.username)#">
						
					)
		
		</cfquery>

		<cfquery name="local.purgePropertyCollectionData" datasource="#this.delegate.datasource#">
		
			delete
			from	dbo.clientCompanies
			where	friendly_name in (
			
						<!--- Create a value list of the client company friendly names --->	
						<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#valuelist(this.companyQAComponent.newClientCompanies.friendly_name)#">
						
					)
		
		</cfquery>
				
	</cffunction>			
	
	<cffunction name="CreateSeedClientCompany"
				access="private"
				hint="This method is used to setup client company test data for the PropertyCollection service and delegate tests.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>		
				
		<cfscript>
		
			// Convert the client company query to an array of Vo objects
			local.clientCompanyCollection = this.companyDelegate.queryToVoArray(this.companyQAComponent.newClientCompanies, 'mce.e2.vo.ClientCompany');
			
			// Define the seed company
			this.seedCompany = local.clientCompanyCollection[1];
			
			// Save the client company 
			this.companyDelegate.saveNewClientCompany(local.clientCompanyCollection[1]);

			// Convert the user query to an array of Vo objects
			local.userCollection = this.userDelegate.queryToVoArray(this.userQAComponent.newUsers, 'mce.e2.vo.User');
			
			// Set the user's client company to the client company primary key that was just saved
			local.userCollection[1].client_company_uid = local.clientCompanyCollection[1].client_company_uid;
			
			// Save the new user to the database
			this.userDelegate.saveNewUser(local.userCollection[1], local.userCollection[1].password);

			// Create a new session for the user
			local.session = this.securityService.attemptLogin(local.userCollection[1].username,local.userCollection[1].password);

		</cfscript>		
				
	</cffunction>
	
	<cffunction name="createPropertyCollectionTestData"
				access="private" output="true"
				hint="This method is used to create the Property Collection test data for testing Property Collection records (create and update actions).">
	
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<cfscript>			
						
			// Initialize a query containing Property Collection data to be tested with (the query should contain all of the columns of the base table)
			this.newPropertyCollections = queryNew('client_company_uid,property_collection_uid,collection_hierarchyid,friendly_name,collection_type_code,is_active,created_by,created_date,modified_by,modified_date');	
						
			// Add a row to the query (instance #1)
			queryAddRow(this.newPropertyCollections,4);
		
			// Set the cell values for this instance of a Property Collection
			querySetCell(this.newPropertyCollections,'client_company_uid', this.companyQAComponent.newClientCompanies.client_company_uid[1], 1);
			querySetCell(this.newPropertyCollections,'property_collection_uid', this.delegate.createUniqueIdentifier(), 1);
			querySetCell(this.newPropertyCollections,'collection_hierarchyid', '/', 1);
			querySetCell(this.newPropertyCollections,'collection_type_code', 'pc_001', 1);
			querySetCell(this.newPropertyCollections,'friendly_name', 'PC 001', 1);
			querySetCell(this.newPropertyCollections,'is_active', 1, 1);
			querySetCell(this.newPropertyCollections,'created_by', 'MX Unit (created)', 1);
			querySetCell(this.newPropertyCollections,'created_date', createOdbcDateTime(now()), 1);
			querySetCell(this.newPropertyCollections,'modified_by', 'MX Unit (created)', 1);
			querySetCell(this.newPropertyCollections,'modified_date', createOdbcDateTime(now()), 1);
					
			// Set the cell values for this instance of a Property Collection
			querySetCell(this.newPropertyCollections,'client_company_uid', this.companyQAComponent.newClientCompanies.client_company_uid[1], 2);
			querySetCell(this.newPropertyCollections,'property_collection_uid', this.delegate.createUniqueIdentifier(), 2);
			querySetCell(this.newPropertyCollections,'collection_hierarchyid', '/', 2);
			querySetCell(this.newPropertyCollections,'collection_type_code', 'PC_002', 2);
			querySetCell(this.newPropertyCollections,'friendly_name', 'PC 002', 2);
			querySetCell(this.newPropertyCollections,'is_active', 1, 2);
			querySetCell(this.newPropertyCollections,'created_by', 'MX Unit (created)', 2);
			querySetCell(this.newPropertyCollections,'created_date', createOdbcDateTime(now()), 2);
			querySetCell(this.newPropertyCollections,'modified_by', 'MX Unit (created)', 2);
			querySetCell(this.newPropertyCollections,'modified_date', createOdbcDateTime(now()), 2);
		
			// Set the cell values for this instance of a Property Collection
			querySetCell(this.newPropertyCollections,'client_company_uid', this.companyQAComponent.newClientCompanies.client_company_uid[1], 3);
			querySetCell(this.newPropertyCollections,'property_collection_uid', this.delegate.createUniqueIdentifier(), 3);
			querySetCell(this.newPropertyCollections,'collection_hierarchyid', '/', 3);
			querySetCell(this.newPropertyCollections,'collection_type_code', 'PC_003', 3);
			querySetCell(this.newPropertyCollections,'friendly_name', 'PC 003', 3);
			querySetCell(this.newPropertyCollections,'is_active', 1, 3);
			querySetCell(this.newPropertyCollections,'created_by', 'MX Unit (created)', 3);
			querySetCell(this.newPropertyCollections,'created_date', createOdbcDateTime(now()), 3);
			querySetCell(this.newPropertyCollections,'modified_by', 'MX Unit (created)', 3);
			querySetCell(this.newPropertyCollections,'modified_date', createOdbcDateTime(now()), 3);
		
			// Set the cell values for this instance of a Property Collection
			querySetCell(this.newPropertyCollections,'client_company_uid', this.companyQAComponent.newClientCompanies.client_company_uid[1], 4);
			querySetCell(this.newPropertyCollections,'property_collection_uid', this.delegate.createUniqueIdentifier(), 4);
			querySetCell(this.newPropertyCollections,'collection_hierarchyid', '/', 4);
			querySetCell(this.newPropertyCollections,'collection_type_code', 'PC_004', 4);
			querySetCell(this.newPropertyCollections,'friendly_name', 'PC 004', 4);
			querySetCell(this.newPropertyCollections,'is_active', 1, 4);
			querySetCell(this.newPropertyCollections,'created_by', 'MX Unit (created)', 4);
			querySetCell(this.newPropertyCollections,'created_date', createOdbcDateTime(now()), 4);
			querySetCell(this.newPropertyCollections,'modified_by', 'MX Unit (created)', 4);
			querySetCell(this.newPropertyCollections,'modified_date', createOdbcDateTime(now()), 4);

			// Duplicate the new Property Collections to create the property collections without primary keys
			this.newPropertyCollectionsWithoutPKs = duplicate(this.newPropertyCollections);

			// Update the friendly names for the modified Property Collections
			querySetCell(this.newPropertyCollectionsWithoutPKs,'property_collection_uid', '', 1);
			querySetCell(this.newPropertyCollectionsWithoutPKs,'property_collection_uid', '', 2);
			querySetCell(this.newPropertyCollectionsWithoutPKs,'property_collection_uid', '', 3);			
			querySetCell(this.newPropertyCollectionsWithoutPKs,'property_collection_uid', '', 4);	

			// Duplicate the new Property Collections to create the baseline modified Property Collections
			this.modifiedPropertyCollections = duplicate(this.newPropertyCollections);

			// Update the friendly names for the modified Property Collections
			querySetCell(this.modifiedPropertyCollections,'friendly_name', 'PCM 001', 1);
			querySetCell(this.modifiedPropertyCollections,'friendly_name', 'PCM 002', 2);
			querySetCell(this.modifiedPropertyCollections,'friendly_name', 'PCM 003', 3);			
			querySetCell(this.modifiedPropertyCollections,'friendly_name', 'PCM 004', 4);			
			
			// Update the modified by property
			querySetCell(this.modifiedPropertyCollections,'modified_by', 'MX Unit (modified)', 1);
			querySetCell(this.modifiedPropertyCollections,'modified_by', 'MX Unit (modified)', 2);
			querySetCell(this.modifiedPropertyCollections,'modified_by', 'MX Unit (modified)', 3);
			querySetCell(this.modifiedPropertyCollections,'modified_by', 'MX Unit (modified)', 4);
		
		</cfscript>
 				
	</cffunction>
	
	<cffunction name="comparePropertyCollectionProperties"
				access="private"
				hint="This method is used to compare the properties for a given Property Collection vo to the values stored in the application database belonging to the same record.">
	
		<!--- Define the arguments for this method --->
		<cfargument name="sourceObject" type="array" required="true" hint="Describes the array collection of Property Collection vo's to be compared.">
		<cfargument name="compareAction" type="string" required="true" hint="Describes the type of comparison being performed (create / modify).">
		<cfargument name="compareType" type="string" required="true" default="delegate" hint="Describes what *.cfc performs the retrieval prior to the comparison being performed (service / delegate).">
		<cfargument name="ignorePropertyList" type="string" required="true" default="" hint="Describes a comma delimited list of properties that should be ignored when performing the property comparison.">
	
		<!--- Initialize local variables --->
		<cfset var local = structNew()/>
				
		<!--- Loop over the array of value objects, and select / verify each object from the application database --->
		<cfloop from="1" to="#arrayLen(arguments.sourceObject)#" index="local.arrayIndex">	

			<!--- Retrieve the compare record using the delegate --->
			<cfif arguments.compareType eq 'delegate'>

				<!--- Retrieve the target comparison object from the application database --->
				<cfset local.retrievedPropertyCollection = this.delegate.getPropertyCollectionAsComponent(property_collection_uid=arguments.sourceObject[local.arrayIndex].property_collection_uid)/>
			
			<!--- Retrieve the compare record using the service --->
			<cfelseif arguments.compareType eq 'service'>

				<!--- Retrieve the target comparison object from the application database --->
				<cfset local.retrievedPropertyCollection = this.service.getPropertyCollection(arguments.sourceObject[local.arrayIndex])/>
			
			</cfif>
						
 			<!--- Loop over the properties of the source object, and validate the exist in the retrieved object --->
			<cfloop collection="#arguments.sourceObject[local.arrayIndex]#" item="local.thisKey">

				<!--- Only compare / evaluate properties that are not in the ignore list --->
				<cfif not listFindNoCase(arguments.ignorePropertyList, local.thisKey)>

					<!--- Define the error message to be rendered when comparing property values --->
					<cfset local.errorMessage = "The values attached to the *.cfc property [#local.thisKey#] are not equal between the compared rateFactor VO's [testing #arguments.compareAction#].">		
				
					<!--- Validate that the total records created equals the records processed --->
					<cfset assertTrue(structKeyExists(local.retrievedPropertyCollection, local.thisKey), "The *.cfc property [#local.thisKey#] was not found in the retrieved PropertyCollection VO component [#arguments.compareAction#].")/>
			
					<!--- Process different comparison logic for date fields --->
					<cfif local.thisKey eq 'created_date' or local.thisKey eq 'modified_date'>
					
						<!--- Massage the retrieved date from the database, and pull off the miliseconds --->
						<cfset local.newDate = this.delegate.removeMilisecondsFromDate(arguments.sourceObject[local.arrayIndex][local.thisKey])>										
					
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(createOdbcDateTime(local.retrievedPropertyCollection[local.thisKey])), trim(local.newDate), local.errorMessage)/>
									
					<cfelse>
		
						<!--- Compare the retrieved property against the source property, and see if their values are the same --->
						<cfset assertEquals(trim(local.retrievedPropertyCollection[local.thisKey]), trim(arguments.sourceObject[local.arrayIndex][local.thisKey]), local.errorMessage)/>

					</cfif>

				</cfif>
			
			</cfloop> 
			
		</cfloop>
								
	</cffunction>

</cfcomponent>