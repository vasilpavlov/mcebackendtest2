<cfcomponent output="false">
	<cffunction name="init">
		<cfargument name="datasource">
		<cfset this.datasource = arguments.datasource>
		<cfreturn this>
	</cffunction>


	<cffunction name="getDataMgr">
 		<cfif not isDefined("application.e2.DataMgr")>
			<cfset application.e2.DataMgr = createObject("component", "datamgr.DataMgr").init(this.datasource)>
		</cfif>
		<cfreturn application.e2.DataMgr>
	</cffunction>

</cfcomponent>