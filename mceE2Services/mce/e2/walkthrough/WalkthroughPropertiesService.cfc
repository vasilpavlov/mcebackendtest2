<cfcomponent extends="mce.e2.service.BaseDataService" output="false">

	<cffunction name="getPropertiesList" returntype="array">
		<cfreturn this.delegate.getPropertiesAsVoArray()>
	</cffunction>

	<cffunction name="saveProperty" returntype="void">
		<cfargument name="propertyVo" type="mce.e2.walkthrough.WalkthroughPropertyVo" required="true">
		<cfreturn this.delegate.savePropertyVo(propertyVo)>
	</cffunction>


	<!--- Called automatically by Coldspring when it creates the instance of this CFC. --->
	<!--- This works because "walkthroughPropertiesDelegate" is defined as a bean in Coldspring --->
	<!--- Coldspring therefore "sees" this setter and calls it for us (this is called autowiring) --->
	<cffunction name="setWalkthroughPropertiesDelegate" hint="dependency injection">
		<cfargument name="bean">
		<cfset this.delegate = bean>
	</cffunction>

</cfcomponent>