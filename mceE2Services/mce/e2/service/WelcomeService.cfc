<cfcomponent displayname="Welcome Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the E2Track welcom page.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setWelcomeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Welcome page data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.WelcomeDelegate" hint="Describes the *.cfc used to manage database interactions related to Welcome page information.">
		<cfset this.WelcomeDelegate = arguments.bean>
	</cffunction>
	

	<cffunction name="getPicks" 
				access="public" returntype="query"
				hint="This method is used to retrieve / return a collection of MCE Picks.">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="rowLimit" required="false" type="numeric" hint="Describes the number of rows to return."/>

		<cfreturn this.WelcomeDelegate.getPicks(arguments.rowLimit)>
	</cffunction>

	<cffunction name="getLastUpdate" 
				access="public" returntype="query"
				hint="This method is used to retrieve the last update date.">
		<cfreturn this.WelcomeDelegate.getLastUpdate()>
	</cffunction>

	<cffunction name="getSparklineSummary" 
				access="public" returntype="query"
				hint="This method is used to retrieve / return a collection of MCE Picks.">
		<cfreturn this.WelcomeDelegate.getSparklineSummary()>
	</cffunction>

</cfcomponent>