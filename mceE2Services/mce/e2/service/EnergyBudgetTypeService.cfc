<cfcomponent displayname="Energy Budget Type Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Energy Budget Type information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyBudgetTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Energy Budget Type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyBudgetTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to Energy Budget Type information.">
		<cfset this.EnergyBudgetTypeDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyBudgetTypeVo" 
				access="public" returntype="mce.e2.vo.EnergyBudgetType"
				hint="This method is used to retrieve / return an empty EnergyBudgetType information value object.">
		<cfreturn this.EnergyBudgetTypeDelegate.getEmptyEnergyBudgetTypeComponent()>
	</cffunction>

	<cffunction name="getAvailableEnergyBudgetTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Energy Budget Types supported by the application.">
		<cfreturn this.EnergyBudgetTypeDelegate.getEnergyBudgetTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyBudgetTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Energy Budget Types.">
		<cfreturn this.EnergyBudgetTypeDelegate.getEnergyBudgetTypeAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyBudgetType" 
				access="public" returntype="mce.e2.vo.EnergyBudgetType" 
				hint="This method is used to return a populated EnergyBudgetType vo (value object)."> 
		<cfargument name="EnergyBudgetType" type="mce.e2.vo.EnergyBudgetType" required="true" hint="Describes the EnergyBudgetType collection VO object containing the properties of the EnergyBudgetType collection to be retrieved." /> 
		<cfreturn this.EnergyBudgetTypeDelegate.getEnergyBudgetTypeAsComponent(energy_type_uid=arguments.EnergyBudgetType.energy_type_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEnergyBudgetType" 
				access="public" returntype="string" 
				hint="This method is used to persist a new EnergyBudgetType record to the application database."> 
		<cfargument name="EnergyBudgetType" type="mce.e2.vo.EnergyBudgetType" required="true" hint="Describes the EnergyBudgetType collection VO object containing the details of the EnergyBudgetType collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.EnergyBudgetType = setAuditProperties(arguments.EnergyBudgetType, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.EnergyBudgetType = setPrimaryKey(local.EnergyBudgetType)>
		
		<!--- Save the modified EnergyBudgetType --->
		<cfset this.EnergyBudgetTypeDelegate.saveNewEnergyBudgetType(EnergyBudgetType=local.EnergyBudgetType)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.EnergyBudgetType.energy_usage_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewEnergyBudgetType" 
				access="public" returntype="mce.e2.vo.EnergyBudgetType" 
				hint="This method is used to persist a new EnergyBudgetType record to the application database."> 
		<cfargument name="EnergyBudgetType" type="mce.e2.vo.EnergyBudgetType" required="true" hint="Describes the EnergyBudgetType collection VO object containing the details of the EnergyBudgetType collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyBudgetType(EnergyBudgetType=arguments.EnergyBudgetType)>

		<!--- Set the primary key --->
		<cfset arguments.EnergyBudgetType.energy_type_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.EnergyBudgetType>

	</cffunction>

	<cffunction name="saveExistingEnergyBudgetType" 
				access="public" returntype="mce.e2.vo.EnergyBudgetType" 
				hint="This method is used to save an existing EnergyBudgetType information to the application database."> 
		<cfargument name="EnergyBudgetType" type="mce.e2.vo.EnergyBudgetType" required="true" hint="Describes the EnergyBudgetType collection VO object containing the details of the EnergyBudgetType collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.EnergyBudgetType = setAuditProperties(arguments.EnergyBudgetType, "modify")>
		
		<!--- Save the modified EnergyBudgetType --->
		<cfset this.EnergyBudgetTypeDelegate.saveExistingEnergyBudgetType(EnergyBudgetType=local.EnergyBudgetType)/>

		<!--- Return the modified EnergyBudgetType --->
		<cfreturn local.EnergyBudgetType>	

	</cffunction>
	
</cfcomponent>