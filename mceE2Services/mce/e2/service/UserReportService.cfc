<cfcomponent displayname="User report Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of userReport report information.">


	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setUserReportDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage user report data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.UserReportDelegate" hint="Describes the *.cfc used to manage database interactions related to user report information.">
		<cfset this.userReportDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setSecurityDelegate"
				access="public" returntype="void"
				hint="This method is used to set the service delegate used to manage user security (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.SecurityDelegate" hint="Describes the *.cfc used to manage service interactions related to user security.">
		<cfset this.securityDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyUserReportVo"
				access="public" returntype="mce.e2.vo.UserReport"
				hint="This method is used to retrieve / return an empty user report information value object.">
		<cfreturn this.userReportDelegate.getEmptyUserReportComponent()>
	</cffunction>

	<cffunction name="getUserReports"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of user report value objects.">
		<cfargument name="user" type="any" required="false" hint="Describes the user VO object containing the properties of the user for which user reports will be retrieved." />
		<cfargument name="userReport" type="any" required="false" hint="Describes the user VO object containing the properties of the user for which user reports will be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the arguments structure --->
		<cfset local.args = structNew()>

		<!--- Get permissions for the current / specified users --->

			<!--- Instantiate the security service --->
			<cfset local.securityService = request.beanFactory.getBean("securityService")>

			<!--- Get the active session --->
			<cfset local.session = local.securityService.getSession()>

			<!--- Define the permission level used to retrieve reports --->
			<cfset local.args.permission_needed = getPermissions()>


		<!--- Evaluate the userReport properties and capture the relevant properties --->
		<cfif structKeyExists(arguments, 'userReport') and (isStruct(arguments.userReport) or isInstanceOf(arguments.userReport, 'mce.e2.vo.UserReport'))>

			<!--- check for the primary key --->
			<cfif structKeyExists(arguments.userReport, 'report_uid') and len(arguments.userReport.report_uid) gt 0>
				<cfset local.args.report_uid = arguments.userReport.report_uid>
			</cfif>

			<!--- check for the group name --->
			<cfif structKeyExists(arguments.userReport, 'group_name') and len(arguments.userReport.group_name) gt 0>
				<cfset local.args.group_name = arguments.userReport.group_name>
			</cfif>

			<!--- check for the report lookup code --->
			<cfif structKeyExists(arguments.userReport, 'report_lookup_code') and len(arguments.userReport.report_lookup_code) gt 0>
				<cfset local.args.report_lookup_code = arguments.userReport.report_lookup_code>
			</cfif>

			<!--- check for the report generator lookup code --->
			<cfif structKeyExists(arguments.userReport, 'report_generator') and len(arguments.userReport.report_generator) gt 0>
				<cfset local.args.report_generator = arguments.userReport.report_generator>
			</cfif>

			<!--- check for the report name --->
			<cfif structKeyExists(arguments.userReport, 'report_name') and len(arguments.userReport.report_name) gt 0>
				<cfset local.args.report_name = arguments.userReport.report_name>
			</cfif>

			<!--- check for the report parameter ui class --->
			<cfif structKeyExists(arguments.userReport, 'report_parameter_ui_class') and len(arguments.userReport.report_parameter_ui_class) gt 0>
				<cfset local.args.report_parameter_ui_class = arguments.userReport.report_parameter_ui_class>
			</cfif>

		</cfif>

		<!--- Return a collection of user Reports based on the arguments provided --->
		<cfreturn this.userReportDelegate.getUserReportsAsArrayOfComponents(argumentCollection=local.args)>

	</cffunction>

	<cffunction name="getUserReport"
				access="public" returntype="mce.e2.vo.UserReport"
				hint="This method is used to return a populated user report vo (value object).">
		<cfargument name="userReport" type="mce.e2.vo.UserReport" required="true" hint="Describes the user report VO object containing the properties of the user report to be retrieved." />
		<cfreturn this.userReportDelegate.getUserReportAsComponent(user_Report_uid=arguments.userReport.report_uid)>
	</cffunction>

	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="saveNewUserReport"
				access="public" returntype="string"
				hint="This method is used to persist a new user report record to the application database.  The method returns the primary key of the user report that was crated.">
		<cfargument name="userReport" type="mce.e2.vo.UserReport" required="true" hint="Describes the user report VO object containing the properties of the user report to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.userReport = setAuditProperties(arguments.userReport, "create")>

		<!--- Set the primary key for the user report if it's not already set --->
		<cfset local.userReport = setPrimaryKey(local.userReport)>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.userReport.user_Report_uid>

		<!--- Create the userReport --->
		<cfset this.userReportDelegate.saveNewUserReport(userReport=local.userReport)>

		<!--- Return the new userReport primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewUserReport"
				access="public" returntype="mce.e2.vo.UserReport"
				hint="This method is used to persist a new user report record to the application database.  The method returns the primary key of the user report that was crated.">
		<cfargument name="userReport" type="mce.e2.vo.UserReport" required="true" hint="Describes the user report VO object containing the properties of the user report to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Create the userReport --->
		<cfset local.primaryKey = saveNewUserReport(userReport=arguments.userReport)>

		<!--- Set the primary key for the user report --->
		<cfset arguments.userReport.user_Report_uid = local.primaryKey>

		<!--- Retrieve the new user report --->
		<cfset local.userReport = getUserReport(userReport=arguments.userReport)>

		<!--- Return the new userReport --->
		<cfreturn local.userReport>

	</cffunction>

	<cffunction name="saveExistingUserReport"
				access="public" returntype="void"
				hint="This method is used to save existing user report information to the application database.">
		<cfargument name="userReport" type="mce.e2.vo.UserReport" required="true" hint="Describes the user report VO object containing the properties of the user report to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.userReport = setAuditProperties(arguments.userReport, "modify")>

		<!--- Modify the userReport --->
		<cfset this.userReportDelegate.saveExistingUserReport(userReport=local.userReport)/>

	</cffunction>

</cfcomponent>