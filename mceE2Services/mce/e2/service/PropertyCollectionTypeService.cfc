<cfcomponent displayname="Property Collection Type Service"
			 output="false" extends="BaseDataService"
			 hint="This component is used to manage all interactions with property collection type data.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setPropertyCollectionTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage property collection type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyCollectionTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to property collection type information.">
		<cfset this.propertyCollectionTypeDelegate = arguments.bean>
	</cffunction>

	<!--- Define the blank / empty VO methods --->
	<cffunction name="getEmptyPropertyCollectionTypeVo" 
				returntype="mce.e2.vo.PropertyCollectionType"
				hint="This method is used to retrieve a property collection type value object (vo) / coldFusion component.">
		<cfreturn this.propertyCollectionTypeDelegate.getEmptyPropertyCollectionTypeComponent()>	
	</cffunction>

	<!--- Define the methods to return collections of or populated value objects --->
	<cffunction name="getPropertyCollectionTypes"
				returntype="array"
				hint="This method is used to retrieve an array collection of property collection type value objects.">
		<cfreturn this.propertyCollectionTypeDelegate.getPropertyCollectionTypesAsArrayOfComponents()>
	</cffunction>

</cfcomponent>