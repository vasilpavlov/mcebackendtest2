<cfcomponent displayname="Emissions Factor Alias Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of emissions factor alias information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEmissionsFactorAliasDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage energy account data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EmissionsFactorAliasDelegate" hint="Describes the *.cfc used to manage database interactions for emissions factor alias data."/> 
		<cfset this.EmissionsFactorAliasDelegate = arguments.bean>
	</cffunction>
	
	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEmissionsFactorAliasVo" 
				access="public" returntype="mce.e2.vo.EmissionsFactorAlias"
				hint="This method is used to retrieve / return an empty emissionsFactorAlias information value object.">
		<cfreturn this.emissionsFactorAliasDelegate.getEmptyEmissionsFactorAliasComponent()>
	</cffunction>

	<cffunction name="getAvailableEmissionsFactorAliases" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of emission factor aliases filtered by energy type.">

		<!--- Define the arguments associated to this method --->
		<cfargument name="EnergyType" type="mce.e2.vo.EnergyType" required="true" hint="Describes the energy type that will be used to retrieve associated emission factor aliases." /> 

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>
		
		<!--- Initialize the arguments structure --->
		<cfset local.args = structNew()>
		
		<!--- Define each of the arguments used to retrieve available energy units --->
		<cfset local.args.energy_type_uid = arguments.energyType.energy_type_uid>
		
		<!--- Retrieve the emissions factor aliases --->
		<cfset local.returnResult = this.emissionsFactorAliasDelegate.getEmissionsFactorAliasesAsArrayOfComponents(argumentCollection=local.args)>
		
		<!--- Return the emissions factor aliases --->
		<cfreturn local.returnResult>
		
	</cffunction>

	<cffunction name="getEmissionsFactorAliases" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy account types.">
		<cfreturn this.emissionsFactorAliasDelegate.getEmissionsFactorAliasesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEmissionsFactorAlias" 
				access="public" returntype="mce.e2.vo.EmissionsFactorAlias" 
				hint="This method is used to return a populated EmissionsFactorAlias vo (value object)."> 
		<cfargument name="EmissionsFactorAlias" type="mce.e2.vo.EmissionsFactorAlias" required="true" hint="Describes the emissionsFactorAlias collection VO object containing the properties of the emissionsFactorAlias collection to be retrieved." /> 
		<cfreturn this.emissionsFactorAliasDelegate.getEmissionsFactorAliasAsComponent(factor_alias_uid=arguments.EmissionsFactorAlias.factor_alias_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEmissionsFactorAlias" 
				access="public" returntype="string" 
				hint="This method is used to persist a new emissionsFactorAlias record to the application database."> 
		<cfargument name="EmissionsFactorAlias" type="mce.e2.vo.EmissionsFactorAlias" required="true" hint="Describes the emissionsFactorAlias collection VO object containing the details of the emissionsFactorAlias collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.emissionsFactorAlias = setAuditProperties(arguments.emissionsFactorAlias, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.emissionsFactorAlias = setPrimaryKey(local.emissionsFactorAlias)>
		
		<!--- Save the modified emissionsFactorAlias --->
		<cfset this.EmissionsFactorAliasDelegate.saveNewEmissionsFactorAlias(emissionsFactorAlias=local.emissionsFactorAlias)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.emissionsFactorAlias.factor_alias_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewEmissionsFactorAlias" 
				access="public" returntype="mce.e2.vo.EmissionsFactorAlias" 
				hint="This method is used to persist a new emissionsFactorAlias record to the application database."> 
		<cfargument name="EmissionsFactorAlias" type="mce.e2.vo.EmissionsFactorAlias" required="true" hint="Describes the emissionsFactorAlias collection VO object containing the details of the emissionsFactorAlias collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEmissionsFactorAlias(emissionsFactorAlias=arguments.emissionsFactorAlias)>

		<!--- Set the primary key --->
		<cfset arguments.emissionsFactorAlias.factor_alias_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.emissionsFactorAlias>

	</cffunction>

	<cffunction name="saveExistingEmissionsFactorAlias" 
				access="public" returntype="mce.e2.vo.EmissionsFactorAlias" 
				hint="This method is used to save an existing emissionsFactorAlias information to the application database."> 
		<cfargument name="emissionsFactorAlias" type="mce.e2.vo.EmissionsFactorAlias" required="true" hint="Describes the emissionsFactorAlias collection VO object containing the details of the emissionsFactorAlias collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.emissionsFactorAlias = setAuditProperties(arguments.emissionsFactorAlias, "modify")>
		
		<!--- Save the modified emissionsFactorAlias --->
		<cfset this.EmissionsFactorAliasDelegate.saveExistingEmissionsFactorAlias(emissionsFactorAlias=local.emissionsFactorAlias)/>

		<!--- Return the modified emissionsFactorAlias --->
		<cfreturn local.emissionsFactorAlias>	

	</cffunction>
	
</cfcomponent>