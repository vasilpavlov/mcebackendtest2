<cfcomponent displayname="Energy Contracts Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of energy contract information.">

	<!--- Bean getter / setter / dependencies ---> 
	<cffunction name="setEnergyContractsDelegate" 
				access="public" returntype="void"   
				hint="This method is used to set the database delegate used to manage energy contract data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyContractsDelegate" hint="Describes the *.cfc used to manage database interactions related to energy contract information.">
		<cfset this.EnergyContractsDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyContractsVo" 
				access="public" returntype="mce.e2.vo.EnergyContracts"
				hint="This method is used to retrieve / return an empty clientContact information value object.">
		<cfreturn this.EnergyContractsDelegate.getEmptyEnergyContractsComponent()>
	</cffunction>

	<cffunction name="getEnergyContracts" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy contracts associated to a given property.">
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object used to retrieve energy contracts." /> 
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object used to retrieve energy contracts." /> 
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.EnergyContractsDelegate.getEnergyContractsAsArrayOfComponents(
					client_company_uid=arguments.ClientCompany.client_company_uid,
					property_uid=arguments.Property.property_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date)>
	</cffunction>	

	<cffunction name="getEnergyContractsUnique" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy contracts associated to a given property.">
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the client company VO object used to retrieve energy contracts." /> 
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object used to retrieve energy contracts." /> 
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.EnergyContractsDelegate.getEnergyContractsAsArrayOfComponentsUnique(
					client_company_uid=arguments.ClientCompany.client_company_uid,
					property_uid=arguments.Property.property_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date,
					mostRecentFirst=true,
					includeRelatedEnergyAccounts=true)>
	</cffunction>	
	
	<cffunction name="getEnergyContractsByClient" 
				access="public" returntype="mce.e2.vo.EnergyContracts" 
				hint="This method is used to return a populated Energy Contracts vo (value object)."> 
		<cfargument name="EnergyContracts" type="mce.e2.vo.EnergyContracts" required="true" hint="Describes the EnergyContracts VO object containing the properties of the EnergyContracts to be retrieved." /> 
		<cfreturn this.EnergyContractsDelegate.getEnergyContractsAsComponent(client_company_uid=arguments.EnergyContracts.client_company_uid)>
	</cffunction>
	
	<cffunction name="getEnergyContractAssociations"
				hint="This method is used to retrieve an array collection of energy contract energy account association objects for all the Energy Contracts in the application."
				output="false"
				returnType="array"
				access="public">
		<cfargument name="EnergyContracts" type="mce.e2.vo.EnergyContracts" required="true" hint="Describes the EnergyContracts VO object containing the properties of the EnergyContracts to be retrieved." />
		<cfreturn this.EnergyContractsDelegate.getEnergyContractAssociationsAsArrayOfComponents(energy_contract_uid=arguments.EnergyContracts.energy_contract_uid)>
	</cffunction>

	<cffunction name="getEnergyContractsByEnergyAccount" 
				access="public" returntype="mce.e2.vo.EnergyContracts" 
				hint="This method is used to return a populated Energy Contracts vo (value object)."> 
		<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" hint="Describes the EnergyContracts VO object containing the properties of the EnergyContracts to be retrieved." /> 
		<cfreturn this.EnergyContractsDelegate.getEnergyContractsAsComponent(energy_account_uid=arguments.EnergyAccount.energy_account_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEnergyContracts" 
				access="public" returntype="string" 
				hint="This method is used to persist a new EnergyContracts record to the application database."> 
		<cfargument name="EnergyContracts" type="mce.e2.vo.EnergyContracts" required="true" hint="Describes the EnergyContracts VO object containing the details of the EnergyContracts to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.energyContracts = setAuditProperties(arguments.EnergyContracts, "create")>
		
		<!--- Set the primary key for the energy contract if it's not already set --->
		<cfset local.energyContracts = setPrimaryKey(local.energyContracts)>
		
		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.energyContracts.energy_contract_uid>
		
		<!--- Save the modified EnergyContracts --->
		<cfset this.EnergyContractsDelegate.saveNewEnergyContracts(EnergyContracts=local.energyContracts)/>
		
		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewEnergyContracts" 
				access="public" returntype="mce.e2.vo.EnergyContracts" 
				hint="This method is used to persist a new EnergyContracts record to the application database."> 
		<cfargument name="EnergyContracts" type="mce.e2.vo.EnergyContracts" required="true" hint="Describes the EnergyContracts VO object containing the details of the EnergyContracts to be saved and returned." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyContracts(EnergyContracts=arguments.EnergyContracts)>

		<!--- Set the primary key --->
		<cfset arguments.EnergyContracts.energy_contract_uid = local.primaryKey>
		
		<!--- Return the new object --->
		<cfreturn arguments.EnergyContracts>

	</cffunction>

	<cffunction name="saveExistingEnergyContracts" 
				access="public" returntype="mce.e2.vo.EnergyContracts" 
				hint="This method is used to save an existing EnergyContracts information to the application database."> 
		<cfargument name="EnergyContracts" type="mce.e2.vo.EnergyContracts" required="true" hint="Describes the EnergyContracts VO object containing the details of the EnergyContracts to be modified." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.EnergyContracts = setAuditProperties(arguments.EnergyContracts, "modify")>
		
		<!--- Save the modified EnergyContracts --->
		<cfset this.EnergyContractsDelegate.saveExistingEnergyContracts(EnergyContracts=local.EnergyContracts)/>

		<!--- Return the modified EnergyContracts --->
		<cfreturn local.EnergyContracts>	

	</cffunction>

	<cffunction name="saveNewEnergyContractAccountAssociation" 
				access="public" returntype="string" 
				hint="This method is used to persist a new energy contract to account association record to the application database."> 
		<cfargument name="EnergyContractAccountAssociation" type="mce.e2.vo.EnergyContractAccountAssociation" required="true" hint="Describes the EnergyContractAccountAssociation VO object containing the details of the EnergyContracts / Account Association to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.association = setAuditProperties(arguments.EnergyContractAccountAssociation, "create")>
		
		<!--- Set the primary key for the energy contract if it's not already set --->
		<cfset local.association = setPrimaryKey(local.association)>
		
		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.association.relationship_id>
		
		<!--- Save the modified EnergyContracts --->
		<cfset this.EnergyContractsDelegate.saveNewEnergyContractAccountAssociation(EnergyContractAccountAssociation=local.association)/>
		
		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveExistingEnergyContractAccountAssociation" 
				access="public" returntype="void" 
				hint="This method is used to save an existing energy contract to account association record to the application database."> 
		<cfargument name="EnergyContractAccountAssociation" type="mce.e2.vo.EnergyContractAccountAssociation" required="true" hint="Describes the energy contract to account association record VO object containing the details of the EnergyContracts / Account Association to be modified." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.association = setAuditProperties(arguments.EnergyContractAccountAssociation, "modify")>
		
		<!--- Save the modified EnergyContracts --->
		<cfset this.EnergyContractsDelegate.saveExistingEnergyContractAccountAssociation(EnergyContractAccountAssociation=local.association)/>

	</cffunction>

</cfcomponent>