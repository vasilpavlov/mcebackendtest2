<cfcomponent displayname="Client Service Bill Per Type Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of client service bill per type information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setClientServiceBillPerTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage client service bill per type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientServiceBillPerTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to client service bill per type information.">
		<cfset this.ClientServiceBillPerTypeDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyClientServiceBillPerTypeVo" 
				access="public" returntype="mce.e2.vo.ClientServiceBillPerType"
				hint="This method is used to retrieve / return an empty ClientServiceBillPerTypeDelegate information value object.">
		<cfreturn this.ClientServiceBillPerTypeDelegate.getEmptyClientServiceBillPerTypeComponent()>
	</cffunction>

	<cffunction name="getAvailableClientServiceBillPerTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client service bill per types supported by the application.">
		<cfreturn this.ClientServiceBillPerTypeDelegate.getClientServiceBillPerTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getClientServiceBillPerTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client service bill per types.">
		<cfreturn this.ClientServiceBillPerTypeDelegate.getClientServiceBillPerTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getClientServiceBillPerTypesForClientService" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client service bill per types.">
		<cfargument name="service_type_uid" type="string" required="true">
		<cfreturn this.ClientServiceBillPerTypeDelegate.getClientServiceBillPerTypesAsArrayOfComponents(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="getClientServiceBillPerType" 
				access="public" returntype="mce.e2.vo.ClientServiceBillPerType" 
				hint="This method is used to return a populated ClientServiceBillPerType vo (value object)."> 
		<cfargument name="ClientServiceBillPerType" type="mce.e2.vo.ClientServiceBillPerType" required="true" hint="Describes the ClientServiceBillPerTypeDelegate collection VO object containing the properties of the ClientServiceBillPerTypeDelegate collection to be retrieved." /> 
		<cfreturn this.ClientServiceBillPerTypeDelegate.getClientServiceBillPerTypeAsComponent(service_type_uid=arguments.ClientServiceBillPerType.service_type_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewClientServiceBillPerType" 
				access="public" returntype="string" 
				hint="This method is used to persist a new ClientServiceBillPerTypeDelegate record to the application database."> 
		<cfargument name="ClientServiceBillPerType" type="mce.e2.vo.ClientServiceBillPerType" required="true" hint="Describes the ClientServiceBillPerTypeDelegate collection VO object containing the details of the ClientServiceBillPerTypeDelegate collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.ClientServiceBillPerTypeDelegate = setAuditProperties(arguments.ClientServiceBillPerTypeDelegate, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.ClientServiceBillPerTypeDelegate = setPrimaryKey(local.ClientServiceBillPerTypeDelegate)>
		
		<!--- Save the modified ClientServiceBillPerTypeDelegate --->
		<cfset this.ClientServiceBillPerTypeDelegate.saveNewClientServiceBillPerType(ClientServiceBillPerTypeDelegate=local.ClientServiceBillPerTypeDelegate)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.ClientServiceBillPerTypeDelegate.energy_usage_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewClientServiceBillPerType" 
				access="public" returntype="mce.e2.vo.ClientServiceBillPerType" 
				hint="This method is used to persist a new ClientServiceBillPerTypeDelegate record to the application database."> 
		<cfargument name="ClientServiceBillPerType" type="mce.e2.vo.ClientServiceBillPerType" required="true" hint="Describes the ClientServiceBillPerTypeDelegate collection VO object containing the details of the ClientServiceBillPerTypeDelegate collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewClientServiceBillPerType(ClientServiceBillPerTypeDelegate=arguments.ClientServiceBillPerTypeDelegate)>

		<!--- Set the primary key --->
		<cfset arguments.ClientServiceBillPerTypeDelegate.service_type_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.ClientServiceBillPerTypeDelegate>

	</cffunction>

	<cffunction name="saveExistingClientServiceBillPerType" 
				access="public" returntype="mce.e2.vo.ClientServiceBillPerType" 
				hint="This method is used to save an existing ClientServiceBillPerTypeDelegate information to the application database."> 
		<cfargument name="ClientServiceBillPerTypeDelegate" type="mce.e2.vo.ClientServiceBillPerType" required="true" hint="Describes the ClientServiceBillPerTypeDelegate collection VO object containing the details of the ClientServiceBillPerTypeDelegate collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.ClientServiceBillPerTypeDelegate = setAuditProperties(arguments.ClientServiceBillPerTypeDelegate, "modify")>
		
		<!--- Save the modified ClientServiceBillPerTypeDelegate --->
		<cfset this.ClientServiceBillPerTypeDelegate.saveExistingClientServiceBillPerType(ClientServiceBillPerTypeDelegate=local.ClientServiceBillPerTypeDelegate)/>

		<!--- Return the modified ClientServiceBillPerTypeDelegate --->
		<cfreturn local.ClientServiceBillPerTypeDelegate>	

	</cffunction>
	
</cfcomponent>