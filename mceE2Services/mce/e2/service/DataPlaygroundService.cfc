<cfcomponent displayname="Data Playground Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all busines logic tied / asociated with the management of data playground data.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setDataPlaygroundDelegate"
				acces="public" returntype="void"
				hint="This method is used to set the database delegate used to manage common data playground data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.DataPlaygroundDelegate" hint="Describes the *.cfc used to manage database interactions related to common data playground information.">
		<cfset this.DataPlaygroundDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setEmissionsAnalysisDelegate"
				access="public" returntype="void"
				hint="Called automatically via IOC / dependency injection (Coldspring).">
		<cfargument name="bean" type="mce.e2.db.EmissionsAnalysisDelegate">
		<cfset this.emissionsAnalysisDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setDataSeriesOptionDelegate"
				acces="public" returntype="void"
				hint="This method is used to set the database delegate used to manage Options For Data Series data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.DataSeriesOptionDelegate" hint="Describes the *.cfc used to manage database interactions related to Options For Data Series information.">
		<cfset this.DataSeriesOptionDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getDataSeriesOptions"
				acces="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Options For Data Series.">
		<cfargument name="namespace_lookup_code" required="true" type="string" hint="Describes the namespace lookup code for a given data series option."/>

		<cfset arguments.permission_needed = getPermissions()>

		<cfreturn this.DataSeriesOptionDelegate.getDataSeriesOptionsAsArrayOfComponents(
					argumentCollection=arguments)>
	</cffunction>

	<cffunction name="getAvailableBudgetTypes" returntype="query" access="public">
		<cfargument name="energyAccountList" type="array" required="true" hint="Describes the primary key / unique identifier for a given energy account.">
		<cfargument name="period_start" type="date" required="true" hint="Describes the start date that will be used to process period data.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end date that will be used to process period data.">

		<cfset var energy_account_uid_list = buildVoPropertyListFromArray(arguments.energyAccountList, 'energy_account_uid')>
		<cfreturn this.DataPlaygroundDelegate.getAvailableBudgetTypes(energy_account_uid_list, arguments.period_start, arguments.period_end)>
	</cffunction>

	<cffunction name="getCombinedDataAnalysis"
				output="false"
				returnType="query"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="property_uid_list" type="string" required="true" hint="The property/properties that you want analysis records for">
		<cfargument name="period_start" type="date" required="true" hint="Describes the start date that will be used to process period data.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end date that will be used to process period data.">
		<cfargument name="grouping" type="string" required="true" hint="Describes the data roll-up mode that will be implemented (acceptable values are 'daily', 'monthly').">
		<cfargument name="profile_lookup_code" type="string" required="false" default="default" hint="Describes the internal identifier / lookup code for a given profile.">
		<cfargument name="energy_account_uid_list" type="string" required="false" hint="Optional; the specific energy account(s) that you want emissions analysis records for. Omit or provide an empty string for all energy accounts associated with the given property/properties.">
		<cfargument name="propertyMetaTypeCode" type="string" required="true" hint="Describes the lookup code for a given property meta type.">
		<cfargument name="propertyMetaTypeCodeForMetrics" type="string" required="false" default="">
		<cfargument name="energyUsageMetaTypeCodes" type="string" required="false" default="">
		<cfargument name="factorLookupCodes" type="string" required="false" default="">
		<cfargument name="energyUsageTypeCodes" type="string" required="false" default="">

		<cfset var qPropertyMetaAnalysis = "">
		<cfset var qRateFactorAnalysis = "">
		<cfset var qResult = "">
		<cfset var rateFactorViaPropertyMetaCodes = "">
		<cfset var code = "">
		<cfset var simpleRateFactorLookupCodes = "">


		<!--- Do a bit of tedious string massaging --->
		<cfset arguments.rateFactorViaPropertyMetaCodes = "">
		<cfloop list="#arguments.factorLookupCodes#" index="code">
			<cfif code contains "{" and code contains "}">
				<cfset arguments.rateFactorViaPropertyMetaCodes = ListAppend(arguments.rateFactorViaPropertyMetaCodes, code)>
			<cfelse>
				<cfset simpleRateFactorLookupCodes = ListAppend(simpleRateFactorLookupCodes, code)>
			</cfif>
		</cfloop>
		<cfset arguments.factorLookupCodes = simpleRateFactorLookupCodes>


		<!--- Perform the main energy analysis... this gets all of the data that comes (conceptually) from energy usage records --->
		<cfset qResult = this.emissionsAnalysisDelegate.getEnergyAnalysis(argumentCollection=arguments)>


		<!--- Combine the base energy analysis with property meta data, if called for --->
		<cfif propertyMetaTypeCode neq "" or arguments.rateFactorViaPropertyMetaCodes neq "">
			<cfset qPropertyMetaAnalysis = this.dataPlaygroundDelegate.getPropertyMetaDataAnalysis(argumentCollection=arguments)>

			<cfquery dbtype="query" name="qResult">
				SELECT
					qResult.*
					<cfloop list="#propertyMetaTypeCode#" index="local.typeCode">
						, qPropertyMetaAnalysis.PropertyMeta_#local.typeCode#
					</cfloop>
					<cfloop list="#arguments.rateFactorViaPropertyMetaCodes#" index="local.typeCode">
						, qPropertyMetaAnalysis.RateFactor_#this.dataPlaygroundDelegate.safeCodeFormat(local.typeCode)#
					</cfloop>
				FROM
					qResult,
					qPropertyMetaAnalysis
				<cfswitch expression="#arguments.grouping#">
					<cfcase value="periodMonth,periodQuarter,periodYear" delimiters=",">
						WHERE
							qResult.periodEnd = qPropertyMetaAnalysis.periodEnd
						ORDER BY
							qResult.periodEnd
					</cfcase>
					<cfcase value="property" delimiters=",">
						WHERE
							qResult.property_uid = qPropertyMetaAnalysis.property_uid
						ORDER BY
							qResult.propertyFriendlyName
					</cfcase>
				</cfswitch>
			</cfquery>
		</cfif>

		<!--- Combine the base energy analysis with rate factor data, if called for --->
<!---		<cfif factorLookupCodes neq "">
			<cfset qRateFactorAnalysis = this.dataPlaygroundDelegate.getRateFactorDataAnalysis(argumentCollection=arguments)>

			<cfquery dbtype="query" name="qResult">
				SELECT
					qResult.*
					<cfloop list="#factorLookupCodes#" index="local.typeCode">
						, qRateFactorAnalysis.RateFactor_#this.DataPlaygroundDelegate.safeCodeFormat(local.typeCode)#
					</cfloop>
				FROM
					qResult,
					qRateFactorAnalysis
				<cfswitch expression="#arguments.grouping#">
					<cfcase value="periodMonth,periodQuarter,periodYear" delimiters=",">
						WHERE
							qResult.periodEnd = qRateFactorAnalysis.periodEnd
						ORDER BY
							qResult.periodEnd
					</cfcase>
					<cfcase value="property" delimiters=",">
						WHERE
							qResult.property_uid = qRateFactorAnalysis.property_uid
						ORDER BY
							qResult.propertyFriendlyName
					</cfcase>
				</cfswitch>
			</cfquery>

		</cfif>--->

		<cfreturn qResult>
	</cffunction>




	<cffunction name="getAvailableMetricsDivisors"
				acces="public" returntype="array"
				hint="This method is used to retrieve / return a collection of usage metric divisors.">

		<!--- Define the arguments of this method --->
		<cfargument name="namespace_lookup_code" required="true" type="string" hint="Describes the namespace lookup code for a given data series option."/>
		<cfargument name="propertyArray" required="true" type="array" hint="Describes a collection of properties used to filter usage metric divisors."/>
		<cfargument name="as_of_date" required="true" type="string" default="#now()#" hint="Describes the date used to filter metric divisors."/>

		<!--- Initialize the local scope --->
		<cfset var local = structnew()>

		<!--- Initialize the arguments scope for the database delegete --->
		<cfset local.args = structNew()>

		<!---  Build out the delegate arguments --->
		<cfset local.args.namespace_lookup_code = arguments.namespace_lookup_code>
		<cfset local.args.property_uid = buildVoPropertyListFromArray(arguments.propertyArray, 'property_uid')>
		<cfset local.args.permission_needed = getPermissions()>

		<!--- Validate that a valid / fair date was passed in --->
		<cfif not isdate(arguments.as_of_date)>

			<!--- If it's not a date, then default the date --->
			<cfset local.args.as_of_date = now()>

		<cfelse>

			<!--- Otherwise, use the date that was specified --->
			<cfset local.args.as_of_date = arguments.as_of_date>

		</cfif>

		<!--- Retrieve the uage metric divisors, and retun an array of objects --->
		<cfreturn this.DataPlaygroundDelegate.getUsageMetricDivisorsAsArrayOfComponents(
					argumentCollection=local.args)>

	</cffunction>




	<cffunction name="getPropertyMetaDataAnalysis"
				acces="public" returntype="query"
				hint="This method is used to retrieve / return a property meta data analysis for the specified energy accounts and property meta data types.">

		<!--- Define the arguments of this method --->
		<cfargument name="property_uid_list" type="string" required="true" hint="The property/properties that you want analysis records for">
		<cfargument name="type_lookup_code" type="string" required="true" hint="Describes the lookup code for a given property meta type.">
		<cfargument name="period_start" type="date" required="true" hint="Describes the start date that will be used to process period data.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end date that will be used to process period data.">
		<cfargument name="grouping" type="string" required="true" hint="Describes the data roll-up mode that will be implemented (acceptable values are 'daily', 'monthly').">

		<!--- Get the result set --->
		<cfreturn this.dataPlaygroundDelegate.getPropertyMetaDataAnalysis(
				argumentCollection = arguments)>

	</cffunction>

<!---	<cffunction name="getEnergyMetaDataAnalysis"
				acces="public" returntype="query"
				hint="This method is used to retrieve / return energy usage meta data analysis for the specified energy accounts and meta data types.">

		<!--- Define the arguments of this method --->
		<cfargument name="energyAccountArray" required="true" type="array" hint="Describes the namespace lookup code for a given data series option."/>
		<cfargument name="metaTypeArray" required="true" type="array" hint="Describes a collection of energy usage meta types used to filter energy usage data."/>
		<cfargument name="period_start" type="date" required="true" hint="Describes the start date that will be used to process period data.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end date that will be used to process period data.">
		<cfargument name="dateRollupMode" type="string" required="true" hint="Describes the data roll-up mode that will be implemented (acceptable values are 'daily', 'monthly').">
		<cfargument name="grouping" type="string" required="false" default="periodMonth" hint="Describes the internal identifier / lookup code for a given profile.">

		<!--- Initialize the local scope --->
		<cfset var local = structnew()>

		<!--- Check that a valid dateRollupMode argument is specified --->
		<cfif not listFindNoCase('monthly,daily', arguments.dateRollupMode) >

			<cfthrow
				message="Unsupported dateRollupMode argument"
				detail="The dateRollupMode argument provided ('#dateRollupMode#') is not supported. The currently implemented modes are: 'monthly' and 'daily'.">

		</cfif>

		<!--- Initialize the arguments scope for the database delegete --->
		<cfset local.args = structNew()>

		<!---  Build out the delegate arguments --->
		<cfset local.args.energy_account_uid = buildVoPropertyListFromArray(arguments.energyAccountArray, 'energy_account_uid')>
		<cfset local.args.type_lookup_code = ArrayToList(arguments.metaTypeArray)>
		<cfset local.args.period_start = arguments.period_start>
		<cfset local.args.period_end = arguments.period_end>
		<cfset local.args.period_end = arguments.period_end>
		<cfset local.args.dateRollupMode = arguments.dateRollupMode>
		<cfset local.args.grouping = arguments.grouping>

		<!--- Get the result set --->
		<cfset local.result = this.dataPlaygroundDelegate.getEnergyMetaDataAnalysis(
				argumentCollection = local.args)>

		<!--- Return the result set --->
		<cfreturn local.result>

	</cffunction>--->

<!---	<cffunction name="getRateFactorDataAnalysis"
				acces="public" returntype="query"
				hint="This method is used to retrieve / return rate factor analysis for the specified rate factor lookup codes.">

		<!--- Define the arguments of this method --->
		<cfargument name="rateFactorArray" required="true" type="array" hint="Describes a the rate factor lookup codes used to filter."/>
		<cfargument name="period_start" type="date" required="true" hint="Describes the start date that will be used to process period data.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end date that will be used to process period data.">
		<cfargument name="dateRollupMode" type="string" required="true" hint="Describes the data roll-up mode that will be implemented (acceptable values are 'daily', 'monthly').">

		<!--- Initialize the local scope --->
		<cfset var local = structnew()>

		<!--- Check that a valid dateRollupMode argument is specified --->
		<cfif not listFindNoCase('monthly,daily', arguments.dateRollupMode) >

			<cfthrow
			message="Unsupported dateRollupMode argument"
			detail="The dateRollupMode argument provided ('#dateRollupMode#') is not supported. The currently implemented modes are: 'monthly' and 'daily'.">

		</cfif>

		<!--- Initialize the arguments scope for the database delegete --->
		<cfset local.args = structNew()>

		<!---  Build out the delegate arguments --->
		<cfset local.args.factor_lookup_code = buildVoPropertyListFromArray(arguments.rateFactorArray, 'factor_lookup_code')>
		<cfset local.args.period_start = arguments.period_start>
		<cfset local.args.period_end = arguments.period_end>
		<cfset local.args.period_end = arguments.period_end>
		<cfset local.args.dateRollupMode = arguments.dateRollupMode>

		<!--- Get the result set --->
		<cfset local.result = this.dataPlaygroundDelegate.getRateFactorDataAnalysis(
				argumentCollection = local.args)>

		<!--- Return the result set --->
		<cfreturn local.result>

	</cffunction>--->

</cfcomponent>