<cfcomponent displayname="Energy Provider Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of energy provider rate class information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyProviderDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage energy provider data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyProviderDelegate" hint="Describes the *.cfc used to manage database interactions for energy providers."/> 
		<cfset this.EnergyProviderDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyProviderVo" 
				access="public" returntype="mce.e2.vo.EnergyProvider"
				hint="This method is used to retrieve / return an empty energyProvider information value object.">
		<cfreturn this.energyProviderDelegate.getEmptyEnergyProviderComponent()>
	</cffunction>

	<cffunction name="getEnergyProviders" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy providers.">
		<cfargument name="provider_type" required="false" type="string"/>

		<cfreturn this.energyProviderDelegate.getEnergyProvidersAsArrayOfComponents(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="getEnergyProvider" 
				access="public" returntype="mce.e2.vo.EnergyProvider" 
				hint="This method is used to return a populated EnergyProvider vo (value object)."> 
		<cfargument name="EnergyProvider" type="mce.e2.vo.EnergyProvider" required="true" hint="Describes the energyProvider collection VO object containing the properties of the energyProvider collection to be retrieved." /> 
		<cfreturn this.energyProviderDelegate.getEnergyProviderAsComponent(energy_provider_uid=arguments.EnergyProvider.energy_provider_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEnergyProvider" 
				access="public" returntype="string" 
				hint="This method is used to persist a new energyProvider record to the application database."> 
		<cfargument name="EnergyProvider" type="mce.e2.vo.EnergyProvider" required="true" hint="Describes the energyProvider collection VO object containing the details of the energyProvider collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.energyProvider = setAuditProperties(arguments.energyProvider, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.energyProvider = setPrimaryKey(local.energyProvider)>
		
		<!--- Save the modified energyProvider --->
		<cfset this.EnergyProviderDelegate.saveNewEnergyProvider(energyProvider=local.energyProvider)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.energyProvider.energy_provider_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewEnergyProvider" 
				access="public" returntype="mce.e2.vo.EnergyProvider" 
				hint="This method is used to persist a new energyProvider record to the application database."> 
		<cfargument name="EnergyProvider" type="mce.e2.vo.EnergyProvider" required="true" hint="Describes the energyProvider collection VO object containing the details of the energyProvider collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyProvider(energyProvider=arguments.energyProvider)>

		<!--- Set the primary key --->
		<cfset arguments.energyProvider.energy_provider_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.energyProvider>

	</cffunction>

	<cffunction name="saveExistingEnergyProvider" 
				access="public" returntype="mce.e2.vo.EnergyProvider" 
				hint="This method is used to save an existing energyProvider information to the application database."> 
		<cfargument name="energyProvider" type="mce.e2.vo.EnergyProvider" required="true" hint="Describes the energyProvider collection VO object containing the details of the energyProvider collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.energyProvider = setAuditProperties(arguments.energyProvider, "modify")>
		
		<!--- Save the modified energyProvider --->
		<cfset this.EnergyProviderDelegate.saveExistingEnergyProvider(energyProvider=local.energyProvider)/>

		<!--- Return the modified energyProvider --->
		<cfreturn local.energyProvider>	

	</cffunction>
	
</cfcomponent>