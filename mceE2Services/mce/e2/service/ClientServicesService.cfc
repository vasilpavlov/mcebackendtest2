<cfcomponent displayname="Client Service Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of client service information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setClientServicesDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage client service data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientServicesDelegate" hint="Describes the *.cfc used to manage database interactions related to client service information.">
		<cfset this.ClientServicesDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setClientServiceTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage client service type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientServiceTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to client service type information.">
		<cfset this.ClientServiceTypeDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setClientServiceStatusTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage client service type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientServiceStatusTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to client service type information.">
		<cfset this.ClientServiceStatusTypeDelegate = arguments.bean>
	</cffunction>
	
	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyClientServicesVo" 
				access="public" returntype="mce.e2.vo.ClientServices"
				hint="This method is used to retrieve / return an empty clientContact information value object.">
		<cfreturn this.ClientServicesDelegate.getEmptyClientServicesComponent()>
	</cffunction>

	<cffunction name="getClientServices" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client services.">
		<cfreturn this.ClientServicesDelegate.getClientServicesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getClientServicesByProperty" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client services associated to a given property.">
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object used to retrieve client services." /> 
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.ClientServicesDelegate.getClientServicesAsArrayOfComponents(
					property_uid=arguments.Property.property_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date)>
	</cffunction>	

	<cffunction name="getClientServicesByClientCompany" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client services associated to a given property.">
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the ClientCompany VO object used to retrieve client services." /> 
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">
		<cfreturn this.ClientServicesDelegate.getClientServicesAsArrayOfComponents(
					client_company_uid=arguments.ClientCompany.client_company_uid,
					selectMethod=arguments.selectMethod,
					relationship_filter_date=arguments.relationship_filter_date)>
	</cffunction>
	
	<!-- JF I'm not sure what the original intention of this function was. Seemingly
		to get one client service for a company using the arg of a clientservice? Doesn't make sense'-->
	<cffunction name="getClientServicesByClient" 
				access="public" returntype="mce.e2.vo.ClientServices" 
				hint="This method is used to return a populated Client Service vo (value object)."> 
		<cfargument name="ClientServices" type="mce.e2.vo.ClientServices" required="true" hint="Describes the ClientServices VO object containing the properties of the ClientServices to be retrieved." /> 
		<cfreturn this.ClientServices.getClientServicesAsComponent(client_company_uid=arguments.ClientServices.client_company_uid)>
	</cffunction>
	
	<cffunction name="getAvailableClientServicesTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of client service types.">
		<cfreturn this.ClientServiceTypeDelegate.getClientServicesTypesAsArrayOfComponents()>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewClientServices" 
				access="public" returntype="string" 
				hint="This method is used to persist a new ClientServices record to the application database."> 
		<cfargument name="ClientServices" type="mce.e2.vo.ClientServices" required="true" hint="Describes the ClientServices VO object containing the details of the ClientServices to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		<cfset var isPropertyLevel = structKeyExists(arguments.ClientServices, "property_uid") and arguments.ClientServices.property_uid neq "">
		
		<cfif not isPropertyLevel>
			<!--- Set the created by / date audit properties --->
			<cfset local.clientServices = setAuditProperties(duplicate(arguments.ClientServices), "create")>
			<cfset local.primaryKeyToReturn = this.ClientServicesDelegate.saveNewClientServices(ClientServices=local.clientServices)/>

		<cfelse>
			<!--- The idea here is that the property_uid can be a list of properties to apply this service to --->
			<cfset local.propertyUidList = arguments.ClientServices.property_uid>
			
			<cftransaction>
				<cftry>
		
					<!--- For each property in the list --->
					<cfloop list="#local.propertyUidList#" index="local.thisPropertyUid">
			
						<!--- Set the created by / date audit properties --->
						<cfset local.clientServices = setAuditProperties(duplicate(arguments.ClientServices), "create")>
					
						<!--- Put the property_uid for "this pass" through the loop into the ClientServices object to persist --->
						<cfset local.clientServices.property_uid = local.thisPropertyUid>
			
						<!--- Save the modified ClientServices --->
						<cfset local.thisPrimaryKey = this.ClientServicesDelegate.saveNewClientServices(ClientServices=local.clientServices)/>
			
						<!--- We'll remember the first primary key generated, to return when done --->			
						<cfif not structKeyExists(local, "primaryKeyToReturn")>
							<cfset local.primaryKeyToReturn = local.thisPrimaryKey>
						</cfif>
					</cfloop>
					<cfcatch type="any">
						<cftransaction action="rollback"/>
						<cfthrow 
							message="One or more of the properties you selected couldn't have the service saved to them. That may be because one of them already has the same service for the same dates."
							detail="Property UID: #local.thisPropertyUid#"/>
					</cfcatch>
				</cftry>
			</cftransaction>
		</cfif>
			
		<!--- Return the first new primary key --->
		<cfreturn local.primaryKeyToReturn>
	</cffunction>
	
	<cffunction name="saveAndReturnNewClientServices" 
				access="public" returntype="mce.e2.vo.ClientServices" 
				hint="This method is used to persist a new ClientServices record to the application database."> 
		<cfargument name="ClientServices" type="mce.e2.vo.ClientServices" required="true" hint="Describes the ClientServices VO object containing the details of the ClientServices to be saved and returned." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewClientServices(ClientServices=arguments.ClientServices)>

		<!--- Set the primary key --->
		<cfset arguments.ClientServices.relationship_id = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.ClientServices>

	</cffunction>

	<cffunction name="saveExistingClientServices" 
				access="public" returntype="mce.e2.vo.ClientServices" 
				hint="This method is used to save an existing ClientServices information to the application database."> 
		<cfargument name="ClientServices" type="mce.e2.vo.ClientServices" required="true" hint="Describes the ClientServices VO object containing the details of the ClientServices to be modified." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.ClientServices = setAuditProperties(arguments.ClientServices, "modify")>
		
		<!--- Save the modified ClientServices --->
		<cfset this.ClientServicesDelegate.saveExistingClientServices(ClientServices=local.ClientServices)/>

		<!--- Return the modified ClientServices --->
		<cfreturn local.ClientServices>	

	</cffunction>
	
</cfcomponent>