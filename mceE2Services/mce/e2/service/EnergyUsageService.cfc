<cfcomponent displayname="Energy Usage Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of energy usage information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyUsageDelegate"
				access="public" returntype="void"
				hint="Called by ColdSpring IOC/DI framework.">
		<cfargument name="bean" type="mce.e2.db.EnergyUsageDelegate">
		<cfset this.EnergyUsageDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyUsageVo"
				access="public" returntype="mce.e2.vo.EnergyUsage"
				hint="This method is used to retrieve / return an empty energyUsage information value object.">
		<cfreturn this.energyUsageDelegate.getEmptyEnergyUsageComponent()>
	</cffunction>



	<cffunction name="getAvailableEnergyUsage"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy usage Vo objects for a given account and period.">

		<!--- Define the arguments for this method --->
		<cfargument name="AccountsArray" type="array" required="true" hint="Describes the collection of energy accounts for which usage data will be retrieved.">
		<cfargument name="periodStartFrom" type="date" required="true" hint="Describes the usage period start date used to retrieve energy usage information.">
		<cfargument name="periodEndThru" type="date" required="true" hint="Describes the usage period end date used to retrieve energy usage information.">
		<cfargument name="SelectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select only reportable data, all = select all previous data).">

		<!--- Intialize the local scope --->
		<cfset var local = structNew()>

		<!--- Intialize the arguments structure --->
		<cfset local.args = structNew()>

		<!--- Initialize the arguments used to retrieve energy account information --->
		<cfset local.args.energy_account_uid = buildVoPropertyListFromArray(propertyName='energy_account_uid', voArray=arguments.accountsArray)>
		<cfset local.args.period_start = arguments.periodStartFrom>
		<cfset local.args.period_end = arguments.periodEndThru>
		<cfset local.args.selectMethod = arguments.SelectMethod>

		<!--- Retrieve the collection of energy usage data --->
		<cfset local.result = this.energyUsageDelegate.getEnergyUsageAsArrayOfComponents(argumentCollection=local.args)>

		<!--- Return the energy usage records --->
		<cfreturn local.result>

	</cffunction>

	<cffunction name="getAvailableEnergyUsageStatuses" access="public" returntype="query">

		<cfset var local = structNew() />
		<!--- TODO:  Kevin, add in logic to pull the current logged in user.  --->
		<cfset local.result = this.energyUsageDelegate.getAvailableEnergyUsageStatuses () >

		<cfreturn local.result />
	</cffunction>

	<cffunction name="getEnergyUsages"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy usages.">
		<cfreturn this.energyUsageDelegate.getEnergyUsageAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyUsage"
				access="public" returntype="mce.e2.vo.EnergyUsage"
				hint="This method is used to return a populated EnergyUsage vo (value object).">
		<cfargument name="EnergyUsage" type="mce.e2.vo.EnergyUsage" required="true" hint="Describes the energyUsage collection VO object containing the properties of the energyUsage collection to be retrieved." />
		<cfreturn this.energyUsageDelegate.getEnergyUsageAsComponent(energy_usage_uid=arguments.EnergyUsage.energy_usage_uid)>
	</cffunction>

	<cffunction name="getEnergyUsageHistoryOverview"
				access="public" returntype="query"
				hint="This method is used to return a query of energy usage history overview information.">
		<cfargument name="EnergyAccounts" type="array" required="true" hint="Describes the array of energyAccount VO objects." />
		<cfargument name="SelectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select only reportable data, all = select all previous data).">
		<cfif super.hasPermission("e2.pub.dataentry.entry.ad-hoc")>
			<cfset arguments.SelectMethod = 'all'>
		</cfif>

		<cfreturn this.energyUsageDelegate.getEnergyUsageHistoryOverview(
					EnergyAccounts=arguments.EnergyAccounts,
					SelectMethod=arguments.SelectMethod,
					includeAccountsFromAnyProvider=super.hasPermission("e2.pub.dataentry.by-provider.any"),
					includeAccountsFromUserProvider=super.hasPermission("e2.pub.dataentry.by-provider.self"),
					user_uid=super.getUserUid() )>
	</cffunction>

	<cffunction name="getEnergyUsageDateRange"
				access="public" returntype="struct"
				hint="This method is used to retrieve / return a collection of energy usage records specifying the minPeriodStart and maxPeriodEnd for each record.">

		<!--- Define the arguments for this method --->
		<cfargument name="AccountsArray" type="array" required="true" hint="Describes the collection of energy accounts for which usage data will be retrieved.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Build the list of energy account information --->
		<cfset local.energyAccountList = buildVoPropertyListFromArray(
				voArray=arguments.accountsArray,
				propertyName="energy_account_uid")>

		<!--- Retrieve the energy usage records containing the date ranges --->
		<cfreturn this.energyUsageDelegate.getEnergyUsageDateRangeAsStruct(
				energy_account_uid=local.energyAccountList)>

	</cffunction>



	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="saveNewEnergyUsage"
				access="public" returntype="string"
				hint="This method is used to persist a new energyUsage record to the application database.">
		<cfargument name="EnergyUsage" type="mce.e2.vo.EnergyUsage" required="true" hint="Describes the energyUsage collection VO object containing the details of the energyUsage collection to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.energyUsage = setAuditProperties(arguments.energyUsage, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.energyUsage = setPrimaryKey(local.energyUsage)>

		<!--- Save the modified energyUsage --->
		<cfset this.EnergyUsageDelegate.saveNewEnergyUsage(energyUsage=local.energyUsage, user_uid=getUserUid())/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.energyUsage.energy_usage_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewEnergyUsage"
				access="public" returntype="mce.e2.vo.EnergyUsage"
				hint="This method is used to persist a new energyUsage record to the application database.">
		<cfargument name="EnergyUsage" type="mce.e2.vo.EnergyUsage" required="true" hint="Describes the energyUsage collection VO object containing the details of the energyUsage collection to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyUsage(energyUsage=arguments.energyUsage)>

		<!--- Set the primary key --->
		<cfset arguments.energyUsage.energy_usage_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.energyUsage>

	</cffunction>

	<cffunction name="saveExistingEnergyUsage"
				access="public" returntype="mce.e2.vo.EnergyUsage"
				hint="This method is used to save an existing energyUsage information to the application database.">
		<cfargument name="energyUsage" type="mce.e2.vo.EnergyUsage" required="true" hint="Describes the energyUsage collection VO object containing the details of the energyUsage collection to be saved." />
		<cfargument name="addHistory" required="false" type="boolean" default="true" hint="Allow internal users to change the status without affecting the history."/>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.energyUsage = setAuditProperties(arguments.energyUsage, "modify")>

		<!--- Save the modified energyUsage --->
		<cfset this.EnergyUsageDelegate.saveExistingEnergyUsage(energyUsage=local.energyUsage, addHistory=arguments.addHistory, user_uid=getUserUid())/>

		<!--- Return the modified energyUsage --->
		<cfreturn local.energyUsage>

	</cffunction>

	<cffunction name="getDefaultComparisonPeriod"
				access="public" returntype="query"
				hint="This method is used to retrieve the default energy comparison period for a given energy usage record.">

		<!--- Initialize the method arguments --->
		<cfargument name="energyUsage" type="any" required="true" hint="Describes the energy usage object for which the default comparison period will be retrieved."/>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Build a list of the accounts to process --->
		<cfset local.qResult = this.energyUsageDelegate.getDefaultComparisonPeriod(
				energy_usage_uid = arguments.energyUsage.energy_usage_uid)>

		<!--- Return the result --->
		<cfreturn local.qResult>

	</cffunction>






	<cffunction name="getEnergyUsageStatusHistory"
				access="public" returntype="query"
				hint="This method is used to return a query of energy usage status history information.">
		<cfargument name="EnergyUsage" required="true" type="mce.e2.vo.EnergyUsage" hint="Describes the VO containing the Energy Usage record that will be persisted."/>
		<cfreturn this.energyUsageDelegate.getEnergyUsageStatusHistory(EnergyUsage=arguments.EnergyUsage)>
	</cffunction>




</cfcomponent>