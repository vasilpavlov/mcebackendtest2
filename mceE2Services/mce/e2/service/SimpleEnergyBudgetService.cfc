<cfcomponent displayname="Simple Energy Budget Service"
			 extends="BaseDataService" output="false">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setReportingDelegate" 
				access="public" returntype="void">
		<cfargument name="bean" type="mce.e2.db.ReportingDelegate">
		<cfset this.reportingDelegate = arguments.bean>
	</cffunction>

</cfcomponent>