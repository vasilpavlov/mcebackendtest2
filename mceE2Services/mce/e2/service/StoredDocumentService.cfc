<cfcomponent displayname="Stored Document Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with document management.">

	<!--- Default the folder delimiter --->
	<cfset this.folderdelimiter = '/'>

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setStoredDocumentDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage stored document data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.StoredDocumentDelegate" hint="Describes the *.cfc used to manage database interactions related to stored document information.">
		<cfset this.StoredDocumentDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setStoredDocumentSetDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage stored document set data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.StoredDocumentSetDelegate" hint="Describes the *.cfc used to manage database interactions related to stored document set information.">
		<cfset this.StoredDocumentSetDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setEnergyUsageDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage energy usage data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyUsageDelegate" hint="Describes the *.cfc used to manage database interactions related to energy usage information.">
		<cfset this.EnergyUsageDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setEnergyContractsDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage energy contract data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyContractsDelegate" hint="Describes the *.cfc used to manage database interactions related to energy contract information.">
		<cfset this.EnergyContractsDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setClientCompanyDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage client company data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.ClientCompanyDelegate" hint="Describes the *.cfc used to manage database interactions related to client company information.">
		<cfset this.ClientCompanyDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setPropertyDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage property data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyDelegate" hint="Describes the *.cfc used to manage database interactions related to property information.">
		<cfset this.PropertyDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyStoredDocumentSetVo"
				access="public" returntype="mce.e2.vo.StoredDocumentSet"
				hint="This method is used to retrieve / return an empty StoredDocumentSet information value object.">
		<cfreturn this.StoredDocumentSetDelegate.getEmptyStoredDocumentSetComponent()>
	</cffunction>

	<cffunction name="getEmptyStoredDocumentVo"
				access="public" returntype="mce.e2.vo.StoredDocument"
				hint="This method is used to retrieve / return an empty StoredDocument information value object.">
		<cfreturn this.StoredDocumentDelegate.getEmptyStoredDocumentComponent()>
	</cffunction>

	<cffunction name="getStoredDocumentsByContext"
				access="public" returntype="Query"
				hint="This method is used to retrieve stored documents sets by context such as client or property.">
		<cfargument name="client_company_uid" required="false" type="string"/>
		<cfargument name="property_uid" required="false" type="string"/>
		<cfargument name="energy_contract_uid" required="false" type="string"/>
		<cfargument name="client_contract_uid" required="false" type="string"/>
		<cfargument name="include_properties" required="false" type="boolean" default="true"/>
		<cfargument name="filter_by_active" required="false" type="boolean" default="true"/>
		<cfreturn this.StoredDocumentDelegate.getStoredDocumentsByContext(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="getStoredDocumentSets"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Storage Document Sets.">
		<cfreturn this.StoredDocumentSetDelegate.getStoredDocumentSetsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getStoredDocumentSet"
				access="public" returntype="mce.e2.vo.StoredDocumentSet"
				hint="This method is used to return a populated Storage Document Set vo (value object).">

		<!--- Define the arguments for this method --->
		<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" hint="Describes the StoredDocumentSet VO object containing the properties of the StoredDocumentSet to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the stored document set --->
		<cfset local.storedDocumentSet = this.storedDocumentSetDelegate.getStoredDocumentSetAsComponent(
				document_set_uid=arguments.StoredDocumentSet.document_set_uid)>


		<cfset local.permissions = this.getPermissions()>

		<!--- Retrieve the stored documents associated to the stored document set --->
		<cfset local.storedDocumentSet.storedDocuments = this.storedDocumentDelegate.getStoredDocumentsAsArrayOfComponents(
				document_set_uid=arguments.StoredDocumentSet.document_set_uid, permission_required=local.permissions)>

		<!--- Return the stored document object --->
		<cfreturn local.storedDocumentSet>

	</cffunction>

	<cffunction name="getDocumentRepositoryProperties"
				access="public" returntype="struct"
				hint="This method is used to retrieve the document repository file path and primary key for a given repository.">

		<!--- Define the arguments for this method --->
		<cfargument name="repository_lookup_code" type="string" required="false" hint="Describes the internal identifier of the repository to which a given file is added / uploaded.">
		<cfargument name="document_repository_uid" type="string" required="false" hint="Defines the primary key / unique identifier representing a given document repository.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the document repository information from the application database --->
		<cfset local.qRepository = this.StoredDocumentSetDelegate.getStoredDocumentRepository(
				argumentCollection=arguments)>

		<!--- Now that the repository has been retrieved, return the path --->
		<cfset local.result = structNew()>

		<!--- Populate the path and repository primary key --->
		<cfset local.result.repository_path = local.qRepository.absolute_filesystem_path>
		<cfset local.result.document_repository_uid = local.qRepository.document_repository_uid>

		<!--- Return the repository properties --->
		<cfreturn local.result>

	</cffunction>

	<cffunction name="validateDocumentFolders"
				access="public" returntype="void"
				hint="This method is used to validate a chain / collection of document folders for a given document repository.">

		<!--- Define the arguments for this method --->
		<cfargument name="directoryPath" type="string" required="true" hint="Describes the full directory path being validated.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize any local variables --->
		<cfset local.directoryPath = ''>

		<!--- Get the delimiter counts --->
		<cfset local.macPathDelimCount =  findNoCase(arguments.directoryPath, '/')>
		<cfset local.pcPathDelimCount =  findNoCase(arguments.directoryPath, '\')>

		<!--- Convert pc delimiters to mac delimeters --->
		<cfif this.folderDelimiter eq '/' and local.pcPathDelimCount gt 0>
			<cfset arguments.directoryPath = replaceNoCase(arguments.directoryPath, '\', '/', 'all')>
		</cfif>

		<!--- Convert mac delimiters to pc delimiters --->
		<cfif this.folderDelimiter eq '\' and local.macPathDelimCount gt 0>
			<cfset arguments.directoryPath = replaceNoCase(arguments.directoryPath, '/', '\', 'all')>
		</cfif>

		<!--- Loop over the directorty --->
		<cfloop list="#arguments.directoryPath#" index="local.folder" delimiters="#this.folderdelimiter#">

			<!--- Append the folder to the overall directory path --->
			<cfset local.directoryPath = local.directoryPath & this.folderdelimiter & local.folder & this.folderdelimiter>

			<!--- Does the directory exist? --->
			<cfif not directoryExists(local.directoryPath)>

				<!--- If not, then create it --->
				<cfdirectory action="create" directory="#local.directoryPath#">

			</cfif>

		</cfloop>

	</cffunction>

	<cffunction name="getStoredDocuments"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of stored document value objects.">

		<cfargument name="stored_document_uid" required="false" type="string" hint="Describes the primary key / unique identifier for a given stored document."/>
		<cfargument name="document_repository_uid" required="false" type="string" hint="Describes the primary key / unique identifier of the repository where a given document will be stored."/>
		<cfargument name="document_set_uid" required="false" type="string" hint="Describes the primary key / unique identifier representing the document set associated to a given stored document."/>
		<cfargument name="permission_needed" required="false" type="string" hint="Describes the permission level assigned to a given report."/>
		<cfargument name="property_uid" required="false" type="string" hint="Describes the uniqueidentifier for a given property."/>
		<cfargument name="client_company_uid" required="false" type="string" hint="Describes the uniqueidentifier for a given client_company."/>

			<!--- Define the permission level used to retrieve reports >
			<cfset local.args.permission_needed = getPermissions()--->

		<!--- Search for a fake document set if no parameters were defined. --->
		<cfif not len(arguments.stored_document_uid) and not len(arguments.document_repository_uid) and not len(arguments.document_set_uid) and not len(arguments.permission_needed)>
			<cfif structkeyexists(arguments, "property_uid") and len(property_uid)>
				<cfset arguments.document_set_uid = this.propertyDelegate.getPropertyAsComponent(arguments.property_uid).document_set_uid>
			</cfif>
			<cfif structkeyexists(arguments, "client_company_uid") and len(client_company_uid)>
				<cfset arguments.document_set_uid = this.ClientCompanyDelegate.getClientCompanyAsComponent(arguments.client_company_uid).document_set_uid>
			</cfif>
			<cfif not len(arguments.document_set_uid)>
				<cfset arguments.document_set_uid = createUniqueIdentifier()>
			</cfif>
		</cfif>

		<!--- Return a collection of user Reports based on the arguments provided --->
		<cfreturn this.storedDocumentDelegate.getStoredDocumentsAsArrayOfComponents(argumentCollection=arguments)>

	</cffunction>
	<cffunction name="getStoredDocument"
				access="public" returntype="mce.e2.vo.StoredDocument"
				hint="This method is used to return a populated Storage Document vo (value object).">

		<!--- Define the arguments for this method --->
		<cfargument name="StoredDocument" type="mce.e2.vo.StoredDocument" required="true" hint="Describes the StoredDocument VO object containing the properties of the StoredDocument to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Retrieve the stored document set --->
		<cfset local.storedDocument = this.storedDocumentDelegate.getStoredDocumentAsComponent(
				storage_document_uid=arguments.StoredDocument.storage_document_uid)>

		<!--- Return the stored document object --->
		<cfreturn local.storedDocument>

	</cffunction>

	<cffunction name="getStoredDocumentContent"
				access="public" returntype="void"
				hint="This method is used to return a stored document's associated file'.">

		<!--- Define the arguments for this method --->
		<cfargument name="stored_document_uid" type="string" required="true" hint="Describes the primary key / unique identifier of the document being retrieved.">
		<cfargument name="original_filename" type="string" required="true" hint="Describes the original filename of the file being added to a given document repository.">
		<cfargument name="content_disposition" type="string" required="true" default="attachment" hint="Describes the content disposition used to render the retrieved file.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		<cfset var contentType = "">

		<!--- Retrieve the stored document set --->
		<cfset local.storedDocument = this.storedDocumentDelegate.getStoredDocumentAsComponent(
				stored_document_uid=arguments.stored_document_uid,
				original_filename=arguments.original_filename)>

		<cfset contentType = guessContentType(local.storedDocument.preferred_filename, local.storedDocument.mime_type)>

		<!--- Retrieve the document repository information from the application database --->
		<cfset local.documentRepository = getDocumentRepositoryProperties(document_repository_uid=local.storedDocument.document_repository_uid)>

		<!--- Build out the folder path where files will be uploaded --->
		<cfset local.repositoryPath = local.documentRepository.repository_path & '\' & local.storedDocument.folder_name & '\' >

		<!--- Set the header properties --->
		<cfheader name="content-disposition" value="#arguments.content_disposition#;filename=#local.storedDocument.preferred_filename#">

		<!--- Serve the document --->
		<cfcontent type="#contentType#" file="#local.repositoryPath##local.storedDocument.content_as_filename#">

	</cffunction>

	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="createStoredDocument"
				access="public" returntype="string"
				hint="This method is used to create / upload a stored document.">

		<!--- Define the arguments for this method --->
		<cfargument name="document_set_uid" type="string" required="true" hint="Describes the primary key / unique identifier of the document set to which a given file is added / uploaded.">
		<cfargument name="repository_lookup_code" type="string" required="true" hint="Describes the internal identifier of the repository to which a given file is added / uploaded.">
		<cfargument name="friendly_name" type="string" required="true" hint="Describes the external / customer facing name for a given document.">
		<cfargument name="original_filename" type="string" required="true" hint="Describes the original filename of the file being added to a given document repository.">
		<cfargument name="document_keywords" type="string" required="false" default="" hint="Describes the keywords associated to a given document.">
		<cfargument name="preferred_filename" type="string" required="true" hint="Describes the preferred / user selected filename for the file being added to a given document repository.">
		<cfargument name="permission_needed" type="string" required="true" hint="Describes the name of the folder where a given file will be uploaded.">
		<cfargument name="folder_name" type="string" required="true" hint="Describes the permissions needed to retrieve this document.">
		<cfargument name="file_content" type="string" required="true" hint="Describes the file content of the file being added to a given document repository.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Create uid to use for document record and file system. --->
		<cfset local.stored_document_uid = createUniqueIdentifier()>
		<cfset local.content_as_filename = arguments.friendly_name & "_" & local.stored_document_uid & replace(arguments.preferred_filename, arguments.friendly_name, "")>

		<!--- Retrieve the document repository information from the application database --->
		<cfset local.documentRepository = getDocumentRepositoryProperties(repository_lookup_code=arguments.repository_lookup_code)>

		<!--- Build out the folder path where files will be uploaded --->
		<cfset local.repositoryPath = local.documentRepository.repository_path & this.folderdelimiter & arguments.folder_name & this.folderdelimiter>

		<!--- Validate the directory --->
		<cfset validateDocumentFolders(local.repositoryPath)>

		<!--- Recieve the uploaded file --->
		<cffile action="upload"
				nameconflict="overwrite"
				filefield="file_content"
				destination="#local.repositoryPath#"
				result="local.cffile">

		<cffile action="rename"
				source="#local.repositoryPath##local.cffile.serverFile#"
				destination="#local.repositoryPath##local.content_as_filename#">

		<!--- Build / create a new storedDocument record --->
		<cfset local.storedDocument = getEmptyStoredDocumentVo()>

		<!--- Set the stored document properties --->
		<cfset local.storedDocument.stored_document_uid = local.stored_document_uid>
		<cfset local.storedDocument.document_set_uid = arguments.document_set_uid>
		<cfset local.storedDocument.friendly_name = arguments.friendly_name>
		<cfset local.storedDocument.document_keywords = arguments.document_keywords>
		<cfset local.storedDocument.original_filename = arguments.original_filename>
		<cfset local.storedDocument.preferred_filename = arguments.preferred_filename>
		<cfset local.storedDocument.content_as_filename = local.content_as_filename>
		<cfset local.storedDocument.folder_name = arguments.folder_name>
		<cfset local.storedDocument.permission_needed = arguments.permission_needed>
		<cfset local.storedDocument.document_repository_uid = local.documentRepository.document_repository_uid>
		<cfset local.storedDocument.storage_date = dateFormat(now(), "m/d/yyyy")>
		<cfset local.storedDocument.mime_type = guessContentType(
			arguments.preferred_filename,
			local.cffile.contentType & '/' & local.cffile.contentSubType
		)>

		<!--- Save the new stored document --->
		<cfset local.storedDocument = saveAndReturnNewStoredDocument(local.storedDocument)>

		<!--- Return the primary key --->
		<cfreturn local.storedDocument.stored_document_uid>

	</cffunction>


	<cffunction name="guessContentType" returntype="string" access="private">
		<cfargument name="filename" type="string" required="true">
		<cfargument name="defaultType" type="string" required="true">

		<cfset var mimetype = arguments.defaultType>
		<cfset var extension = "">

		<cfif ListLen(arguments.filename, ".") gte 2>
			<cfset extension = trim(ListLast(filename, "."))>
		</cfif>

		<!--- Stolen from Utilities.cfc in ColdBox distribution (open source) --->
		<cfif listFindNocase("txt,js,css,cfm,cfc,html,htm,jsp",extension)>
			<cfset mimetype = "text/plain">
		<cfelseif extension eq "gif">
			<cfset mimetype = "image/gif">
		<cfelseif extension eq "jpg">
			<cfset mimetype = "image/jpg">
		<cfelseif extension eq "png">
			<cfset mimetype = "image/png">
		<cfelseif extension eq "wav">
			<cfset mimetype = "audio/wav">
		<cfelseif extension eq "mp3">
			<cfset mimetype = "audio/mpeg3">
		<cfelseif extension eq "pdf">
			<cfset mimetype = "application/pdf">
		<cfelseif extension eq "zip">
			<cfset mimetype = "application/zip">
		<cfelseif extension eq "ppt" or extension eq "pptx">
			<cfset mimetype = "application/powerpoint">
		<cfelseif extension eq "doc" or extension eq "docx">
			<cfset mimetype = "application/word">
		<cfelseif extension eq "xls" or extension eq "xlsx" or extension eq "csv">
			<cfset mimetype = "application/excel">
		<cfelse>
			<cfset mimetype = "application/octet-stream">
		</cfif>

		<cfreturn mimetype>
	</cffunction>


	<cffunction name="saveNewStoredDocument"
				access="public" returntype="string"
				hint="This method is used to persist a new StoredDocument record to the application database.">
		<cfargument name="StoredDocument" type="mce.e2.vo.StoredDocument" required="true" hint="Describes the StoredDocument VO object containing the details of the StoredDocument to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.StoredDocument = setAuditProperties(arguments.StoredDocument, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.StoredDocument = setPrimaryKey(local.StoredDocument)>

		<!--- Save the modified StoredDocumentSet --->
		<cfset this.StoredDocumentDelegate.saveNewStoredDocument(StoredDocument=local.StoredDocument)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.StoredDocument.stored_document_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewStoredDocument"
				access="public" returntype="mce.e2.vo.StoredDocument"
				hint="This method is used to persist a new StoredDocument record to the application database.">
		<cfargument name="StoredDocument" type="mce.e2.vo.StoredDocument" required="true" hint="Describes the StoredDocument VO object containing the details of the StoredDocument to be saved and returned." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewStoredDocument(StoredDocument=arguments.StoredDocument)>

		<!--- Set the primary key --->
		<cfset arguments.StoredDocument.stored_document_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.StoredDocument>

	</cffunction>

	<cffunction name="saveExistingStoredDocument"
				access="public" returntype="mce.e2.vo.StoredDocument"
				hint="This method is used to save an existing StoredDocument information to the application database.">
		<cfargument name="StoredDocument" type="mce.e2.vo.StoredDocument" required="true" hint="Describes the StoredDocument VO object containing the details of the StoredDocument to be modified." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.StoredDocument = setAuditProperties(arguments.StoredDocument, "modify")>

		<!--- Save the modified StoredDocument --->
		<cfset this.StoredDocumentDelegate.saveExistingStoredDocument(StoredDocument=local.StoredDocument)/>

		<!--- Return the modified StoredDocument --->
		<cfreturn local.StoredDocument>

	</cffunction>

	<cffunction name="removeStoredDocument"
				access="public" returntype="void"
				hint="This method is used to remove an existing StoredDocument from the application database.">
		<cfargument name="StoredDocument" type="mce.e2.vo.StoredDocument" required="true" hint="Describes the StoredDocument VO object being removed from the application database." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--_ Validate that a valid document set primary key was specified --->
		<cfif not isUniqueIdentifier(arguments.storedDocument.document_set_uid)>
			<cfset arguments.storedDocument = getStoredDocument(arguments.storedDocument)>
		</cfif>

		<!--- Set the modified by / date audit properties --->
		<cfset local.StoredDocument = setAuditProperties(arguments.StoredDocument, "modify")>

		<!--- Deactivate the stored document --->
		<cfset local.StoredDocument.is_active = 0>

		<!--- Save the modified StoredDocument, and deactivate it --->
		<cfset this.StoredDocumentDelegate.deactivateStoredDocument(
				document_set_uid = local.storedDocument.document_set_uid,
				stored_document_uid = local.storedDocument.stored_document_uid,
				modified_by = local.storedDocument.modified_by,
				modified_date = local.storedDocument.modified_date)/>

	</cffunction>

	<!--- Create the methods to set / retrieve new stored document sets --->
	<cffunction name="saveNewStoredDocumentSet"
				access="public" returntype="string"
				hint="This method is used to persist a new StoredDocumentSet record to the application database.">
		<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" hint="Describes the StoredDocumentSet VO object containing the details of the StoredDocumentSet to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the created by / date audit properties --->
		<cfset local.StoredDocumentSet = setAuditProperties(arguments.StoredDocumentSet, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.StoredDocumentSet = setPrimaryKey(local.StoredDocumentSet)>

		<!--- Save the modified StoredDocumentSet --->
		<cfset this.StoredDocumentSetDelegate.saveNewStoredDocumentSet(StoredDocumentSet=local.StoredDocumentSet)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.StoredDocumentSet.document_set_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewStoredDocumentSet"
				access="public" returntype="mce.e2.vo.StoredDocumentSet"
				hint="This method is used to persist a new StoredDocumentSet record to the application database.">
		<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" hint="Describes the StoredDocumentSet VO object containing the details of the StoredDocumentSet to be saved and returned." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewStoredDocumentSet(StoredDocumentSet=arguments.StoredDocumentSet)>

		<!--- Set the primary key --->
		<cfset arguments.StoredDocumentSet.document_set_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.StoredDocumentSet>

	</cffunction>

	<cffunction name="associateNewStoredDocumentSetToClientCompany"
				access="public" returntype="mce.e2.vo.StoredDocumentSet"
				hint="This method is used to persist a stored document set and associate it to a specific client company instance.">

		<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" hint="Describes the StoredDocumentSet VO object containing the details of the StoredDocumentSet to be saved and returned." />
		<cfargument name="ClientCompany" type="mce.e2.vo.ClientCompany" required="true" hint="Describes the Client Company VO object containing the details of the ClientCompany record to be associated." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Save the stored document set and return the saved object --->
		<cfif arguments.storedDocumentSet.document_set_uid eq "">
			<cfset local.StoredDocumentSet = saveAndReturnNewStoredDocumentSet(arguments.storedDocumentSet)>
		<cfelse>
			<cfset local.StoredDocumentSet = arguments.storedDocumentSet>
		</cfif>

		<!--- If we have an client company uid (if not we'll expect to get called again later) --->
		<cfif arguments.ClientCompany.client_company_uid neq "">
			<!--- Set the document set property --->
			<cfset local.clientCompany = structNew()>
			<cfset local.clientCompany = setAuditProperties(local.clientCompany, "modify")>
			<cfset local.clientCompany.client_company_uid = arguments.ClientCompany.client_company_uid>
			<cfset local.clientCompany.document_set_uid = local.storedDocumentSet.document_set_uid>

			<!--- Save the existing energy usage property --->
			<cfset this.ClientCompanyDelegate.saveExistingClientCompany(local.clientCompany)>
		</cfif>

		<!--- Return the created stored document set --->
		<cfreturn local.StoredDocumentSet>

	</cffunction>

	<cffunction name="associateNewStoredDocumentSetToProperty"
				access="public" returntype="mce.e2.vo.StoredDocumentSet"
				hint="This method is used to persist a stored document set and associate it to a specific property instance.">

		<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" hint="Describes the StoredDocumentSet VO object containing the details of the StoredDocumentSet to be saved and returned." />
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the Property VO object containing the details of the Property record to be associated." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Save the stored document set and return the saved object --->
		<cfif arguments.storedDocumentSet.document_set_uid eq "">
			<cfset local.StoredDocumentSet = saveAndReturnNewStoredDocumentSet(arguments.storedDocumentSet)>
		<cfelse>
			<cfset local.StoredDocumentSet = arguments.storedDocumentSet>
		</cfif>

		<!--- If we have an client company uid (if not we'll expect to get called again later) --->
		<cfif arguments.Property.property_uid neq "">
			<!--- Set the document set property --->
			<cfset local.property = arguments.Property>
			<cfset local.property = setAuditProperties(local.property, "modify")>
			<cfset local.property.property_uid = arguments.Property.property_uid>
			<cfset local.property.document_set_uid = local.storedDocumentSet.document_set_uid>
			
			
			<!--- Save the existing property --->
			<cfset this.PropertyDelegate.saveExistingProperty(local.property)>
		</cfif>

		<!--- Return the created stored document set --->
		<cfreturn local.StoredDocumentSet>

	</cffunction>

	<cffunction name="associateNewStoredDocumentSetToEnergyContract"
				access="public" returntype="mce.e2.vo.StoredDocumentSet"
				hint="This method is used to persist a stored document set and associate it to a specific energy contract instance.">

		<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" hint="Describes the StoredDocumentSet VO object containing the details of the StoredDocumentSet to be saved and returned." />
		<cfargument name="EnergyContracts" type="mce.e2.vo.EnergyContracts" required="true" hint="Describes the Energy Contracts VO object containing the details of the EnergyContracts record to be associated." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Save the stored document set and return the saved object --->
		<cfif arguments.storedDocumentSet.document_set_uid eq "">
			<cfset local.StoredDocumentSet = saveAndReturnNewStoredDocumentSet(arguments.storedDocumentSet)>
		<cfelse>
			<cfset local.StoredDocumentSet = arguments.storedDocumentSet>
		</cfif>

		<!--- If we have an energy usage uid (if not we'll expect to get called again later) --->
		<cfif arguments.energyContracts.energy_contract_uid neq "">
			<!--- Set the document set property --->
			<cfset local.energyContract = this.EnergyContractsDelegate.getEmptyEnergyContractsComponent()>
			<cfset local.energyContract = setAuditProperties(local.energyContract, "modify")>
			<cfset local.energyContract.energy_contract_uid = arguments.energyContracts.energy_contract_uid>
			<cfset local.energyContract.document_set_uid = local.storedDocumentSet.document_set_uid>

			<!--- Save the existing energy usage property --->
			<cfset this.EnergyContractsDelegate.saveExistingEnergyContracts(local.energyContract,false)>
		</cfif>

		<!--- Return the created stored document set --->
		<cfreturn local.StoredDocumentSet>

	</cffunction>

	<cffunction name="associateNewStoredDocumentSetToEnergyUsage"
				access="public" returntype="mce.e2.vo.StoredDocumentSet"
				hint="This method is used to persist a stored document set and associate it to a specific energy usage instance.">

		<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" hint="Describes the StoredDocumentSet VO object containing the details of the StoredDocumentSet to be saved and returned." />
		<cfargument name="EnergyUsage" type="mce.e2.vo.EnergyUsage" required="true" hint="Describes the Energy Usage VO object containing the details of the EnergyUsage record to be associated." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Save the stored document set and return the saved object --->
		<cfif arguments.storedDocumentSet.document_set_uid eq "">
			<cfset local.StoredDocumentSet = saveAndReturnNewStoredDocumentSet(arguments.storedDocumentSet)>
		<cfelse>
			<cfset local.StoredDocumentSet = arguments.storedDocumentSet>
		</cfif>

		<!--- If we have an energy usage uid (if not we'll expect to get called again later) --->
		<cfif arguments.energyUsage.energy_usage_uid neq "">
			<!--- Set the document set property --->
			<cfset local.energyUsage = structNew()>
			<cfset local.energyUsage = setAuditProperties(local.energyUsage, "modify")>
			<cfset local.energyUsage.energy_usage_uid = arguments.energyUsage.energy_usage_uid>
			<cfset local.energyUsage.document_set_uid = local.storedDocumentSet.document_set_uid>

			<!--- Save the existing energy usage property --->
			<cfset this.energyUsageDelegate.saveExistingEnergyUsage(local.energyUsage,false)>
		</cfif>
		
		<!--- Return the created stored document set --->
		<cfreturn local.StoredDocumentSet>

	</cffunction>

	<cffunction name="saveExistingStoredDocumentSet"
				access="public" returntype="mce.e2.vo.StoredDocumentSet"
				hint="This method is used to save an existing StoredDocumentSet information to the application database.">
		<cfargument name="StoredDocumentSet" type="mce.e2.vo.StoredDocumentSet" required="true" hint="Describes the StoredDocumentSet VO object containing the details of the StoredDocumentSet to be modified." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Set the modified by / date audit properties --->
		<cfset local.StoredDocumentSet = setAuditProperties(arguments.StoredDocumentSet, "modify")>

		<!--- Save the modified StoredDocumentSet --->
		<cfset this.StoredDocumentSetDelegate.saveExistingStoredDocumentSet(StoredDocumentSet=local.StoredDocumentSet)/>

		<!--- Return the modified StoredDocumentSet --->
		<cfreturn local.StoredDocumentSet>

	</cffunction>

</cfcomponent>