<cfcomponent displayname="Property Meta Type Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Property Meta Type information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setPropertyMetaTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Property Meta Type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyMetaTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to Property Meta Type information.">
		<cfset this.PropertyMetaTypeDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyPropertyMetaTypeVo" 
				access="public" returntype="mce.e2.vo.PropertyMetaType"
				hint="This method is used to retrieve / return an empty PropertyMetaType information value object.">
		<cfreturn this.PropertyMetaTypeDelegate.getEmptyPropertyMetaTypeComponent()>
	</cffunction>

	<cffunction name="getPropertyMetaTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Property Meta Types.">
		<cfreturn this.PropertyMetaTypeDelegate.getPropertyMetaTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getPropertyMetaType" 
				access="public" returntype="mce.e2.vo.PropertyMetaType" 
				hint="This method is used to return a populated PropertyMetaType vo (value object)."> 
		<cfargument name="PropertyMetaType" type="mce.e2.vo.PropertyMetaType" required="true" hint="Describes the PropertyMetaType collection VO object containing the properties of the PropertyMetaType collection to be retrieved." /> 
		<cfreturn this.PropertyMetaTypeDelegate.getPropertyMetaTypeAsComponent(meta_type_uid=arguments.PropertyMetaType.meta_type_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewPropertyMetaType" 
				access="public" returntype="string" 
				hint="This method is used to persist a new PropertyMetaType record to the application database."> 
		<cfargument name="PropertyMetaType" type="mce.e2.vo.PropertyMetaType" required="true" hint="Describes the PropertyMetaType collection VO object containing the details of the PropertyMetaType collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.PropertyMetaType = setAuditProperties(arguments.PropertyMetaType, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.PropertyMetaType = setPrimaryKey(local.PropertyMetaType)>
		
		<!--- Save the modified PropertyMetaType --->
		<cfset this.PropertyMetaTypeDelegate.saveNewPropertyMetaType(PropertyMetaType=local.PropertyMetaType)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.PropertyMetaType.meta_type_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewPropertyMetaType" 
				access="public" returntype="mce.e2.vo.PropertyMetaType" 
				hint="This method is used to persist a new PropertyMetaType record to the application database."> 
		<cfargument name="PropertyMetaType" type="mce.e2.vo.PropertyMetaType" required="true" hint="Describes the PropertyMetaType collection VO object containing the details of the PropertyMetaType collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewPropertyMetaType(PropertyMetaType=arguments.PropertyMetaType)>

		<!--- Set the primary key --->
		<cfset arguments.PropertyMetaType.meta_type_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.PropertyMetaType>

	</cffunction>

	<cffunction name="saveExistingPropertyMetaType" 
				access="public" returntype="mce.e2.vo.PropertyMetaType" 
				hint="This method is used to save an existing PropertyMetaType information to the application database."> 
		<cfargument name="PropertyMetaType" type="mce.e2.vo.PropertyMetaType" required="true" hint="Describes the PropertyMetaType collection VO object containing the details of the PropertyMetaType collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.PropertyMetaType = setAuditProperties(arguments.PropertyMetaType, "modify")>
		
		<!--- Save the modified PropertyMetaType --->
		<cfset this.PropertyMetaTypeDelegate.saveExistingPropertyMetaType(PropertyMetaType=local.PropertyMetaType)/>

		<!--- Return the modified PropertyMetaType --->
		<cfreturn local.PropertyMetaType>	

	</cffunction>
	
</cfcomponent>