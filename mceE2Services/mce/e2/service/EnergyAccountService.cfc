<cfcomponent displayname="Energy Account Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of energy account information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyAccountDelegate"
				access="public" returntype="void"
				hint="This method is used to set the database delegate used to manage energy account data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyAccountDelegate" hint="Describes the *.cfc used to manage database interactions for energy accounts."/>
		<cfset this.EnergyAccountDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyAccountVo"
				access="public" returntype="mce.e2.vo.EnergyAccount"
				hint="This method is used to retrieve / return an empty energyAccount information value object.">
		<cfreturn this.energyAccountDelegate.getEmptyEnergyAccountComponent()>
	</cffunction>

	<cffunction name="getEnergyAccounts"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy account types.">
		<cfreturn this.energyAccountDelegate.getEnergyAccountsAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyAccountsByProperty"
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy account types.">
		<cfargument name="property_uid" required="true" type="string" hint="Describes the primary key of the property associated to a given energy account."/>
		<cfreturn this.energyAccountDelegate.getEnergyAccountsByProperty(property_uid=arguments.property_uid)>
	</cffunction>

	<cffunction name="getEnergyAccount"
				access="public" returntype="mce.e2.vo.EnergyAccount"
				hint="This method is used to return a populated EnergyAccount vo (value object).">
		<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" hint="Describes the energyAccount collection VO object containing the properties of the energyAccount collection to be retrieved." />
		<cfreturn this.energyAccountDelegate.getEnergyAccountAsComponent(energy_account_uid=arguments.EnergyAccount.energy_account_uid)>
	</cffunction>

	<!--- Proxy methods --->
	<cffunction name="getAvailableEnergyAccountSummaries"
				returntype="array"
				hint="Retrieves energy accounts for a given property or properties.">

		<!--- Define the arguments for this method --->
		<cfargument name="propertyArray" type="array" required="false" hint="Describes the array collection of properties that will be used to retrieve energy account summaries." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Initialize the arguments structure --->
		<cfset local.args = structNew()>

		<!--- Only calculate the property_uid list if the array was passed to this method --->
		<cfif structKeyExists(arguments, 'propertyArray')>

			<!--- Build out the property_uid list --->
			<cfset local.args.property_uid = buildVoPropertyListFromArray(voArray=arguments.propertyArray, propertyName='property_uid')>

		</cfif>

		<!--- default the other arguments --->
		<cfset local.args.relationship_filter_date = arguments.relationship_filter_date>
		<cfset local.args.selectMethod = arguments.selectMethod>

		<!--- Retrieve the account summary --->
		<cfset local.accountSummaryArray = this.energyAccountDelegate.getEnergyAccountSummaryAsArrayOfComponents(argumentCollection=local.args)>

		<!--- Return the result --->
		<cfreturn local.accountSummaryArray>

	</cffunction>

	<cffunction name="getAvailableEnergyAccountSummaryByEnergyType"
				returntype="query"
				hint="Retrieves energy accounts for a given property or properties.">

		<!--- Define the arguments for this method --->
		<cfargument name="propertyArray" type="array" required="false" hint="Describes the array collection of properties that will be used to retrieve energy account summaries." />

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Initialize the arguments structure --->
		<cfset local.args = structNew()>

		<!--- Only calculate the property_uid list if the array was passed to this method --->
		<cfif structKeyExists(arguments, 'propertyArray')>

			<!--- Build out the property_uid list --->
			<cfset local.args.property_uid = buildVoPropertyListFromArray(voArray=arguments.propertyArray, propertyName='property_uid')>

		</cfif>


		<!--- Retrieve the account summary --->
		<<cfset local.qResult = this.energyAccountDelegate.getAvailableEnergyAccountSummaryByEnergyType(argumentCollection=local.args)>

		<!--- Return the result --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getCorporateEmissionsSources"
				returntype="query"
				hint="Retrieves Corporate Emissions Sources.">

		<!--- Define the arguments for this method --->
		<cfargument name="propertyArray" type="array" required="false" hint="Describes the array collection of properties that will be used to retrieve energy account summaries." />

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Initialize the arguments structure --->
		<cfset local.args = structNew()>

		<!--- Only calculate the property_uid list if the array was passed to this method --->
		<cfif structKeyExists(arguments, 'propertyArray')>

			<!--- Build out the property_uid list --->
			<cfset local.args.property_uid = buildVoPropertyListFromArray(voArray=arguments.propertyArray, propertyName='property_uid')>

		</cfif>


		<!--- Retrieve the account summary --->
		<<cfset local.qResult = this.energyAccountDelegate.getCorporateEmissionsSources(argumentCollection=local.args)>

		<!--- Return the result --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getEnergyAccountSummariesForDataAnalysis"
				returntype="query"
				hint="Retrieves energy account summaries to be used for data analysis for a given property or properties.">

		<!--- Define the arguments for this method --->
		<cfargument name="propertyArray" type="array" required="false" hint="Describes the array collection of properties that will be used to retrieve energy account summaries." />
		<cfargument name="selectMethod" type="string" required="true" default="current" hint="Describes the method used to select data (current = select current / active data, all = select all previous data).">
		<cfargument name="relationship_filter_date" type="string" required="true" default="#now()#" hint="Describes the date used to filter active records.">

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Initialize the arguments structure --->
		<cfset local.args = structNew()>

		<!--- Only calculate the property_uid list if the array was passed to this method --->
		<cfif structKeyExists(arguments, 'propertyArray')>

			<!--- Build out the property_uid list --->
			<cfset local.args.property_uid = buildVoPropertyListFromArray(voArray=arguments.propertyArray, propertyName='property_uid')>

		</cfif>

		<!--- default the other arguments --->
		<cfset local.args.relationship_filter_date = arguments.relationship_filter_date>
		<cfset local.args.selectMethod = arguments.selectMethod>

		<!--- Retrieve the account summary --->
		<cfset local.qResult = this.energyAccountDelegate.getEnergyAccountSummaryForDataAnalysis(argumentCollection=local.args)>

		<!--- Return the result --->
		<cfreturn local.qResult>

	</cffunction>

	<cffunction name="getEnergyAccountSummary"
				access="public" returntype="array"
				hint="Retrieves an energy account summary for a given energy account.">

		<!--- Define the arguments for this method --->
		<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" hint="Describes the energyAccount collection VO object containing the details of the energyAccount collection to be retrieved." />

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>

		<!--- Initialize the arguments structure --->
		<cfset local.args = structNew()>

		<!--- Retrieve the account summary --->
		<cfset local.accountSummaryArray = this.energyAccountDelegate.getEnergyAccountSummaryAsArrayOfComponents(energy_account_uid=arguments.energyAccount.energy_account_uid)>

		<!--- Return the result --->
		<cfreturn local.accountSummaryArray>

	</cffunction>

	<cffunction name="getPropertyMetaDataAnalysis"
				access="public" return="array"
				hint="Retrieves energy analysis driven by property meta data.">

		<!--- Define the arguments associated with this method --->
		<cfargument name="energyAccountArray" type="array" required="true" hint="Describes the array of energy accounts to process.">
		<cfargument name="propertyMetaTypeArray" type="array" required="true" hint="Describes the array of property meta types to process.">
		<cfargument name="dateRollupMode" type="string" required="true" hint="Describes the data roll-up mode that will be implemented (acceptable values are 'daily', 'monthly').">
		<cfargument name="period_start" type="date" required="true" hint="Describes the start date that will be used to process period data.">
		<cfargument name="period_end" type="date" required="true" hint="Describes the end date that will be used to process period data.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the arguments scope --->
		<cfset local.args = structNew()>

		<!--- Verify that only values of 'daily' and 'monthly' are used for dateRollupMode --->
		<cfif arguments.dateRollUp neq 'daily' and arguments.dateRollUp neq 'monthly'>

			<!--- If not, then throw an error --->
			<cfthrow message="Unsupported getPropertyMetaDataAnalysis.dateRollupMode argument"
					 detail="The getPropertyMetaDataAnalysis.dateRollupMode argument provided ('#dateRollupMode#') is not supported. The currently implemented modes are: 'daily', 'monthly'.">

		</cfif>

		<!--- Build the primary key lists --->
		<cfset local.args.energy_account_uid = buildVoPropertyListFromArray(arguments.energyAccountArray, 'energy_account_uid')>
		<cfset local.args.meta_type_uid = buildVoPropertyListFromArray(arguments.propertyMetaTypeArray, 'meta_type_uid')>

		<!--- Complete the contents of the arguments scope --->
		<cfset local.args.dateRollupMode = arguments.dateRollupMode>
		<cfset local.args.period_start = arguments.period_start>
		<cfset local.args.period_end = arguments.period_end>

		<!--- Retrieve the meta data analysis --->
		<cfset local.result = this.energyAccountDelegate.getPropertyMetaDataAnalysis(
				argumentCollection=local.args)>

		<!--- Return the query result ---->
		<cfreturn local.args>

	</cffunction>

<!---	<cffunction name="getEnergyMetaDataAnalysis"
				access="public" return="array"
				hint="Retrieves energy analysis driven by energy meta data.">

		<!--- Define the arguments associated with this method --->
		<cfargument name="energyAccountArray" type="mce.e2.vo.EnergyMetaDataAnalysis" required="true" hint="Describes the property object used to filter data analysis information.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

	</cffunction>--->

<!---	<cffunction name="getRateFactorDataAnalysis"
				access="public" return="array"
				hint="Retrieves energy analysis driven by rate factor data.">

		<!--- Define the arguments associated with this method --->
		<cfargument name="RateFactorDataAnalysis" type="mce.e2.vo.RateFactorDataAnalysis" required="true" hint="Describes the property object used to filter data analysis information.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

	</cffunction>--->

	<cffunction name="saveAndReturnNewEnergyAccount"
				access="public" returntype="mce.e2.vo.EnergyAccount"
				hint="This method is used to persist a new energyAccount record to the application database.">
		<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" hint="Describes the energyAccount collection VO object containing the details of the energyAccount collection to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyAccount(energyAccount=arguments.energyAccount)>

		<!----
		After we save the new account we check if it is a related account and is for Utility, Full Service.
		If yes we need to add a native acount which is with start date for the new account and is Retail Access,
		in other words we change the master account from Full Service to RetailAccess.
		
		As for now it is working only for ConEdison 9-I and 9-II accounts, but it will work with future accounts given
		the Data Entry Fields friendly_name contain Full Access and Retail Access in it and some equal text in front of it
		for matching Full Service and Retail access templates for example:
		
		Electricity (9-I, Con Edison - Bills) - (kWh - Full Service ) 
		Electricity (9-I, Con Edison - Bills) - (kWh - Retail Access ) 
		---->		
		<cfif StructKeyExists(EnergyAccount, "master_energy_account_uid") and (EnergyAccount.master_energy_account_uid neq "")>
			<cfset arguments.EnergyAccount.energy_account_uid = arguments.EnergyAccount.master_energy_account_uid>

			<!--- Get the completely rendered energy account --->
			<cfset local.master = getEnergyAccount(arguments.energyAccount)>
			<cfset local.masterMod = getEnergyAccount(arguments.energyAccount)>
			<cfset local.strpos = findnocase("FULL SERVICE", local.master.friendlymetatypegroupname)>
			
			<cfif (UCase(local.master.friendlyenergyaccounttypename) eq "UTILITY") and (local.strpos gt 0)>
				
				<cfset local.newstr = left(local.master.friendlymetatypegroupname, local.strpos - 1) & "%Retail Access%">
				<cfset local.newmetauid = this.EnergyAccountDelegate.getMetaGroupUidByName(local.newstr)>
								
				<cfif local.newmetauid neq "">
					<cfset local.masterMod.meta_group_uid = local.newmetauid>
					
					<cfset local.nativeAccounts = getNativeAccountHistory(local.master)>
					
					<!--- we need to get the last native account for the master and give it as precending --->
					<cfset local.precending = StructNew()>
					<cfset local.cols = ListToArray(local.nativeAccounts.ColumnList)>
					<cfset local.lastIdx = local.nativeAccounts.RecordCount>
					
					<cfloop index="local.colIdx" from="1" to="#ArrayLen(local.cols)#" step="1">
						<cfset local.colName = local.cols[local.colIdx]>
						<cfset local.precending[local.colName] = local.nativeAccounts[local.colName][local.lastIdx]>
					</cfloop>
					
					<cfset insertNativeHistoryRecord(local.master, local.masterMod, "insert-after", arguments.EnergyAccount.status_date, "", local.precending)>
				</cfif>
			</cfif>
		</cfif>
		
		<!--- Set the primary key --->
		<cfset arguments.EnergyAccount.energy_account_uid = local.primaryKey>

		<!--- Get the completely rendered energy account --->
		<cfset local.energyAccount = getEnergyAccount(arguments.energyAccount)>

		<!--- Return the new object --->
		<cfreturn local.energyAccount>

	</cffunction>

	<cffunction name="saveNewEnergyAccountAndReturnEnergyAccountSummary"
				access="public" returntype="mce.e2.vo.EnergyAccountSummary"
				hint="This method is used to persist a new energyAccount record to the application database, and return a corresponding energy account summary record.">
		<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" hint="Describes the energyAccount collection VO object containing the details of the energyAccount collection to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyAccount(energyAccount=arguments.energyAccount)>

		<!--- Set the primary key --->
		<cfset arguments.EnergyAccount.energy_account_uid = local.primaryKey>

		<!--- Get the completely rendered energy account --->
		<cfset local.energyAccountSummary = getEnergyAccountSummary(arguments.energyAccount)>

		<!--- Return the new object --->
		<cfreturn local.energyAccount>

	</cffunction>

	<!--- Author all the methods that will modify data across the application database --->
	<cffunction name="saveNewEnergyAccount"
				access="public" returntype="string"
				hint="This method is used to persist a new energyAccount record to the application database.">
		<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" hint="Describes the energyAccount collection VO object containing the details of the energyAccount collection to be retrieved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>		
		
		<!--- Set the created by / date audit properties --->
		<cfset local.energyAccount = setAuditProperties(arguments.energyAccount, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.energyAccount = setPrimaryKey(local.energyAccount)>

		<!--- Step 1: Save the created energyAccount --->
		<cfset this.EnergyAccountDelegate.saveNewEnergyAccount(energyAccount=local.energyAccount)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.energyAccount.energy_account_uid>

		<!--- Step 2: Create an instance of and save the new energy native account --->
		<cfset local.energyNativeAccountObject = createNativeEnergyAccountObject(energyAccount=local.energyAccount)>
		<cfset this.EnergyAccountDelegate.saveNewEnergyNativeAccount(EnergyNativeAccount=local.energyNativeAccountObject)>

		<!--- Get the default rate model / rate class association --->
		<cfset local.defaultRateClassRateModelAssociation = queryRowToStruct(this.EnergyAccountDelegate.getEnergyProviderRateClassRateModelAssociation(
				rate_class_uid=local.energyAccount.rate_class_uid))>

		<!--- Step 3: Create an instance of and save the new energy native account / energy provider rate class association --->
		<cfset local.energyNativeAccountEnergyProviderRateClassAssocationObject = createEnergyNativeAccountEnergyProviderRateClassAssociationObject(
				energyAccount=local.energyAccount,
				energyNativeAccount=local.energyNativeAccountObject,
				defaultRateClassRateModelAssociation=local.defaultRateClassRateModelAssociation)>

		<!--- Save the energy native account / energy provider rate class association --->
		<cfset this.EnergyAccountDelegate.saveNewEnergyNativeAccountEnergyProviderRateClassAssociation(
				associationObject=local.energyNativeAccountEnergyProviderRateClassAssocationObject)>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveExistingEnergyAccount"
				access="public" returntype="void"
				hint="This method is used to save an existing energyAccount information to the application database.">
		<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true" hint="Describes the energyAccount collection VO object containing the details of the energyAccount collection to be saved." />

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get the original instance of the energy account --->
		<cfset local.originalEnergyAccount = getEnergyAccount(EnergyAccount=arguments.energyAccount)>

		<!--- Set the created by / date audit properties --->
		<cfset local.energyAccount = setAuditProperties(arguments.energyAccount, "modify")>

		<!--- Step 1: Save the modified / updated energyAccount --->
		<cfset this.EnergyAccountDelegate.saveExistingEnergyAccount(energyAccount=local.energyAccount)/>

		<!--- Step 2: Create an instance of and save the existing energy native account --->
		<cfset local.energyNativeAccountObject = createNativeEnergyAccountObject(energyAccount=local.energyAccount)>
		<cfset this.EnergyAccountDelegate.saveExistingEnergyNativeAccount(EnergyNativeAccount=local.energyNativeAccountObject)>

		<!--- Get the default rate model / rate class association --->
		<cfset local.defaultRateClassRateModelAssociation = queryRowToStruct(this.EnergyAccountDelegate.getEnergyProviderRateClassRateModelAssociation(
				rate_class_uid=local.energyAccount.rate_class_uid))>

		<!--- Step 3: Create an instance of and save the new energy native account / energy provider rate class association --->
		<cfset local.energyNativeAccountEnergyProviderRateClassAssocationObject = createEnergyNativeAccountEnergyProviderRateClassAssociationObject(
				energyAccount=local.energyAccount,
				energyNativeAccount=local.energyNativeAccountObject,
				defaultRateClassRateModelAssociation=local.defaultRateClassRateModelAssociation,
				originalEnergyAccount=local.originalEnergyAccount)>

		<!--- Save the energy native account / energy provider rate class association --->
		<cfset this.EnergyAccountDelegate.saveExistingEnergyNativeAccountEnergyProviderRateClassAssociation(
				associationObject=local.energyNativeAccountEnergyProviderRateClassAssocationObject)>

	</cffunction>

	<cffunction name="createNativeEnergyAccountObject"
				hint="This method is used to create a new energy native account object based on the definition of an existing energy account."
				output="false"
				returnType="struct"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyAccount" required="true" type="mce.e2.vo.EnergyAccount" hint="Describes the VO containing the Energy Account record that will be persisted."/>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the structure used to store the object definition --->
		<cfset local.args = structNew()>

		<!--- Populate the object definition --->
		<cfif structKeyExists(arguments.energyAccount, 'native_account_uid') and isUniqueIdentifier(arguments.energyAccount.native_account_uid)>

			<!--- Set the primary key based on the existing record pulled back with the energy account definition --->
			<cfset local.args.native_account_uid = arguments.energyAccount.native_account_uid>

		<cfelse>

			<!--- Otherwise, just create a new primary key --->
			<cfset local.args.native_account_uid = createUniqueIdentifier()>

			<!--- Default the created by audit properties --->
			<cfset local.args.created_by = arguments.energyAccount.created_by>
			<cfset local.args.created_date = createOdbcDateTime(now())>

			<!--- Set the relationship start / end --->
			<cfset local.args.relationship_start = createOdbcDateTime(now())>
			<cfset local.args.relationship_end = ''>

		</cfif>

		<!--- Default the native account properties --->
		<cfset local.args.energy_account_uid = arguments.energyAccount.energy_account_uid>
		<cfset local.args.energy_provider_uid = arguments.energyAccount.energy_provider_uid>
		<cfset local.args.native_account_number = arguments.energyAccount.native_account_number>
		<cfset local.args.meta_group_uid = arguments.energyAccount.meta_group_uid>

		<!--- Define the audit properties --->
		<cfset local.args.is_active = arguments.energyAccount.is_active>
		<cfset local.args.modified_by = arguments.energyAccount.modified_by>
		<cfset local.args.modified_date = createOdbcDateTime(now())>

		<!--- returnt he object --->
		<cfreturn local.args>

	</cffunction>

	<cffunction name="createEnergyNativeAccountEnergyProviderRateClassAssociationObject"
				hint="This method is used to create a new energy native account / energy provider rate class association object based on the definition of an existing energy account."
				output="false"
				returnType="struct"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="EnergyAccount" required="true" type="mce.e2.vo.EnergyAccount" hint="Describes the VO containing the energy account record that will be persisted."/>
		<cfargument name="EnergyNativeAccount" required="true" type="struct" hint="Describes the VO containing the energy native account record that will be persisted."/>
		<cfargument name="DefaultRateClassRateModelAssociation" required="true" type="struct" hint="Describes the VO containing the default rate model / rate class association."/>
		<cfargument name="OriginalEnergyAccount" required="false" type="mce.e2.vo.EnergyAccount" hint="Describes the VO containing the original version of the energy account record that will be persisted."/>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Initialize the structure used to store the object definition --->
		<cfset local.args = structNew()>

		<!--- Set the primary key / relationship id --->
		<cfif structKeyExists(arguments.energyAccount,'energyNativeAccountEnergyProviderRateClass_relId' )>
			<cfset local.args.relationship_id = arguments.energyAccount.energyNativeAccountEnergyProviderRateClass_relId>
		</cfif>

		<!--- Populate the object definition --->
		<cfset local.args.native_account_uid = arguments.energyNativeAccount.native_account_uid>
		<cfset local.args.rate_class_uid = arguments.energyAccount.rate_class_uid>

		<!--- Set the overrride rate model if the default is not being used --->
		<cfif arguments.EnergyAccount.rate_model_uid neq arguments.DefaultRateClassRateModelAssociation.rate_model_uid>

			<!--- Override the rate model, since it's different from the default --->
			<cfset local.args.overridden_rate_model_uid = arguments.energyAccount.rate_model_uid>

		<cfelse>

			<!--- Otherwise, null out the overrriden rate model since the default is used --->
			<cfset local.args.overridden_rate_model_uid = ''>

		</cfif>

		<!--- Set the relationship start / end --->
		<cfset local.args.effective_input_set_uid = ''>

		<!--- Was an original energy account passed to this method? --->
		<cfif structKeyExists(arguments, 'OriginalEnergyAccount')>

			<!--- Was a different rate class specified / associated with this account? --->
			<cfif arguments.energyAccount.rate_class_uid neq arguments.originalEnergyAccount.rate_class_uid>

				<!--- Reset the relationship start / end since a new relationship is being specified --->
				<cfset local.args.relationship_start = createOdbcDateTime(now())>
				<cfset local.args.relationship_end = ''>

			</cfif>

		<cfelse>

			<!--- Define the created by audit properties --->
			<cfset local.args.created_by = arguments.energyAccount.created_by>
			<cfset local.args.created_date = createOdbcDateTime(now())>

		</cfif>

		<!--- Define the audit properties for all types of changes --->
		<cfset local.args.is_active = arguments.energyAccount.is_active>
		<cfset local.args.modified_by = arguments.energyAccount.modified_by>
		<cfset local.args.modified_date = createOdbcDateTime(now())>

		<!--- returnt he object --->
		<cfreturn local.args>

	</cffunction>

	<cffunction name="energyNativeAccountEnergyProviderRateClassAssociationExists"
				hint="This method is used to validate if a energy native account / rate class association already exists."
				output="false"
				returnType="boolean"
				access="public">

		<!--- Declare and initialize each of the function arguments --->
		<cfargument name="AssociationObject" required="true" type="struct" hint="Describes the VO containing the energy native account  / energy provider rate class association record that will be persisted."/>

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Default the output variable --->
		<cfset local.result = false>

		<!--- Retrieve the associations from the database --->
		<cfset local.qResult = this.energyAccountDelegate.getEnergyNativeAccountEnergyProviderRateClassAssociation(
				argumentCollection=arguments.associationObject)>

		<!--- If at least one record was returned, set the return value to true --->
		<cfif local.qResult.recordCount gt 0>
			<cfset local.result = true>
		</cfif>

		<!--- Return the return value --->
		<cfreturn local.result>

	</cffunction>




	<!---
		***************************************
		** Related to "Native Account History"
		***************************************
	--->
	<!--- Used to get data to display in "Native Account History" grid --->
	<cffunction name="getNativeAccountHistory" returntype="query" access="public">
		<cfargument name="EnergyAccount" type="mce.e2.vo.EnergyAccount" required="true"/>
		<cfreturn this.energyAccountDelegate.getNativeAccountHistory(arguments.EnergyAccount.energy_account_uid)>
	</cffunction>

	<cffunction name="checkForEnergyUsageRecords" returntype="boolean" access="public">
		<cfargument name="energy_account_uid" type="string" required="true"/>
		<cfargument name="relationship_start" type="string" required="true"/>
		<cfargument name="relationship_end" type="string" required="true"/>
		
		<cfreturn this.energyAccountDelegate.checkForEnergyUsageRecords(arguments.energy_account_uid, arguments.relationship_start, arguments.relationship_end)>
	</cffunction>

	<!--- Called when user does the "Insert Before" or "Insert After" workflow from "Native Account History" view in UI --->
	<cffunction name="insertNativeHistoryRecord" returntype="boolean" access="public">
		<cfargument name="originalEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true"/>
		<cfargument name="modifiedEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true"/>
		<cfargument name="insertPosition" type="string" required="true"/>
		<cfargument name="relationship_start" type="string" required="true"/>
		<cfargument name="relationship_end" type="string" required="true"/>
		<cfargument name="precedingRecord" type="any" required="false" default=""/>
		<cfargument name="succedingRecord" type="any" required="false" default=""/>

		<!--- Local variables --->
		<cfset var usersAuditLabel = getAuditLabel()>
		<cfset var startDateForNewRecord = DateFormat(arguments.relationship_start, "mm/dd/yyyy")>
		<cfset var endDateForNewRecord = DateFormat(arguments.relationship_end, "mm/dd/yyyy")>
		<cfset var newEnergyNativeAccountUid = "">
		<cfset var newStartDateForPreceding = "">
		<cfset var newStartDateForSuceeding = "">
		<cfset var newEndDateForPreceding = "">
		<cfset var newEndDateForSuceeding = "">

		<!--- Do we need to insert a new EnergyNativeAccount record? --->
		<cfset var insertNativeAccountRecord = isNativeAccountRecordNeeded(originalEnergyAccount, modifiedEnergyAccount)>
		<!--- Do we need to insert a new relationship (EnergyNativeAccounts_EnergyProviderRateClasses) record? --->
		<cfset var insertRelationshipsRecord = isRelationshipsRecordNeeded(originalEnergyAccount, modifiedEnergyAccount) or insertNativeAccountRecord>
		
		<!---Finish this, it should be separate method and should be called from UI and then ask the user if he wants to continue--->
		<!---cfset this.energyAccountDelegate.checkIfThereAreEnergyUsages(startDateForNewRecord, endDateForNewRecord)--->

		<!--- We can bail out now if there is nothing to do --->
		<cfif not (insertNativeAccountRecord or insertRelationshipsRecord)>
			<cfreturn false>
		</cfif>

		<!--- If there is a record before the new one, we'll want its end date to be right before the new record's start date--->
		<cfif isStruct(precedingRecord)>
			<cfset newEndDateForPreceding   = DateAdd("d", -1, startDateForNewRecord)>
		</cfif>
		<!--- If there is a record after the new one, we'll want its start date to be right after the new record's end date--->
		<cfif isStruct(succedingRecord)>
			<cfset newStartDateForSuceeding = DateAdd("d", +1, endDateForNewRecord)>
		</cfif>

		<cfset modifiedEnergyAccount.relationship_start = startDateForNewRecord>
		<cfset modifiedEnergyAccount.relationship_end = endDateForNewRecord>
		<cfset setAuditProperties(modifiedEnergyAccount)>

		<cftry>
			<!--- Start a database transaction, so we can undo any changes if something goes wrong --->
			<cftransaction action="begin">

			<!--- If we determined that a new EnergyNativeAccount record should be created --->
			<cfif insertNativeAccountRecord>
				<cfset modifiedEnergyAccount.native_account_uid = "">

				<cfif isStruct(precedingRecord)>
					<cfset this.energyAccountDelegate.adjustNativeAccountDates(precedingRecord.native_account_uid, newStartDateForPreceding, newEndDateForPreceding, usersAuditLabel)>
				</cfif>
				<cfif isStruct(succedingRecord)>
					<cfset this.energyAccountDelegate.adjustNativeAccountDates(succedingRecord.native_account_uid, newStartDateForSuceeding, newEndDateForSuceeding, usersAuditLabel)>
				</cfif>
				<cfset newEnergyNativeAccountUid = this.energyAccountDelegate.insertNativeHistoryRecord(modifiedEnergyAccount, startDateForNewRecord, endDateForNewRecord, usersAuditLabel)>
				<cfset modifiedEnergyAccount.native_account_uid = newEnergyNativeAccountUid>
			</cfif>

			<!--- If we determined that a new native account "relationship" record should be created --->
			<cfif insertRelationshipsRecord>
				<cfif isStruct(precedingRecord)>
					<cfset this.energyAccountDelegate.adjustNativeRelationshipsDates(precedingRecord.relationship_id, newStartDateForPreceding, newEndDateForPreceding, usersAuditLabel)>
				</cfif>
				<cfif isStruct(succedingRecord)>
					<cfset this.energyAccountDelegate.adjustNativeRelationshipsDates(succedingRecord.relationship_id, newStartDateForSuceeding, newEndDateForSuceeding, usersAuditLabel)>
				</cfif>
				<cfset newRelationshipRecordId = this.energyAccountDelegate.insertNativeRelationshipRecord(modifiedEnergyAccount, startDateForNewRecord, endDateForNewRecord, usersAuditLabel)>
			</cfif>
			
			<!-- Added on 2013-09-20 checks if there is some energy usages that are affected from account history change and if so, make them rejected and add a user comment-->

			<!--- If we get here we can safely commit the transaction --->
			<cftransaction action="commit">

			<!--- If any errors occur --->
			<cfcatch type="any">
				<!--- Roll back the transaction --->
				<cftransaction action="rollback">
				<!--- Here we could add some other custom handling; for now just re-exposing the exception --->
				<cfrethrow>
			</cfcatch>
		</cftry>

		<cfreturn true>
	</cffunction>

	<cffunction name="isNativeAccountRecordNeeded" returntype="boolean" access="private">
		<cfargument name="originalEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true"/>
		<cfargument name="modifiedEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true"/>

		<cfreturn
		   (originalEnergyAccount.native_account_number neq modifiedEnergyAccount.native_account_number) or
		   (originalEnergyAccount.energy_provider_uid neq modifiedEnergyAccount.energy_provider_uid) or
		   (originalEnergyAccount.meta_group_uid neq modifiedEnergyAccount.meta_group_uid)>
	</cffunction>

	<cffunction name="isRelationshipsRecordNeeded" returntype="boolean" access="private">
		<cfargument name="originalEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true"/>
		<cfargument name="modifiedEnergyAccount" type="mce.e2.vo.EnergyAccount" required="true"/>

		<cfreturn
			(originalEnergyAccount.rate_class_uid neq modifiedEnergyAccount.rate_class_uid) or
			(originalEnergyAccount.rate_model_uid neq modifiedEnergyAccount.rate_model_uid)>
	</cffunction>
	
	<cffunction name="deleteNativeAccountHistory" returntype="boolean" access="public">
		<cfargument name="native_account_uid" type="string" required="true"/>

		<cfset this.energyAccountDelegate.deleteNativeAccountHistory(arguments.native_account_uid)>
		
		<cfreturn true>
	</cffunction>

</cfcomponent>