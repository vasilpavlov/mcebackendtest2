<cfcomponent name="Security Service"
			 extends="BaseDataService" output="true"
			 hint="This component is used to manage all security-related requests /interactions.">

	<!--- Define the methods to set the bean / delegates --->
	<cffunction name="setSecurityDelegate" hint="Called automatically via coldspring">
		<cfargument name="bean">
		<cfset this.SecurityDelegate = bean>
	</cffunction>

	<cffunction name="setUserPermissionDelegate" hint="Called automatically via coldspring">
		<cfargument name="bean">
		<cfset this.userPermissionDelegate = bean>
	</cffunction>

	<cffunction name="setClientCompanyDelegate" hint="Called automatically via coldspring">
		<cfargument name="bean">
		<cfset this.clientCompanyDelegate = bean>
	</cffunction>

	<cffunction name="setUserDelegate" hint="Called automatically via coldspring">
		<cfargument name="bean">
		<cfset this.userDelegate = bean>
	</cffunction>

	<cffunction name="setSessionBeanFactory" hint="Called automatically via coldspring">
		<cfargument name="bean" type="any">
		<cfset this.SessionBeanFactory = bean>
	</cffunction>


	<cffunction name="setup" access="public" returntype="void">
		<cfset loadPermissions()>
	</cffunction>


	<!--- Define the business logic methods --->
	<cffunction name="attemptLogin"
				returntype="mce.e2.security.SessionBean"
				hint="This method is used to process login attempts.">
		<cfargument name="username" type="string" required="true" hint="Describes the userName that is used to attempt a login.">
		<cfargument name="password" type="string" required="true" hint="Describes the password that is used to attempt a login.">

		<cfset var key = "">

		<!--- A user is attempting to log in, so we better log the current user out, if there is one --->
		<cfset attemptLogout()>

		<!--- Attempt to login via the database --->
		<cfset key = this.SecurityDelegate.attemptLogin(username, password, request.e2.system_instance_uid, cgi.REMOTE_ADDR, cgi.HTTP_USER_AGENT)>

		<!--- If the database returns a session GUID, we can consiser ourselves authenticated --->
		<cfif isValidSessionKeyFormat(key)>
			<cfset session.mce.e2.session.sessionKey = key>
			<cfset session.mce.e2.session.auditUserLabel = username>
			<cfset session.mce.e2.session.isLoggedIn = key neq "">
		</cfif>

		<!--- Return the new session info --->
		<cfreturn getSession(throwTimeoutExceptionIfNotLoggedIn=false)>

	</cffunction>

	<cffunction name="attemptLogout"
				returntype="void"
				hint="This method is used to attempt a logout action from the application.  It will remove the active session from the application's session management.">
		<cfif IsDefined("session.mce.e2.session")>
			<cfset StructDelete(session.mce.e2, "session")/>
		</cfif>
	</cffunction>

	<cffunction name="getSession"
				returntype="any"
				hint="This method is used to return the active session's sessionBean component.">
		<cfargument name="throwTimeoutExceptionIfNotLoggedIn" type="boolean" default="true">

		<cfset var sessionBean = this.SessionBeanFactory.getSession()>

		<cfif (not sessionBean.isLoggedIn) and arguments.throwTimeoutExceptionIfNotLoggedIn>
			<cfthrow
				message="The session has timed out."
				errorcode="mce.e2.timeoutException">
		</cfif>

		<cfreturn sessionBean/>
	</cffunction>

	<cffunction name="getEndUsersClientCompany" returntype="mce.e2.vo.ClientCompany" hint="Used by the UI to retrieve the company VO that represents the end-user client company.">
		<cfreturn this.clientCompanyDelegate.getClientCompanyAsComponent(getSession().client_company_uid)/>
	</cffunction>

	<cffunction name="getInternalClientCompany" returntype="mce.e2.vo.ClientCompany" hint="Used by the UI to retrieve the company VO that represents the 'internal' client company (that is, the 'MCE' client company).">
		<!--- For now let's assume that only back office users should be asking for this info --->
		<cfset enforcePermission("bo.pub.login")>
		<!--- Return VO for "the" internal client company --->
		<cfreturn this.clientCompanyDelegate.getInternalClientCompany()/>
	</cffunction>

	<cffunction name="getInternalUsers" returntype="Array">
		<!--- For now let's assume that only back office users should be asking for this info --->
		<cfset enforcePermission("bo.pub.login")>
		<!--- Return VO for "the" internal client company --->
		<cfreturn this.userDelegate.getUsersAsArrayOfComponents(client_company_uid=this.clientCompanyDelegate.getInternalClientCompanyUid()) />
	</cffunction>

	<!--- Define any methods used to manage / secure property collections for a given user / session --->
	<cffunction name="cleanPropertyCollections"
				returntype="array" output="true"
				hint="This method is used to return an array of available property collections for a given user, filtering out collections that the user does not have access to.">
		<cfargument name="propertyCollectionsArray" type="array" required="true" hint="Describes the array collection of propertyCollection vo obects being evaluated.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Make a copy of the arguments scope --->
		<cfset local.args = duplicate(arguments)>

		<!--- Get the list of available / accessible propertyCollection primary keys --->
		<cfloop from="#arrayLen(local.args.propertyCollectionsArray)#" to="1" step="-1" index="local.arrayElement">

			<!--- Create a reference to the current array element --->
			<cfset local.pcVo = local.args.propertyCollectionsArray[local.arrayElement]>

			<!--- Check and see if the property collection primary key is in the session list of appropriate property collections --->
			<cfif not isPropertyCollectionAllowed(local.pcVo.property_collection_uid)>

				<!--- If it is not, delete the array element --->
				<cfset arrayDeleteAt(local.args.propertyCollectionsArray, local.arrayElement)>

			</cfif>

		</cfloop>

		<!--- Return the completed array --->
		<cfreturn local.args.propertyCollectionsArray>

	</cffunction>

	<cffunction name="isPropertyCollectionAllowed"
				returntype="boolean"
				hint="This method is used to validate if a property collection is allowed for a given user.">

		<!--- Define the arguments for the method --->
		<cfargument name="property_collection_uid" type="string" required="true" hint="Describes the primary key / unique identifier of a given property collection record.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get the current session --->
		<cfset local.thisSession = getSession()>

		<!--- Default the output value --->
		<cfset local.returnResult = true>

 		<!--- Is the current property collection primary key in the allowed list of property collections? --->
		<cfif not listFindNoCase(local.thisSession.propertyCollectionsList, arguments.property_collection_uid)>

			<!--- If not, then set the output value to false --->
			<cfset local.returnResult = false>

		</cfif>

		<!--- Return the output value --->
		<cfreturn local.returnResult>

	</cffunction>

	<!--- Private methods --->
	<cffunction name="isValidSessionKeyFormat"
				access="private" returntype="boolean"
				hint="This method is used to determine if the session key is in a valid format.">

		<!--- Define the arguments for this method --->
		<cfargument name="str" type="string" required="true" hint="Describes the session key being evaluated / validated.">

		<!--- We could validate this further, but for now a length check should suffice --->
		<!--- ADL:  The baseDataService has a method to validate uniqueIdentifiers; we could use this instead --->
		<cfreturn len(str) eq 36>

	</cffunction>


	<cffunction name="loadPermissions" access="public">

		<cfset var xmlDoc = "">
		<cfset var nodes = "">
		<cfset var node = "">
		<cfset var perm = "">
		<cfset var permExists = false>
		<cfset var xmlFile = ExpandPath("/mce/e2/config/user-permissions.xml")>

		<!--- Parse the xml document and find all of the <permission> nodes --->
		<cffile action="read" file="#xmlFile#" variable="xmlContent">
		<cfset xmlDoc = XmlParse(xmlContent)>
		<cfset nodes = XmlSearch(xmlDoc, "config/permission")>

		<!--- For each <permission> node --->
		<cfloop array="#nodes#" index="node">
			<cfset permExists = this.userPermissionDelegate.getUserPermission(user_permission_code=node.XmlAttributes.code).recordCount gt 0>
			<cfif not permExists>
				<cfset perm = CreateObject("component", "mce.e2.vo.UserPermission")>
				<cfset perm.user_permission_code = node.XmlAttributes.code>
				<cfset perm.friendly_name = node.XmlAttributes.name>
				<cfset perm.created_by = "SecurityService">
				<cfset this.userPermissionDelegate.saveNewUserPermission(perm)>
			</cfif>
		</cfloop>
	</cffunction>

</cfcomponent>