<cfcomponent displayname="Energy Account Type Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of energy account type information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyAccountTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage energy account type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyAccountTypeDelegate" hint="Describes the *.cfc used to manage database interactions for energy account types."/> 
		<cfset this.EnergyAccountTypeDelegate = arguments.bean>
	</cffunction>
	
	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyAccountTypeVo" 
				access="public" returntype="mce.e2.vo.EnergyAccountType"
				hint="This method is used to retrieve / return an empty energyAccountType information value object.">
		<cfreturn this.energyAccountTypeDelegate.getEmptyEnergyAccountTypeComponent()>
	</cffunction>

	<cffunction name="getAvailableEnergyAccountTypes"
				access="public"
				returnType="array"
				hint="This method is used to retrieve a collection of available energy account types.">

		<!--- Define the arguments associated to this method --->
		<cfargument name="EnergyType" type="mce.e2.vo.EnergyType" required="false" hint="Describes the energy type that will be used to retrieve associated energy account types." /> 

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>
		
		<!--- Initialize the arguments structure --->
		<cfset local.args = structNew()>
		
		<!--- Was a valid energy account type specified? --->
		<cfif structKeyExists(arguments, 'energyType')>
		
			<!--- Define each of the arguments used to retrieve available energy account types --->
			<cfset local.args.energy_type_uid = arguments.energyType.energy_type_uid>
		
		</cfif>

		<!--- Retrieve the energy account types --->
		<cfset local.energyAccountTypes = this.energyAccountTypeDelegate.getEnergyAccountTypesAsArrayOfComponents(argumentCollection=local.args)>
		
		<!--- Return the available energy account types --->
		<cfreturn local.energyAccountTypes>
		
	</cffunction>

	<cffunction name="getEnergyAccountTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy account types.">
		<cfreturn this.energyAccountTypeDelegate.getEnergyAccountTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyAccountType" 
				access="public" returntype="mce.e2.vo.EnergyAccountType" 
				hint="This method is used to return a populated EnergyAccountType vo (value object)."> 
		<cfargument name="EnergyAccountType" type="mce.e2.vo.EnergyAccountType" required="true" hint="Describes the energyAccountType collection VO object containing the properties of the energyAccountType collection to be retrieved." /> 
		<cfreturn this.energyAccountTypeDelegate.getEnergyAccountTypeAsComponent(account_type_uid=arguments.EnergyAccountType.account_type_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEnergyAccountType" 
				access="public" returntype="string" 
				hint="This method is used to persist a new energyAccountType record to the application database."> 
		<cfargument name="EnergyAccountType" type="mce.e2.vo.EnergyAccountType" required="true" hint="Describes the energyAccountType collection VO object containing the details of the energyAccountType collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.energyAccountType = setAuditProperties(arguments.energyAccountType, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.energyAccountType = setPrimaryKey(local.energyAccountType)>
		
		<!--- Save the modified energyAccountType --->
		<cfset this.EnergyAccountTypeDelegate.saveNewEnergyAccountType(energyAccountType=local.energyAccountType)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.energyAccountType.account_type_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewEnergyAccountType" 
				access="public" returntype="mce.e2.vo.EnergyAccountType" 
				hint="This method is used to persist a new energyAccountType record to the application database."> 
		<cfargument name="EnergyAccountType" type="mce.e2.vo.EnergyAccountType" required="true" hint="Describes the energyAccountType collection VO object containing the details of the energyAccountType collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyAccountType(energyAccountType=arguments.energyAccountType)>

		<!--- Set the primary key --->
		<cfset arguments.energyAccountType.account_type_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.energyAccountType>

	</cffunction>

	<cffunction name="saveExistingEnergyAccountType" 
				access="public" returntype="mce.e2.vo.EnergyAccountType" 
				hint="This method is used to save an existing energyAccountType information to the application database."> 
		<cfargument name="energyAccountType" type="mce.e2.vo.EnergyAccountType" required="true" hint="Describes the energyAccountType collection VO object containing the details of the energyAccountType collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.energyAccountType = setAuditProperties(arguments.energyAccountType, "modify")>
		
		<!--- Save the modified energyAccountType --->
		<cfset this.EnergyAccountTypeDelegate.saveExistingEnergyAccountType(energyAccountType=local.energyAccountType)/>

		<!--- Return the modified energyAccountType --->
		<cfreturn local.energyAccountType>	

	</cffunction>
	
</cfcomponent>