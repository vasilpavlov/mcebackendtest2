<cfcomponent displayname="Property Meta Data Service"
			 extends="BaseDataService" output="true"
			 hint="The component is used to manage all business logic tied / associated with the management of Property Meta Data information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setPropertyMetaDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Property Meta Data data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.PropertyMetaDelegate" hint="Describes the *.cfc used to manage database interactions related to Property Meta Data information.">
		<cfset this.PropertyMetaDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyPropertyMetaVo" 
				access="public" returntype="mce.e2.vo.PropertyMeta"
				hint="This method is used to retrieve / return an empty PropertyMeta information value object.">
		<cfreturn this.PropertyMetaDelegate.getEmptyPropertyMetaComponent()>
	</cffunction>

	<cffunction name="getPropertyMetaData" 
				access="public" returntype="array" 
				hint="This method is used to return a collection of associated property meta data."> 
		<cfargument name="Property" type="mce.e2.vo.Property" required="true" hint="Describes the property VO object used to retrieve associated meta data." /> 
		<cfargument name="is_public_facing" required="true" type="boolean" hint="Describes if a given property meta type is public (external / customer) or private (internal) facing."/>
		
		<cfset var aPropertyMetaData = this.propertyMetaDelegate.getPropertyMetaDataAsArrayOfComponents(property_uid=arguments.Property.property_uid,
is_public_facing=arguments.is_public_facing,
userPermissions = getPermissions())> 
		<cfset this.PropertyMetaDelegate.applyChoicesToPropertyMeta(aPropertyMetaData)>
		<cfreturn aPropertyMetaData>
	</cffunction>
	
	<cffunction name="getPropertyMeta" 
				access="public" returntype="mce.e2.vo.PropertyMeta" 
				hint="This method is used to return a populated PropertyMeta vo (value object)."> 
		<cfargument name="PropertyMeta" type="mce.e2.vo.PropertyMeta" required="true" hint="Describes the PropertyMeta collection VO object containing the properties of the PropertyMeta collection to be retrieved." /> 
		<cfargument name="is_public_facing" required="true" type="boolean" hint="Describes if a given property meta type is public (external / customer) or private (internal) facing."/>
		
		<cfreturn this.PropertyMetaDelegate.getPropertyMetaDataAsComponent(property_meta_uid=arguments.PropertyMeta.property_meta_uid,is_public_facing=arguments.is_public_facing)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewPropertyMeta" 
				access="public" returntype="string" output="true" 
				hint="This method is used to persist a new PropertyMeta record to the application database."> 
		<cfargument name="PropertyMeta" type="mce.e2.vo.PropertyMeta" required="true" hint="Describes the PropertyMeta collection VO object containing the details of the PropertyMeta collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.PropertyMeta = setAuditProperties(arguments.PropertyMeta, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.PropertyMeta = setPrimaryKey(local.PropertyMeta)>
		
		<!--- Save the modified PropertyMeta --->
		<cfset this.PropertyMetaDelegate.saveNewPropertyMeta(PropertyMeta=local.PropertyMeta)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.PropertyMeta.property_meta_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewPropertyMeta" 
				access="public" returntype="mce.e2.vo.PropertyMeta" 
				hint="This method is used to persist a new PropertyMeta record to the application database."> 
		<cfargument name="PropertyMeta" type="mce.e2.vo.PropertyMeta" required="true" hint="Describes the PropertyMeta collection VO object containing the details of the PropertyMeta collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Add a blank relationship_end date if it doesn't exist. --->
		<cfif not structkeyexists(arguments.PropertyMeta, "relationship_end")>
			<cfset arguments.PropertyMeta.relationship_end = "">
		</cfif>
		
		<!--- Determine if this record already exists. ---> 	
		<cfset local.existingPropertyMeta = this.PropertyMetaDelegate.getPropertyMetaData(property_uid = arguments.PropertyMeta.property_uid, meta_type_uid = arguments.PropertyMeta.meta_type_uid, selectMethod = 'active', relationshipConflictStart = arguments.PropertyMeta.relationship_start, relationshipConflictEnd = arguments.PropertyMeta.relationship_end)>

		<cfif local.existingPropertyMeta.property_meta_uid eq "">

			<!--- Capture the primary key and create the object --->
			<cfset local.primaryKey = saveNewPropertyMeta(PropertyMeta=arguments.PropertyMeta)>
	
			<!--- Set the primary key --->
			<cfset arguments.PropertyMeta.property_meta_uid = local.primaryKey>
	
			<!--- Return the new object --->
			<cfreturn this.PropertyMetaDelegate.getPropertyMetaDataAsComponent(property_meta_uid = arguments.PropertyMeta.property_meta_uid, selectMethod='active', relationship_start = arguments.PropertyMeta.relationship_start, relationship_end = arguments.PropertyMeta.relationship_end)>

		<cfelse>
		
			<cfthrow detail="Another record already exists that conflicts with the start and end dates you provided." />
		
		</cfif>

	</cffunction>

	<cffunction name="saveExistingPropertyMeta" 
				access="public" returntype="mce.e2.vo.PropertyMeta" 
				hint="This method is used to save an existing PropertyMeta information to the application database."> 
		<cfargument name="PropertyMeta" type="mce.e2.vo.PropertyMeta" required="true" hint="Describes the PropertyMeta collection VO object containing the details of the PropertyMeta collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Add a blank relationship_end date if it doesn't exist. --->
		<cfif not structkeyexists(arguments.PropertyMeta, "relationship_end")>
			<cfset arguments.PropertyMeta.relationship_end = "">
		</cfif>
		
		<!--- Determine if this record already exists. ---> 	
		<cfset local.existingPropertyMeta = this.PropertyMetaDelegate.getPropertyMetaData(property_uid = arguments.PropertyMeta.property_uid, meta_type_uid = arguments.PropertyMeta.meta_type_uid, exclude_property_meta_uid = arguments.PropertyMeta.property_meta_uid, selectMethod = 'active', relationshipConflictStart = arguments.PropertyMeta.relationship_start, relationshipConflictEnd = arguments.PropertyMeta.relationship_end)>

		<cfif local.existingPropertyMeta.property_meta_uid eq "">

			<!--- Set the modified by / date audit properties --->
			<cfset local.PropertyMeta = setAuditProperties(arguments.PropertyMeta, "modify")>
			
			<!--- Save the modified PropertyMeta --->
			<cfset this.PropertyMetaDelegate.saveExistingPropertyMeta(PropertyMeta=local.PropertyMeta)/>
	
			<!--- Return the modified PropertyMeta --->
			<cfreturn this.PropertyMetaDelegate.getPropertyMetaDataAsComponent(property_meta_uid = arguments.PropertyMeta.property_meta_uid, selectMethod='all', relationship_start = arguments.PropertyMeta.relationship_start, relationship_end = arguments.PropertyMeta.relationship_end)>
			
		<cfelse>
		
			<cfthrow detail="Another record already exists that conflict with the start and end dates you provided." />
		
		</cfif>

	</cffunction>
	
</cfcomponent>