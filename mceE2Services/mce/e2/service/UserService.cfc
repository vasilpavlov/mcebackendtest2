<cfcomponent displayname="User Service"
			 extends="BaseDataService" output="true"
			 hint="The component is used to manage all business logic tied / associated with the management of user information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setUserDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage user data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.UserDelegate" hint="Describes the *.cfc used to manage database interactions related to user information.">
		<cfset this.userDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setUserGroupDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage user group data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.UserGroupDelegate" hint="Describes the *.cfc used to manage database interactions related to user group information.">
		<cfset this.userGroupDelegate = arguments.bean>
	</cffunction>
	
	<cffunction name="setUserRoleDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage user role data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.UserRoleDelegate" hint="Describes the *.cfc used to manage database interactions related to user role information.">
		<cfset this.userGroupDelegate = arguments.bean>
	</cffunction>

	<cffunction name="setUserGroupService" 
				access="public" returntype="void" 
				hint="This method is used to set the service delegate used to manage user group data (dependency injection).">
		<cfargument name="bean" type="mce.e2.service.UserGroupService" hint="Describes the *.cfc used to manage service interactions related to user group information.">
		<cfset this.userGroupService = arguments.bean>
	</cffunction>
	
	<cffunction name="setClientContactService" 
				access="public" returntype="void" 
				hint="This method is used to set the service delegate used to manage client contacts (dependency injection).">
		<cfargument name="bean" type="mce.e2.service.ClientContactService" hint="Describes the *.cfc used to manage service interactions related to client contacts.">
		<cfset this.ClientContactService = arguments.bean>
	</cffunction>	
	
	<cffunction name="setSecurityService" 
				access="public" returntype="void" 
				hint="This method is used to set the service delegate used to manage user security (dependency injection).">
		<cfargument name="bean" type="mce.e2.service.SecurityService" hint="Describes the *.cfc used to manage service interactions related to user security.">
		<cfset this.SecurityService = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyUserVo" 
				access="public" returntype="mce.e2.vo.User"
				hint="This method is used to retrieve / return an empty user information value object.">
		<cfreturn this.userDelegate.getEmptyUserComponent()>
	</cffunction>

	<cffunction name="getUsers" 
				access="public" returntype="array" 
				hint="This method is used to return an array of populated user vo's (value objects)."> 
		<cfreturn this.userDelegate.getUsersAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getUser" 
				access="public" returntype="mce.e2.vo.User" 
				hint="This method is used to return a populated user vo (value object)."> 
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user to be retrieved." /> 
		<cfreturn this.userDelegate.getUserAsComponent(user_uid=arguments.user.user_uid)>
	</cffunction>
	
	<cffunction name="getCurrentUserCompanyRoleInformation"
				access="public" returntype="mce.e2.vo.UserCompanyRoles"
				hint="This method is used to retrieve extended information about a given user.">

		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>
		
		<!--- Get the active session --->
		<cfset local.session = this.securityService.getSession()>
		
		<!--- Create an empty user object --->
		<cfset local.user = getEmptyUserVo()>

		<!---  Populate the properties of the user --->
		<cfset local.user.user_uid = local.session.user_uid>

		<!--- Retrieve the extended user information --->		
		<cfreturn getUserCompanyRoleInformation(local.user)>

	</cffunction>	
	
	<cffunction name="getUserCompanyRoleInformation"
				access="public" returntype="mce.e2.vo.UserCompanyRoles"
				hint="This method is used to retrieve extended information about a given user.">
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user to be retrieved." /> 
		<cfreturn this.userDelegate.getUserCompanyRolesAsComponent(user_uid=arguments.user.user_uid)>
	</cffunction>
	
	<cffunction name="getCurrentUserCompanyContacts"
				access="public" returntype="array"
				hint="This method is used to retrieve company contacts associated to the company of the current user.">
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>
		
		<!--- Get the active session --->
		<cfset local.session = this.securityService.getSession()>
		
		<!--- Create an empty company object --->
		<cfset local.user = getEmptyUserVo()>

		<!--- Populate the properties of the company --->
		<cfset local.user.client_company_uid = local.session.client_company_uid>
		<cfset local.user.user_uid = local.session.user_uid>
				
		<!--- Retrieve the client contacts associated to this user's company --->		
		<cfreturn getUserCompanyContacts(local.user)>		
				
	</cffunction>
	
	<cffunction name="getUserCompanyContacts"
				access="public" returntype="array"
				hint="This method is used to retrieve company contacts associated to the company of the current user.">
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user to be retrieved." /> 
				
		<!--- Initialize the local scope --->		
		<cfset var local = structNew()>
		
		<!--- If no client_company_uid is specified, then retrieve it --->
		<cfif ( structKeyExists(arguments.user, 'client_company_uid') and not isUniqueIdentifier(arguments.user.client_company_uid)) or
				not structKeyExists(arguments.user, 'client_company_uid')>
			<cfset arguments.user = getUser(arguments.user)>		
		</cfif>
		
		<!--- Create an empty company object --->
		<cfset local.company = createObject("component","mce.e2.vo.ClientCompany")>

		<!--- Populate the properties of the company --->
		<cfset local.company.client_company_uid = arguments.user.client_company_uid>
				
		<!--- Retrieve the client contacts associated to the user's company --->		
		<cfreturn this.ClientContactService.getClientContactsByClientCompany(
					clientCompany=local.company)>
				
	</cffunction>	
	
	<cffunction name="getUserGroups" 
				access="public" returntype="array" 
				hint="This method is used to return an array collection of populated user group vo's' (value object) associated to the specified user."> 
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user for which associated user groups will be retrieved." /> 
		<cfreturn this.userGroupDelegate.getUserGroupsAsArrayOfComponents(user_uid=arguments.user.user_uid)>
	</cffunction>	
	
	<cffunction name="getAssociatedUserGroups" 
				access="public" returntype="array" 
				hint="This method is used to return an array collection of populated user group vo's' (value object) associated to the specified user."> 
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user for which associated user groups will be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Retrieve the collection of user groups --->
		<cfset local.result = this.userGroupDelegate.getUserGroupsAsArrayOfComponents(
					client_company_uid=arguments.user.client_company_uid,
					user_uid=arguments.user.user_uid,
					selectMethod="inclusive")>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>		

	<cffunction name="getUnAssociatedUserGroups" 
				access="public" returntype="array" 
				hint="This method is used to return an array collection of populated user group vo's' (value object) that are not associated to the specified user."> 
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user for which unassociated user groups will be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Retrieve the collection of user groups --->
		<cfset local.result = this.userGroupDelegate.getUserGroupsAsArrayOfComponents(
					client_company_uid=arguments.user.client_company_uid,
					user_uid=arguments.user.user_uid,
					selectMethod="exclusive")>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>		

	<cffunction name="getUserGroupAssociations" 
				access="public" returntype="array" 
				hint="This method is used to return an array collection of populated user group vo's' (value object) that are / are not associated to a given user."> 
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user for which associated / unassociated user groups will be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Retrieve the collection of user groups --->
		<cfset local.result = this.userGroupDelegate.getUserGroupsAsArrayOfComponents(
					client_company_uid=arguments.user.client_company_uid,
					user_uid=arguments.user.user_uid,
					selectMethod="userAssociations")>

		<!--- Return the array collection --->
		<cfreturn local.result>

	</cffunction>		
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewUser" 
				access="public" returntype="string" 
				hint="This method is used to persist a new user record to the application database.  The method returns the primary key of the user that was crated."> 
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user to be retrieved." /> 
		<cfargument name="password" type="string" required="true" hint="Describes the password associated to a given user.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Validate that a user record is properly defined --->
		<cfset validateVo(arguments.user)>

		<!--- Validate the username --->
		<cfset validateUserName(arguments.user)>
				
		<!--- Set the created by / date audit properties --->
		<cfset local.user = setAuditProperties(arguments.user, "create")>

		<!--- Set the password in the user object and encrypt it --->
		<cfset local.user.password_hash_code = request.beanFactory.getBean("PasswordEncrypter").encryptString(arguments.password)>

		<!--- Set the primary key for the user if it's not already set --->
		<cfset local.user = setPrimaryKey(local.user)>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.user.user_uid>

		<!--- Create the user --->		
		<cfset this.userDelegate.saveNewUser(user=duplicate(local.user))>

		<!--- Return the new user primary key --->
		<cfreturn local.primaryKey>

	</cffunction>

	<cffunction name="saveAndReturnNewUser" 
				access="public" returntype="mce.e2.vo.User" 
				hint="This method is used to persist a new user record to the application database.  The method returns the primary key of the user that was crated."> 
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user to be retrieved." /> 
		<cfargument name="password" type="string" required="true" hint="Describes the password associated to a given user.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Create the user --->		
		<cfset local.primaryKey = saveNewUser(user=arguments.user,password=arguments.password)>
	
		<!--- Set the primary key for the user --->
		<cfset arguments.user.user_uid = local.primaryKey>

		<!--- Retrieve the new user --->
		<cfset local.user = getUser(user=arguments.user)>

		<!--- Return the new user --->
		<cfreturn local.user>

	</cffunction>
	
	<cffunction name="saveExistingUser" 
				access="public" returntype="void" 
				hint="This method is used to save existing user information to the application database."> 
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user to be saved." /> 
		<cfargument name="password" type="string" required="false" hint="Describes the password associated to a given user.">

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Validate that a user record is properly defined --->
		<cfset validateVo(arguments.user)>		

		<!--- Validate the username --->
		<cfset validateUserName(arguments.user)>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.user = setAuditProperties(arguments.user, "modify")>

		<!--- Did the password change / was it provided? --->
		<cfif structKeyExists(arguments, "password") and len(arguments.password) gt 0>

			<!--- Set the password in the user object and encrypt it --->
			<cfset local.user.password_hash_code = request.beanFactory.getBean("PasswordEncrypter").encryptString(arguments.password)>

		<cfelse>
		
			<!--- Does the password hash code exist? --->
			<cfif structKeyExists(local.user, 'password_hash_code')>
		
				<!--- Otherwise, delete the password property --->
				<cfset structDelete(local.user, 'password_hash_code')>

			</cfif>

		</cfif>

		<!--- Modify the user --->		
		<cfset this.userDelegate.saveExistingUser(user=local.user)/>
				
	</cffunction>

	<!--- Author all the methods that will manage associations between user groups and other objects --->	
	<cffunction name="associateUserToUserGroup"
				access="public"
				returnType="void"
				hint="This method is used to add / associate a given user to a user group.">

		<!--- Define the arguments for this method --->
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user being associated to a user group." /> 
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which users are being associated.">

		<!--- Create the user / user group association --->		
		<cfset this.userGroupService.associateUserToUserGroup(argumentCollection=arguments)>

	</cffunction>

	<cffunction name="disAssociateUserFromUserGroup"
				access="public"
				returnType="void"
				hint="This method is used to remove / disassociate a given user from a user group.">

		<!--- Define the arguments for this method --->
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the properties of the user being removed from a user group." /> 
		<cfargument name="userGroup" type="mce.e2.vo.UserGroup" required="true" hint="Describes the user group to which users are being associated.">

		<!--- Remove the user / user group association --->		
		<cfset this.userGroupService.disassociateUserFromUserGroup(argumentCollection=arguments)>
		
	</cffunction>

	<cffunction name="processUserGroupAssociations"
				access="public"
				returntype="void"
				hint="This method is used to process changes to a collection of user associations to a specified user group.">
				
		<!--- Define the arguments for this method --->
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user to which user groups are being associated.">
		<cfargument name="userGroupArray" type="array" required="true" hint="Describes the array collection of user group VO objects containing the user groups whose association to the specified user is changing." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Remove the relationship_id from the user definition if it exists --->
		<cfset structDelete(arguments.user, 'relationship_id')>

		<!--- Get the associated user groups for this user --->
		<cfset local.associatedUserGroups = getAssociatedUserGroups(arguments.user)>				
	
		<!--- Determine the associations to process --->		
		<cfset local.associationsToProcess = reviewAssociations(
				associatedObjects=local.associatedUserGroups,
				associationsToProcess=arguments.userGroupArray,
				primaryKey="user_group_uid")>		
						
		<!--- Loop over the save array, and process the user / user group associations --->		
		<cfloop from="1" to="#arrayLen(local.associationsToProcess.addArray)#" index="local.arrayIndex">
		
			<!--- Process each new user / group association --->
			<cfset associateUserToUserGroup(
					user=arguments.user,
					userGroup=local.associationsToProcess.addArray[local.arrayIndex])>
		
		</cfloop>		
				
		<!--- Loop over the save array, and process the user / user group associations --->		
		<cfloop from="1" to="#arrayLen(local.associationsToProcess.removeArray)#" index="local.arrayIndex">
		
			<!--- Process each existing user / group association --->
			<cfset disAssociateUserFromUserGroup(
					user=arguments.user,
					userGroup=local.associationsToProcess.removeArray[local.arrayIndex])>
		
		</cfloop>		

	</cffunction>		

	<!--- Author private / internal methods --->
	<cffunction name="validateUserName"
				access="public" returnType="boolean"
				hint="This method is used to validate that the username for a given user is unique.">
		<cfargument name="user" type="mce.e2.vo.User" required="true" hint="Describes the user VO object containing the username / user_uid to validate uniqueness against." /> 
		<cfargument name="throwError" type="boolean" required="true" default="true" hint="Describes whether an error should be thrown by this method in the event a username conflict is found.">
		<!--- Initialize the local scope --->
		<cfset var local = structNew()>

		<!--- Get the results of the validate unserName query; determine if this username doesn't already exist --->
		<cfset local.isUserNameUnique = this.userDelegate.isUserNameUnique(user_uid=arguments.user.user_uid,username=arguments.user.username)>

		<!--- Is the username unique/ --->
		<cfif local.isUserNameUnique is false>

			<!--- Log the error and throw an exception --->
			<cfset request.e2.logger.error("Username Uniqueness Conflict", "The username [#arguments.user.userName#] is already being used by another active user.")>

		</cfif>

		<!--- Return the uniqueness flag --->
		<cfreturn local.isUserNameUnique>

	</cffunction>

</cfcomponent>