<cfcomponent extends="BaseDataService" output="false">
	<cfproperty name="alertingDelegate" type="mce.e2.db.AlertingDelegate" hint="Provided by dependency injection framework">


	<!--- I am called by coldspring IOC/DI framework --->
	<cffunction name="init">
		<cfargument name="AlertingDelegate" type="mce.e2.db.AlertingDelegate" required="true">
		<cfargument name="AlertingBridge" type="mce.e2.service.AlertingBridge" required="true">

		<cfset this.AlertingDelegate = arguments.AlertingDelegate>
		<cfset this.AlertingBridge = arguments.AlertingBridge>

		<cfreturn this>
	</cffunction>


	<cffunction name="getCurrentAlertsForUser" returntype="query" output="false">
		<cfargument name="ui_category" type="string" required="true">
		<cfargument name="is_active_flag" type="boolean" required="true">
		<cfargument name="honorAlertVisibilityDate" type="boolean" required="false" default="true">
		<cfargument name="alertUidsToCheckRelevancyFor" type="string" required="false" default="">
		<cfargument name="accountUidsToCheckRelevancyFor" type="string" required="false" default="">
		<cfargument name="service_type_uid" type="string" required="false" default="">
		<cfargument name="date_from" type="date" required="false">
		<cfargument name="date_thru" type="date" required="false">

		<!--- Get the alerts (this is our main duty here) --->
		<cfset var qAlerts = this.alertingDelegate.getCurrentAlertsForUser(user_uid=getUserUid(),
						ui_category=arguments.ui_category,
						is_active_flag=arguments.is_active_flag,
						honorAlertVisibilityDate:arguments.honorAlertVisibilityDate,
						service_type_uid=arguments.service_type_uid)>

		<cfset qAlerts = handleCheckRelevancyForQueryData(qAlerts,
			arguments.alertUidsToCheckRelevancyFor,
			arguments.accountUidsToCheckRelevancyFor)>

		<!--- Return the result --->
		<cfreturn qAlerts>
	</cffunction>


	<cffunction name="handleCheckRelevancyForQueryData" access="private" returntype="query">
		<cfargument name="alertsQuery" type="query" required="true">
		<cfargument name="alertUidsToCheckRelevancyFor" type="string" required="false" default="">
		<cfargument name="accountUidsToCheckRelevancyFor" type="string" required="false" default="">

		<!--- Local variables --->
		<cfset var noLongerRelevant = "">
		<cfset var qAccountAlerts = "">
		<cfset var qRemainingAlerts = "">
		<cfset var qAlerts = arguments.alertsQuery>

		<!--- We might hae been told to simply check "all" alert uids being returned --->
		<cfif arguments.alertUidsToCheckRelevancyFor eq "all">
			<cfset arguments.alertUidsToCheckRelevancyFor = ValueList(qAlerts.alert_uid)>
		<cfelseif arguments.accountUidsToCheckRelevancyFor neq "">
			<cfquery dbtype="query" name="qAccountAlerts">
				SELECT * FROM qAlerts
				WHERE energy_account_uid IN (<cfqueryparam value="#arguments.accountUidsToCheckRelevancyFor#" cfsqltype="cf_sql_idstamp" list="true">)
			</cfquery>

			<cfset arguments.alertUidsToCheckRelevancyFor = valueList(qAccountAlerts.alert_uid)>
		<cfelseif arguments.alertUidsToCheckRelevancyFor eq "auto">
			<cfif qAlerts.recordCount lt 100>
				<cfset arguments.alertUidsToCheckRelevancyFor = ValueList(qAlerts.alert_uid)>
			<cfelse>
				<cfset arguments.alertUidsToCheckRelevancyFor = "">
			</cfif>
		</cfif>


		<!--- If we got some alerts and are meant to check whether they are still relevant --->
		<cfif arguments.alertUidsToCheckRelevancyFor neq "" and qAlerts.recordCount gt 0>
			<!--- Which ones are no longer relevant? --->
			<cfset noLongerRelevant = removeIfNoLongerRelevant(arguments.alertUidsToCheckRelevancyFor)>
			<!--- If there are ones that are no longer relevant --->
			<cfif noLongerRelevant neq "">
				<cfset queryAddColumn(qAlerts, "temp_noLongerRelevant", "Bit", ArrayNew(1))>
				<!--- Loop through the alerts we're about to return --->
				<cfloop query="qAlerts">
					<!--- If this alert is one of the ones we decided was no longer relevant, "tag" it with a special value --->
					<cfif ListFind(noLongerRelevant, qAlerts.alert_uid) gt 0>
						<cfset QuerySetCell(qAlerts, "temp_noLongerRelevant", "1", qAlerts.currentRow)>
					</cfif>
				</cfloop>

				<!--- Re-query the query to get the ones that don't have our special value --->
				<cfquery name="qRemainingAlerts" dbtype="query">
					SELECT * FROM qAlerts
					WHERE temp_noLongerRelevant <> 1
				</cfquery>

				<cfset qAlerts = qRemainingAlerts>
			</cfif>
		</cfif>

		<cfreturn qAlerts>
	</cffunction>


	<cffunction name="getCurrentAlertCategoryCountsForUser" returntype="query" output="false">
		<cfreturn this.alertingDelegate.getCurrentAlertCategoryCountsForUser(getUserUid())>
	</cffunction>


	<cffunction name="getAlertsForUserByDateRange" returntype="query" output="false">
		<cfargument name="ui_category" type="string" required="true">
		<cfargument name="is_active_flag" type="boolean" required="true">
		<cfargument name="date_from" type="date" required="true">
		<cfargument name="date_thru" type="date" required="true">
		<cfargument name="honorAlertVisibilityDate" type="boolean" required="true" default="true">

		<cfreturn this.alertingDelegate.getCurrentAlertsForUser(getUserUid(), ui_category, is_active_flag, honorAlertVisibilityDate, "", date_from, date_thru)>
	</cffunction>

	<cffunction name="setAlertIsActive" returntype="boolean" output="false">
		<cfargument name="alert_uid" type="string" required="true">
		<cfargument name="is_active_flag" type="boolean" required="true">
		<cfargument name="alert_status_code" type="string" required="false" default="">
		<cfargument name="user" type="string" required="false">

		<cfset var result = "">

		<cfif not StructKeyExists(arguments, "user")>
			<cfset arguments.user = getUserUid()>
		</cfif>

		<cfset result = this.alertingDelegate.setAlertIsActive(arguments.user, arguments.alert_uid, arguments.is_active_flag, arguments.alert_status_code)>

		<cfreturn result>
	</cffunction>

	<cffunction name="snoozeTaskAlert" returntype="boolean" output="false">
		<cfargument name="alert_uid" type="string" required="true">

		<cfset var result = "">

		<cfset result = this.alertingDelegate.snoozeTaskAlert(arguments.alert_uid)>

		<cfreturn result>
	</cffunction>

	<cffunction name="getAlertMeta" returntype="query" output="false">
		<cfargument name="alert_uid" type="string" required="true">

		<cfreturn this.alertingDelegate.getAlertMeta(arguments.alert_uid)>
	</cffunction>


	<!--- For use by the Back Office UI, to display available subscriptions given a list of properties --->
	<cffunction name="getAlertSubscriptionsForProperty" returntype="array" output="false">
		<cfargument name="property_uid_list" type="array" required="true">
		<cfargument name="is_active_flag" type="boolean" required="true">
		<cfset var local=structNew()>
		<cfset local.energy_account_uid_list = ArrayToList(arguments.property_uid_list)>

		<cfreturn this.alertingDelegate.getAlertSubscriptionsForProperty(local.energy_account_uid_list, is_active_flag)>
	</cffunction>

	<!--- For use by the Back Office UI, to display available subscriptions given a list of energy accounts --->
	<cffunction name="getAlertSubscriptionsForAccounts" returntype="array" output="false">
		<cfargument name="energy_account_uid_list" type="array" required="true">
		<cfargument name="is_active_flag" type="boolean" required="true">
		<cfset var local=structNew()>
		<cfset local.energy_account_uid_list = buildVoPropertyListFromArray(energy_account_uid_list, "energy_account_uid")>

		<cfreturn this.alertingDelegate.getAlertSubscriptionsForAccounts(local.energy_account_uid_list, is_active_flag)>
	</cffunction>

	<!--- For future use by the Back Office UI, to display available subscriptions given a company uid --->
	<cffunction name="getAlertSubscriptionsForCompany" returntype="array" output="false">
		<cfargument name="client_company_uid" type="string" required="true">
		<cfargument name="is_active_flag" type="boolean" required="true">

		<cfreturn this.alertingDelegate.getAlertSubscriptionsForCompany(client_company_uid, is_active_flag)>
	</cffunction>

	<cffunction name="saveNewAlertSubscription" returntype="mce.e2.vo.AlertSubscription" output="false">
		<cfargument name="subscription" type="mce.e2.vo.AlertSubscription" required="true">

		<cfreturn this.alertingDelegate.saveNewAlertSubscription(arguments.subscription)>
	</cffunction>

	<cffunction name="saveExistingAlertSubscription" returntype="mce.e2.vo.AlertSubscription" output="false">
		<cfargument name="subscription" type="mce.e2.vo.AlertSubscription" required="true">

		<cfreturn this.alertingDelegate.saveExistingAlertSubscription(arguments.subscription)>
	</cffunction>

	<cffunction name="saveAlertSubscriptionAssociationsForAccount" returntype="boolean">
		<cfargument name="energy_account_uid" type="string" required="true">
		<cfargument name="subscriptionsToAssociate" type="array" required="true" hint="List of subscription UIDs">
		<cfargument name="subscriptionsToNotAssociate" type="array" required="true" hint="List of subscription UIDs">

		<cfreturn this.alertingDelegate.saveAlertSubscriptionAssociationsForAccount(
			energy_account_uid,
			ArrayToList(subscriptionsToAssociate),
			ArrayToList(subscriptionsToNotAssociate) )>
	</cffunction>

	<cffunction name="checkRelevancy" returntype="string" hint="Of a list of alert uids, returns those uids that are no longer relevant">
		<cfargument name="alert_uid" type="string" required="false" hint="Alert UID(s), as a list">
		<cfreturn this.alertingBridge.checkRelevancy(alert_uid)>
	</cffunction>

	<cffunction name="removeIfNoLongerRelevant" returntype="string">
		<cfargument name="alertUidList" type="string" required="false" hint="Alert UID(s), as a list">

		<cfset var noLongerRelevant = this.alertingBridge.checkRelevancy(alertUidList)>
		<cfset var alert_uid = "">

		<cfloop list="#noLongerRelevant#" index="alert_uid">
			<cfset setAlertIsActive(alert_uid, false, "Complete")>
		</cfloop>

		<cfreturn noLongerRelevant>
	</cffunction>


	<cffunction name="getAlertTypes" access="public" returntype="array">
		<cfreturn this.alertingDelegate.getAlertTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getAlertDeliveryMethods" access="public" returntype="query">
		<cfreturn this.alertingDelegate.getAlertDeliveryMethods()>
	</cffunction>

	<cffunction name="getAlertDigests" access="public" returntype="query">
		<cfreturn this.alertingDelegate.getAlertDigests()>
	</cffunction>
</cfcomponent>