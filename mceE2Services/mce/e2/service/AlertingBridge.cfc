<cfcomponent displayname="Rate Model Bridge"
    output="false"
    hint="Provides a bridge to the RateModelService in the rate model engine project.">


 <!--- Bean getter / setter / dependencies (called by Coldspring IOC/DI framework) --->
 <cffunction name="init">
  <cfargument name="appSettingsBean" type="mce.e2.config.AppSettingsBean">

  <cfset this.appSettingsBean = appSettingsBean>

  <cfreturn this>
 </cffunction>


 <cffunction name="checkRelevancy" returntype="string">
  <cfargument name="alert_uid" type="string" required="true">

  <cfset var result = "">
  <cfset arguments.alert_uid = removeDuplicates(arguments.alert_uid)>

  <cfif ListLen(arguments.alert_uid) gt 0>
   <!--- Run the engine based on the arguments --->
     <cfinvoke
    method="checkRelevancy"
    webservice="#this.appSettingsBean.alertingBridge.AlertingServicesWsdlUrl#"
    alert_uid="#arguments.alert_uid#"
    returnVariable="result">
  </cfif>

  <cfreturn result>
 </cffunction>


 <cffunction name="removeDuplicates" returntype="string">
  <cfargument name="list" type="string" required="true">

  <cfset var map = StructNew()>
  <cfset var result = ArrayNew(1)>
  <cfset var item = "">

  <cfloop list="#list#" index="item">
   <cfif not StructKeyExists(map, item)>
    <cfset arrayAppend(result, item)>
    <cfset map[item] = true>
   </cfif>
  </cfloop>

  <cfreturn ArrayToList(result)>
 </cffunction>
</cfcomponent>