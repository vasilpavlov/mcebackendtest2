<cfcomponent displayname="Energy Contracts Meta Type Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Energy Contracts Meta Type information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyContractsMetaTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Energy Contracts Meta Type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyContractsMetaTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to Energy Contracts Meta Type information.">
		<cfset this.energyContractsMetaTypeDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyContractsMetaTypeVo" 
				access="public" returntype="mce.e2.vo.EnergyContractsMetaType"
				hint="This method is used to retrieve / return an empty EnergyContractsMetaType information value object.">
		<cfreturn this.energyContractsMetaTypeDelegate.getEmptyEnergyContractsMetaTypeComponent()>
	</cffunction>

	<cffunction name="getEnergyContractsMetaTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Energy Contracts Meta Types.">
		<cfreturn this.energyContractsMetaTypeDelegate.getEnergyContractsMetaTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyContractsMetaType" 
				access="public" returntype="mce.e2.vo.EnergyContractsMetaType" 
				hint="This method is used to return a populated EnergyContractsMetaType vo (value object)."> 
		<cfargument name="EnergyContractsMetaType" type="mce.e2.vo.EnergyContractsMetaType" required="true" hint="Describes the EnergyContractsMetaType collection VO object containing the properties of the EnergyContractsMetaType collection to be retrieved." /> 
		<cfreturn this.energyContractsMetaTypeDelegate.getEnergyContractsMetaTypeAsComponent(meta_type_uid=arguments.EnergyContractsMetaType.meta_type_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEnergyContractsMetaType" 
				access="public" returntype="string" 
				hint="This method is used to persist a new EnergyContractsMetaType record to the application database."> 
		<cfargument name="EnergyContractsMetaType" type="mce.e2.vo.EnergyContractsMetaType" required="true" hint="Describes the EnergyContractsMetaType collection VO object containing the details of the EnergyContractsMetaType collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.EnergyContractsMetaType = setAuditProperties(arguments.EnergyContractsMetaType, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.EnergyContractsMetaType = setPrimaryKey(local.EnergyContractsMetaType)>
		
		<!--- Save the modified EnergyContractsMetaType --->
		<cfset this.energyContractsMetaTypeDelegate.saveNewEnergyContractsMetaType(EnergyContractsMetaType=local.EnergyContractsMetaType)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.EnergyContractsMetaType.meta_type_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewEnergyContractsMetaType" 
				access="public" returntype="mce.e2.vo.EnergyContractsMetaType" 
				hint="This method is used to persist a new EnergyContractsMetaType record to the application database."> 
		<cfargument name="EnergyContractsMetaType" type="mce.e2.vo.EnergyContractsMetaType" required="true" hint="Describes the EnergyContractsMetaType collection VO object containing the details of the EnergyContractsMetaType collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyContractsMetaType(EnergyContractsMetaType=arguments.EnergyContractsMetaType)>

		<!--- Set the primary key --->
		<cfset arguments.EnergyContractsMetaType.meta_type_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.EnergyContractsMetaType>

	</cffunction>

	<cffunction name="saveExistingEnergyContractsMetaType" 
				access="public" returntype="mce.e2.vo.EnergyContractsMetaType" 
				hint="This method is used to save an existing EnergyContractsMetaType information to the application database."> 
		<cfargument name="EnergyContractsMetaType" type="mce.e2.vo.EnergyContractsMetaType" required="true" hint="Describes the EnergyContractsMetaType collection VO object containing the details of the EnergyContractsMetaType collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.EnergyContractsMetaType = setAuditProperties(arguments.EnergyContractsMetaType, "modify")>
		
		<!--- Save the modified EnergyContractsMetaType --->
		<cfset this.energyContractsMetaTypeDelegate.saveExistingEnergyContractsMetaType(EnergyContractsMetaType=local.EnergyContractsMetaType)/>

		<!--- Return the modified EnergyContractsMetaType --->
		<cfreturn local.EnergyContractsMetaType>	

	</cffunction>
	
</cfcomponent>