<cfcomponent displayname="Energy Unit Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of energy unit information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setEnergyUnitDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage energy unit data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.EnergyUnitDelegate" hint="Describes the *.cfc used to manage database interactions related to energy unit information.">
		<cfset this.EnergyUnitDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyEnergyUnitVo" 
				access="public" returntype="mce.e2.vo.EnergyUnit"
				hint="This method is used to retrieve / return an empty energyUnit information value object.">
		<cfreturn this.energyUnitDelegate.getEmptyEnergyUnitComponent()>
	</cffunction>

	<cffunction name="getAvailableEnergyUnits" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy units.">

		<!--- Define the arguments associated to this method --->
		<cfargument name="EnergyType" type="mce.e2.vo.EnergyType" required="true" hint="Describes the energy type that will be used to retrieve associated energy units." /> 

		<!--- Initialize the local structure for all variables --->
		<cfset var local = structnew()>
		
		<!--- Initialize the arguments structure --->
		<cfset local.args = structNew()>
		
		<!--- Define each of the arguments used to retrieve available energy units --->
		<cfset local.args.energy_type_uid = arguments.energyType.energy_type_uid>
		
		<!--- Retrieve the energy units --->
		<cfset local.energyUnits = this.energyUnitDelegate.getEnergyUnitsAsArrayOfComponents(argumentCollection=local.args)>
		
		<!--- Return the available energy units --->
		<cfreturn local.energyUnits>

	</cffunction>

	<cffunction name="getEnergyUnits" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of energy units.">
		<cfreturn this.energyUnitDelegate.getEnergyUnitAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getEnergyUnit" 
				access="public" returntype="mce.e2.vo.EnergyUnit" 
				hint="This method is used to return a populated EnergyUnit vo (value object)."> 
		<cfargument name="EnergyUnit" type="mce.e2.vo.EnergyUnit" required="true" hint="Describes the energyUnit collection VO object containing the properties of the energyUnit collection to be retrieved." /> 
		<cfreturn this.energyUnitDelegate.getEnergyUnitAsComponent(energy_unit_uid=arguments.EnergyUnit.energy_unit_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewEnergyUnit" 
				access="public" returntype="string" 
				hint="This method is used to persist a new energyUnit record to the application database."> 
		<cfargument name="EnergyUnit" type="mce.e2.vo.EnergyUnit" required="true" hint="Describes the energyUnit collection VO object containing the details of the energyUnit collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.energyUnit = setAuditProperties(arguments.energyUnit, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.energyUnit = setPrimaryKey(local.energyUnit)>
		
		<!--- Save the modified energyUnit --->
		<cfset this.EnergyUnitDelegate.saveNewEnergyUnit(energyUnit=local.energyUnit)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.energyUnit.energy_usage_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewEnergyUnit" 
				access="public" returntype="mce.e2.vo.EnergyUnit" 
				hint="This method is used to persist a new energyUnit record to the application database."> 
		<cfargument name="EnergyUnit" type="mce.e2.vo.EnergyUnit" required="true" hint="Describes the energyUnit collection VO object containing the details of the energyUnit collection to be retrieved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewEnergyUnit(energyUnit=arguments.energyUnit)>

		<!--- Set the primary key --->
		<cfset arguments.energyUnit.energy_unit_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.energyUnit>

	</cffunction>

	<cffunction name="saveExistingEnergyUnit" 
				access="public" returntype="mce.e2.vo.EnergyUnit" 
				hint="This method is used to save an existing energyUnit information to the application database."> 
		<cfargument name="energyUnit" type="mce.e2.vo.EnergyUnit" required="true" hint="Describes the energyUnit collection VO object containing the details of the energyUnit collection to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.energyUnit = setAuditProperties(arguments.energyUnit, "modify")>
		
		<!--- Save the modified energyUnit --->
		<cfset this.EnergyUnitDelegate.saveExistingEnergyUnit(energyUnit=local.energyUnit)/>

		<!--- Return the modified energyUnit --->
		<cfreturn local.energyUnit>	

	</cffunction>
	
</cfcomponent>