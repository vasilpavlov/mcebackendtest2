<cfcomponent displayname="Report Services Service"
			 extends="BaseDataService" output="false">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setReportingDelegate" 
				access="public" returntype="void">
		<cfargument name="bean" type="mce.e2.db.ReportingDelegate">
		<cfset this.reportingDelegate = arguments.bean>
	</cffunction>
	<cffunction name="setSqlServerReportGenerator" 
				access="public" returntype="void">
		<cfargument name="bean" type="mce.e2.db.SqlServerReportGenerator">
		<cfset this.reportGenerator = arguments.bean>
	</cffunction>
					 
	<cffunction name="requestReport" returntype="string" access="public"> 
		<cfargument name="report_lookup_code" type="string" required="true"> 
		<cfargument name="parameters" type="Array" required="false"> 
			<cfset var local = structNew()>
			<!--- Retrieve the user's session --->
			<cfset local.session = request.beanFactory.getBean("securityService").getSession()>
		<cfreturn this.reportingDelegate.insertReportView(report_lookup_code, parameters, local.session.sessionkey)>
	</cffunction> 
	
	<cffunction name="viewReport" returntype="string" access="public"> 
		<cfargument name="report_view_uid" type="string" required="true"> 
		<cfargument name="report_format" type="string" required="false" default="pdf"> 
		<cfargument name="user_session_uid" type="string" required="true">
		<cfargument name="hdroutput" type="string" required="true" default="inline">
		<!-- you can get the current session uid from existing code --> 
		<cfset reportinfo = this.reportingDelegate.getReportView(report_view_uid, user_session_uid)> 
			<cfif report_format eq "pdf"> 
				<cfreturn this.reportGenerator.generateReportPdf(report_name=reportInfo.userrptview.report_name,parameters=reportinfo.params,report_format=arguments.report_format,hdroutput=arguments.hdroutput)>
			<cfelseif report_format eq "excel">
				<cfreturn this.reportGenerator.generateReportPdf(report_name=reportInfo.userrptview.report_name,parameters=reportinfo.params,report_format=arguments.report_format,hdroutput=arguments.hdroutput)>
			<cfelseif report_format eq "csv">
				<cfreturn this.reportGenerator.generateReportPdf(report_name=reportInfo.userrptview.report_name,parameters=reportinfo.params,report_format=arguments.report_format,hdroutput=arguments.hdroutput)>
			<cfelse>
				<cfthrow message="unknown report format"> 
			</cfif>
	</cffunction>
	
	<cffunction name="getReportName" returntype="query"  access="remote">
		<cfargument name="report_lookup_code" type="string" required="true">
		<cfreturn this.reportingDelegate.getReportName(report_lookup_code=arguments.report_lookup_code)>
	</cffunction>

	<cffunction name="getReports" returntype="query"  access="remote">
		<cfreturn this.reportingDelegate.getReports()>
	</cffunction>
</cfcomponent>