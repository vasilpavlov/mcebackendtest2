<cfcomponent displayname="Address Type Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all business logic tied / associated with the management of Address Type information.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setAddressTypeDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage Address Type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.AddressTypeDelegate" hint="Describes the *.cfc used to manage database interactions related to Address Type information.">
		<cfset this.AddressTypeDelegate = arguments.bean>
	</cffunction>
	
	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getEmptyAddressTypeVo" 
				access="public" returntype="mce.e2.vo.AddressType"
				hint="This method is used to retrieve / return an empty AddressType information value object.">
		<cfreturn this.AddressTypeDelegate.getEmptyAddressTypeComponent()>
	</cffunction>

	<cffunction name="getAddressTypes" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of Address Types.">
		<cfreturn this.AddressTypeDelegate.getAddressTypesAsArrayOfComponents()>
	</cffunction>

	<cffunction name="getAddressType" 
				access="public" returntype="mce.e2.vo.AddressType" 
				hint="This method is used to return a populated Address Type vo (value object)."> 
		<cfargument name="AddressType" type="mce.e2.vo.AddressType" required="true" hint="Describes the AddressType VO object containing the properties of the AddressType to be retrieved." /> 
		<cfreturn this.AddressType.getAddressTypeAsComponent(address_type_uid=arguments.AddressType.address_type_uid)>
	</cffunction>
	
	<!--- Author all the methods that will modify data across the application database --->	
	<cffunction name="saveNewAddressType" 
				access="public" returntype="string" 
				hint="This method is used to persist a new AddressType record to the application database."> 
		<cfargument name="AddressType" type="mce.e2.vo.AddressType" required="true" hint="Describes the AddressType VO object containing the details of the AddressType to be saved." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the created by / date audit properties --->
		<cfset local.AddressType = setAuditProperties(arguments.AddressType, "create")>

		<!--- Set the primary key (if it's not already set) --->
		<cfset local.AddressType = setPrimaryKey(local.AddressType)>
		
		<!--- Save the modified AddressType --->
		<cfset this.AddressTypeDelegate.saveNewAddressType(AddressType=local.AddressType)/>

		<!--- Capture the primary key --->
		<cfset local.primaryKey = local.AddressType.address_type_uid>

		<!--- Return the new primary key --->
		<cfreturn local.primaryKey>

	</cffunction>
	
	<cffunction name="saveAndReturnNewAddressType" 
				access="public" returntype="mce.e2.vo.AddressType" 
				hint="This method is used to persist a new AddressType record to the application database."> 
		<cfargument name="AddressType" type="mce.e2.vo.AddressType" required="true" hint="Describes the AddressType VO object containing the details of the AddressType to be saved and returned." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Capture the primary key and create the object --->
		<cfset local.primaryKey = saveNewAddressType(AddressType=arguments.AddressType)>

		<!--- Set the primary key --->
		<cfset arguments.AddressType.address_type_uid = local.primaryKey>

		<!--- Return the new object --->
		<cfreturn arguments.AddressType>

	</cffunction>

	<cffunction name="saveExistingAddressType" 
				access="public" returntype="mce.e2.vo.AddressType" 
				hint="This method is used to save an existing AddressType information to the application database."> 
		<cfargument name="AddressType" type="mce.e2.vo.AddressType" required="true" hint="Describes the AddressType VO object containing the details of the AddressType to be modified." /> 

		<!--- Initialize the local scope --->
		<cfset var local = structNew()>
		
		<!--- Set the modified by / date audit properties --->
		<cfset local.AddressType = setAuditProperties(arguments.AddressType, "modify")>
		
		<!--- Save the modified AddressType --->
		<cfset this.AddressTypeDelegate.saveExistingAddressType(AddressType=local.AddressType)/>

		<!--- Return the modified AddressType --->
		<cfreturn local.AddressType>	

	</cffunction>
	
</cfcomponent>