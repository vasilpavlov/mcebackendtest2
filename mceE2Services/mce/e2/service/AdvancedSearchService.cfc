<cfcomponent displayname="Advanced Search Service"
			 extends="BaseDataService" output="false"
			 hint="The component is used to manage all advanced search functions.">

	<!--- Bean getter / setter / dependencies --->
	<cffunction name="setAdvancedSearchDelegate" 
				access="public" returntype="void" 
				hint="This method is used to set the database delegate used to manage company type data (dependency injection).">
		<cfargument name="bean" type="mce.e2.db.AdvancedSearchDelegate" hint="Describes the *.cfc used to manage database interactions related to searching for information.">
		<cfset this.advancedSearchDelegate = arguments.bean>
	</cffunction>

	<!--- Author all the methods to retrieve vo's or querries from the application database --->
	<cffunction name="getCompanyResults" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of properties.">
		<cfargument name="companySearch" required="true" type="mce.e2.vo.CompanySearch" hint="Describes vo for a given set of company search criteria."/>
		<cfreturn this.advancedSearchDelegate.getSearchCompaniesAsArrayOfComponents(arguments.companySearch)>
	</cffunction>

	<cffunction name="getPropertyResults" 
				access="public" returntype="array"
				hint="This method is used to retrieve / return a collection of properties.">
		<cfargument name="propertySearch" required="true" type="mce.e2.vo.PropertySearch" hint="Describes vo for a given set of property search criteria."/>
		<cfreturn this.advancedSearchDelegate.getSearchPropertiesAsArrayOfComponents(arguments.propertySearch)>
	</cffunction>
	
</cfcomponent>