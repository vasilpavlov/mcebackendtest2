<cfcomponent displayname="Rate Model Bridge"
			 output="false"
			 hint="Provides a bridge to the RateModelService in the rate model engine project.">


	<!--- Bean getter / setter / dependencies (called by Coldspring IOC/DI framework) --->
	<cffunction name="init">
		<cfargument name="energyUsageDelegate" type="mce.e2.db.EnergyUsageDelegate" hint="Describes the *.cfc used to manage database interactions related to energy usage information.">
		<cfargument name="energyAccountDelegate" type="mce.e2.db.EnergyAccountDelegate">
		<cfargument name="appSettingsBean" type="mce.e2.config.AppSettingsBean">

		<cfset this.EnergyUsageDelegate = arguments.energyUsageDelegate>
		<cfset this.EnergyAccountDelegate = arguments.energyAccountDelegate>
		<cfset this.appSettingsBean = appSettingsBean>

		<cfreturn this>
	</cffunction>



	<!--- Executes the rate model engine for a given EnergyEntryInfo VO --->
	<cffunction name="executeRateModelForEnergyEntryInfo" access="public" returntype="struct"
		hint="Convenience method to execute the rate model engine for a given energy usage record, based on an EnergyEntryInfo VO">

		<cfargument name="energyEntryInfo" type="mce.e2.vo.EnergyEntryInfo" required="true"/>

		<!--- Get the corresponding EnergyUsage VO for this Energy Entry VO --->
		<cfset var energyUsage = this.energyUsageDelegate.getEnergyUsageAsComponent(energy_usage_uid=energyEntryInfo.energy_usage_uid)>

		<!--- Run the rate model based on the EnergyUsage VO  --->
		<cfreturn executeRateModelForEnergyUsage(energyUsage)>
	</cffunction>



	<!--- Executes the rate model engine for a given EnergyUsage VO --->
	<cffunction name="executeRateModelForEnergyUsage" access="public" returntype="struct"
		hint="Convenience method to execute the rate model engine for a given energy usage record">

		<cfargument name="energyUsage" type="mce.e2.vo.EnergyUsage" required="true"/>

		<!--- Local variables --->
		<cfset var requestArgs = "">
		<cfset var rateModelResponse = "">

		<!--- Get energy account VO for this energy usage (so we know the model lookup code, start/end dates, etc, to pass to engine) --->
		<cfset var energyAccount = this.EnergyAccountDelegate.getEnergyAccountAsComponent(energy_account_uid=energyUsage.energy_account_uid)>
		<!--- Get the rate model lookup code associated with the account --->
		<cfset var modelLookupCode = energyAccount.model_lookup_code>

		<!--- Assemble the arguments that we want to pass to the rate model engine --->
		<cfinvoke
			component="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments"
			method="init"
			modelLookupCode="#modelLookupCode#"
			accountIdentifier="#energyUsage.energy_account_uid#"
			periodStart="#energyUsage.period_start#"
			periodEnd="#energyUsage.period_end#"
			returnVariable="requestArgs">

		<!--- Return the response from the rate model engine --->
		<cfset rateModelResponse = runRateModel(requestArgs)>

		<cfreturn rateModelResponse>
	</cffunction>



	<!--- Proxy method that calls the model --->
	<!--- The call is made via a web service so that the engine can be sitting on a separate box, in a cluster, etc --->
	<!--- See the AppSettingsBean and the corresponding coldspring_app_settings.xml file for how to provide the WSDL URL for the web service --->
	<cffunction name="runRateModel" access="public" returntype="any">
		<cfargument name="requestArgs" type="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments" required="true">

		<cfset var requestArgsAsStruct = requestArgs.toAnonymousStruct()>

		<cftry>
			<!--- Run the engine based on the arguments --->
	  		<cfinvoke
				method="runBasic"
				webservice="#this.appSettingsBean.rateEngineBridge.RateEngineServicesWsdlUrl#"
				modelLookupCode="#requestArgsAsStruct.modelLookupCode#"
				accountId="#requestArgsAsStruct.accountIdentifier#"
				periodStart="#requestArgsAsStruct.periodStart#"
				periodEnd="#requestArgsAsStruct.periodEnd#"
				returnVariable="rateModelResponse">

			<!--- Log that the call succeeded --->
			<cfset logRateModelCall(requestArgs, "Rate model called successfully", "information")>

			<cfcatch type="any">
				<!--- Log that the call succeeded --->
				<cfset logRateModelCall(requestArgs, "Rate model call failed", "warning")>
				<!--- Rethrow the exception so that it is available to whoever called us --->
				<cfrethrow>
			</cfcatch>
		</cftry>

		<!--- Return the response from the rate model engine --->
		<cfreturn rateModelResponse>
	</cffunction>


	<!--- Convenience function to write to log file with info about the rate model arguments --->
	<cffunction name="logRateModelCall" access="private" returntype="void">
		<cfargument name="requestArgs" type="mceRateModelEngine.com.mce.rate.engine.RateModelRequestArguments" required="true">
		<cfargument name="msg" type="string" required="true">
		<cfargument name="type" type="string" required="true">

		<!--- Log file will be located in the "logs" folder in CF's home directory; can view/query log under Logging in CF Admin. --->
		<cflog
			file="mceRateModelBridge"
			type="#arguments.type#"
			text="#msg#: Model '#requestArgs.modelLookupCode#', account '#requestArgs.accountIdentifier#', period #requestArgs.periodStart# to #requestArgs.periodEnd#">

	</cffunction>

</cfcomponent>