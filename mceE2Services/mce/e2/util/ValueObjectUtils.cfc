<cfcomponent>

	<!--- Utility function to create an instance of a value object --->
	<cffunction name="createValueObject" hint="Convenience method to create an instance of a value object" returntype="any">
		<cfargument name="className" type="string" required="true" hint="The class name of the value object to create, generally starting with 'mce.e2.vo.'">
		<cfreturn createObject("component", className)>
	</cffunction>

	<!--- Utility function, can be used to convert a one-row query into a "vo" (value object) --->
	<cffunction name="queryToVo" returntype="any" hint="Convenience method to convert a one-row query into a 'vo' (value object). Use when you have fetched a record from the database and want to return it as a VO instance.">
		<cfargument name="qRecords" type="query" required="true" hint="The query object to convert to a VO">
		<cfargument name="componentName" type="string" required="true" hint="The name of the VO class you want to get back; generaly starting with 'mce.e2.vo'.">

		<cfset var result = "">
		<cfset var voArray = "">

		<cfif qRecords.recordCount neq 1>
			<cfinvoke
				component="#request.e2.logger#"
				method="error"
				message="Could not convert query to a single instance of component '#componentName#'."
				detail="This probably means that the supplied query did not have exactly one row (it has #qRecords.recordCount# rows). If you want to be able to deal with any number of rows, you may want to use the queryToVoArray() method instead of queryToVo().">
		</cfif>

		<cfset voArray = queryToVoArray(arguments.qRecords, componentName, 1)>
		<cfset result = voArray[1]>

		<cfreturn result>
	</cffunction>

	<!--- Utility function, can be used to convert a multi-row query into an array of "vo's" (value objects) --->
	<cffunction name="queryToVoArray" returntype="array">
		<cfargument name="qRecords" type="query" required="true" hint="The query object to convert to VOs">
		<cfargument name="componentName" type="string" required="true" hint="The name of the VO class you want to get back; generaly starting with 'mce.e2.vo'.">
		<cfargument name="maxRows" type="numeric" required="false" default="#qRecords.recordCount#">
		<cfargument name="startRow" type="numeric" required="false" default="1">

		<!--- This is what we'll return --->
		<cfset var result = ArrayNew(1)>

		<!--- Get a reference to the class that we want instances of --->
		<cfset var voClass = createValueObject(componentName)>
		<cfset var qualifiedName = getMetaData(voClass).fullname>
		<cfset var voToDuplicate = CreateObject("component", qualifiedName)>
		<cfset var arrayPos = "">
		<cfset var endRow = arguments.maxRows + arguments.startRow - 1>

		<!--- Shortcut if there are zero query records --->
		<cfif arguments.qRecords.recordCount eq 0>
			<cfreturn result>
		</cfif>

		<!--- For each record in the query --->
		<cfloop query="arguments.qRecords" startrow="#arguments.startRow#" endrow="#endRow#">
			<!--- Create a fresh instance of the class and place it in our result array --->
			<cfset ArrayAppend(result, duplicate(voToDuplicate))>
			<cfset arrayPos = ArrayLen(result)>
			<!--- For each column in the specified row of the query --->
			<cfloop list="#qRecords.columnList#" index="col">
				<cfset result[arrayPos][col] = arguments.qRecords[col][arguments.qRecords.currentRow]>
			</cfloop>
		</cfloop>

		<!--- Return the result array --->
		<cfreturn result>
	</cffunction>


	<!--- Similar to queryToVoArray method (above), except returns anonymous structures (objects) rather than VOs --->
	<cffunction name="queryToObjectArray" returntype="array" output="false">
		<cfargument name="qRecords" type="query" required="true" hint="The query object to convert to VOs">

		<!--- This is what we'll return --->
		<cfset var result = ArrayNew(1)>
		<cfset var s = "">
		<cfset var col = "">

		<!--- Shortcut if there are zero query records --->
		<cfif qRecords.recordCount eq 0>
			<cfreturn result>
		</cfif>

		<!--- Resize the array to the number of query records --->
		<cfset ArrayResize(result, qRecords.recordCount)>

		<!--- For each record in the query --->
		<cfloop query="arguments.qRecords">
			<cfset s = StructNew()>
			<cfloop list="#arguments.qRecords.columnList#" index="col">
				<cfset s[lcase(col)] = arguments.qRecords[col][arguments.qRecords.currentRow]>
			</cfloop>
			<cfset result[qRecords.currentRow] = s>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="objectArrayToQuery" returntype="query">
		<cfargument name="objects" type="array" required="true">
		<cfargument name="columnNames" type="string" required="true">

		<!--- Local variables --->
		<cfset var qResult = QueryNew(arguments.columnNames)>
		<cfset var item = "">
		<cfset var col = "">
		<cfset var row = 0>

		<!--- Resize the number of rows in the query object --->
		<cfset QueryAddRow(qResult, ArrayLen(arguments.objects))>

		<!--- For each "field" of the objects (or, for each column of the new query) --->
		<cfloop list="#arguments.columnNames#" index="col">
			<!--- For each item in the array (or, for each row of the new query) --->
			<cfloop from="1" to="#ArrayLen(arguments.objects)#" index="row">
				<cfset QuerySetCell(qResult, col, arguments.objects[row][col], row)>
			</cfloop>
		</cfloop>

		<!--- Return the new query --->
		<cfreturn qResult>
	</cffunction>

</cfcomponent>