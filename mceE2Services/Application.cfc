<cfcomponent output="true">

	<!--- Application configuration --->
	<cfset this.name = "mceE2Services">
	<cfset this.sessionManagement = true>
	<cfset this.sessionTimeout = CreateTimespan(0, 1, 0, 0)>

	<!--- Set this to true to force the app to be re-initialized before every request is processed --->
	<!--- (good for dev, bad for production/performance). Set this to false in production --->
	<cfset this.mceOptions.isDevMode = true>
	<!--- Attempts to speed up processing when in "dev mode" (see above) by checking file dates on the source code --->
	<!--- This adds a bit of I/O overhead, but is much faster than always initializing Coldspring blindly with each request--->
	<!--- Should have no effect whatsoever in production (when isDevMode is false) --->
	<cfset this.mceOptions.optimizeApplicationReconfigurationWhenInDevMode = true>

	<!--- CF runs this method before the first HTTP request to this app after the CF server starts up --->
	<cffunction name="onApplicationStart"
		hint="I run automatically on the first page request after the ColdFusion server starts up.">

		<cfset configureApplication()>
	</cffunction>

	<!--- CF runs this method before every new HTTP request is processed --->
	<cffunction name="onRequestStart" returnType="boolean" output="true"
		hint="I run automatically on every page request (this is a ColdFusion feature); I should be kept short and sweet because I run very often.">

		<cfargument name="thePage" type="string" required="true">

		<!--- Copy the runtime objects, etc., in the "e2" application variable, to the request scope --->
		<!--- All other code throughout the application should refer to these items via request.e2.blah --->
		<cfset request.e2 = application.e2>

		<!--- Reconfigures the application if needed, based on isDevMode flags and other considerations --->
		<cfset reconfigureApplicationIfNeeded()>

		<!--- Grab a reference to the application's shared Coldspring factory reference --->
		<cfset request.beanFactory = application.beanFactory>

		<cfreturn true>
	</cffunction>

	<cffunction name="reconfigureApplicationIfNeeded" access="private" returntype="void"
		hint="Reconfigures the application via configureApplication() if needed, based on isDevMode flags and other considerations">

		<cfset var lastTouch = application.e2.beansLastTouched>
		<cfset var isNeeded = false>

		<!--- If appropriate, re-configure everything each time (good for dev, bad for production/performance) --->
		<cfif this.mceOptions.isDevMode>
			<!--- Determine whether the application should be reconfigured --->
			<cfif this.mceOptions.optimizeApplicationReconfigurationWhenInDevMode>
				<!--- In this mode, we check whether any of the source code files in the app have changed --->
				<cfset lastTouch = min(now(), getLastTouchDate("/mce/e2/", "*.cfc"))>
				<cfif lastTouch gt request.e2.beansLastTouched>
					<cfset isNeeded = true>
				</cfif>
			<cfelse>
				<!--- In this mode, we assume that the app should always be reconfigured with every page request --->
				<cfset isNeeded = true>
			</cfif>
		</cfif>

		<cfif isNeeded>
			<cfset configureApplication()>
			<cfset application.e2.beansLastTouched = lastTouch>
		</cfif>
	</cffunction>

	<!--- This method initializes various runtime objects, etc., that other parts of the app depend on --->
	<!--- It puts everything in the "application.e2" structure, which gets copied to "request.e2" for each page request --->
	<cffunction name="configureApplication" access="private"
		hint="Configures the application's bean factory (Coldspring) and other matters.">

		<cflock scope="application" type="exclusive" timeout="20">
			<cfset application.e2.system_instance_uid = "909CBF21-3196-4736-96AE-7FAE0819D1F0">
			<cfset application.e2.logger = CreateObject("component", "com.mce.log.SimpleLogger").init("info")>
			<cfif not StructKeyExists(application.e2, "beansLastTouched")>
				<cfset application.e2.beansLastTouched = CreateDate(1970, 1, 1)>
			</cfif>

			<cfset application.beanFactory = configureColdspring()>
		</cflock>
	</cffunction>

	<!--- Internal function to configure the Coldspring framework (http://www.coldspringframework.org) --->
	<cffunction name="configureColdspring" access="private" returntype="coldspring.beans.DefaultXmlBeanFactory"
		hint="Internal function to configure the Coldspring framework">

		<cfset var factory = CreateObject('component', 'coldspring.beans.DefaultXmlBeanFactory').init() />
		<cfset var beanConfigFile = "/mce/e2/config/coldspring.xml">
		<cfset var remoteBeanConfigFile = "/mce/e2/config/coldspring_remote.xml">
		<cfset var settingsBeanConfigFile = "/mce/e2/config/coldspring_app_settings.xml">

		<!--- We have the Coldspring configuration split into different xml files --->
		<cfset factory.loadBeans(settingsBeanConfigFile) />
		<cfset factory.loadBeans(beanConfigFile) />
		<cfset factory.loadBeans(remoteBeanConfigFile) />

		<!--- Load up the second Coldspring XML file, which is for remote proxies only, via this simple utility function --->
		<!--- This will tell Coldspring to load the XML, and it will also tell Coldspring to generate each actual remote bean --->
  		<cfinvoke
			component="mce.e2.util.ColdspringUtils"
			method="configureColdspringRemoteBeans"
			factory="#factory#"
			remoteBeanConfigFile="#remoteBeanConfigFile#">

		<cfreturn factory>
	</cffunction>

	<!--- Returns the most recent last-modified date from a set of directories --->
	<cffunction name="getLastTouchDate" access="private" returntype="date"
		hint="I return the most recent last-modified date within a directory or set of directories">

		<cfargument name="pathList" type="string" required="true" hint="A comma separated list of paths to check last-modified dates in; can be relative paths or absolute paths (starting with /), filesystem paths won't work.">
		<cfargument name="filter" type="string" required="true" hint="A cfdirectory-style filter; only matching files will have their last-modified dates checked">
		<cfargument name="recurse" type="boolean" required="false" default="true" hint="Whether to check dates recursively in subdirectories; if false, only those directories explicitly specified will be checked">

		<cfset var result = CreateDate(1970, 1, 1)>
		<cfset var thisPath = "">
		<cfset var files = "">

		<cfloop list="#pathList#" index="thisPath">
			<cfdirectory
				name="files"
				directory="#ExpandPath(thisPath)#"
				recurse="#arguments.recurse#"
				action="list"
				filter="#arguments.filter#">

			<cfloop query="files">
				<cfif files.dateLastModified gt result>
					<cfset result = files.dateLastModified>
				</cfif>
			</cfloop>
		</cfloop>

		<cfreturn result>
	</cffunction>


</cfcomponent>