<!––– establish parameters –––>
<cfparam name="URL.method" default="" />
<cfparam name="URL.name" default="" />



<––– get the content from the http data –––>
<cfset httpContent = GetHttpRequestData()>

<!––– make sure content was passed in –––>
<cfif len(httpContent.content) gt 0>
<!––– write the content to local pdf –––>
<cfheader name="Content-Disposition" value="#URL.method#; filename=#URL.name#" />
<cfcontent type="application/pdf" variable="#httpContent.content#">

<cfelse>
<!––– redirect to home page –––>
<cflocation url="/" />

</cfif>