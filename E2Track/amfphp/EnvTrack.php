<?php
// Create new service for PHP Remoting as Class
include "../../ets2/util/mceutils.php";

$MM_authorizedUsers = "0,1,2,99";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
    // For security, start by assuming the visitor is NOT authorized.
  $isValid = False;

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username.
  // Therefore, we know that a user is NOT logged in if that Session variable is blank.
  if (!empty($UserName)) {
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login.
    // Parse the strings into arrays.
    $arrUsers = Explode(",", $strUsers);
    $arrGroups = Explode(",", $strGroups);
    if (in_array($UserName, $arrUsers)) {
      $isValid = true;
    }
    // Or, you may restrict access to only certain users based on their username.
    if (in_array($UserGroup, $arrGroups)) {
      $isValid = true;
    }
    if (($strUsers == "") && false) {
      $isValid = true;
    }
  }
  return $isValid;
}

    // the php code that embeds the Flex app will pass in the session's PHPSESSID via
    // flashVars. If that session id is correct, then we can grab the user id out
    // of the session
    function validateUser($sessionKey){
        if ($sessionKey == 'mcedemo1'){
            return $sessionKey;
        }
        else{
            if ($sessionKey == $PHPSESSID){
                return ($HTTP_SESSION_VARS['MM_Username']);
            }
        }
        return false;
    }


class EnvTrack
{



    function getPropertyInfo($sessionKey, $clientLocationID){
       require_once('mce.php');
        $user = validateUser($sessionKey);
        if (!$user){
            return ("Not authorized");
        }

        mysql_select_db($database_mce, $mce);

          $sql =
            "SELECT
                etsheader.OwnerCompany
            FROM
                userproperties
                LEFT JOIN etsheader ON etsheader.ClientLocationID = userproperties.ClientLocationID
            WHERE
                etsheader.ClientLocationID = $clientLocationID
                AND userproperties.Username = '$user'";

        return (mysql_query($sql, $mce));
    }


    function getYearlyEmissionsForCompany($sessionKey){
        require_once('mce.php');
        $user = validateUser($sessionKey);
        if (!$user){
            return ("Not authorized");
        }

        mysql_select_db($database_mce, $mce);

        $sql =
        "SELECT
            Yearno as PeriodName,
            a.EnergyType as EnergyTypeName,
            sum(Consumption) as EnergyConsumption,
            sum(etsdata.Emissions)/1000 as EmissionsTotal,
            a.ConsumpUnits as EnergyUnits
        FROM
            userproperties
            LEFT JOIN etsheader ON etsheader.ClientLocationID = userproperties.ClientLocationID
            LEFT JOIN etsdata ON etsdata.ClientLocationID = userproperties.ClientLocationID
            LEFT JOIN accounts a ON a.AccountNumber = etsdata.AccountNumber AND a.LDC_ID=etsdata.LDC_ID
        WHERE
            userproperties.Username = '$user'
            AND etsdata.Emissions is not null
            AND etsdata.YearNo > 2005
        GROUP BY
            YearNo, Energytype, ConsumpUnits
        ORDER BY
            Yearno, EnergyType ";
        $result = mysql_query($sql, $mce);
        return $result;
    }

    function getPropertyEmissionsForPeriod($sessionKey, $year){
        require_once('mce.php');
        $user = validateUser($sessionKey);

        //$user = $HTTP_SESSION_VARS['MM_Username'];

        mysql_select_db($database_mce, $mce);

        $sql =
            "SELECT
                etsheader.PropertyAddress1 as PropertyName,
                etsheader.ClientLocationID,
                accounts.EnergyType as EnergyTypeName,
                sum(Consumption) as EnergyConsumption,
                sum(etsdata.Emissions)/1000 as EmissionsTotal,
                accounts.ConsumpUnits as EnergyUnits
            FROM
                userproperties
                LEFT JOIN etsheader ON etsheader.ClientLocationID = userproperties.ClientLocationID
                LEFT JOIN etsdata ON etsdata.ClientLocationID = userproperties.ClientLocationID
                LEFT JOIN accounts ON accounts.AccountNumber = etsdata.AccountNumber AND accounts.LDC_ID=etsdata.LDC_ID
            WHERE
                userproperties.Username = '$user'
                AND etsdata.Emissions is not null
                AND etsdata.YearNo = $year
            GROUP BY
                ClientLocationID, PropertyAddress1, Energytype, ConsumpUnits
            ORDER BY
                PropertyAddress1, EnergyType";

        return (mysql_query($sql, $mce));
    }

    function getYearlyEmissionsForProperty($sessionKey, $clientLocationID){
        require_once('mce.php');
        $user = validateUser($sessionKey);

        //$user = $HTTP_SESSION_VARS['MM_Username'];

        mysql_select_db($database_mce, $mce);

        $sql =
        "SELECT
            Yearno as PeriodName,
            a.EnergyType as EnergyTypeName,
            sum(Consumption) as EnergyConsumption,
            sum(etsdata.Emissions)/1000 as EmissionsTotal,
            a.ConsumpUnits as EnergyUnits
        FROM
            userproperties
            LEFT JOIN etsheader ON etsheader.ClientLocationID = userproperties.ClientLocationID
            LEFT JOIN etsdata ON etsdata.ClientLocationID = userproperties.ClientLocationID
            LEFT JOIN accounts a ON a.AccountNumber = etsdata.AccountNumber AND a.LDC_ID=etsdata.LDC_ID
        WHERE
            userproperties.Username = '$user'
            AND etsdata.ClientLocationID = $clientLocationID
            AND etsdata.Emissions is not null
            AND etsdata.YearNo > 2005
        GROUP BY
            YearNo, Energytype, ConsumpUnits
        ORDER BY
            Yearno, EnergyType ";

        return (mysql_query($sql, $mce));
    }

    function getRegionalFuelMixForProperty($sessionKey, $clientLocationID){
        require_once('mce.php');
        $user = validateUser($sessionKey);

        //$user = $HTTP_SESSION_VARS['MM_Username'];

        mysql_select_db($database_mce, $mce);

        $sql =
        "SELECT DISTINCT
            Coal, Oil, Gas, Nuclear, OtherFossil, Renewable, Hydro
        FROM userproperties
        LEFT JOIN etsheader ON etsheader.ClientLocationID = userproperties.ClientLocationID
        LEFT JOIN etsdata ON etsdata.ClientLocationID = userproperties.ClientLocationID
        LEFT JOIN accounts ON accounts.AccountNumber = etsdata.AccountNumber
            AND accounts.LDC_ID=etsdata.LDC_ID
        LEFT JOIN emissionfactors on accounts.EmFactorID = emissionfactors.EMFactorID
        WHERE
            userproperties.Username = '$user'
            AND etsheader.ClientLocationID = $clientLocationID";

         $rs = mysql_query($sql, $mce);
         $row_rs1 = mysql_fetch_assoc($rs);

         $rowset['Oil'] = $row_rs1['Oil'];
         $rowset['Gas'] = $row_rs1['Gas'];
         $rowset['Hydro'] = $row_rs1['Hydro'];

         return $rowset;

    }

}
?>