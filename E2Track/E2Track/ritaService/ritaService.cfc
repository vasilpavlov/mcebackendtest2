<cfcomponent>
     <cffunction name="getBuildingDownload" access="remote" returntype="query" >
		 <CFSTOREDPROC procedure="dbo.procGetBuildingDownload" datasource="rita">
				<CFPROCRESULT name="rs1">
 		 </CFSTOREDPROC>
	  <cfreturn rs1>
	</cffunction>
  
       <cffunction name="getMeterDownload" access="remote" returntype="query" >
		 <CFSTOREDPROC procedure="dbo.procGetMeterDownload" datasource="rita">
				<CFPROCRESULT name="rs1">
 		 </CFSTOREDPROC>
	  <cfreturn rs1>
	</cffunction>
  
	<cffunction name="getExceptionTypeDownload" access="remote" returntype="query" >
		 <CFSTOREDPROC procedure="dbo.procGetExceptionTypeDownload" datasource="rita">
				<CFPROCRESULT name="rs1">
 		 </CFSTOREDPROC>
	  <cfreturn rs1>
	</cffunction>
	
    <cffunction name="insertBuildingNoteUpload" access="remote" returntype="string" >
		<cfargument name="building_id" type="string" required="yes">
		<cfargument name="building_notes" type="string" required="yes" >
        <cfargument name="timestamp" type="date" required="yes">
		 <CFSTOREDPROC procedure="dbo.procInsertBuildingNotesUpload" datasource="rita">
				<CFPROCPARAM  dbvarname="@building_id" value="#building_id#" cfsqltype="cf_sql_varchar" />
                <CFPROCPARAM  dbvarname="@building_notes" value="#building_notes#" cfsqltype="cf_sql_longvarchar" />
                <CFPROCPARAM dbvarname="@timestamp" value="#CreateODBCDateTime(timestamp)#" cfsqltype="CF_SQL_TIMESTAMP" />
 		 </CFSTOREDPROC>
		<cfreturn "SUCCESS">
	</cffunction>
   
      <cffunction name="insertMeterUpload" access="remote" returntype="any" >
		<cfargument name="meter_id" type="string" required="yes">
        <cfargument name="consumption" type="string" required="yes">
		<cfargument name="demand" type="string" required="yes">
		<cfargument name="cumulative_demand" type="string" required="yes">
		<cfargument name="did_reset_demand" type="numeric" required="yes">
		<cfargument name="meter_notes" type="string" required="yes" default="">
        <cfargument name="timestamp" type="date" required="yes">
		<cfargument name="reading_notes" type="string" required="yes" default="">
        <cfargument name="exception_type_id" type="numeric" required="yes" default="-1">
		 <CFSTOREDPROC procedure="dbo.procInsertMeterUpload" datasource="rita">
				<CFPROCPARAM type="IN" dbvarname="@meter_id" value="#meter_id#" cfsqltype="CF_SQL_varchar" />
                <CFPROCPARAM type="IN" dbvarname="@consumption" value="#consumption#" cfsqltype="cf_sql_float" />
                 <CFPROCPARAM type="IN" dbvarname="@demand" value="#demand#" cfsqltype="cf_sql_float" />
                 <CFPROCPARAM type="IN" dbvarname="@cumulative_demand" value="#cumulative_demand#" cfsqltype="cf_sql_float" />
                <CFPROCPARAM type="IN" dbvarname="@did_reset_demand" value="#did_reset_demand#" cfsqltype="cf_sql_bit" />
                <CFPROCPARAM type="IN" dbvarname="@meter_notes" value="#meter_notes#" cfsqltype="cf_sql_varchar" />
                <CFPROCPARAM type="IN" dbvarname="@timestamp" value="#timestamp#" cfsqltype="cf_sql_date" />
                <CFPROCPARAM type="IN" dbvarname="@reading_notes" value="#reading_notes#" cfsqltype="cf_sql_varchar" />
                <CFPROCPARAM type="IN" dbvarname="@exception_type_id" value="#exception_type_id#" cfsqltype="cf_sql_integer" />
 		 </CFSTOREDPROC>
		<cfreturn "SUCCESS">
	</cffunction>

</cfcomponent>